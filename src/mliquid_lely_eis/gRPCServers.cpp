/* gRPCServers.cpp
 * 
 * Thread function starting gRPC services (in separate threads) and monitoring 
 * the app's exit flag to shutdown the services correctly.
 */


/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */


// Std. C++ library types / STL 

// IO, and Strings
#include <iostream>
#include <iomanip>

// Strings &  analysis
#include <string>

// Timing and threading includes
#include <pthread.h>                    // POSIX threads

// errors and exepctions
#include "ilmsens/ilmsens_error.h"      // Ilmsens error codes
//#include "ilmsens/tools/ilmsens_tools_error.hpp"

// gRPC common stuff
#include <grpcpp/grpcpp.h>

using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

// gRPC and protocol buffer generated code
#ifdef WIN32

  #include "win_ilmsens_mliquid_dairy_lely.grpc.pb.h"
  #include "win_ilmsens_mliquid_dairy_lely.pb.h"

#else

  #include "gRPC/ilmsens_mliquid_dairy_lely.grpc.pb.h"
  #include "gRPC/ilmsens_mliquid_dairy_lely.pb.h"

#endif

using namespace ilmsens::mliquid::dairy;

// Shared variables, memories, and synchronisation ressources
#include "shared.hpp"

// sensor tools
#include "dataReader.hpp"

// main header for module
#include "gRPCServers.hpp" 
 

/* -------------------------------------------------------------------------------------------------
 * Local variables
 * -------------------------------------------------------------------------------------------------
 */

// thread handles
static pthread_t sGRPCServer;          // thread running actual gRPC server

// pre-defined status messages
static IlmsensStatusMessage sStatusMsgOK;  // Ilmsens status message reporting everything was fine
static IlmsensStatusMessage sStatusMsgOOR; // Ilmsens status message reporting a value was out of range
static IlmsensStatusMessage sStatusMsgFIN; // Ilmsens status message reporting a finished streaming message request
static IlmsensStatusMessage sStatusMsgWS ; // Ilmsens status message reporting a wrong state for the current request

// pre-allocated message for measured data
static IlmsensMeasuredData sMeasData;      // Ilmsens measurement data message used to forward IRF frames

/* -------------------------------------------------------------------------------------------------
 * helper functions
 * -------------------------------------------------------------------------------------------------
 */

void resetMeasDataMessage(unsigned int pNumSamp)
{
  //update length of IRFs?
  if (pNumSamp)
  {
    auto tIRFSamples = sMeasData.mutable_irf_rx1();
    tIRFSamples->Resize((int) pNumSamp, 0.0);
    tIRFSamples = sMeasData.mutable_irf_rx2();
    tIRFSamples->Resize((int) pNumSamp, 0.0);
  }

  //set status and other signular fields to default values
  IlmsensStatusMessage* tStat = new IlmsensStatusMessage(sStatusMsgOK);
  sMeasData.set_allocated_status(tStat);

  sMeasData.set_sequence_number(0);

  sMeasData.set_adc_level_rx1(0);
  sMeasData.set_adc_level_rx2(0);

  sMeasData.set_mcu_temperature(GRPC_INVALID_TEMP);
  sMeasData.set_probe_temperature(GRPC_INVALID_TEMP);
  sMeasData.set_sensor_temperature(GRPC_INVALID_TEMP);
}

/* -------------------------------------------------------------------------------------------------
 * gRPC implementation classes
 * -------------------------------------------------------------------------------------------------
 */

class IlmsensSensorInfosImpl final : public IlmsensSensorInfos::Service
{
  // get sensor ID (should be unique, i.e. include some serial number)
  ::grpc::Status GetSensorID(::grpc::ServerContext* /* context */, const ::google::protobuf::Empty* /* request */, ::ilmsens::mliquid::dairy::IlmsensStrResult* response) override
  {
    // get infos from shared structure
    mMtxSharedData.lock();
      std::string tID(mState.mID);
    mMtxSharedData.unlock();

    //return the retrieved value in the response
    response->set_result(tID);
    //generate the status message which is part of the result message
    IlmsensStatusMessage* tStat = new IlmsensStatusMessage(sStatusMsgOK);
    response->set_allocated_status(tStat);
    std::cout << ANSI_COLOR_YELLOW << "gRPCServices::INFO: GetSensorID requested returning ID '" << tID << "'." << ANSI_COLOR_RESET << std::endl;
    return Status::OK;
  }

  // get MLBS order
  ::grpc::Status GetMLBSOrder(::grpc::ServerContext* /* context */, const ::google::protobuf::Empty* /* request */, ::ilmsens::mliquid::dairy::IlmsensUIntResult* response) override
  {
    // get infos from shared structure
    mMtxSharedData.lock();
      unsigned int tOrder = mState.mModInfo.mConfig.mOrder;
    mMtxSharedData.unlock();

    //return the retrieved value in the response
    response->set_result(tOrder);
    //generate the status message which is part of the result message
    IlmsensStatusMessage* tStat = new IlmsensStatusMessage(sStatusMsgOK);
    response->set_allocated_status(tStat);
    std::cout << ANSI_COLOR_YELLOW << "gRPCServices::INFO: GetMLBSOrder requested returning order " << tOrder << "." << ANSI_COLOR_RESET << std::endl;
    return Status::OK;
  }

  // get IRF period length (number of samples per IRF)
  ::grpc::Status GetNumSamp(::grpc::ServerContext* /* context */, const ::google::protobuf::Empty* /* request */, ::ilmsens::mliquid::dairy::IlmsensUIntResult* response) override
  {
    // get infos from shared structure
    mMtxSharedData.lock();
      unsigned int tNumSamp = mState.mMLBSLen;
    mMtxSharedData.unlock();

    //return the retrieved value in the response
    response->set_result(tNumSamp);
    //generate the status message which is part of the result message
    IlmsensStatusMessage* tStat = new IlmsensStatusMessage(sStatusMsgOK);
    response->set_allocated_status(tStat);
    std::cout << ANSI_COLOR_YELLOW << "gRPCServices::INFO: GetNumSamp requested returning " << tNumSamp << " samples." << ANSI_COLOR_RESET << std::endl;
    return Status::OK;
  }

  // get number of hardware averages
  ::grpc::Status GetHardwareAvgerages(::grpc::ServerContext* /* context */, const ::google::protobuf::Empty* /* request */, ::ilmsens::mliquid::dairy::IlmsensUIntResult* response) override
  {
    // get infos from shared structure
    mMtxSharedData.lock();
      unsigned int tAvg = mState.mHW_AVG;
    mMtxSharedData.unlock();

    //return the retrieved value in the response
    response->set_result(tAvg);
    //generate the status message which is part of the result message
    IlmsensStatusMessage* tStat = new IlmsensStatusMessage(sStatusMsgOK);
    response->set_allocated_status(tStat);
    std::cout << ANSI_COLOR_YELLOW << "gRPCServices::INFO: GetHardwareAverages requested returning " << tAvg << " averages." << ANSI_COLOR_RESET << std::endl;
    return Status::OK;
  }

  // get RF system clock
  ::grpc::Status GetRFSystemClk(::grpc::ServerContext* /* context */, const ::google::protobuf::Empty* /* request */, ::ilmsens::mliquid::dairy::IlmsensDblResult* response) override
  {
    // get infos from shared structure
    mMtxSharedData.lock();
      double tClk = mState.mClk;
    mMtxSharedData.unlock();

    //return the retrieved value in the response
    response->set_result(tClk);
    //generate the status message which is part of the result message
    IlmsensStatusMessage* tStat = new IlmsensStatusMessage(sStatusMsgOK);
    response->set_allocated_status(tStat);
    std::cout << ANSI_COLOR_YELLOW << "gRPCServices::INFO: GetRFSystemClock requested returning " << std::fixed << std::setprecision(6) << tClk << " GHz." << ANSI_COLOR_RESET << std::endl;
    return Status::OK;
  }
};

class IlmsensMeasurementSetupImpl final : public IlmsensMeasurementSetup::Service
{
  // get number of software averages (in packets of HW avg.)
  ::grpc::Status GetSoftwareAvgerages(::grpc::ServerContext* /* context */, const ::google::protobuf::Empty* /* request */, ::ilmsens::mliquid::dairy::IlmsensUIntResult* response) override
  {
    // get infos from shared structure
    mMtxSharedData.lock();
      unsigned int tAvg = mState.mSW_AVG;
    mMtxSharedData.unlock();

    //return the retrieved value in the response
    response->set_result(tAvg);
    //generate the status message which is part of the result message
    IlmsensStatusMessage* tStat = new IlmsensStatusMessage(sStatusMsgOK);
    response->set_allocated_status(tStat);
    std::cout << ANSI_COLOR_YELLOW << "gRPCServices::INFO: GetSoftwareAverages requested returning " << tAvg << " averages." << ANSI_COLOR_RESET << std::endl;
    return Status::OK;
  }

  // get number of wait cycles (in packets of HW avg.)
  ::grpc::Status GetWaitCycles(::grpc::ServerContext* /* context */, const ::google::protobuf::Empty* /* request */, ::ilmsens::mliquid::dairy::IlmsensUIntResult* response) override
  {
    // get infos from shared structure
    mMtxSharedData.lock();
      unsigned int tWCyc = mState.mWaitCyc;
    mMtxSharedData.unlock();

    //return the retrieved value in the response
    response->set_result(tWCyc);
    //generate the status message which is part of the result message
    IlmsensStatusMessage* tStat = new IlmsensStatusMessage(sStatusMsgOK);
    response->set_allocated_status(tStat);
    std::cout << ANSI_COLOR_YELLOW << "gRPCServices::INFO: GetWaitCycles requested returning " << tWCyc << " cycles." << ANSI_COLOR_RESET << std::endl;
    return Status::OK;
  }

  // set number of software averages (returns actually set value)
  ::grpc::Status SetSoftwareAvgerages(::grpc::ServerContext* /* context */, const ::ilmsens::mliquid::dairy::IlmsensUIntValue* request, ::ilmsens::mliquid::dairy::IlmsensUIntResult* response) override
  {
    // get new value from request
    int tValOK = 0;
    unsigned int tAvg = request->value();

    // apply to shared structure
    mMtxSharedData.lock();
      //check against limits
      if ((mState.mModInfo.mAvgLim[0] <= tAvg) && (tAvg <= mState.mModInfo.mAvgLim[1]))
      {
        // check that no measurement is currently running
        if (mState.mRunFlag)
        {
          //must not change properties during a run
          tAvg = mState.mSW_AVG;
          tValOK = -1;
        }
        else
        {
          // all was fine
          mState.mSW_AVG   = tAvg;
          mState.mMeasRate = ilmsens::mliquid::lely_eis::datareader::getMeasurementRate();
          tValOK = 1;
        }
      }
      else
      {
        //must keep old value
        tAvg   = mState.mSW_AVG;
        tValOK = 0;
      }
    mMtxSharedData.unlock();

    //return the retrieved value in the response
    response->set_result(tAvg);
    //generate the status message which is part of the result message
    if (tValOK == 1)
    {
      IlmsensStatusMessage* tStat = new IlmsensStatusMessage(sStatusMsgOK);
      response->set_allocated_status(tStat);
    }
    else if (tValOK == -1)
    {
      IlmsensStatusMessage* tStat = new IlmsensStatusMessage(sStatusMsgWS);
      response->set_allocated_status(tStat);
    }
    else
    {
      IlmsensStatusMessage* tStat = new IlmsensStatusMessage(sStatusMsgOOR);
      response->set_allocated_status(tStat);
    }
    std::cout << ANSI_COLOR_YELLOW << "gRPCServices::INFO: SetSoftwareAverages requested. Finally set " << tAvg << " averages." << ANSI_COLOR_RESET << std::endl;
    return Status::OK;
  }

  // set number of wait cycles (returns actually set value)
  ::grpc::Status SetWaitCycles(::grpc::ServerContext* /* context */, const ::ilmsens::mliquid::dairy::IlmsensUIntValue* request, ::ilmsens::mliquid::dairy::IlmsensUIntResult* response) override
  {
    // get new value from request
    int tValOK = 0;
    unsigned int tWCyc = request->value();

    // apply to shared structure
    mMtxSharedData.lock();
      //check against limits
      if ((mState.mModInfo.mWaitLim[0] <= tWCyc) && (tWCyc <= mState.mModInfo.mWaitLim[1]))
      {
        // check that no measurement is currently running
        if (mState.mRunFlag)
        {
          //must not change properties during a run
          tWCyc  = mState.mWaitCyc;
          tValOK = -1;
        }
        else
        {
          // all was fine
          mState.mWaitCyc = tWCyc;
          mState.mMeasRate = ilmsens::mliquid::lely_eis::datareader::getMeasurementRate();
          tValOK = 1;
        }
      }
      else
      {
        //must keep old value
        tWCyc  = mState.mWaitCyc;
        tValOK = 0;
      }
    mMtxSharedData.unlock();

    //return the retrieved value in the response
    response->set_result(tWCyc);
    //generate the status message which is part of the result message
    if (tValOK == 1)
    {
      IlmsensStatusMessage* tStat = new IlmsensStatusMessage(sStatusMsgOK);
      response->set_allocated_status(tStat);
    }
    else if (tValOK == -1)
    {
      IlmsensStatusMessage* tStat = new IlmsensStatusMessage(sStatusMsgWS);
      response->set_allocated_status(tStat);
    }
    else
    {
      IlmsensStatusMessage* tStat = new IlmsensStatusMessage(sStatusMsgOOR);
      response->set_allocated_status(tStat);
    }
    std::cout << ANSI_COLOR_YELLOW << "gRPCServices::INFO: SetWaitCycles requested. Finally set " << tWCyc << " cycles." << ANSI_COLOR_RESET << std::endl;
    return Status::OK;
  }

  // get current measurement rate [Hz] as a result of averages and wait cycles
  ::grpc::Status GetMeasurementRate(::grpc::ServerContext* /* context */, const ::google::protobuf::Empty* /* request */, ::ilmsens::mliquid::dairy::IlmsensDblResult* response) override
  {
    // get infos from shared structure
    mMtxSharedData.lock();
      double tRate = mState.mMeasRate;
    mMtxSharedData.unlock();

    //return the retrieved value in the response
    response->set_result(tRate);
    //generate the status message which is part of the result message
    IlmsensStatusMessage* tStat = new IlmsensStatusMessage(sStatusMsgOK);
    response->set_allocated_status(tStat);
    std::cout << ANSI_COLOR_YELLOW << "gRPCServices::INFO: GetMeasurementRate requested returning " << std::fixed << std::setprecision(6) << tRate << " Hz." << ANSI_COLOR_RESET << std::endl;
    return Status::OK;
  }
};

class IlmsensMeasurementImpl final : public IlmsensMeasurement::Service 
{
  // start a measurement and set number of IRFs to aquire 
  //   0: continuous measurement, i.e. continue until stop is called
  // > 0: measure this number of IRFs, then stop automatically
  ::grpc::Status StartMeasurement(::grpc::ServerContext* /* context */, const ::ilmsens::mliquid::dairy::IlmsensUIntValue* request, ::ilmsens::mliquid::dairy::IlmsensStatusMessage* response) override
  {
    //get # of IRFs used in this measurement
    unsigned int tNumIRF = request->value();

    // find out, if a measurement is already running
    mMtxSharedData.lock();
      bool tRunning = mState.mRunning;
    mMtxSharedData.unlock();

    if (!tRunning)
    {
      //prepare measurement frame message object, but keep IRF lengths
      resetMeasDataMessage(0);

      //drain PR cache, if data is still lingering from last call
      mMtxPRCache.lock();
        bool tEmpty = mPRCache.empty();
        while (!tEmpty)
        {
          mPRCache.pop();
          tEmpty = mPRCache.empty();
        }
      mMtxPRCache.unlock();

      //set the number of IRFs to acquire and start measurement
      mMtxSharedData.lock();
        mState.mNumIRF  = tNumIRF;
        mState.mRunFlag = true;
      mMtxSharedData.unlock();

      //generate the status message
      response->set_status_code(sStatusMsgOK.status_code());
      response->set_description(sStatusMsgOK.description());
    }
    else
    {
      //generate the status message
      response->set_status_code(sStatusMsgWS.status_code());
      response->set_description("Measurement start requested, but measurement is still running (mighty be race condition!). Nothing to do!");
      std::cout << ANSI_COLOR_YELLOW << "gRPCServices::INFO: StartMeasurement requested while dataReader indicates an ongoing measurement: nothing to do!" << ANSI_COLOR_RESET << std::endl;
    }

    std::cout << ANSI_COLOR_YELLOW << "gRPCServices::INFO: StartMeasurement requested with " << tNumIRF << " IRFs to measure (0 = continuous)." << ANSI_COLOR_RESET << std::endl;
    return Status::OK;
  }

  // immediately stop ongoing measurement run
  // (due to gRPC's asynchronous transport mechanism, streamed data may still 
  //  arrive after completion of this method!)
  ::grpc::Status StopMeasurement(::grpc::ServerContext* /* context */, const ::google::protobuf::Empty* /* request */, ::ilmsens::mliquid::dairy::IlmsensStatusMessage* response) override
  {
    //stop measurement
    mMtxSharedData.lock();
      mState.mRunFlag = false;
    mMtxSharedData.unlock();

    //generate the status message
    response->set_status_code(sStatusMsgOK.status_code());
    response->set_description(sStatusMsgOK.description());
    std::cout << ANSI_COLOR_YELLOW << "gRPCServices::INFO: StopMeasurement requested." << ANSI_COLOR_RESET << std::endl;
    return Status::OK;
  }

  // retrieve measurement data during a measurement run
  ::grpc::Status RetrieveMeasuredData(::grpc::ServerContext* /* context */, const ::google::protobuf::Empty* /* request */, ::grpc::ServerWriter< ::ilmsens::mliquid::dairy::IlmsensMeasuredData>* writer) override
  {
    std::cout << ANSI_COLOR_YELLOW << "gRPCServices::INFO: RetrieveMeasuredData requested." << ANSI_COLOR_RESET << std::endl;
    // keep track of number of transferred IRFs in the RPC request
    unsigned int tFrameCnt = 0;

    // get and update state
    mMtxSharedData.lock();
      bool tRunFlag = mState.mRunFlag;
      bool tRunning = mState.mRunning;
      unsigned int tNumIRF = mState.mNumIRF;
      mState.mStreaming = true;
    mMtxSharedData.unlock();

    // test processing cache for new data as long as a measurement is running
//    std::cout << "Retrieve Data RPC called: runflag = " << std::dec << tRunFlag << " | #IRFs = " << tNumIRF << std::endl;
    while (tRunFlag || tRunning || (tNumIRF && (tFrameCnt < tNumIRF)))
    {
      // Waiting for new data in PR cache
      mMtxPRCache.lock();
        bool tEmpty = mPRCache.empty();

      if (!tEmpty)
      {
        // new data has arrived from processing thread, so copy and forward it
          auto tTXData = mPRCache.front();
          mPRCache.pop();
        mMtxPRCache.unlock();

        // this frame will be sent so it counts
        tFrameCnt ++;

        // fill in IRF frame message
        sMeasData.set_sequence_number(tTXData.mIRFNum);

        sMeasData.set_adc_level_rx1(tTXData.mADCLevel1);
        sMeasData.set_adc_level_rx2(tTXData.mADCLevel2);

        sMeasData.set_mcu_temperature(tTXData.mMCUTemp);
        sMeasData.set_probe_temperature(tTXData.mSenTemp);
        sMeasData.set_sensor_temperature(tTXData.mModTemp);

        // check array sizes
        auto tIRF1Samples = sMeasData.mutable_irf_rx1();
        auto tIRF2Samples = sMeasData.mutable_irf_rx2();
        int tNumSamp = (int)tTXData.mCorrRx1.size();
        if (tIRF1Samples->size() != tNumSamp) tIRF1Samples->Resize(tNumSamp, 0.0);
        if (tIRF2Samples->size() != tNumSamp) tIRF2Samples->Resize(tNumSamp, 0.0);

        //actually copy the data
        memcpy((void *)tIRF1Samples->mutable_data(), (void *)tTXData.mCorrRx1.data(), tTXData.mCorrRx1.size()*sizeof(double));
        memcpy((void *)tIRF2Samples->mutable_data(), (void *)tTXData.mCorrRx2.data(), tTXData.mCorrRx2.size()*sizeof(double));
#if 0
        for (int tI = 0; tI < tNumSamp; tI ++)
        {
          tIRF1Samples->Set(tI, tTXData.mCorrRx1[tI]);
          tIRF2Samples->Set(tI, tTXData.mCorrRx2[tI]);
        }
#endif

        //check, if measurement will be ended after this IRF
        if (tNumIRF && (tFrameCnt >= tNumIRF))
        {
          // will be last IRF in this stream
          sMeasData.mutable_status()->set_status_code(sStatusMsgFIN.status_code());
          sMeasData.mutable_status()->set_description(sStatusMsgFIN.description());

          //send it and end the RCP request in any case
//          std::cout << "Retrieve Data RPC: sending last frame to channel!" << std::endl;
          grpc::WriteOptions tOpts;
          writer->WriteLast(sMeasData, tOpts);

          //must go
          break;
        }
        else
        {
          //just send it as is with std. status of "ILMSENS_SUCCESS"
          if (!writer->Write(sMeasData))
          {
            // could not write (stream has been closed)
//            std::cout << "Retrieve Data RPC::ERROR: could not write frame to channel!" << std::endl;
            break;
          }
        }
      }
      else
      {
        // cache was empty
        mMtxPRCache.unlock();

        // wait some time to lower CPU burden
        ilmsens::mliquid::lely_eis::delay_ms(10);
      }

      // update run flags
      mMtxSharedData.lock();
        tRunFlag = mState.mRunFlag;
        tRunning = mState.mRunning;
      mMtxSharedData.unlock();
    }

    // update state
    mMtxSharedData.lock();
      mState.mStreaming = false;
    mMtxSharedData.unlock();

//    std::cout << "Retrieve Data RPC finished: runflag = " << std::dec << tRunFlag << " | #IRFs = " << tNumIRF << std::endl;
    // finally end the RPC request
    std::cout << ANSI_COLOR_YELLOW << "gRPCServices::INFO: RetrieveMeasuredData finished. Sent " << tFrameCnt << " frames to client." << ANSI_COLOR_RESET << std::endl;
    return Status::OK;
  }
};


namespace ilmsens 
{
namespace mliquid
{
namespace lely_eis 
{
namespace grpc
{

  /* -------------------------------------------------------------------------------------------------
 * Thread workers
 * -------------------------------------------------------------------------------------------------
 */

/* thread handler for running an already setup gRPC server instance */
void* gRPCServer(void* pServer)
{
  //cast the provided pointer as a server
  ::grpc::Server* tServer = static_cast< ::grpc::Server* >(pServer);
  
  // service gRPC requests until server is being stopped (from main thread)
  tServer->Wait();

  return(NULL);
}

/* main thread function for gRPC services
   -> spaws additional threads for specific services */
void* gRPCServices(void*)
{
  // we are starting to work
  std::cout << ANSI_COLOR_YELLOW << "gRPCServices::INFO: thread created." << ANSI_COLOR_RESET << std::endl;

  // generate TCP listening port definition
  std::string tGRPCListenAdr = "0.0.0.0:" + std::to_string(GRPC_TCP_PORT);

  // generate static status messages include in gRPC return messages
  sStatusMsgOK.set_status_code(IlmsensStatus::ILMSENS_SUCCESS);
  sStatusMsgOK.set_description("Operation completed successfully.");

  sStatusMsgOOR.set_status_code(IlmsensStatus::ILMSENS_ERROR_INVALID_PARAM);
  sStatusMsgOOR.set_description("The parameter value was out of range. Keeping old setting.");

  sStatusMsgFIN.set_status_code(IlmsensStatus::ILMSENS_FINISHED);
  sStatusMsgFIN.set_description("Operation completed successfully. This is the last message of the stream.");

  sStatusMsgWS.set_status_code(IlmsensStatus::ILMSENS_ERROR_STATE);
  sStatusMsgWS.set_description("Operation not allowed in current state. Keeping old setting.");

  // reset IRF frame message
  mMtxSharedData.lock();
    unsigned int tMLBSLen = mState.mMLBSLen;
  mMtxSharedData.unlock();
  resetMeasDataMessage(tMLBSLen);

  try
  {
    // create gRPC server builder
    ServerBuilder tSrvBuilder;
    
    // Listen on the given address without any authentication mechanism.
    tSrvBuilder.AddListeningPort(tGRPCListenAdr, ::grpc::InsecureServerCredentials());

    // add all Ilmsens gRPC services to same server
    IlmsensSensorInfosImpl      tSenInfService;
    IlmsensMeasurementSetupImpl tMeasSetupService;
    IlmsensMeasurementImpl      tMeasService;

    tSrvBuilder.RegisterService(&tSenInfService);
    tSrvBuilder.RegisterService(&tMeasSetupService);
    tSrvBuilder.RegisterService(&tMeasService);

    // create and start the actual gRPC server
    std::unique_ptr<Server> tGRPCServer(tSrvBuilder.BuildAndStart());
    
    std::cout << ANSI_COLOR_YELLOW << "gRPCServices::INFO: gRPC server now listening on TCP '" << tGRPCListenAdr << "' ..." << ANSI_COLOR_RESET << std::endl;

    // create server working thread
    signed int tRes = pthread_create(&sGRPCServer, NULL, &gRPCServer, tGRPCServer.get());
    if(tRes != 0) 
    {
      //could not create the server working thread :-(
      std::cout << ANSI_COLOR_YELLOW << "gRPCServices::ERROR: cannot create gRPC server working thread!" << ANSI_COLOR_RESET << std::endl;
      throw ilmsens::mliquid::lely_eis::Error("gRPCServices::ERROR: cannot create gRPC server working thread!", ilmsens_error::ILMSENS_ERROR_NO_MEMORY, errno);

    }

    // now do polling loop until exit flag has been raised
    mMtxExit.lock();
      bool tExitFlag = mExitShutdwn;
    mMtxExit.unlock();

    while(!tExitFlag)
    {
      // check processing cache when no-one is measuring asnd streaming
      mMtxSharedData.lock();
        bool tCanDrain = (!mState.mRunFlag) && (!mState.mRunning) && (!mState.mStreaming);
      mMtxSharedData.unlock();

      if (tCanDrain)
      {
        // check for data in processing cache and drain it
        mMtxPRCache.lock();
          bool tEmpty = mPRCache.empty();
          while (!tEmpty)
          {
            mPRCache.pop();
            tEmpty = mPRCache.empty();
          }
        mMtxPRCache.unlock();
      }

      //wait some time to lower CPU burden
      delay_ms(100);

      //update exit flag
      mMtxExit.lock();
        tExitFlag = mExitShutdwn;
      mMtxExit.unlock();
    } // gRPC services exit polling loop

    // server must shut down now
    std::cout << ANSI_COLOR_YELLOW << "gRPCServices::INFO: shutting down gRPC worker thread ..." << ANSI_COLOR_RESET << std::endl;
    tGRPCServer->Shutdown();

    //wait for thread to finally finish
    pthread_join(sGRPCServer, NULL);
  }
  catch(ilmsens::mliquid::lely_eis::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "gRPCServices::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl 
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
  }
  catch(std::exception& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "gRPCServices::Std-EXCEPTION: " << ANSI_COLOR_RESET 
              << tErr.what() << std::endl;
  }
  catch(...) 
  {
    std::cerr << ANSI_COLOR_RED << "gRPCServices::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;
  }

  // can go now
  std::cout << ANSI_COLOR_YELLOW << "gRPCServices: shutdown complete, exiting thread" << ANSI_COLOR_RESET << std::endl;
  return(NULL);
} // gRPCServices

} //namespace grpc
} //namespace lely_eis
} //namespace mliquid
} //namespace ilmsens
