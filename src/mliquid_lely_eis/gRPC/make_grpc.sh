#!/bin/bash

# build protocol buffer classes
echo "Building protocol buffer classes ..."
protoc -I=./ --cpp_out=./cpp ./ilmsens_mliquid_dairy_lely.proto

# build gprc service classes
echo "Building gRPC service classes ..."
protoc -I=./ --grpc_out=./cpp --plugin=protoc-gen-grpc=`which grpc_cpp_plugin` ./ilmsens_mliquid_dairy_lely.proto
