#ifndef POLLEXTSENSORS_HPP
#define POLLEXTSENSORS_HPP

/** COM constants */
#define COM_ANSWER_TO_MS   100        ///< COM port timeout for waiting for beginning of answer from sensor

namespace ilmsens 
{
  namespace mliquid
  {
    namespace lely_eis 
    {
      namespace sensors
      {

        /** Entry point for processing thread */
        void* pollExtSensors(void*);

      } //namespace sensors
    } //namespace lely_eis
  } //namespace mliquid
} //namespace ilmsens

#endif //POLLEXTSENSORS_HPP
