/// @file shared.hpp
/// @brief Global variables shared by all threads.
///
/// The variables defined here can be accessed by all threads of the program. 
/// Mutex mechanism is used to synchronise access.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>


#ifndef SHARED_HPP
#define SHARED_HPP

// C++ STL and std.-libs
#include <stdexcept>
#include <mutex>

// processing-related cache buffer types
#include "data_cache_FIFO.hpp"

// exceptions & error handling
#include "ilmsens/ilmsens_error.h"

// Ilmsens HAL constants and types
#include "ilmsens/hal/ilmsens_hal.h"


/**   
 * @defgroup mliquid_lely_eis_global_defn Common definitions used by the app.
 * @brief Defintions used by all threads, e.g. ANSI color codes, etc.
 * @{
 */  


/** Defining colored outputs in terminal */
#ifdef WIN32
  // no console colouring available by default
  #define ANSI_COLOR_RED     "| [R] | "
  #define ANSI_COLOR_GREEN   "| [G] | "
  #define ANSI_COLOR_YELLOW  "| [Y] | "
  #define ANSI_COLOR_BLUE    "| [B] | "
  #define ANSI_COLOR_MAGENTA "| [M] | "
  #define ANSI_COLOR_CYAN    "| [C] | "
  #define ANSI_COLOR_RESET   " |"
#else
  #define ANSI_COLOR_RED     "\x1b[31m"
  #define ANSI_COLOR_GREEN   "\x1b[32m"
  #define ANSI_COLOR_YELLOW  "\x1b[33m"
  #define ANSI_COLOR_BLUE    "\x1b[34m"
  #define ANSI_COLOR_MAGENTA "\x1b[35m"
  #define ANSI_COLOR_CYAN    "\x1b[36m"
  #define ANSI_COLOR_RESET   "\x1b[0m"
#endif

/** @} mliquid_lely_eis_global_defn */


/**   
 * @defgroup mliquid_lely_eis_global_types Type definitions used by the app.
 * @brief Types for shared program states.
 * @{
 */  

/** Sensor configuration and state data structure for sharing between threads */
struct sSharedData 
{
  bool mSenAct;                                   ///< dataReader could activate the sensor?
  bool mRunFlag;                                  ///< Start/stop a measurement -> can be changed by multiple threads
  bool mRunning;                                  ///< Measurement is actually running? -> can only be set by dataReader thread!
  bool mProcRun;                                  ///< Processing is running?
  bool mStreaming;                                ///< Measurement data is currently being sent to a client by gRPC

  bool mDebFlag;                                  ///< Output debug messages?
  bool mSaveFlag;                                 ///< Save raw data to storage?
  bool mSendIRF;                                  ///< Send IRF alongside IRF info via streaming?
  bool mProcRdy;                                  ///< Processing is ready?

  bool mExtSenThreadError;                        ///< Error in Serial Thread?
  bool mExtSenDoPoll;                             ///< Flag to indicate whether polling should be activated in serial Thread

  unsigned int mMLBSLen;                          ///< Length of M-Sequence (Samples)
  double       mClk;                              ///< RF system clock f0 [GHz]
  unsigned int mACK;                              ///< Acknoledge Code (ASCII "ACK ")
  unsigned int mNACK;                             ///< Not-Acknoledge Code (ASCII "NACK")
  unsigned int mSW_AVG;                           ///< Software averages
  unsigned int mWaitCyc;                          ///< Wait cycles between averaging cycles
  unsigned int mHW_AVG;                           ///< Hardware averages
  double       mSampClk;                          ///< equivalent sampling rate of UWB sensor [GHz]
  double       mMeasRate;                         ///< measurement rate of sensor


  unsigned int mExtPollInt;                       ///< time between polls of external sensor values [ms]
  unsigned int mLaserCOM;                         ///< COM port number for Laser distance metre
  float mLaserDistance;                           ///< distance sended from laser sensor

  unsigned int mNumIRF;                           ///< Number of IRFs to acquire per run, 0 = infinite run

  unsigned int mStrWait;                          ///< min. wait interval between streaming IRFs via TCP server
  
  unsigned int mMID;                              ///< Measurement ID (to be set by client)

  bool         mDoTimeZero;                       ///< do time-zero alignment to maxmum amplitude?
  double       mBGR_a;                            ///< exponential dynamic background removal alpha constant ([0.0 .. 1.0[, 0.0 means BGR off)

  char                mID[ILMSENS_HAL_MOD_ID_BUF_SIZE];        ///< Device ID string as read from sensor
  ilmsens_hal_ModInfo mModInfo;                                ///< complete module info after configuration

  //default constructor
  sSharedData():
    mSenAct(false),
    mRunFlag(false),
    mRunning(false),
    mProcRun(false),
    mStreaming(false),
    mDebFlag(false),
    mSaveFlag(false),
    mSendIRF(true),
    mProcRdy(false),
    mExtSenThreadError(false),
    mExtSenDoPoll(false),
    mMLBSLen(511),
    mClk(12.800),
    mACK(0x41434b20),
    mNACK(0x4e41434b),
    mSW_AVG(10),
    mWaitCyc(0),
    mHW_AVG(48),
    mSampClk(12.800),
    mMeasRate(99.84),
    mExtPollInt(500),
    mLaserCOM(0),
    mLaserDistance(-1.0),
    mNumIRF(0),
    mStrWait(1000),
    mMID(0),
    mDoTimeZero(false),
    mBGR_a(0.0)
  {}
};

/** Configuration and state data type */
typedef struct sSharedData TSharedData; 

/** @} mliquid_lely_eis_global_types */

  
/**   
 * @defgroup mliquid_lely_eis_global_synch Mutex definitions for synchronisation of threads.
 * @brief Mutexes for shared program states and memory caches.
 * @{
 */  

// Synchronisation mutexes for configuration and measurement state 
extern std::mutex mMtxExit;                       ///< Mutex for App's EXIT-Flag
extern std::mutex mMtxLogging;                    ///< Mutex for Logging Shutdown-Flag
extern std::mutex mMtxProcessing;                 ///< Mutex for Processing Shutdown-Flag

// Synchronisation mutexes for configuration and measurement state 
extern std::mutex mMtxSharedData;                 ///< Mutex for access to shared data structure

// Synchronisation mutexes for shared memories
extern std::mutex mMtxDRCache;                    ///< Synchronisation mutex for DR cache (datareader & processing)
extern std::mutex mMtxPRCache;                    ///< Synchronisation mutex for PR cache (processing & streaming)

/** @} mliquid_lely_eis_global_synch */

 
/**   
 * @defgroup mliquid_lely_eis_global_state State and sensor configuration information.
 * @brief Variables representing program or thread state and sensor configuration.
 * @{
 */  

extern TSharedData mState;                        ///< global state flags and sensor configuration

extern bool mExitShutdwn;                         ///< Application shutdown flag
extern bool mLogMShutdwn;                         ///< Logging thread shutdown flag
extern bool mProcShutdwn;                         ///< Processing thread shutdown flag

/** @} mliquid_lely_eis_global_state */

 
/**   
 * @defgroup mliquid_lely_eis_global_memory Global memory caches or buffers.
 * @brief Memories for transferring measured data between different threads.
 * @{
 */  

extern TFIFOCache mDRCache;                       ///< Cache for DataReader to buffer data for processing thread
extern TFIFOCache mPRCache;                       ///< Cache for Processing to buffer data for streaming thread

/** @} mliquid_lely_eis_global_memory */

 
/**   
 * @defgroup mliquid_lely_eis_exception Exceptions & safety tools used by this app.
 * @brief Exceptions only used by this app and its threads. Tools for exception safe programming.
 * @{
 */  

namespace ilmsens 
{
  namespace mliquid
  {
    namespace lely_eis 
    {

      /**  Application-specific error class storing an Ilmsens error code along with a message */
      class Error : public std::runtime_error 
      {
        typedef std::runtime_error Base;

        public:
          Error(const char* pMsg, int pCode = ILMSENS_ERROR_UNKNOWN, int pErrorNum = 0)
            : Base(pMsg), mErrorCode(pCode), mErrorNum(pErrorNum) {}
          
          int getErrorCode() const { return mErrorCode; }
          int getErrorNum () const { return mErrorNum; }

        private:
          int mErrorCode;
          int mErrorNum;
      };

    } //namespace lely_eis
  } //namespace mliquid
} //namespace ilmsens

/** @} mliquid_lely_eis_exception */

 
/**   
 * @defgroup mliquid_lely_eis_tools Common tools used by this app.
 * @brief Tools used by this app and its threads, e.g. sleep function.
 * @{
 */  

namespace ilmsens 
{
  namespace mliquid
  {
    namespace lely_eis 
    {

      void delay_ms(unsigned int pDel_ms);

    } //namespace lely_eis
  } //namespace mliquid
} //namespace ilmsens

/** @} mliquid_lely_eis_tools */

#endif //SHARED_HPP
