#ifndef GRPCSERVERS_HPP
#define GRPCSERVERS_HPP

/** TCP constants */
#define GRPC_TCP_PORT   8110        ///< TCP port for registering gRPC services

/** default constants */
#define GRPC_INVALID_TEMP  -300.0   ///< invalid temperature value

namespace ilmsens 
{
  namespace mliquid
  {
    namespace lely_eis 
    {
      namespace grpc
      {

        /** Entry point for gRPC main thread */
        void* gRPCServices(void*);

      } //namespace grpc
    } //namespace lely_eis
  } //namespace mliquid
} //namespace ilmsens

#endif //GRPCSERVERS_HPP
