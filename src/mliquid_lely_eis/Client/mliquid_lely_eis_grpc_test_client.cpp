/// @file mliquid_lely_eis_grpc_test_client.cpp
/// @brief Simple gRPC client to test m:liquid.lely_EIS app with its gRPC services
/// @author Ralf.Herrmann@ilmsens.com

// Std. C++ library types / STL 

// IO, and Strings
#include <iostream>
#include <iomanip>
#include <thread>

// Strings & analysis
#include <string>

//pther common headers
#include <cmath>
#include <sstream>
#include <stdexcept>
#include <vector>
#include <chrono>

using namespace std::chrono;

// gRPC common stuff
#include <grpcpp/grpcpp.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::Status;


// gRPC and protocol buffer generated code
#ifdef WIN32

  #include "win_ilmsens_mliquid_dairy_lely.grpc.pb.h"
  #include "win_ilmsens_mliquid_dairy_lely.pb.h"

#else

  #include "gRPC/ilmsens_mliquid_dairy_lely.grpc.pb.h"
  #include "gRPC/ilmsens_mliquid_dairy_lely.pb.h"

#endif

using namespace ilmsens::mliquid::dairy;

////////////////////////////////////////////////////////////////////////
// default sensor setup
////////////////////////////////////////////////////////////////////////

// app version
#define MLIQUID_LELY_EIS_GRPC_TEST_CLIENT_MAJOR 1
#define MLIQUID_LELY_EIS_GRPC_TEST_CLIENT_MINOR 0
#define MLIQUID_LELY_EIS_GRPC_TEST_CLIENT_BUILD 0

// gRPC ressources
#define MLIQUID_LELY_EIS_GRPC_PORT 8110 // default port for gRPC server

// sensor parameters
#define MLBS_ORDER_DEFAULT   9       //can be 9, 12, or 15 at the moment
#define MLBS_CLK_DEFAULT     13.312  //default master clock rate [GHz]


// measurement setup
#define SW_AVG_DEFAULT       32      //default software averages

#define REPONSE_CNT_DEFAULT  10      //default impulse response count per test run


////////////////////////////////////////////////////////////////////////
// configuration variables
////////////////////////////////////////////////////////////////////////

// gRPC ressources
static unsigned sGRPCPort  =  MLIQUID_LELY_EIS_GRPC_PORT;

//sensor parameters
static unsigned sMLBSOrder = MLBS_ORDER_DEFAULT;      
static double   sMasterClk = MLBS_CLK_DEFAULT; 

//measurement parameters
static unsigned sSWAvg    = SW_AVG_DEFAULT;    

//test run setup
static unsigned sIRFCnt   = REPONSE_CNT_DEFAULT;


////////////////////////////////////////////////////////////////////////
// helper functions
////////////////////////////////////////////////////////////////////////

//apply defaults to all config variables
void resetDefaults()
{
  // gRPC ressources
  sGRPCPort  =  MLIQUID_LELY_EIS_GRPC_PORT;

  //sensor parameters
  sMLBSOrder = MLBS_ORDER_DEFAULT;
  sMasterClk = MLBS_CLK_DEFAULT;

  //measurement parameters
  sSWAvg    = SW_AVG_DEFAULT;
  sIRFCnt   = REPONSE_CNT_DEFAULT;
}

//parse command line to set various parameters
void parseCommandLine(unsigned argc, char* argv[])
{
  //apply defaults first
  resetDefaults();

  for (unsigned tI = 1; tI < argc; ++tI) 
  {
    if (0 == std::string("--mlbsOrder").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sMLBSOrder = std::stoi(argv[tI + 1]);
      }
      else 
      {
        throw std::runtime_error("missing MLBS order argument.");
      }
    } 
    else if (0 == std::string("--rfClock").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sMasterClk = std::stod(argv[tI + 1]);
      }
      else 
      {
        throw std::runtime_error("missing master clock [GHz] argument.");
      }
    } 
    else if (0 == std::string("--softwareAvg").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sSWAvg = std::stoi(argv[tI+1]);
      } 
      else
      {
        throw std::runtime_error("missing software averages argument.");
      }
    } 
    else if (0 == std::string("--responseCount").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sIRFCnt = std::stoi(argv[tI+1]);
      } 
      else
      {
        throw std::runtime_error("missing impulse response count argument.");
      }
    } 
    else if (0 == std::string("--gRPCTCPPort").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sGRPCPort = std::stoi(argv[tI+1]);
      } 
      else
      {
        throw std::runtime_error("missing impulse response count argument.");
      }
    } 

  } // arguent parsing for loop
} // parseCommandLine


////////////////////////////////////////////////////////////////////////
// useful macros
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
// useful types
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
// entry point for example application
////////////////////////////////////////////////////////////////////////

int main(int argc, char* argv[])
{
  ////////////////////////////////////////////////////////////////////////
  // get config parameters from command line
  ////////////////////////////////////////////////////////////////////////
  try
  {
    parseCommandLine(argc, argv);
  }
  catch (const std::runtime_error &tEx)
  {
    std::cerr << "Error parsing command line: " << tEx.what() << std::endl;
    return(1);
  }

  ////////////////////////////////////////////////////////////////////////
  // Output project version
  ////////////////////////////////////////////////////////////////////////

  std::cout << "gRPC test client app version is V" << std::dec << MLIQUID_LELY_EIS_GRPC_TEST_CLIENT_MAJOR << "." << MLIQUID_LELY_EIS_GRPC_TEST_CLIENT_MINOR << "." << MLIQUID_LELY_EIS_GRPC_TEST_CLIENT_BUILD << "." << std::endl << std::endl;

  std::cout << "Using the following configuration: " << std::endl
            << "* MLBS order              : " << std::dec << sMLBSOrder << std::endl
            << "* RF system clock [GHz]   : " << std::dec << std::setprecision(6) << sMasterClk << std::endl
            << "* Software averages       : " << std::dec << sSWAvg << std::endl
            << "* # of IRF for run        : " << std::dec << sIRFCnt << std::endl
            << "* gRPC TCP port           : " << std::dec << sGRPCPort << std::endl
            << std::endl;

  ////////////////////////////////////////////////////////////////////////
  // prepare gRPC channel and service stubs
  ////////////////////////////////////////////////////////////////////////

  std::string tGRPC_URL = "localhost:" + std::to_string(sGRPCPort);
  auto mGRPCChannel = grpc::CreateChannel(tGRPC_URL, grpc::InsecureChannelCredentials());

  unsigned int mErrCnt = 0;

  ////////////////////////////////////////////////////////////////////////
  // get all infos about sensor using gRPC service IlmsensSensorInfos
  ////////////////////////////////////////////////////////////////////////

  auto tIlmsensSensorInfosGRPCStub = IlmsensSensorInfos::NewStub(mGRPCChannel);

  std::cout << std::endl << "============================================================" << std::endl;
  std::cout << "gRPC test client: testing service 'IlmsensSensorInfos':" << std::endl;
  std::cout << "=======================================================" << std::endl << std::endl;

  google::protobuf::Empty tEmptyRequest;
  IlmsensStrResult tIDMsg;
  ClientContext tGRPCContext00;
  Status tGRPCStat = tIlmsensSensorInfosGRPCStub->GetSensorID(&tGRPCContext00, tEmptyRequest, &tIDMsg);
  if (tGRPCStat.ok())
  {
    std::cout << "IlmsensSensorInfos::GetSensorID: OK | ID: " << tIDMsg.result() << " | Status code: " << std::dec << tIDMsg.status().status_code() << " | Status message: " << tIDMsg.status().description() << std::endl;
  }
  else
  {
    mErrCnt ++;
    std::cout << "IlmsensSensorInfos::GetSensorID: FAILED with gRPC status code " << tGRPCStat.error_code() << " and message '" << tGRPCStat.error_message() << "'!" << std::endl;
  }

  IlmsensUIntResult tUIVal;
  ClientContext tGRPCContext01;
  tGRPCStat = tIlmsensSensorInfosGRPCStub->GetMLBSOrder(&tGRPCContext01, tEmptyRequest, &tUIVal);
  if (tGRPCStat.ok())
  {
    std::cout << "IlmsensSensorInfos::GetMLBSOrder: OK | MLBS Order: " << tUIVal.result() << " | Status code: " << std::dec << tUIVal.status().status_code() << " | Status message: " << tUIVal.status().description() << std::endl;
  }
  else
  {
    mErrCnt ++;
    std::cout << "IlmsensSensorInfos::GetMLBSOrder: FAILED with gRPC status code " << tGRPCStat.error_code() << " and message '" << tGRPCStat.error_message() << "'!" << std::endl;
  }

  ClientContext tGRPCContext02;
  tGRPCStat = tIlmsensSensorInfosGRPCStub->GetNumSamp(&tGRPCContext02, tEmptyRequest, &tUIVal);
  if (tGRPCStat.ok())
  {
    std::cout << "IlmsensSensorInfos::GetNumSamp: OK | # samples per IRF: " << tUIVal.result() << " | Status code: " << std::dec << tUIVal.status().status_code() << " | Status message: " << tUIVal.status().description() << std::endl;
  }
  else
  {
    mErrCnt ++;
    std::cout << "IlmsensSensorInfos::GetNumSamp: FAILED with gRPC status code " << tGRPCStat.error_code() << " and message '" << tGRPCStat.error_message() << "'!" << std::endl;
  }

  ClientContext tGRPCContext03;
  tGRPCStat = tIlmsensSensorInfosGRPCStub->GetHardwareAvgerages(&tGRPCContext03, tEmptyRequest, &tUIVal);
  if (tGRPCStat.ok())
  {
    std::cout << "IlmsensSensorInfos::GetHardwareAvgerages: OK | HW averages: " << tUIVal.result() << " | Status code: " << std::dec << tUIVal.status().status_code() << " | Status message: " << tUIVal.status().description() << std::endl;
  }
  else
  {
    mErrCnt ++;
    std::cout << "IlmsensSensorInfos::GetHardwareAvgerages: FAILED with gRPC status code " << tGRPCStat.error_code() << " and message '" << tGRPCStat.error_message() << "'!" << std::endl;
  }

  IlmsensDblResult tDblVal;
  ClientContext tGRPCContext04;
  tGRPCStat = tIlmsensSensorInfosGRPCStub->GetRFSystemClk(&tGRPCContext04, tEmptyRequest, &tDblVal);
  if (tGRPCStat.ok())
  {
    std::cout << "IlmsensSensorInfos::GetRFSystemClk: OK | RF system clock [GHz]: " << std::setprecision(6) << tDblVal.result() << " | Status code: " << std::dec << tDblVal.status().status_code() << " | Status message: " << tDblVal.status().description() << std::endl;
  }
  else
  {
    mErrCnt ++;
    std::cout << "IlmsensSensorInfos::GetRFSystemClk: FAILED with gRPC status code " << tGRPCStat.error_code() << " and message '" << tGRPCStat.error_message() << "'!" << std::endl;
  }


  ////////////////////////////////////////////////////////////////////////
  // get all measurement infos using gRPC service IlmsensMeasurementSetup
  ////////////////////////////////////////////////////////////////////////

  auto tIlmsensMeasurementSetupGRPCStub = IlmsensMeasurementSetup::NewStub(mGRPCChannel);

  std::cout << std::endl << "============================================================" << std::endl;
  std::cout << "gRPC test client: testing service 'IlmsensMeasurementSetup':" << std::endl;
  std::cout << "============================================================" << std::endl << std::endl;

  ClientContext tGRPCContext05;
  tGRPCStat = tIlmsensMeasurementSetupGRPCStub->GetSoftwareAvgerages(&tGRPCContext05, tEmptyRequest, &tUIVal);
  if (tGRPCStat.ok())
  {
    std::cout << "IlmsensMeasurementSetup::GetSoftwareAvgerages: OK | SW averages: " << tUIVal.result() << " | Status code: " << std::dec << tUIVal.status().status_code() << " | Status message: " << tUIVal.status().description() << std::endl;
  }
  else
  {
    mErrCnt ++;
    std::cout << "IlmsensMeasurementSetup::GetSoftwareAvgerages: FAILED with gRPC status code " << tGRPCStat.error_code() << " and message '" << tGRPCStat.error_message() << "'!" << std::endl;
  }

  ClientContext tGRPCContext06;
  tGRPCStat = tIlmsensMeasurementSetupGRPCStub->GetWaitCycles(&tGRPCContext06, tEmptyRequest, &tUIVal);
  if (tGRPCStat.ok())
  {
    std::cout << "IlmsensMeasurementSetup::GetWaitCycles: OK | Wait cycles: " << tUIVal.result() << " | Status code: " << std::dec << tUIVal.status().status_code() << " | Status message: " << tUIVal.status().description() << std::endl;
  }
  else
  {
    mErrCnt ++;
    std::cout << "IlmsensMeasurementSetup::GetWaitCycles: FAILED with gRPC status code " << tGRPCStat.error_code() << " and message '" << tGRPCStat.error_message() << "'!" << std::endl;
  }

  ClientContext tGRPCContext07;
  tGRPCStat = tIlmsensMeasurementSetupGRPCStub->GetMeasurementRate(&tGRPCContext07, tEmptyRequest, &tDblVal);
  if (tGRPCStat.ok())
  {
    std::cout << "IlmsensMeasurementSetup::GetMeasurementRate: OK | Measurement rate [Hz]: " << std::setprecision(6) << tDblVal.result() << " | Status code: " << std::dec << tUIVal.status().status_code() << " | Status message: " << tUIVal.status().description() << std::endl;
  }
  else
  {
    mErrCnt ++;
    std::cout << "IlmsensMeasurementSetup::GetMeasurementRate: FAILED with gRPC status code " << tGRPCStat.error_code() << " and message '" << tGRPCStat.error_message() << "'!" << std::endl;
  }

  std::cout << std::endl << "-------------------------------------------------------------------------------------" << std::endl;
  std::cout << "Setting configured SW averages value of " << std::dec << sSWAvg << " ..." << std::endl;
  std::cout << "-------------------------------------------------------------------------------------" << std::endl << std::endl;

  ClientContext tGRPCContext08;
  IlmsensUIntValue tUINewVal;
  tUINewVal.set_value(sSWAvg);
  tGRPCStat = tIlmsensMeasurementSetupGRPCStub->SetSoftwareAvgerages(&tGRPCContext08, tUINewVal, &tUIVal);
  if (tGRPCStat.ok())
  {
    std::cout << "IlmsensMeasurementSetup::SetSoftwareAvgerages: OK | Returned actual SW averages: " << tUIVal.result() << " | Status code: " << std::dec << tUIVal.status().status_code() << " | Status message: " << tUIVal.status().description() << std::endl;
  }
  else
  {
    mErrCnt ++;
    std::cout << "IlmsensMeasurementSetup::SetSoftwareAvgerages: FAILED with gRPC status code " << tGRPCStat.error_code() << " and message '" << tGRPCStat.error_message() << "'!" << std::endl;
  }

  std::cout << std::endl << "-------------------------------------------------------------------------------------" << std::endl;
  std::cout << "Re-reading SW averages and measurement rate ..." << std::endl;
  std::cout << "-------------------------------------------------------------------------------------" << std::endl << std::endl;

  ClientContext tGRPCContext09;
  tGRPCStat = tIlmsensMeasurementSetupGRPCStub->GetSoftwareAvgerages(&tGRPCContext09, tEmptyRequest, &tUIVal);
  if (tGRPCStat.ok())
  {
    std::cout << "IlmsensMeasurementSetup::GetSoftwareAvgerages: OK | SW averages: " << tUIVal.result() << " | Status code: " << std::dec << tUIVal.status().status_code() << " | Status message: " << tUIVal.status().description() << std::endl;
  }
  else
  {
    mErrCnt ++;
    std::cout << "IlmsensMeasurementSetup::GetSoftwareAvgerages: FAILED with gRPC status code " << tGRPCStat.error_code() << " and message '" << tGRPCStat.error_message() << "'!" << std::endl;
  }

  ClientContext tGRPCContext10;
  tGRPCStat = tIlmsensMeasurementSetupGRPCStub->GetMeasurementRate(&tGRPCContext10, tEmptyRequest, &tDblVal);
  if (tGRPCStat.ok())
  {
    std::cout << "IlmsensMeasurementSetup::GetMeasurementRate: OK | Measurement rate [Hz]: " << std::setprecision(6) << tDblVal.result() << " | Status code: " << std::dec << tUIVal.status().status_code() << " | Status message: " << tUIVal.status().description() << std::endl;
  }
  else
  {
    mErrCnt ++;
    std::cout << "IlmsensMeasurementSetup::GetMeasurementRate: FAILED with gRPC status code " << tGRPCStat.error_code() << " and message '" << tGRPCStat.error_message() << "'!" << std::endl;
  }


  ////////////////////////////////////////////////////////////////////////
  // start a measurement, get data and stop it again
  ////////////////////////////////////////////////////////////////////////

  auto tIlmsensMeasurementGRPCStub = IlmsensMeasurement::NewStub(mGRPCChannel);

  std::cout << std::endl << "============================================================" << std::endl;
  std::cout << "gRPC test client: testing service 'IlmsensMeasurement':" << std::endl;
  std::cout << "============================================================" << std::endl << std::endl;

  std::cout << std::endl << "-------------------------------------------------------------------------------------" << std::endl;
  std::cout << "Starting a measurement of " << std::dec << sIRFCnt << " IRFs ..." << std::endl;
  std::cout << "-------------------------------------------------------------------------------------" << std::endl << std::endl;

  ClientContext tGRPCContext11;
  IlmsensStatusMessage tIStat;
  tUINewVal.set_value(sIRFCnt);
  tGRPCStat = tIlmsensMeasurementGRPCStub->StartMeasurement(&tGRPCContext11, tUINewVal, &tIStat);
  if (tGRPCStat.ok())
  {
    std::cout << "IlmsensMeasurement::StartMeasurement: OK | Status code: " << std::dec << tIStat.status_code() << " | Status message: " << tIStat.description() << std::endl;
  }
  else
  {
    mErrCnt ++;
    std::cout << "IlmsensMeasurement::StartMeasurement: FAILED with gRPC status code " << tGRPCStat.error_code() << " and message '" << tGRPCStat.error_message() << "'!" << std::endl;
  }

  std::cout << std::endl << "-------------------------------------------------------------------------------------" << std::endl;
  std::cout << "Reading measurement data ..." << std::endl;
  std::cout << "-------------------------------------------------------------------------------------" << std::endl << std::endl;

  ClientContext tGRPCContext12;
  std::unique_ptr< ClientReader< IlmsensMeasuredData> > tFrameReader( tIlmsensMeasurementGRPCStub->RetrieveMeasuredData(&tGRPCContext12, tEmptyRequest) );

  IlmsensMeasuredData tFrame;
  while (tFrameReader->Read(&tFrame)) 
  {
    std::cout << " Received measurement frame: #" << std::dec << tFrame.sequence_number() 
              << " | ADC levels Rx1/2: " << tFrame.adc_level_rx1() << "% / " << tFrame.adc_level_rx2() << " %"
              << " | Temps. (MCU, Probe, Sensor): " << std::fixed << std::setprecision(2) << tFrame.mcu_temperature() << "�C, " << tFrame.probe_temperature() << "�C, " << tFrame.sensor_temperature() << "�C "
              << " | IRF length Rx1/2: " << tFrame.irf_rx1().size() << " / " << tFrame.irf_rx2().size()
              << std::endl;
    std::cout << " Reported status was       : Status code: " << std::dec << tFrame.status().status_code() << " | Status message: " << tFrame.status().description() << std::endl << std::endl;
  }

  //finished reading, get the status
  tGRPCStat = tFrameReader->Finish();
  if (tGRPCStat.ok())
  {
    std::cout << "IlmsensMeasurement::RetrieveMeasuredData: OK" << std::endl;
  }
  else
  {
    mErrCnt ++;
    std::cout << "IlmsensMeasurement::RetrieveMeasuredData: FAILED with gRPC status code " << tGRPCStat.error_code() << " and message '" << tGRPCStat.error_message() << "'!" << std::endl;
  }

  std::cout << std::endl << "-------------------------------------------------------------------------------------" << std::endl;
  std::cout << "Stopping the measurement ..." << std::endl;
  std::cout << "-------------------------------------------------------------------------------------" << std::endl << std::endl;

  ClientContext tGRPCContext13;
  tGRPCStat = tIlmsensMeasurementGRPCStub->StopMeasurement(&tGRPCContext13, tEmptyRequest, &tIStat);
  if (tGRPCStat.ok())
  {
    std::cout << "IlmsensMeasurement::StopMeasurement: OK | Status code: " << std::dec << tIStat.status_code() << " | Status message: " << tIStat.description() << std::endl;
  }
  else
  {
    mErrCnt ++;
    std::cout << "IlmsensMeasurement::StopMeasurement: FAILED with gRPC status code " << tGRPCStat.error_code() << " and message '" << tGRPCStat.error_message() << "'!" << std::endl;
  }


  //std::this_thread::sleep_for(std::chrono::seconds(1));

  std::cout << std::endl << "-------------------------------------------------------------------------------------" << std::endl;
  std::cout << "Starting a continuous measurement and stopping it after reading " << std::dec << sIRFCnt << " IRFs ..." << std::endl;
  std::cout << "-------------------------------------------------------------------------------------" << std::endl << std::endl;

  ClientContext tGRPCContext14;
  tUINewVal.set_value(0);
  tGRPCStat = tIlmsensMeasurementGRPCStub->StartMeasurement(&tGRPCContext14, tUINewVal, &tIStat);
  if (tGRPCStat.ok())
  {
    std::cout << "IlmsensMeasurement::StartMeasurement: OK | Status code: " << std::dec << tIStat.status_code() << " | Status message: " << tIStat.description() << std::endl;
  }
  else
  {
    mErrCnt ++;
    std::cout << "IlmsensMeasurement::StartMeasurement: FAILED with gRPC status code " << tGRPCStat.error_code() << " and message '" << tGRPCStat.error_message() << "'!" << std::endl;
  }

  std::cout << std::endl << "-------------------------------------------------------------------------------------" << std::endl;
  std::cout << "Reading measurement data ..." << std::endl;
  std::cout << "-------------------------------------------------------------------------------------" << std::endl << std::endl;

  ClientContext tGRPCContext15;
  tFrameReader = tIlmsensMeasurementGRPCStub->RetrieveMeasuredData(&tGRPCContext15, tEmptyRequest);

  unsigned int tFrameCnt = 0;
  while (tFrameReader->Read(&tFrame)) 
  {
    // get the frame
    std::cout << " Received measurement frame: #" << std::dec << tFrame.sequence_number() 
              << " | ADC levels Rx1/2: " << tFrame.adc_level_rx1() << "% / " << tFrame.adc_level_rx2() << " %"
              << " | Temps. (MCU, Probe, Sensor): " << std::fixed << std::setprecision(2) << tFrame.mcu_temperature() << "�C, " << tFrame.probe_temperature() << "�C, " << tFrame.sensor_temperature() << "�C "
              << " | IRF length Rx1/2: " << tFrame.irf_rx1().size() << " / " << tFrame.irf_rx2().size()
              << std::endl;
    std::cout << " Reported status was       : Status code: " << std::dec << tFrame.status().status_code() << " | Status message: " << tFrame.status().description() << std::endl << std::endl;

    // check for end of measurement
    tFrameCnt ++;
    if (tFrameCnt == sIRFCnt)
    {
      // should stop the measurement now

      std::cout << std::endl << "  -------------------------------------------------------------------------------------" << std::endl;
      std::cout << "  Stopping the measurement because enough IRFs had been read ..." << std::endl;
      std::cout << "  -------------------------------------------------------------------------------------" << std::endl << std::endl;

      ClientContext tGRPCContext16;
      tGRPCStat = tIlmsensMeasurementGRPCStub->StopMeasurement(&tGRPCContext16, tEmptyRequest, &tIStat);
      if (tGRPCStat.ok())
      {
        std::cout << "  IlmsensMeasurement::StopMeasurement: OK | Status code: " << std::dec << tIStat.status_code() << " | Status message: " << tIStat.description() << std::endl;
      }
      else
      {
        mErrCnt ++;
        std::cout << "  IlmsensMeasurement::StopMeasurement: FAILED with gRPC status code " << tGRPCStat.error_code() << " and message '" << tGRPCStat.error_message() << "'!" << std::endl;
      }
      std::cout << std::endl;
    }
  }

  //finished reading, get the status
  tGRPCStat = tFrameReader->Finish();
  if (tGRPCStat.ok())
  {
    std::cout << "IlmsensMeasurement::RetrieveMeasuredData: OK" << std::endl;
  }
  else
  {
    mErrCnt ++;
    std::cout << "IlmsensMeasurement::RetrieveMeasuredData: FAILED with gRPC status code " << tGRPCStat.error_code() << " and message '" << tGRPCStat.error_message() << "'!" << std::endl;
  }

  ////////////////////////////////////////////////////////////////////////
  // Show stats and go
  ////////////////////////////////////////////////////////////////////////

  std::cout << std::endl << "============================================================" << std::endl;
  std::cout << "gRPC test client: " << std::dec << mErrCnt << " errors did occur during testing. Quitting." << std::endl;
  std::cout << "============================================================" << std::endl << std::endl;

  return( mErrCnt == 0 ? 0 : 2);
}
