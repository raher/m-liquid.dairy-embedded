# make all headers visible
include_directories( ${CMAKE_CURRENT_SOURCE_DIR} )

#add gRPC and protocol buffer code
add_subdirectory(gRPC)

#add gRPC client app
add_subdirectory(Client)

#collect header list
file(GLOB HEADER_LIST  *.h*)
set(ILMSENS_MLIQUID_LELY_EIS_HEADERS "${HEADER_LIST}" CACHE INTERNAL "Header list for m:liquid.lely_eis app")

#collect source list
file(GLOB SOURCE_LIST  *.c*)
set(ILMSENS_MLIQUID_LELY_EIS_SOURCES "${SOURCE_LIST}" CACHE INTERNAL "Sources list for m:liquid.lely_eis app")

# create app executable target
add_executable(mliquid_lely_eis ${ILMSENS_MLIQUID_LELY_EIS_HEADERS} ${ILMSENS_MLIQUID_LELY_EIS_SOURCES})
target_link_libraries(mliquid_lely_eis ${ILMSENS_HAL_LIBRARY} ${Poco_LIBRARIES} ${CMAKE_THREAD_LIBS_INIT} ilmsens_networking ilmsens_processing ilmsens_tools rs232_easy ilmsens_mliquid_dairy_lely_proto ilmsens_mliquid_dairy_lely_grpc)
# gRPC::grpc++)
set_property(TARGET mliquid_lely_eis PROPERTY FOLDER "Apps")

install(TARGETS mliquid_lely_eis DESTINATION bin)
