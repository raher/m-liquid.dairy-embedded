﻿//============================================================================
// Name        : m:liquid.lely_eis measurement and processing application
// Author      : Ralf Herrmann
// Version     : 0.1.0
// Copyright   : (C) Ilmsens GmbH 2019
// Description : TCP gRPC control+streaming server & Lely EIS measurement application  
//               for embedded ARM-Systems using an Ilmsens m:liquid UWB-Sensor.
// Changelog:
//
// Rev. 0.1.0 (08/2019, HER)
//
//  + initial version based on m:liquid.humidity application V0.1 with minimal processing
//  + added gRPC server thread handling all service requests
//  - removed ECC sensor support since not used in m:liquid EIS measurements
//
//============================================================================


/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */

// Std. C++ library types / STL 
#include <csignal>     // signal handling
#include <cerrno>      // IO/system call error handling

// Poco logging includes
#include "Poco/AutoPtr.h"
#include "Poco/Logger.h"
#include "Poco/ConsoleChannel.h"
#include "Poco/PatternFormatter.h"
#include "Poco/FormattingChannel.h"
#include "Poco/LogStream.h"
#include "Poco/Message.h"

// Timing and threading includes
#include <chrono>                       // For execution time measurements
#include <pthread.h>                    // POSIX threads
#include <sched.h>                      // for Realtime Priority

// synchronisation
#include <mutex>

// File, IO, and Strings
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstring>

// Ilmsens includes
#include "ilmsens/ilmsens_error.h"      // Ilmsens error codes

// Ilmsens m:liquid.lely_eis includes
#include "ilmsens/mliquid_lely_eis/ilmsens_mliquid_lely_eis_version.h" //project version & revision

// Shared variables, memories, and synchronisation ressources
#include "shared.hpp"

// TCP server threads
#include "logTCPServer.hpp"
#include "ctrlTCPServer.hpp"
#include "strmTCPServer.hpp"

// gRPC server threads
#include "gRPCServers.hpp"

//dataReader for handling the UWB sensor
#include "dataReader.hpp"

// embedded MCU board tools
#include "ilmsens/tools/embedded_MCU_tools.hpp"

// external sensor polling
#include "pollExtSensors.hpp" 

// Header for main program
#include "main.hpp" 

/* -------------------------------------------------------------------------------------------------
 * Using name spaces
 * -------------------------------------------------------------------------------------------------
 */

using namespace std;


/* -------------------------------------------------------------------------------------------------
 * Local definitions
 * -------------------------------------------------------------------------------------------------
 */


/* -------------------------------------------------------------------------------------------------
 * Local types and classes
 * -------------------------------------------------------------------------------------------------
 */


/* -------------------------------------------------------------------------------------------------
 * Local variables 
 * -------------------------------------------------------------------------------------------------
 */

// logging
static Poco::Message::Priority sLogLevel = LOG_LEVEL_DEFAULT;

// console redirects
static bool sDoRedir = false;

// measurement timing
static unsigned int sSWAvg = LE_SW_AVG_DEFAULT;
static unsigned int sWC    = LE_WC_DEFAULT;

// measurement configuration
static bool sMeasAutoStart  = LE_AUTO_START_MEAS;
static unsigned int sNumIRF = LE_NUM_IRF;

// streaming configuration
static bool sStreamIRFs    = LE_STREAM_IRFS;
static unsigned int sStreamWait = LE_STREAM_WAIT_INT;

// raw data storage
static bool sActStorage    = LE_ACT_STORAGE;

// external sensor config
static unsigned int sRefreshTime = LE_EXT_SEN_QUERY_INT;
static unsigned int sLaserCOM    = LE_DIST_LASER_COM;


// thread handles
static pthread_t sLogThread;          // Logging redirect TCP server
static pthread_t sDRThread;           // DataReader thread controlling the sensor
static pthread_t sCtrlThread;         // Controlling TCP server
static pthread_t sStrmThread;         // Streaming TP server
static pthread_t sExtSenThread;       // Polling thread for external sensors (e.g. temperature, or laser distance)
static pthread_t sGRPCThread;         // gRPC thread for handling gRPC services


/* -------------------------------------------------------------------------------------------------
 * Logging & logging helpers
 * 
 * Powered by Poco:-)
 * -------------------------------------------------------------------------------------------------
 */

// Configure Poco logging: create a formatted console output
void configureLogging(int pRootLevel)
{
  using Poco::AutoPtr;
  AutoPtr< Poco::ConsoleChannel >   tConsole  (new Poco::ConsoleChannel);
  AutoPtr< Poco::PatternFormatter > tFormatter(new Poco::PatternFormatter);
  
  tFormatter->setProperty("pattern", "%E.%i %N:%I %s:%q: %t");
  
  AutoPtr< Poco::FormattingChannel > tChannel(new Poco::FormattingChannel(tFormatter, tConsole));

  // set newly created formatted console channel as default channel at Poco root
  Poco::Logger::root().setChannel(tChannel);
  Poco::Logger::root().setLevel(pRootLevel);
}

// Logger for main app
Poco::Logger& log()
{
  static Poco::Logger& sLog = Poco::Logger::get("ilmsens.mliquid.lely_eis");
  return(sLog);
}

// Logstream for main app
Poco::LogStream& logStream()
{
  // REVISIT: needs protection?
  static Poco::LogStream sStream(log());
  return(sStream);
}


/* -------------------------------------------------------------------------------------------------
 * Command line helpers
 * 
 * -------------------------------------------------------------------------------------------------
 */

// apply defaults to all config variables that can be changed from command line
void resetDefaults(void)
{
  //logging
  sLogLevel = LOG_LEVEL_DEFAULT;

  // console redirects
  sDoRedir = false;

  // measurement timing
  sSWAvg = LE_SW_AVG_DEFAULT;
  sWC    = LE_WC_DEFAULT;

  // measurement configuration
  sMeasAutoStart = LE_AUTO_START_MEAS;
  sNumIRF        = LE_NUM_IRF;

  // raw data storage
  sActStorage    = LE_ACT_STORAGE;

  // streaming configuration
  sStreamIRFs    = LE_STREAM_IRFS;
  sStreamWait    = LE_STREAM_WAIT_INT;

  // external sensor config
  sRefreshTime = LE_EXT_SEN_QUERY_INT;
  sLaserCOM    = LE_DIST_LASER_COM;

  //reset thread handles to same value as calling thread (marking them as invalid)
  sLogThread    = pthread_self();
  sDRThread     = pthread_self();
  sCtrlThread   = pthread_self();
  sStrmThread   = pthread_self();
  sExtSenThread = pthread_self();
  sGRPCThread   = pthread_self();
}

// show help message
void printCommandLineHelp(void)
{
  //output help text and exit
  std::cout << ANSI_COLOR_GREEN << "m:liquid.lely_eis::HELP: The following command line options are supported:" << std::endl
            << "--logLevel <Level> : set verbosity of logging output (0 = off .. 8 = trace)"<< std::endl
            << std::endl
            << "--SWAvg    <Avg>   : set default software averages of measurement (1 .. 4095)"<< std::endl
            << "--WaitCyc  <WC>    : set default wait cycles of measurement (0 .. 1023)"<< std::endl
            << std::endl
            << "--noAutoMeas       : do not automatically start a measurement"<< std::endl
            << "--startMeas        : automatically start a measurement after sensor setup"<< std::endl
            << "--numIRF   <num>   : number of IRFs per measurement run (0 = run continuously)"<< std::endl
            << std::endl
            << "--noStreamIRF      : do not stream IRF data alongside info vector"<< std::endl
            << "--streamIRF        : stream IRF data alongside info vector"<< std::endl
            << "--streamWait <int> : min .wait interval [ms] between streaming IRFs via TCP server to client"<< std::endl
            << std::endl
            << "--storeRaw         : activate saving of raw measured data to mass storage"<< std::endl
            << std::endl
            << "--LaserCOM <COM#>  : COM port number for external laser distance metre"<< std::endl
            << "--extPollInt <Int> : interval [ms] between polling external  sensors (e.g. laser distance metre)"<< std::endl
            << std::endl
            << "--redirCon         : redirect/copy console output (cout & cerr) to TCP server at port 8200 for remote supervision"<< std::endl
            << "--help / -?        : show this help screen"<< std::endl
            << ANSI_COLOR_RESET << std::endl;
}

// parse command line to set various parameters
void parseCommandLine(int argc, char* argv[])
{
  // subsequently parse command line strings
  for (int tI = 1; tI < argc; ++tI) 
  {
    if (0 == std::string("--logLevel").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sLogLevel = (Poco::Message::Priority)std::stoi(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mliquid::lely_eis::Error("Missing log level argument.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--SWAvg").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sSWAvg = (unsigned int)std::stoi(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mliquid::lely_eis::Error("Missing software averages value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--WaitCyc").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sWC = (unsigned int)std::stoi(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mliquid::lely_eis::Error("Missing wait cycles value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--streamWait").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sStreamWait = (unsigned int)std::stoi(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mliquid::lely_eis::Error("Missing wait interval value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--numIRF").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sNumIRF = (unsigned int)std::stoi(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mliquid::lely_eis::Error("Missing wait interval value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--LaserCOM").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sLaserCOM = (unsigned int)std::stoi(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mliquid::lely_eis::Error("Missing COM port number.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--extPollInt").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sRefreshTime = (unsigned int)std::stoi(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mliquid::lely_eis::Error("Missing external sensor polling interval [ms] value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--noAutoMeas").compare(argv[tI])) 
    {
      sMeasAutoStart = false;
    } else if (0 == std::string("--startMeas").compare(argv[tI])) 
    {
      sMeasAutoStart = true;
    } else if (0 == std::string("--noStreamIRF").compare(argv[tI])) 
    {
      sStreamIRFs = false;
    } else if (0 == std::string("--streamIRF").compare(argv[tI])) 
    {
      sStreamIRFs = true;
    } else if (0 == std::string("--storeRaw").compare(argv[tI])) 
    {
      sActStorage = true;
    } else if (0 == std::string("--redirCon").compare(argv[tI])) 
    {
      sDoRedir = true;
    } else if ( (0 == std::string("--help").compare(argv[tI])) || (0 == std::string("-?").compare(argv[tI])) )
    {
      printCommandLineHelp();
      throw ilmsens::mliquid::lely_eis::Error("Command line help requested.", ILMSENS_SUCCESS);
    }
    else
    {
      //output warning
      std::cerr << ANSI_COLOR_YELLOW << "m:liquid.lely_eis::WARNING: unknown command line option '" << argv[tI] << "'. Use '--help' to get a list of valid options." << ANSI_COLOR_RESET << std::endl;
    }
  }
}


/* -------------------------------------------------------------------------------------------------
 * Signal handling
 * -------------------------------------------------------------------------------------------------
 */

// initiate a shutdown
void doShutDown(void)
{
  // make sure all other threads are signalled to end
  mMtxExit.lock();
    mExitShutdwn = true;
  mMtxExit.unlock();

  //wait some time for synchronisation
  ilmsens::mliquid::lely_eis::delay_ms(200);

  /* Wait until all created threads are ended */
  if (!pthread_equal(sCtrlThread, pthread_self()))   pthread_join(sCtrlThread, NULL);
  if (!pthread_equal(sStrmThread, pthread_self()))   pthread_join(sStrmThread, NULL);
  if (!pthread_equal(sDRThread, pthread_self()))     pthread_join(sDRThread, NULL);
  if (!pthread_equal(sExtSenThread, pthread_self())) pthread_join(sExtSenThread, NULL);

  /* shut down gRPC thread */
  if (!pthread_equal(sGRPCThread, pthread_self()))  pthread_join(sGRPCThread, NULL);

  /* Finally, wait until logging thread has ended */
  if (sDoRedir)
  {
    mMtxLogging.lock();
      mLogMShutdwn = true;
    mMtxLogging.unlock();
    if (!pthread_equal(sLogThread, pthread_self())) pthread_join(sLogThread, NULL);
  }

  ilmsens::mliquid::lely_eis::delay_ms(10);
}

// system signal handler
void handleTermSigs(int pSigNum) 
{
  /* output signal just received */
  std::cerr << "m:liquid.lely_eis::SIGNAL: app received signal " << std::dec << pSigNum << "." << std::endl;
  
  switch (pSigNum)
  {
    // caused by abort()
    case SIGABRT:
      std::cerr << "m:liquid.lely_eis::SIGNAL: signal was SIGABRT." << std::endl;
      exit(EXIT_FAILURE);

    // floating point error (e.g. divide by zero)
    case SIGFPE:
      std::cerr << "m:liquid.lely_eis::SIGNAL: signal was SIGFPE." << std::endl;
      break;

    // illegal instruction
    case SIGILL:
      std::cerr << "m:liquid.lely_eis::SIGNAL: signal was SIGILL." << std::endl;
      break;

    // interrupt, e.g. CTRL+C
    case SIGINT:
      std::cerr << "m:liquid.lely_eis::SIGNAL: signal was SIGINT." << std::endl;
      doShutDown();
      exit(EXIT_SUCCESS);

    // segmentation violation fault
    case SIGSEGV:
      std::cerr << "m:liquid.lely_eis::SIGNAL: signal was SIGSEGV." << std::endl;
      exit(EXIT_FAILURE);

    // termination request
    case SIGTERM:
      std::cerr << "m:liquid.lely_eis::SIGNAL: signal was SIGTERM." << std::endl;
      doShutDown();
      exit(EXIT_FAILURE);

    default:
      std::cerr << "m:liquid.lely_eis::SIGNAL: this unknown signal is unhandled!" << std::endl;
  }
}


/* -------------------------------------------------------------------------------------------------
 * Main program entry point
 * 
 * Checks command line options (if required), and dispatches the controlling and streaming thread.
 * 
 * When the EXIT command has been received from a client, the corresponding threads shut down
 * and main() cleans up any remaining ressources.
 * -------------------------------------------------------------------------------------------------
 */


int main(int argc, char* argv[])
{
  /* apply default configuration first */
  resetDefaults();


  /* setup signal handlers */

#ifdef _MSC_VER
  #define _CRT_SECURE_NO_WARNINGS 

  #pragma warning(push)
  #pragma warning(disable:4996)
#endif

  if (signal(SIGABRT, handleTermSigs) == SIG_ERR)
  {
    std::cerr << "m:liquid.lely_eis::SIGNAL: could not set sognal handler for SIGABRT. Error was: " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  } 
  else if (signal(SIGFPE, handleTermSigs) == SIG_ERR)
  {
    std::cerr << "m:liquid.lely_eis::SIGNAL: could not set sognal handler for SIGFPE. Error was: " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  } 
  else if (signal(SIGILL, handleTermSigs) == SIG_ERR)
  {
    std::cerr << "m:liquid.lely_eis::SIGNAL: could not set sognal handler for SIGILL. Error was: " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  } 
  else if (signal(SIGINT, handleTermSigs) == SIG_ERR)
  {
    std::cerr << "m:liquid.lely_eis::SIGNAL: could not set sognal handler for SIGINT. Error was: " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  } 
  else if (signal(SIGSEGV, handleTermSigs) == SIG_ERR)
  {
    std::cerr << "m:liquid.lely_eis::SIGNAL: could not set sognal handler for SIGSEGV. Error was: " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  } 
  else if (signal(SIGTERM, handleTermSigs) == SIG_ERR)
  {
    std::cerr << "m:liquid.lely_eis::SIGNAL: could not set sognal handler for SIGTERM. Error was: " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  }

#ifdef _MSC_VER
  #undef _CRT_SECURE_NO_WARNINGS 

  #pragma warning(pop)
#endif

  int tExitCode = EXIT_SUCCESS;

  /* Main part - create threads and wait for their termination */
  try 
  {
    /* print greeting */
    std::cout << std::endl << ANSI_COLOR_GREEN << "**** Welcome to m:liquid.lely_eis V" << std::dec 
              << ILMSENS_MLIQUID_LELY_EIS_VER_MAJOR << "."
              << ILMSENS_MLIQUID_LELY_EIS_VER_MINOR << "."
              << ILMSENS_MLIQUID_LELY_EIS_VER_BUILD
              << "! ****" << ANSI_COLOR_RESET << std::endl << std::endl;

    /* parse command line */
    parseCommandLine(argc, argv);

    /* setup Poco logging */
    configureLogging(sLogLevel);

    /* check, if cout and cerr should be redirected to TCP server */
    signed int tRes;
    if (sDoRedir)
    {
      /*** Create TCP server thread for log messages ***/
      tRes = pthread_create(&sLogThread, NULL, &ilmsens::mliquid::lely_eis::logging::logTCPServer, NULL);
      if(tRes != 0) 
      {
        std::cerr << ANSI_COLOR_RED << "m:liquid.lely_eis::ERROR: cannot create logging TCP server thread! Logging will be local only." << ANSI_COLOR_RESET << std::endl;
        sDoRedir = false;
        sLogThread = pthread_self();
      }
      else
      {
        std::cout << ANSI_COLOR_GREEN << "m:liquid.lely_eis::INFO: created thread for logging TCP server." << ANSI_COLOR_RESET << std::endl;
      }
    }

    // get & print current XU4/MCU temperatures
    const TTemperatures& tMCUTemps = readMCUTemperature();
    std::cout << ANSI_COLOR_CYAN << "Received " << std::dec << tMCUTemps.size() << " temperature values from XU4 board:" << std::endl;

    for (unsigned int tI = 0; tI < tMCUTemps.size(); tI++)
    {
      std::cout << "Temperature # " << std::dec << tI << ": " << tMCUTemps[tI] << "°C" << std::endl;
    }

    std::cout << ANSI_COLOR_RESET << std::endl;

    /*** Create always running DataReader thread ***/

    // configure measurement run
    mMtxSharedData.lock();
      mState.mSenAct   = false;
      mState.mRunFlag  = false;

      mState.mSW_AVG   = sSWAvg;
      mState.mWaitCyc  = sWC;

      mState.mSaveFlag = sActStorage;

      mState.mSendIRF  = sStreamIRFs;
      mState.mStrWait  = sStreamWait;
      mState.mNumIRF   = sNumIRF;

      mState.mLaserCOM    = sLaserCOM;
      mState.mExtPollInt  = sRefreshTime;
    mMtxSharedData.unlock();

    /* Thread for sensor's DataReader */
    tRes = pthread_create(&sDRThread, NULL, &ilmsens::mliquid::lely_eis::datareader::dataReader, 0);
    if(tRes != 0) 
    {
      //shut down the other valid threads
      sDRThread = pthread_self();
      doShutDown();

      throw ilmsens::mliquid::lely_eis::Error("m:liquid.lely_eis::ERROR: cannot create DataReader thread!", ILMSENS_ERROR_NO_MEMORY, tRes);
    }
    else
    {
      std::cout << ANSI_COLOR_GREEN << "m:liquid.lely_eis::INFO: created DataReader thread." << ANSI_COLOR_RESET << std::endl;
    }

    // wait, until dataReader has activated the sensor
    mMtxSharedData.lock();
      bool tSenAct = mState.mSenAct;
    mMtxSharedData.unlock();
    auto tStartWait = std::chrono::system_clock::now();
    while (!tSenAct)
    {
      // wait some time to lower CPU burden
      ilmsens::mliquid::lely_eis::delay_ms(100);
      
      //mind the timeout
      auto tElWait = std::chrono::duration_cast< std::chrono::milliseconds > (std::chrono::system_clock::now() - tStartWait);
      if (tElWait.count() > LE_SENSOR_SETUP_TIMEOUT)
      {
        // need to stop waiting, could not activate sensor in time
        std::cout << std::endl << ANSI_COLOR_GREEN << "m:liquid.lely_eis::ERROR: sensor setup could not be finished within timeout, exiting m:liquid.lely_eis." << ANSI_COLOR_RESET << std::endl;
        // app not successful
        tExitCode = EXIT_FAILURE;
        break;
      }

      //update sensor status
      mMtxSharedData.lock();
        tSenAct = mState.mSenAct;
      mMtxSharedData.unlock();
    }
    
    if (tSenAct)
    {
      /*** Create always running TCP server threads ***/

      /* Thread for controlling TCP server */
      tRes = pthread_create(&sCtrlThread, NULL, &ilmsens::mliquid::lely_eis::controlling::ctrlTCPServer, NULL);
      if(tRes != 0) 
      {
        //shut down the other valid threads
        sCtrlThread = pthread_self();
        doShutDown();

        throw ilmsens::mliquid::lely_eis::Error("m:liquid.lely_eis::ERROR: cannot create controlling TCP server thread!", ILMSENS_ERROR_NO_MEMORY, tRes);
      }
      else
      {
        std::cout << ANSI_COLOR_GREEN << "m:liquid.lely_eis::INFO: created thread for controlling TCP server." << ANSI_COLOR_RESET << std::endl;
      }
      
#if 0
      ///* Thread for streaming TCP server */
      tRes = pthread_create(&sStrmThread, NULL, &ilmsens::mliquid::lely_eis::streaming::strmTCPServer, NULL);
      if(tRes != 0) 
      {
        //shut down the other valid threads
        sStrmThread = pthread_self();
        doShutDown();

        throw ilmsens::mliquid::lely_eis::Error("m:liquid.lely_eis::ERROR: cannot create streaming TCP server thread!", ILMSENS_ERROR_NO_MEMORY, tRes);
      }
      else
      {
        std::cout << ANSI_COLOR_GREEN << "m:liquid.lely_eis::INFO: created thread for streaming TCP server." << ANSI_COLOR_RESET << std::endl;
      }
#endif
      
#if 0
      ///* Thread for polling external sensors */
      tRes = pthread_create(&sExtSenThread, NULL, &ilmsens::mliquid::lely_eis::sensors::pollExtSensors, NULL);
      if(tRes != 0) 
      {
        //shut down the other valid threads
        sExtSenThread = pthread_self();
        doShutDown();

        throw ilmsens::mliquid::lely_eis::Error("m:liquid.lely_eis::ERROR: cannot create external sensor polling thread!", ILMSENS_ERROR_NO_MEMORY, tRes);
      }
      else
      {
        std::cout << ANSI_COLOR_GREEN << "m:liquid.lely_eis::INFO: created thread for polling external sensors." << ANSI_COLOR_RESET << std::endl;
      }
#endif
      
      ///* Thread for gRPC services */
      tRes = pthread_create(&sGRPCThread, NULL, &ilmsens::mliquid::lely_eis::grpc::gRPCServices, NULL);
      if(tRes != 0) 
      {
        //shut down the other valid threads
        sGRPCThread = pthread_self();
        doShutDown();

        throw ilmsens::mliquid::lely_eis::Error("m:liquid.lely_eis::ERROR: cannot create gRPC service thread!", ILMSENS_ERROR_NO_MEMORY, tRes);
      }
      else
      {
        std::cout << ANSI_COLOR_GREEN << "m:liquid.lely_eis::INFO: created thread for gRPC services." << ANSI_COLOR_RESET << std::endl;
      }

      /* start a continuous measurement run (?) */
      mMtxSharedData.lock();
        mState.mRunFlag = sMeasAutoStart;
      mMtxSharedData.unlock();


      /* Wait until all threads are ended (upon EXIT command from client) */
      if (!pthread_equal(sCtrlThread, pthread_self()))   pthread_join(sCtrlThread, NULL);
      if (!pthread_equal(sStrmThread, pthread_self()))   pthread_join(sStrmThread, NULL);
      if (!pthread_equal(sDRThread, pthread_self()))     pthread_join(sDRThread, NULL);
      if (!pthread_equal(sExtSenThread, pthread_self())) pthread_join(sExtSenThread, NULL);

      /* shut down gRPC thread */
      if (!pthread_equal(sGRPCThread, pthread_self()))   pthread_join(sGRPCThread, NULL);
    }
    else
    {
      //shut down the other valid threads
      doShutDown();
    }

    std::cout << std::endl << ANSI_COLOR_GREEN << "m:liquid.lely_eis::INFO: All Threads closed, exiting m:liquid.lely_eis." << ANSI_COLOR_RESET << std::endl;
  }
  catch(ilmsens::mliquid::lely_eis::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "m:liquid.lely_eis::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;

    // not successful (?)
    if (tErr.getErrorCode() != ILMSENS_SUCCESS) tExitCode = EXIT_FAILURE;
  }
  catch(std::exception& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "m:liquid.lely_eis::Std-EXCEPTION: " << ANSI_COLOR_RESET 
              << tErr.what() << std::endl;

    // not successful
    tExitCode = EXIT_FAILURE;
  }
  catch(...) 
  {
    std::cerr << ANSI_COLOR_RED "m:liquid.lely_eis::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;

    // not successful
    tExitCode = EXIT_FAILURE;
  }


  /* all was fine, we can go now */
  std::cout << std::endl << ANSI_COLOR_GREEN << "*** Good Bye! ***" << ANSI_COLOR_RESET << std::endl;
  return (tExitCode);
}
