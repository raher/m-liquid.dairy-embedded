/* logTCPServer.cpp
 * 
 * Thread function for TCP server which will intercept & copy console outputs and send data to a
 * connected client. When no connection is made, copied data is discarded.
 */


/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */


// Std. C++ library types / STL 

// File, IO, and Strings
#include <iostream>
#include <iomanip>
#include <cstring>

/*
// Poco logging includes
#include "Poco/Logger.h"
#include "Poco/LogStream.h"
#include "Poco/Message.h"
*/

// Timing and threading includes
#include <pthread.h>                    // POSIX threads
#include <chrono>

// Shared variables, memories, and synchronisation ressources
#include "shared.hpp"

// TCP stuff & server class
#ifndef WIN32
  #include <arpa/inet.h> // for htonl() call
#endif
#include "ilmsens/networking/TCP_server_single.hpp"
#include "ilmsens/networking/ilmsens_networking_defn.h"
#include "ilmsens/networking/ilmsens_networking_error.hpp"

// OS intercept classes
#include "ilmsens/tools/os_intercept.hpp"
#include "ilmsens/tools/ilmsens_tools_error.hpp"

// Other Ilmsens stuff
#include "ilmsens/ilmsens_error.h"      // Ilmsens error codes

// main header for module
#include "logTCPServer.hpp" 


/* -------------------------------------------------------------------------------------------------
 * Using clauses
 * -------------------------------------------------------------------------------------------------
 */

using namespace ilmsens::networking;


/* -------------------------------------------------------------------------------------------------
 * Local defines
 * -------------------------------------------------------------------------------------------------
 */


/* -------------------------------------------------------------------------------------------------
 * Local helper functions
 * -------------------------------------------------------------------------------------------------
 */


namespace ilmsens 
{
namespace mliquid
{
namespace lely_eis 
{
namespace logging
{


/* -------------------------------------------------------------------------------------------------
 * Logging & logging helpers
 * 
 * Powered by Poco:-)
 * -------------------------------------------------------------------------------------------------
 */

/*
// Logger for main app
Poco::Logger& log()
{
  static Poco::Logger& sLog = Poco::Logger::get("ilmsens.mliquid.lely_eis.log");
  return(sLog);
}

// Logstream for main app
Poco::LogStream& logStream()
{
  // REVISIT: needs protection?
  static Poco::LogStream sStream(log());
  return(sStream);
}
*/


/* -------------------------------------------------------------------------------------------------
 * Logging thread
 * 
 * Creates TCP server socket for sending console (log) messages to client.
 * 
 * Accesses a cache string shared with console stream buffers of cout/cerr. 
 * 
 * If no client is connected, the data is just discarded. If a client is connected, data
 * is forwarded to the client from time to time.
 * 
 * -------------------------------------------------------------------------------------------------
 */

void* logTCPServer(void*)
{
  bool tLogExit = false;

  unsigned int tLogPort = TCP_LOG_PORT;
  TCP_server_single tServer;

  try
  {
    std::cout << ANSI_COLOR_BLUE << "Logging: Thread created." << ANSI_COLOR_RESET << std::endl;
    
    /* Bind Thread to single CPU Core */
#ifndef WIN32
    cpu_set_t tCPUSet;
    CPU_ZERO(&tCPUSet);

    CPU_SET(3, &tCPUSet); // CPU #3
    if(pthread_setaffinity_np(pthread_self(), sizeof(tCPUSet), &tCPUSet) != 0) 
    {
      std::cerr << ANSI_COLOR_RED "Logging::ERROR: pthread_setaffinity_np did not succeed: " << ANSI_COLOR_RESET 
                << strerror(errno) << std::endl;
    }
#endif
    
    /* start server (listening socket) */
    tServer.startServer(tLogPort, TCP_LOG_SEND_TO_MS);
  }
  catch(ilmsens::mliquid::lely_eis::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "Logging::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what()  << std::endl 
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;

    // not successful
    tLogExit = true;
  }
  catch(ilmsens::networking::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "Logging::Net-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what()  << std::endl 
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;

    // not successful
    tLogExit = true;
  }
  catch(std::exception& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "Logging::Std-EXCEPTION: " << ANSI_COLOR_RESET 
              << tErr.what() << std::endl;

    // not successful
    tLogExit = true;
  }
  catch(...) 
  {
    std::cerr << ANSI_COLOR_RED "Logging::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;

    // not successful
    tLogExit = true;
  }

  
  /* Intercept console streams to copy their messages into a string */
  
  // create buffer strings and mutexes for intercepting cout and cerr
  std::mutex  tMtxCOUT;
  std::mutex  tMtxCERR;
  std::string tCOUTBuf = "";
  std::string tCERRBuf = "";
  
  // intercepting ostreams
  ilmsens::tools::OSInterceptString tCOUTIntercept(std::cout, tCOUTBuf, tMtxCOUT);
  ilmsens::tools::OSInterceptString tCERRIntercept(std::cerr, tCERRBuf, tMtxCERR);

  // logging thread is always running (always waiting for client)
  // until the EXIT command has been received
  while(!tLogExit) 
  {
    try 
    {
      std::cout << ANSI_COLOR_BLUE << "Logging: Waiting for Client at port " << std::dec << tLogPort << " ..." << ANSI_COLOR_RESET << std::endl;

      // accept client connection with timeout
      while (!tLogExit)
      {
        bool tClient = tServer.acceptClient(TCP_LOG_WAIT_TO_MS);
        
        if (tClient)
        {
          // a new client was accepted
          std::cout << ANSI_COLOR_BLUE << "Logging::INFO: connection initiated with client " << tServer.getClientIP() << " ..." << ANSI_COLOR_RESET << std::endl;
          break;  
        }
        else
        {
          // no client was connected, so empty the log strings
          tMtxCOUT.lock();
            if (tCOUTBuf.length()) tCOUTBuf.clear();
          tMtxCOUT.unlock();

          tMtxCERR.lock();
            if (tCERRBuf.length()) tCERRBuf.clear();
          tMtxCERR.unlock();

          // update shutdown flag
          mMtxLogging.lock();
            tLogExit = mLogMShutdwn;
          mMtxLogging.unlock();
        }
      } // waiting for client
      
      // a client is here?
      if (!tLogExit)
      {
        /* TRANSFER messages  to client*/
        bool tExitClient = false;
        
        auto tLast = std::chrono::system_clock::now();
        bool tDoSend = false;
        while(!tExitClient && !tLogExit)
        {
          if (tDoSend)
          {
            tLast   = std::chrono::system_clock::now();
            tDoSend = false;
          }
          else
          {
            auto tDiff = std::chrono::duration_cast< std::chrono::milliseconds >(std::chrono::system_clock::now() - tLast);
            if (tDiff.count() >= TCP_LOG_TO_MS) tDoSend = true;
          }

          // check, if enough data is there for a transport or the timeout elapsed
          if ((tCOUTBuf.length() >= TCP_LOG_SEND_THRES) ||
              (tCOUTBuf.length() && tDoSend)
             )
          {
            // extract characters until last newline (maybe whole string)
            tMtxCOUT.lock();

              std::string tCpy = tCOUTBuf;
              size_t tNLPos = tCpy.find_last_of('\n');
              if (tNLPos == std::string::npos)
              {
                tCOUTBuf.clear();
              }
              else
              {
                tCOUTBuf.erase(0, tNLPos+1);
              }

            tMtxCOUT.unlock();

            // send new charaters
            if ((tNLPos != std::string::npos) && ((tNLPos+1) < tCpy.length()))
            {
              tCpy.erase(tNLPos+1); // clear stuff that was not erased from buffer
            }
            
            unsigned int tTxSize = (unsigned int)tCpy.length();
            /*
            tCOUTIntercept.getOrgStream() << "Logging::INFO: sending COUT data to client: " 
                                          << std::dec << tTxSize 
                                          << " bytes. First char is: " << (unsigned int) tCpy.c_str()[0] 
                                          << ". 2nd char is: " << (unsigned int) tCpy.c_str()[1] 
                                          << std::endl;
            */
            unsigned int tTxLen  = 0;
            const char *tBuf = tCpy.c_str();
            while ( tTxLen < tTxSize )
            {
              int tLen = tServer.sendTO((void *)(tBuf + tTxLen), tTxSize - tTxLen);
              if(tLen < 0) 
              {
                // client connection has been lost
                tExitClient = true;
                break;
              }
              else
              {
                // count the bytes sent
                tTxLen += tLen;
              }
            }
          }
          if (tExitClient) break;

          // check, if enough data is there for a transport or the timeout elapsed
          if ((tCERRBuf.length() >= TCP_LOG_SEND_THRES) ||
              (tCERRBuf.length() && tDoSend)
             )
          {
            // extract characters until last newline (maybe whole string)
            tMtxCERR.lock();

              std::string tCpy = tCERRBuf;
              size_t tNLPos = tCpy.find_last_of('\n');
              if (tNLPos == std::string::npos)
              {
                tCERRBuf.clear();
              }
              else
              {
                tCERRBuf.erase(0, tNLPos+1);
              }

            tMtxCERR.unlock();

            // send new charaters
            if ((tNLPos != std::string::npos) && ((tNLPos+1) < tCpy.length()))
            {
              tCpy.erase(tNLPos+1); // clear stuff that was not erased from buffer
            }
            
            unsigned int tTxSize = (unsigned int)tCpy.length();
            unsigned int tTxLen  = 0;
            const char *tBuf = tCpy.c_str();
            while ( tTxLen < tTxSize )
            {
              int tLen = tServer.sendTO((void *)(tBuf + tTxLen), tTxSize - tTxLen);
              if(tLen < 0) 
              {
                // client connection has been lost
                tExitClient = true;
                break;
              }
              else
              {
                // count the bytes sent
                tTxLen += tLen;
              }
            }
          }
          if (tExitClient) break;
              
          // test, if connection is still alive...do non-blocking read with short timeout
          char tBuf[TCP_LOG_MSG_SIZE];
          int tLen = tServer.receiveTO((void *)tBuf, TCP_LOG_MSG_SIZE, 1);
          if (tLen < 0)
          {
            // client connection has been closed,
            // need to stop streaming TCP connection
            tExitClient = true;
          }
          else
          {
            // wait some time to lower CPU burden
            delay_ms(10);
          }

          // update exit and shutdown flags 
          mMtxLogging.lock();
            tLogExit = mLogMShutdwn;
          mMtxLogging.unlock();
        }
      }
        
      if (tLogExit)
      {
        std::cout << ANSI_COLOR_BLUE << "Logging: received shutdown flag, closing thread ..." << ANSI_COLOR_RESET << std::endl;
      }
      else
      {
        std::cout << ANSI_COLOR_BLUE << "Logging: no or bad client connection on logging socket ..." << ANSI_COLOR_RESET << std::endl;
      }
    } 
    catch(ilmsens::mliquid::lely_eis::Error& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "Logging::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
                << " Description     : " << tErr.what()  << std::endl 
                << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
                << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
    }
    catch(ilmsens::tools::Error& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "Logging::Tls-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
                << " Description     : " << tErr.what()  << std::endl 
                << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
                << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
    }
    catch(ilmsens::networking::Error& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "Logging::Net-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
                << " Description     : " << tErr.what()  << std::endl 
                << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
                << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
    }
    catch(std::exception& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "Logging::Std-EXCEPTION: " << ANSI_COLOR_RESET 
                << tErr.what() << std::endl;
    }
    catch(...) 
    {
      std::cerr << ANSI_COLOR_RED << "Logging::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;
    }
    
    // close client's TCP connection
    tServer.closeClient();
  }

  // can go now, TCP server will be shutdown by destructor
  std::cout << ANSI_COLOR_BLUE << "Logging: shutdown complete, exiting thread." << ANSI_COLOR_RESET << std::endl;
  return(NULL);
}

} //namespace logging
} //namespace lely_eis
} //namespace mliquid
} //namespace ilmsens
