/// @file os_intercept.hpp
/// @brief Tools for intercepting output streams.
///
/// Output streams (e.g. cout/cerr) will be intercepted and their.
/// content copied to provided output objects.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef OS_INTERCEPT_HPP
#define OS_INTERCEPT_HPP

#include <ostream>
#include <streambuf>
#include <string>
#include <mutex>

namespace ilmsens 
{
  namespace tools
  {
    /** Scoped guard object using a mutex to synchronise access to other objects. */
    typedef std::lock_guard<std::mutex> TGuard;

    /** Class for intercepting output streams into a string protected by a mutex. */
    class OSInterceptString : public std::streambuf
    {
      public:

        OSInterceptString(std::ostream & pSrc, std::string & pBufStr, std::mutex & pLock);
        ~OSInterceptString();
        
        std::ostream& getOrgStream(void) { return(mNew); };

      protected:

        virtual std::streamsize xsputn(const char *pMsg, std::streamsize pCnt);
        virtual int_type overflow(int_type pChr);
        virtual int sync(void);

      private:

        // make it non-copyable
        OSInterceptString(const OSInterceptString&);
        OSInterceptString& operator=(const OSInterceptString&);

        // externally supplied protection of stream access
        std::mutex&     mLock;

        // ostream objects
        std::ostream&   mSrc;
        std::ostream    mNew;

        // stream buffers
        std::streambuf* mOrg;
        
        // externally supplied string buffers
        std::string&    mBufStr;
    
        // thread counters & profiling
        //int mCnt;
        //int mProf;
    };

    /** Class for swapping buffers of provided output streams. */
    class OSInterceptRedir
    {
      public:

        OSInterceptRedir(std::ostream & pOS, std::ostream & pRedir);

        ~OSInterceptRedir();

        void doRedir(void);
        void undoRedir(void);

      private:

        // make it non-copyable
        OSInterceptRedir(const OSInterceptRedir&);
        OSInterceptRedir& operator=(const OSInterceptRedir&);

        // streams have been set?
        bool mRedir;

        // references to the streams
        std::ostream & mOS;
        std::ostream & mRS;
    };

  } //namespace tools
} //namespace ilmsens

#endif // OS_INTERCEPT_HPP
