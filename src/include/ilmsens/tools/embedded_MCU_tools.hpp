/// @file embedded_MCU_tools.hpp
/// @brief Tools, types, and helpers for MCUs on embedded board.
///
/// Board-specific functions such as reading CPU temperatures, etc.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef EMBEDDED_MCU_TOOLS_HPP
#define EMBEDDED_MCU_TOOLS_HPP

#include <vector>


/* -------------------------------------------------------------------------------------------------
 * Common definitions for all boards
 * -------------------------------------------------------------------------------------------------
 */

#define TEMP_UPDATE_INT_MS 1000           ///< Min. interval between temperature updates [ms]


/* -------------------------------------------------------------------------------------------------
 * XU4 definitions
 * -------------------------------------------------------------------------------------------------
 */

#define XU4_TEMP_MAX_LEN   79             ///< Maximum buffer length for reading XU4 board temperatures
#define XU4_TEMP_MIN_LEN   76             ///< Minimum length for reading XU4 board temperatures


/* -------------------------------------------------------------------------------------------------
 * MCU related types
 * -------------------------------------------------------------------------------------------------
 */

typedef std::vector<int> TTemperatures;   ///< Type holding temperature values read from XU4 board


/* -------------------------------------------------------------------------------------------------
 * XU4 functions
 * -------------------------------------------------------------------------------------------------
 */


/** Reading temperature(s) of embedded board from sys-file */
const TTemperatures& readMCUTemperature(void);

#endif //EMBEDDED_MCU_TOOLS_HPP
