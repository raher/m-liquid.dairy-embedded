/// @file TCP_server_single.hpp
/// @brief TCP server classes.
///
/// A TCP server is provided, that accepts a single client at a time.
/// All I/O is done synchronously, however with defined timeout.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef TCP_SERVER_SINGLE_HPP
#define TCP_SERVER_SINGLE_HPP

#if defined(WIN32)
 #define WIN32_LEAN_AND_MEAN
 //#include <windows.h>
 #include <winsock2.h>
 #include <ws2tcpip.h>
#endif

#define IMSENS_NETWORKING_IP_STR_BUF_LEN 128    // buffer length for IP address strings (incl. terminating \0)

namespace ilmsens 
{
  namespace networking
  {

    class TCP_server_single
    {
      public:

        TCP_server_single(unsigned int pTCPPort = 0, unsigned int pSendTO_ms = 0);
        ~TCP_server_single();

        /* make connections */
        void startServer (unsigned int pTCPPort = 80, 
                          unsigned int pSendTO_ms = 0, 
                          unsigned int pBindTO_ms = 0);       // register listening socket for server
        bool acceptClient(unsigned int pTO_ms = 1000);        // wait for and accept a client connection with timeout

        const char* getClientIP(void);                        // get IP string of connected client
        
        /* break connections */
        void closeClient(void);                 // close connection to current client
        void closeServer(void);                 // close connection to client and also listenung socket of server
        
        /* receiving data from client */
        int receiveWait (void *pBuffer, unsigned int pNumBytes);                     
        int receiveTO   (void *pBuffer, unsigned int pNumBytes, unsigned int pTO_ms = 0);

        /* sending data to client */
        int sendTO      (void *pBuffer, unsigned int pNumBytes);

      private:

        // TCP port
        unsigned int mTCPPort;
        
        // TCp send timeout [ms]
        unsigned int mSendTO;
        
        // server & client socket handles
#if defined(WIN32)
        SOCKET       mServerSock;               // server's listening socket
        SOCKET       mClientSock;               // socket when client is fully connected

        // Winsock 2 context
        bool    mWSAInit;                       //Winsock2 has been initialised?
        WSADATA mWSAData;                       //Winsock2 stack/context
#else
        int          mServerSock;               // server's listening socket
        int          mClientSock;               // socket when client is fully connected
#endif

        // client info & state
        bool         mClient;                   // true, if a client connection is active
        char         mClientIP[IMSENS_NETWORKING_IP_STR_BUF_LEN]; // IP address of connected client
    }; // TCP_server_single

  } //namespace networking
} //namespace ilmsens

#endif // TCP_SERVER_SINGLE_HPP
