
/// @file ilmsens_mliquid_error.hpp
/// @brief Exception class used in m:liquid applications.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#include <string>
#include <stdexcept>
#include "ilmsens/ilmsens_error.h"

#ifndef ILMSENS_MLIQUID_ERROR_HPP
#define ILMSENS_MLIQUID_ERROR_HPP

/**   
 * @defgroup mliquid_exception Exceptions & safety tools used or m:liquid apps.
 * @brief Exceptions only used by m:liquid apps.
 * @{
 */  

namespace ilmsens 
{
  namespace mliquid
  {
    /**  Application-specific error class storing an Ilmsens error code along with a message */
    class Error : public std::runtime_error 
    {
      typedef std::runtime_error Base;

      public:
        Error(const std::string& pMsg, ilmsens_error pCode = ILMSENS_ERROR_UNKNOWN)
          : Base(pMsg), mErrorCode(pCode) {}
        
        ilmsens_error getErrorCode() const { return mErrorCode; }

      private:
        ilmsens_error mErrorCode;
    };
  } //namespace mliquid
} //namespace ilmsens

/** @} mliquid_exception */

#endif // ILMSENS_MLIQUID_ERROR_HPP
