/// @file KKF.hpp
/// @brief Cross correlation with ideal M-sequence of given real IRF.
///
/// Cross correleation is performed by using FFT/IFFT method.
/// Ideal M-sequence is the reference function for correlation.
/// It is loaded from /usr/include/ilmsens/processing/mlbsXX.txt
/// where XX is the MLBS order. This function operates on real-valued
/// time domain data
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef KKF_HPP
#define KKF_HPP

#include <memory>

#include <fftw3.h>    // FFTW

#include "FFTw_tools.hpp"
#include "IRF_FRF.hpp"

using namespace ilmsens::processing::types;

namespace ilmsens 
{
  namespace processing
  {

    /** Class for correlating raw M-sequence - data with ideal MLBS using cross-correlation */
    class KKF
    {
      public:

        KKF(unsigned int pMLBSLen = 511);
        ~KKF();

        const TRealIRF&     calculateKKF(const TRealIRF& pData);  ///< calculate Cross Correlation Function of given data
        const TRealIRF&     getLastIRF  (void);                   ///< return IRF of last correlation operation
        const fftw_complex* getLastFRF  (void);                   ///< return FRF of last correlation operation

      protected:

        // MLBS properties
        unsigned int mMLBSLen;                                    ///< Length of MLBS (samples)

      private:

        // Reference MLBS as read from file
        TRealIRF mRefMLBS;                                        ///< Ideal MLBS in time domain used as reference fopr correlation

        // Result of correlation
        TRealIRF mCorrIRF;                                        ///< Result of KKF (time-domain)

        // Data variables for FFTW
        std::unique_ptr< fftw_complex[], ilmsens::processing::fftw_tools::sFFTwDelete > mFDRef;   ///< Data vector for reference spectrum from ideal MLBS

        std::unique_ptr< double[], ilmsens::processing::fftw_tools::sFFTwDelete >       mTDRaw;   ///< Data vectors time domain for KKF
        std::unique_ptr< double[], ilmsens::processing::fftw_tools::sFFTwDelete >       mTDCorr;  ///< Data vectors time domain for KKF

        std::unique_ptr< fftw_complex[], ilmsens::processing::fftw_tools::sFFTwDelete > mFDRaw;   ///< Data vectors frequency domain for KKF
        std::unique_ptr< fftw_complex[], ilmsens::processing::fftw_tools::sFFTwDelete > mFDCorr;  ///< Data vectors frequency domain for KKF

        // FFTW-Plans for KKF
        fftw_plan mFFTPlan;                                       ///< FFT of measured data
        fftw_plan mIFFTPlan;                                      ///< IFFT of correleated data
    };

  } //namespace processing
} //namespace ilmsens

#endif // KKF_HPP
