/// @file HannWin.hpp
/// @brief Creates Hanning-Window as vector
///
/// @author Saskia Holzlehner <saskia.holzlehner@yahoo.de>

#ifndef HANNWIN_HPP
#define HANNWIN_HPP

#include "IRF_FRF.hpp"

using namespace ilmsens::processing::types;

namespace ilmsens 
{
  namespace processing
  {

    /** Vector for the Tukey-Window */
    class HannWin
    {
      public:
        HannWin(unsigned int pWinLen);
        ~HannWin();

        const TSampleVec& getHannVec(void);         ///< return hann window as vector

      private:
        
        // window properties
        unsigned int mWinLen;	

        // vector which represents the tukey window
        TSampleVec mHannVec;

        // first half of den hann window
        TSampleVec calcHalfHann(unsigned int pHalfWinLen, unsigned int pWinLen);
    };

  } //namespace processing
} //namespace ilmsens

#endif // HANNWIN_HPP
