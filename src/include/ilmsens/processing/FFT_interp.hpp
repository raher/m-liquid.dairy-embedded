/// @file FFT_interp.hpp
/// @brief FFT of given IRF with frequency interpolation by zero padding in time domain.
///
/// Before FFT, time domain data is padded with zeros. This results in interpolation
/// of the spectrum. However, the user must take care for proper windowing
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef FFT_INTERP_HPP
#define FFT_INTERP_HPP

#include <memory>

#include <fftw3.h>    // FFTW

#include "FFTw_tools.hpp"
#include "IRF_FRF.hpp"

using namespace ilmsens::processing::types;

namespace ilmsens 
{
  namespace processing
  {

    /** Class for correlating raw M-sequence - data with ideal MLBS using cross-correlation and FFT interpolation */
    class FFT_interp
    {
      public:

        FFT_interp(unsigned int pIRFLen, unsigned int pIPLen);
        ~FFT_interp();

        const fftw_complex* getInterpolatedFRF(TRealIRF& pData);  ///< caculate and return interpolated FRF

      private:

        // interpolation properties
        unsigned int mIRFLen;                                     ///< Length of IRF (samples)
        unsigned int mIPLen;                                      ///< Length of interpolated data (samples)

        // Data variables for FFTW
        std::unique_ptr< double[], ilmsens::processing::fftw_tools::sFFTwDelete >       mTDIp;    ///< Data vector interpolated time domain
        std::unique_ptr< fftw_complex[], ilmsens::processing::fftw_tools::sFFTwDelete > mFDIp;    ///< Data vector interpolated frequency domain for KKF
        
        // FFTW-Plans
        fftw_plan mFFTPlan;                                       ///< FFT of padded data
    };

  } //namespace processing
} //namespace ilmsens

#endif // FFT_INTERP_HPP
