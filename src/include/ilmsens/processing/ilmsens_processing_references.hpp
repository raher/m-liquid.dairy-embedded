/// @file ilmsens_processing_references.hpp
/// @brief Tools related to reference functions for MLBS sensors.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>


#ifndef ILMSENS_PROCESSING_REFERENCES_HPP
#define ILMSENS_PROCESSING_REFERENCES_HPP

// Ext. libraries
#include "fftw3.h"

// Ilmsens
#include "IRF_FRF.hpp"


/**   
 * @defgroup processing_exception Exceptions & safety tools used for Ilmsens processing.
 * @brief Exceptions only used by Ilmsens processing classes.
 * @{
 */  

namespace ilmsens 
{
  namespace processing
  {
    namespace references
    {

      /**  load ideal MLBS from text file and provide the (possibly oversampled) time domain reference for correlation */
      ilmsens::processing::types::TRealIRF& getMLBSReferenceTD(unsigned int pOrder = 9, unsigned int pOV = 1, unsigned int pLOIdx = 0);

      /**  load pre-computed extended ideal MLBS file and provide the time doamain reference for correlation */
      ilmsens::processing::types::TRealIRF& getPCMLBSReferenceTD(unsigned int pOrder = 9, unsigned int pOV = 1, unsigned int pLOIdx = 0);

      /**  load pre-computed frequency domain filtering window that can be used during correlation */
      ilmsens::processing::types::TRealIRF& getFilterWindowFD(std::string pTypeID, unsigned int pOrder = 9, unsigned int pOV = 1, unsigned int pLOIdx = 0);

      /**  normalize given reference spectrum to obtain reference only using the phases */
      void getNormalisedSpectrumKeepPhases(fftw_complex *pSpec, size_t pLen);

    } //    namespace references
  } //namespace processing
} //namespace ilmsens

/** @} processing_exception */

#endif // ILMSENS_PROCESSING_REFERENCES_HPP
