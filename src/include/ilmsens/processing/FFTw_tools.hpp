/// @file FFTw_tools.hpp
/// @brief Helpers for using C++ wrappers/smart pointers around FFTw3 library.
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>


#ifndef FFTW_TOOLS_HPP
#define FFTW_TOOLS_HPP

#include <fftw3.h>    // FFTW

/**   
 * @defgroup fftw_cleanup_helpers Deleters for FFTw-allocated memories.
 * @brief The deleters can be used for freeing FFTw-memory in C++-destructors (e.g. smart pointers).
 * @{
 */  
 
namespace ilmsens 
{
  namespace processing
  {
    namespace fftw_tools
    {

      /** FFTw memory delete helper struct for use with smart pointers such as std::unique_ptr. */
      struct sFFTwDelete
      {
        void operator()(void* pDel) { fftw_free(pDel); }
      };    

    } //namespace fftw_tools
  } //namespace processing
} //namespace ilmsens

/** @} fftw_cleanup_helpers */

#endif //FFTW_TOOLS_HPP
