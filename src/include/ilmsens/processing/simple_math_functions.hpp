/// @file simple_math_functions.hpp
/// @brief Simple math funtions/tools that work on scalars.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef SIMPLE_MATH_FUNCTIONS_HPP
#define SIMPLE_MATH_FUNCTIONS_HPP

#include "IRF_FRF.hpp"

using namespace ilmsens::processing::types;

namespace ilmsens 
{
  namespace processing
  {

    /**************************/
    /** Rounding             **/
    /**************************/
    uint32_t getNextPow2_32(uint32_t pVal);

  } //namespace processing
} //namespace ilmsens

#endif // SIMPLE_MATH_FUNCTIONS
