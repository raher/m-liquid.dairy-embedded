/// @file ilmsens_mexplore_error.hpp
/// @brief Exception class used in m:explore applications.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#include <string>
#include <stdexcept>
#include "ilmsens/ilmsens_error.h"

#ifndef ILMSENS_MEXPLORE_ERROR_HPP
#define ILMSENS_MEXPLORE_ERROR_HPP

/**   
 * @defgroup mexplore_exception Exceptions & safety tools used or m:explore apps.
 * @brief Exceptions only used by m:explore apps.
 * @{
 */  

namespace ilmsens 
{
  namespace mexplore
  {
    /**  Application-specific error class storing an Ilmsens error code along with a message */
    class Error : public std::runtime_error 
    {
      typedef std::runtime_error Base;

      public:
        Error(const std::string& pMsg, ilmsens_error pCode = ILMSENS_ERROR_UNKNOWN)
          : Base(pMsg), mErrorCode(pCode) {}
        
        ilmsens_error getErrorCode() const { return mErrorCode; }

      private:
        ilmsens_error mErrorCode;
    };
  } //namespace mexplore
} //namespace ilmsens

/** @} mliquid_exception */

#endif // ILMSENS_MEXPLORE_ERROR_HPP
