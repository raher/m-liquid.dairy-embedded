/* simple_math_functions.CPP
 * 
 * Simple math funtions/tools that work on scalars.
 */

//#include <iostream>
//#include <iomanip>
//#include <string>
//#include <cmath>

#define __func__ __FUNCTION__                //fix for VS below 2015

#include "ilmsens/processing/ilmsens_processing_error.hpp"
#include "ilmsens/processing/simple_math_functions.hpp"

/************************/
/** convenience macros **/


namespace ilmsens 
{
namespace processing
{

/**************************/
/** Rounding             **/
/**************************/

uint32_t getNextPow2_32(uint32_t pVal)
{
  pVal--;
  pVal |= pVal >> 1;
  pVal |= pVal >> 2;
  pVal |= pVal >> 4;
  pVal |= pVal >> 8;
  pVal |= pVal >> 16;
  pVal++;
  pVal += (pVal == 0); //corner case when pVal == 0
  return(pVal);
}

} //namespace processing
} //namespace ilmsens
