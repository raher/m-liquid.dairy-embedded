/* KKF_cx.CPP
 * 
 * Calculating cross correlation between stimulus and complexed received signal. For better performance, 
 * we first convert the signals into frequency domain (FFT), calculate the cross correlation spectrum 
 * and after that we do a backtransformation (IFFT) into time domain..
 * 
 * For FFT/IFFT we are using FFTW-Library 3.3.4.
 */

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

#include <cmath>

#include "ilmsens/processing/ilmsens_processing_error.hpp"
#include "ilmsens/processing/ilmsens_processing_references.hpp"
#include "ilmsens/processing/KKF_cx.hpp"


namespace ilmsens 
{
namespace processing
{

/* Constructor */
KKF_cx::KKF_cx(unsigned int pMLBSLen)
: mMLBSLen(pMLBSLen), 
  mFDRef ( fftw_alloc_complex((size_t) pMLBSLen) ),
  mTDRaw ( fftw_alloc_complex((size_t) pMLBSLen) ),
  mTDCorr( fftw_alloc_complex((size_t) pMLBSLen) ),
  mFDRaw ( fftw_alloc_complex((size_t) pMLBSLen) ),
  mFDCorr( fftw_alloc_complex((size_t) pMLBSLen) )
{
  /* Reading ideal M-Sequence from File */
  mRefMLBS.resize(mMLBSLen);

  /*
  // guess file name
  std::string tFName;
  switch (mMLBSLen)  
  {
    case 511:     // 9th order 
      tFName = "/usr/include/ilmsens/processing/mlbs09.txt";
      break;
    
    case 4095:    // 12th order
      tFName = "/usr/include/ilmsens/processing/mlbs12.txt";
      break;
    
    case 32767:   // 15th order
      tFName = "/usr/include/ilmsens/processing/mlbs15.txt";
      break;
      
    default:
      std::cerr << "KKF_cx::ERROR: Unknown MLBS-Length: " << std::dec << mMLBSLen << "!" << std::endl;
      throw ilmsens::processing::Error("KKF_cx: unknown MLBS length!", ILMSENS_ERROR_INVALID_PARAM);
  }

  // read in the file
  std::ifstream tMLBSFile;
  tMLBSFile.open(tFName.c_str(), std::ifstream::in);
  if (tMLBSFile.is_open())
  {
    std::string tLine;
    for (unsigned int tI = 0; tI < mMLBSLen; tI++) 
    {
      getline(tMLBSFile, tLine);
      mRefMLBS[tI] = atof(tLine.c_str());
    }
    tMLBSFile.close();
  }
  else 
  {
    std::cerr << "KKF_cx::ERROR: Cannot open reference M-sequence file: ''" << tFName << "'." << std::endl;
    throw ilmsens::processing::Error("KKF_cx: cannot open M-sequence reference file!", ILMSENS_ERROR_IO);
  }
  */

  // guess MLBS order and get reference MLBS
  switch (mMLBSLen)  
  {
    case 511:     // 9th order 
      mRefMLBS = ilmsens::processing::references::getMLBSReferenceTD(9, 1);
      break;
    
    case 4095:    // 12th order
      mRefMLBS = ilmsens::processing::references::getMLBSReferenceTD(12, 1);
      break;
    
    case 32767:   // 15th order
      mRefMLBS = ilmsens::processing::references::getMLBSReferenceTD(15, 1);
      break;
      
    default:
      std::cerr << "KKF::ERROR: Unknown MLBS-Length: " << std::dec << mMLBSLen << "!" << std::endl;
      throw ilmsens::processing::Error("KKF: unknown MLBS length!", ILMSENS_ERROR_INVALID_PARAM);
  }
   
  /* Calculate Spectrum of ideal M-Sequence */

#if 0
  fftw_complex *tTD_mseq = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * mMLBSLen); // Time-Domain ideal M-Sequence
  fftw_plan tFFTPlan = fftw_plan_dft_1d(mMLBSLen, tTD_mseq, mFDRef.get(), FFTW_FORWARD, FFTW_PATIENT); // FFT plan
  //fill ideal MLBS into vector
  double tNormScale = sqrt((double)mMLBSLen);
  for (unsigned int tI = 0; tI < mMLBSLen; tI++) 
  {
    tTD_mseq[tI][0] = mRefMLBS[tI] / tNormScale;  // Real Part
    tTD_mseq[tI][1] = 0;                          // Imaginary Part
  }
#endif

  double *tTD_mseq = (double*) fftw_alloc_real((size_t) mMLBSLen);                            // Time-Domain ideal M-Sequence
  fftw_plan tFFTPlan = fftw_plan_dft_r2c_1d(mMLBSLen, tTD_mseq, mFDRef.get(), FFTW_PATIENT);  // FFT plan for real MLBS
  double tNormScale = sqrt((double)mMLBSLen);
  for (unsigned int tI = 0; tI < mMLBSLen; tI++) 
  {
    tTD_mseq[tI] = mRefMLBS[tI] / tNormScale;  // Real Part
  }

  // do FFT of ideal MLBS
  fftw_execute(tFFTPlan);

  //clean up temporary FFTw ressources
  fftw_destroy_plan(tFFTPlan);
  fftw_free(tTD_mseq);

  /* Allocate all required FFTw memories for KKF calculation */
  mCorrIRF.resize(mMLBSLen);

  /* Create FFTw plans for KKF */
  mFFTPlan  = fftw_plan_dft_1d(mMLBSLen, mTDRaw.get(), mFDRaw.get(), FFTW_FORWARD, FFTW_PATIENT);    //FFT plan
  mIFFTPlan = fftw_plan_dft_1d(mMLBSLen, mFDCorr.get(), mTDCorr.get(), FFTW_BACKWARD, FFTW_PATIENT); //IFFT plan
}

/* Destructor */
KKF_cx::~KKF_cx()
{
  //destroy FFTw plans
  fftw_destroy_plan(mFFTPlan); //FFT
  fftw_destroy_plan(mIFFTPlan); //IFFT
}

/* Calculate Crosscorrelation between received complex impulse response and ideal m-sequence */
const TCplxIRF& KKF_cx::calculateKKF(const TCplxIRF& pData) 
{
  fftw_complex *tTDRaw  = mTDRaw.get(); // get raw pointer (not required, but faster:-)
  for (unsigned int tI = 0; tI < mMLBSLen; tI++)
  {
    tTDRaw[tI][0] = pData[tI].mRe; // Real part
    tTDRaw[tI][1] = pData[tI].mIm; // Imag. part
  }
  
  /* FFT: Calculate Spectrum of raw data */
  fftw_execute(mFFTPlan);
  
  /* Calculate Cross-Spectrum FFT(x(t)) * conj(FFT(mlbs(t))) */
  fftw_complex *tFDRaw  = mFDRaw.get();  // get raw pointer (not required, but faster:-)
  fftw_complex *tFDRef  = mFDRef.get();  // get raw pointer (not required, but faster:-)
  fftw_complex *tFDCorr = mFDCorr.get(); // get raw pointer (not required, but faster:-)
  for (unsigned int tI = 0; tI < mMLBSLen; tI++)
  {
    //complex multiply, ATTENTION: mFDRef is used in complex conjugate form!
    tFDCorr[tI][0] = (tFDRaw[tI][0] * tFDRef[tI][0]) + (tFDRaw[tI][1] * tFDRef[tI][1]); // Real part
    tFDCorr[tI][1] = (tFDRaw[tI][1] * tFDRef[tI][0]) - (tFDRaw[tI][0] * tFDRef[tI][1]); // Imag. part
  }
  
  /* Calculate IFFT of Cross-Spectrum */
  fftw_execute(mIFFTPlan);
  
  /* Normalize and copy */
  double tNormScale = (double)mMLBSLen;
  fftw_complex *tTDCorr = mTDCorr.get(); // get raw pointer (not required, but faster:-)
  for (unsigned int tI = 0; tI < mMLBSLen; tI++)
  {
    mCorrIRF[tI].mRe = tTDCorr[tI][0] / tNormScale;
    mCorrIRF[tI].mIm = tTDCorr[tI][1] / tNormScale;
  }

  return(mCorrIRF);
}

/* return FRF of last correlation operation */
const fftw_complex* KKF_cx::getLastFRF(void)
{
  return (mFDCorr.get());
}             

} //namespace processing
} //namespace ilmsens
