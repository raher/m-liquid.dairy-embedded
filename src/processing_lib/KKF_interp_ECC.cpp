/* KKF_interp_ECC.CPP
 * 
 * Calculating cross correlation between stimulus and received signal. For better performance, 
 * we first convert the signals into frequency domain (FFT), calculate the cross correlation spectrum.
 * The received spectrum will then be up-converted into the 2nd nyquist zone where it existed physically.
 * After that we do a backtransformation (IFFT) into time domain. Additionally, an interpolated
 * vector can be retrieved.
 * 
 * For FFT/IFFT we are using FFTW-Library 3.3.4.
 */

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

#include <cmath>
#include <cstring>

#include "ilmsens/processing/ilmsens_processing_error.hpp"
#include "ilmsens/processing/ilmsens_processing_references.hpp"
#include "ilmsens/processing/KKF_interp_ECC.hpp"


/* -------------------------------------------------------------------------------------------------
 * Local definitions
 * -------------------------------------------------------------------------------------------------
 */

#define ECC_LO_FAK_DEFAULT 2     ///< default factor for ECC sensor LO wrt MLBS clock

namespace ilmsens 
{
namespace processing
{



/* Constructor */
KKF_interp_ECC::KKF_interp_ECC(unsigned int pMLBSOrder, unsigned int pOV, unsigned int pIPLen, bool pUseFilterWin)
  : mMLBSOrder(pMLBSOrder),
  mOV(pOV),
  mMLBSLen(((1<<pMLBSOrder)-1)*pOV), 
  mIPLen(pIPLen),
  mFrqWin(this->mMLBSLen, 1.0),
  mFDRef ( fftw_alloc_complex ((size_t) this->mMLBSLen) ),
  mTDRaw ( fftw_alloc_real    ((size_t) this->mMLBSLen) ),
  mTDCorr( fftw_alloc_real    ((size_t) this->mMLBSLen) ),
  mFDRaw ( fftw_alloc_complex ((size_t) this->mMLBSLen) ),
  mFDCorr( fftw_alloc_complex ((size_t) this->mMLBSLen) ),
  mTDIp ( fftw_alloc_real((size_t) pIPLen) ),
  mFDIp ( fftw_alloc_complex((size_t) pIPLen) )
{
  /* Check interpolation length */
  if (mIPLen < (mMLBSLen * ECC_LO_FAK_DEFAULT))
  {
    // not allowed
    throw ilmsens::processing::Error("KKF_interp_ECC::ERROR: interpolation length must not be smaller than oversampled and up-mixed MLBS length!", ILMSENS_ERROR_INVALID_PARAM);
  }

  /* Getting ideal oversampled M-Sequence reference */
  mRefMLBS = ilmsens::processing::references::getPCMLBSReferenceTD(mMLBSOrder, mOV, ECC_LO_FAK_DEFAULT);

  /* prepare frequency domain filtering window */
  if (pUseFilterWin)
  {
    mFrqWin = ilmsens::processing::references::getFilterWindowFD(std::string("ECC"), mMLBSOrder, mOV, ECC_LO_FAK_DEFAULT);
  }

   
  /* Calculate Spectrum of ideal M-Sequence */

  double *tTD_mseq = (double*) fftw_alloc_real((size_t) mMLBSLen);                            // Time-Domain ideal M-Sequence
  fftw_plan tFFTPlan = fftw_plan_dft_r2c_1d(mMLBSLen, tTD_mseq, mFDRef.get(), FFTW_PATIENT);  // FFT plan for real MLBS
  double tNormScale = sqrt((double)mMLBSLen);
  for (unsigned int tI = 0; tI < mMLBSLen; tI++) 
  {
    tTD_mseq[tI] = mRefMLBS[tI] / tNormScale;  // Real Part
  }
  
  // do FFT of ideal MLBS
  fftw_execute(tFFTPlan);
  
  //clean up temporary FFTw ressources
  fftw_destroy_plan(tFFTPlan);
  fftw_free(tTD_mseq);
   
  /* make sure, only the phase of reference spectrum is used */
  ilmsens::processing::references::getNormalisedSpectrumKeepPhases(mFDRef.get(), (size_t)mMLBSLen);

  /* Allocate all required FFTw memories for KKF calculation */
  mCorrIRF.resize(mMLBSLen);

  /* Create FFTw plans for KKF */
  mFFTPlan  = fftw_plan_dft_r2c_1d(mMLBSLen, mTDRaw.get(), mFDRaw.get(), FFTW_PATIENT);   //FFT plan
  mIFFTPlan = fftw_plan_dft_c2r_1d(mMLBSLen, mFDCorr.get(), mTDCorr.get(), FFTW_PATIENT); //IFFT plan
  
  /* Allocate all required FFTw memories for KKF calculation */
  mInterpIRF.resize(mIPLen);

  /* Cleanup FFTW data vectors for interpolation */
  fftw_complex *tFDIp  = mFDIp.get(); // get raw pointer (not required, but faster:-)
  for (unsigned int tI = 0; tI < mIPLen; tI++)
  {
    tFDIp[tI][0] = 0.0;  // Real Part
    tFDIp[tI][1] = 0.0;  // Imaginary Part
  }

  /* Create FFTw plans for interpolation */
  mIFFTPlanIp = fftw_plan_dft_c2r_1d(mIPLen, mFDIp.get(), mTDIp.get(), FFTW_PATIENT); // IFFT plan
}

/* Destructor */
KKF_interp_ECC::~KKF_interp_ECC()
{
  //destroy FFTw plans
  fftw_destroy_plan(mFFTPlan); //FFT
  fftw_destroy_plan(mIFFTPlan); //IFFT
  fftw_destroy_plan(mIFFTPlanIp); //IFFT with interpolation
}


/* Calculate Crosscorrelation between received impulse response and ideal m-sequence */
const TRealIRF& KKF_interp_ECC::calculateKKF(const TRealIRF& pData) 
{
  double *tTDRaw  = mTDRaw.get(); // get raw pointer (not required, but faster:-)
  for (unsigned int tI = 0; tI < mMLBSLen; tI++)
  {
    tTDRaw[tI] = pData[tI]; // Real part
  }

  /* FFT: Calculate Spectrum of raw data */
  fftw_execute(mFFTPlan);

  /* Calculate Cross-Spectrum FFT(x(t)) * conj(FFT(mlbs_ref(t))) */
  fftw_complex *tFDRaw  = mFDRaw.get();  // get raw pointer (not required, but faster:-)
  fftw_complex *tFDRef  = mFDRef.get();  // get raw pointer (not required, but faster:-)
  fftw_complex *tFDCorr = mFDCorr.get(); // get raw pointer (not required, but faster:-)
  for (unsigned int tI = 0; tI < mMLBSLen; tI++)
  {
    //complex multiply, ATTENTION: mFDRef is used in complex conjugate form!
    tFDCorr[tI][0] = (tFDRaw[tI][0] * tFDRef[tI][0]) + (tFDRaw[tI][1] * tFDRef[tI][1]); // Real part
    tFDCorr[tI][1] = (tFDRaw[tI][1] * tFDRef[tI][0]) - (tFDRaw[tI][0] * tFDRef[tI][1]); // Imag. part
  }

  /* Calculate IFFT of Cross-Spectrum */
  fftw_execute(mIFFTPlan);

  /* Normalize and copy */
  double tNormScale = (double) mMLBSLen;
  double *tTDCorr = mTDCorr.get(); // get raw pointer (not required, but faster:-)
  for (unsigned int tI = 0; tI < mMLBSLen; tI++)
  {
    mCorrIRF[tI] = tTDCorr[tI] / tNormScale;
  }

  return(mCorrIRF);
}

/* return IRF of last correlation operation */
const TRealIRF& KKF_interp_ECC::getLastIRF(void)
{
  return(mCorrIRF);
}

/* return FRF of last correlation operation */
const fftw_complex* KKF_interp_ECC::getLastFRF(void)
{
  return (mFDCorr.get());
}


/* Calculate ECC-up-mixing and interpolation of correlated IRF */
const TRealIRF& KKF_interp_ECC::calculateECC_KKF(const TRealIRF& pData) 
{
  /* Start by calculating Crosscorrelation between received impulse response and ideal m-sequence */
  double *tTDRaw  = mTDRaw.get(); // get raw pointer (not required, but faster:-)
  for (unsigned int tI = 0; tI < mMLBSLen; tI++)
  {
    tTDRaw[tI] = pData[tI]; // Real part
  }

  /* FFT: Calculate Spectrum of raw data */
  fftw_execute(mFFTPlan);

  /* Calculate Cross-Spectrum FFT(x(t)) * conj(FFT(mlbs_ref(t))) */
  fftw_complex *tFDRaw  = mFDRaw.get();  // get raw pointer (not required, but faster:-)
  fftw_complex *tFDRef  = mFDRef.get();  // get raw pointer (not required, but faster:-)
  fftw_complex *tFDCorr = mFDCorr.get(); // get raw pointer (not required, but faster:-)
  for (unsigned int tI = 0; tI < mMLBSLen; tI++)
  {
    //complex multiply, ATTENTION: mFDRef is used in complex conjugate form!
    tFDCorr[tI][0] = (tFDRaw[tI][0] * tFDRef[tI][0]) + (tFDRaw[tI][1] * tFDRef[tI][1]); // Real part
    tFDCorr[tI][1] = (tFDRaw[tI][1] * tFDRef[tI][0]) - (tFDRaw[tI][0] * tFDRef[tI][1]); // Imag. part
  }

  /* Finish by caculating interpolated ECC IRF from previously calculated spectrum */
  return(calculateInterpolation());
}

/* Calculate interpolation of last correlated FRF */
const TRealIRF& KKF_interp_ECC::calculateInterpolation(void) 
{
  /* Copy last FRF to zero padded interpolation buffer -> shifts used spectrum to 2nd Nyquist zone as well! */
  fftw_complex *tFDCorr = mFDCorr.get(); // get raw pointer (not required, but faster:-)
  fftw_complex *tFDIp   = mFDIp.get();   // get raw pointer (not required, but faster:-)

  //clear spectrum buffer for interpolated FRF
  memset((void *)tFDIp, 0, (size_t)mIPLen * sizeof(fftw_complex));

  // copy correlated coefficients of positive frequencies (excluding DC) to 2nd nyquist zone and apply frequency window
  unsigned int tNumPos  = (mMLBSLen >> 1) + 1;
  for (unsigned int tI = 1; tI < tNumPos; tI++)
  {
    unsigned int tDestIdx = mMLBSLen-tI;
    tFDIp[tDestIdx][0] = tFDCorr[tI][0]        * mFrqWin[tI];   // Real part
    tFDIp[tDestIdx][1] = -1.0 * tFDCorr[tI][1] * mFrqWin[tI];   // Imag. part -> coefficients must be copied in complex conjugate form!
  }

  // negative frequencies -> create complex conjugate spectrum of previously filled positive side
  unsigned int tNumPosIP  = (mIPLen >> 1) + 1;
  for (unsigned int tI = 1; tI < tNumPosIP; tI++)
  {
    unsigned int tDestIdx = mIPLen-tI;
    tFDIp[tDestIdx][0] = tFDIp[tI][0];         // Real part
    tFDIp[tDestIdx][1] = -1.0 * tFDIp[tI][1];  // Imag. part -> make it complex conjugate!
  }
  
  /* Calculate IFFT of zero padded Cross-Spectrum */
  fftw_execute(mIFFTPlanIp);
  
  /* Normalize and copy */
  double tNormScale = (double)mMLBSLen; // revert scaling of FFT/IFFT to get original amplitudes 
                                        // like in uninterpolated data
  double *tTDIp = mTDIp.get();          // get raw pointer (not required, but faster:-)
  for (unsigned int tI = 0; tI < mIPLen; tI++)
  {
    mInterpIRF[tI] = tTDIp[tI] / tNormScale;
  }
  
  return(mInterpIRF);
}

/* return FRF of last interpolation operation */
const fftw_complex* KKF_interp_ECC::getLastECC_FRF(void)
{
  return (mFDIp.get());
}

} //namespace processing
} //namespace ilmsens
