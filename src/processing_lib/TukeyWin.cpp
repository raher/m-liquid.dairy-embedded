/* TUKEYWIN.CPP
 * 
 * Calculates the Tukey-Window for short time fouriertransformation
 * Code is taken from MatLab R2018a
 *
 */

#include <iostream>
#include <cmath>
#define _USE_MATH_DEFINES
#include <math.h>

#include "ilmsens/processing/TukeyWin.hpp"
#include "ilmsens/processing/HannWin.hpp"

using namespace ilmsens::processing::types;

namespace ilmsens
{
  namespace processing
  {
    
    /* Constructor */
    TukeyWin::TukeyWin(unsigned int pWinLen, double pTaper)
      : mWinLen   (pWinLen),
        mTukeyVec (pWinLen, 1.0)
    {
      if (pTaper <= 0)    // rectangular window
      {}
      else if (pTaper >= 1)    // Hann-Window
      {
        // TukeyWin = HannWin(mWinLen);
        HannWin hannWin(mWinLen);
        mTukeyVec = hannWin.getHannVec();
      }
      else
      {
        double period = pTaper/2.0 * (double)mWinLen;
        
        // left taper
        //int tl = (int) floor(period * (mWinLen - 1));         // left  taper idx:0..t1
        int tl = (int) floor(period);         // left  taper idx:0..t1
        for (int i = 0; i <= tl; i++)
        {
          mTukeyVec[i] = (1 + cos(M_PI / period * (i - period))) / 2.0;
        }

        // right taper
        int tr = mWinLen - tl;                   // right taper idx:tr..mWinLen-1
        for (unsigned int i = tr; i < mWinLen; i++)
        {
          mTukeyVec[i-1] = (1 + cos(M_PI / period * (i - (double)mWinLen + period))) / 2.0;
        }
        mTukeyVec[mWinLen-1] = 0.0; // make last sample to be 0.0
      }
    }

    TukeyWin::TukeyWin(unsigned int pWinLen)
    { 
      TukeyWin(pWinLen, 0.5);
    }

    TukeyWin::TukeyWin(TSampleVec pWinVec,
                       double     pTapperLBegin,
                       double     pTapperLEnd,
                       double     pTapperRBegin,
                       double     pTapperREnd)
    : mWinLen (0),
      mTukeyVec (0)
    {
      if(pWinVec.size() > 0)
      {
        mWinLen = static_cast<unsigned int> (pWinVec.size());
        if(pTapperLBegin <= pTapperLEnd   &&                                                 // check for valid intervals
           pTapperLEnd   <= pTapperRBegin &&
           pTapperRBegin <= pTapperREnd      )
        {
          mTukeyVec.resize(mWinLen);
          double tTapperLWidth = pTapperLEnd - pTapperLBegin;
          double tTapperRWidth = pTapperREnd - pTapperRBegin;

          for(unsigned int tWinIdx = 0; tWinIdx < mWinLen; tWinIdx++)
          {
            if(pWinVec[tWinIdx] < pTapperLBegin || pWinVec[tWinIdx] > pTapperREnd)           // margin
            {
              mTukeyVec[tWinIdx] = 0.0;
            }
            else if(pWinVec[tWinIdx] >= pTapperLBegin && pWinVec[tWinIdx] <= pTapperLEnd)    // left tapper
            {
              mTukeyVec[tWinIdx] = (- cos(M_PI / tTapperLWidth * (pWinVec[tWinIdx] - pTapperLBegin)) + 1) / 2.0;
            }
            else if(pWinVec[tWinIdx] > pTapperLEnd && pWinVec[tWinIdx] < pTapperRBegin)     // between tappers
            {
              mTukeyVec[tWinIdx] = 1;
            }
            else if(pWinVec[tWinIdx] >= pTapperRBegin && pWinVec[tWinIdx] <= pTapperREnd)   // right tapper
            {
              mTukeyVec[tWinIdx] = (cos(M_PI / tTapperRWidth * (pWinVec[tWinIdx] - pTapperRBegin)) + 1) / 2.0;
            }
          }
        }
      }
    }
    
    /* Destructor */
    TukeyWin::~TukeyWin()
    {
    }

    /* Getter */
    const TSampleVec& TukeyWin::getTukeyVec(void)
    {
      return(mTukeyVec);
    }
  
  } //namespace processing
} //namespace ilmsens
