#get headers in this folder as well
include_directories( ${CMAKE_CURRENT_SOURCE_DIR} )

#static processing library

#make a list of sources to be added to target
file(GLOB_RECURSE LIB_SRC_LIST *.c *.cpp)


add_library(ilmsens_processing STATIC ${LIB_SRC_LIST} ${ILMSENS_LIB_PROCESSING_HEADERS})
target_link_libraries(ilmsens_processing ${FFTW_LIBRARIES})
set_property(TARGET ilmsens_processing PROPERTY POSITION_INDEPENDENT_CODE ON)
set_property(TARGET ilmsens_processing PROPERTY FOLDER "Ilmsens Libraries")
