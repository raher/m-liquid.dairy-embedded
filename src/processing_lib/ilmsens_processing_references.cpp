/* ilmsens_processing_references.cpp
 *
 * Tools related to reference functions for MLBS sensors.
 * 
 */


/* -------------------------------------------------------------------------------------------------
 * Local definitions
 * -------------------------------------------------------------------------------------------------
 */

#ifdef WIN32
  #define REF_PATH "ilmsens\\processing\\"  ///< path to reference MLBS files (Windows)
#else
  #define REF_PATH "/usr/include/ilmsens/processing/"  ///< path to reference MLBS files (Linux)
#endif

/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */

// STL
#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>

// math stuff (partially STL)
#ifndef WIN32
  #include <cfenv>
  #include <cerrno>
  #include <cstring>
#endif
#include <cmath>

//file IO
#include <sys/stat.h>
#include <cstdio>

// Ilmsens
#include "ilmsens/processing/ilmsens_processing_error.hpp"
#include "ilmsens/processing/ilmsens_processing_references.hpp"

namespace ilmsens 
{
namespace processing
{
namespace references
{

  /**  load ideal MLBS from text file and provide the (possibly oversampled) time domain reference for correlation */
  ilmsens::processing::types::TRealIRF& getMLBSReferenceTD(unsigned int pOrder, unsigned int pOV, unsigned int /* pLOIdx */)
  {
    static ilmsens::processing::types::TRealIRF sRefIRF;

    /* Reading ideal M-Sequence from File */
    std::string tFName;
    switch (pOrder)  
    {
      case 9:     // 9th order 
        tFName = std::string(REF_PATH) + "mlbs09.txt";
        break;
    
      case 12:    // 12th order
        tFName = std::string(REF_PATH) + "mlbs12.txt";
        break;
    
      case 15:   // 15th order
        tFName = std::string(REF_PATH) + "mlbs15.txt";
        break;
      
      default:
        std::cerr << "getMLBSReferenceTD::ERROR: Unknown MLBS order: " << std::dec << pOrder << "!" << std::endl;
        throw ilmsens::processing::Error("getMLBSReferenceTD: unknown MLBS order!", ILMSENS_ERROR_INVALID_PARAM);
    }

    // get vector lengths
    unsigned int tMLBSLen = (1 << pOrder) - 1;
    unsigned int tNumSamp = tMLBSLen * pOV;

    // alloate buffers
    sRefIRF.resize(tNumSamp);

    ilmsens::processing::types::TRealIRF tRefMLBS;
    tRefMLBS.resize(tMLBSLen);
      
    // read in the file
    std::ifstream tMLBSFile;
    tMLBSFile.open(tFName.c_str(), std::ifstream::in);
    if (tMLBSFile.is_open())
    {
      std::string tLine;
      for (unsigned int tI = 0; tI < tMLBSLen; tI++) 
      {
        getline(tMLBSFile, tLine);
        tRefMLBS[tI] = atof(tLine.c_str());
      }
      tMLBSFile.close();
    }
    else 
    {
      std::cerr << "getMLBSReferenceTD::ERROR: Cannot open reference M-sequence file: '" << tFName << "'." << std::endl;
      throw ilmsens::processing::Error("getMLBSReferenceTD: cannot open M-sequence reference file!", ILMSENS_ERROR_IO);
    }

    // generate oversampled reference MLBS 
    if (pOV > 1)
    {
      for (unsigned int tI = 0; tI < tMLBSLen; tI++) 
      {
        ilmsens::processing::types::TRealIRF::value_type tCurVal = tRefMLBS[tI];

        // store current value
        for (unsigned int tO = 0; tO < pOV; tO++) 
        {
          sRefIRF[tI*pOV+tO] = tCurVal;
        }
      }
    }
    else
    {
      //direct copy
      sRefIRF = tRefMLBS;
    }


    return(sRefIRF);
  }
  
#ifdef _MSC_VER
  #define _CRT_SECURE_NO_WARNINGS 

  #pragma warning(push)
  #pragma warning(disable:4996)
#endif
  /**  load pre-computed extended ideal MLBS file and provide the time doamain reference for correlation */
  ilmsens::processing::types::TRealIRF& getPCMLBSReferenceTD(unsigned int pOrder, unsigned int pOV, unsigned int pLOIdx)
  {
    static ilmsens::processing::types::TRealIRF sRefIRF;

    // prepare extended file name for loading pre-computed reference data
    std::string tFName = std::string(REF_PATH) + "MLBS" + std::to_string(pOrder) + "_OV" + std::to_string(pOV) + "_LO" + std::to_string(pLOIdx) + ".dat";

    //get file size and prepare buffer for doubles
    struct stat tStat;
    int tErg = stat(tFName.c_str(), &tStat);
    if (tErg < 0)
    {
      // an error occurred
      std::cerr << "getPCMLBSReferenceTD::ERROR: Cannot find or get size of pre-computed M-sequence reference file: '" << tFName << "'." << std::endl;
      throw ilmsens::processing::Error("getPCMLBSReferenceTD: cannot find or get size of pre-computed M-sequence reference file!", ILMSENS_ERROR_IO);
    }
    else
    {
      std::cout << "getPCMLBSReferenceTD::INFO: loading pre-computed M-sequence reference file: '" << tFName << "'." << std::endl;
    }

    off_t tSizeBytes = tStat.st_size;
    size_t tNumDouble = (size_t)tSizeBytes / sizeof(double);
    std::vector<double> tRefDatDBL(tNumDouble, 0.0);

    // open file
    FILE *tFID = fopen(tFName.c_str(), "rb");
    if (tFID == NULL)
    {
      // an error occurred
      std::cerr << "getPCMLBSReferenceTD::ERROR: Cannot open pre-computed M-sequence reference file: '" << tFName << "'." << std::endl;
      throw ilmsens::processing::Error("getPCMLBSReferenceTD: cannot open pre-computed M-sequence reference file!", ILMSENS_ERROR_IO);
    }
    
    // read in reference data
    size_t tNumRead = fread((void *)tRefDatDBL.data(), sizeof(double), tNumDouble, tFID);
    if (tNumRead != tNumDouble)
    {
      // an error occurred
      std::string tErrMsg = "<unkown IO error>";
      if (ferror(tFID))
      {
        tErrMsg = std::string(std::strerror(errno));
      }

      fclose(tFID);
      std::cerr << "getPCMLBSReferenceTD::ERROR: Cannot read pre-computed M-sequence reference file: '" << tFName << "'. Error was: " << tErrMsg << std::endl;
      throw ilmsens::processing::Error("getPCMLBSReferenceTD: cannot read pre-computed M-sequence reference file!", ILMSENS_ERROR_IO);
    }
    //close the file
    fclose(tFID);

    // alloate output buffer
    sRefIRF.resize(tNumDouble);

    //copy and convert
    for (size_t tI = 0; tI < tNumDouble; tI++)
    {
      sRefIRF[tI] = (ilmsens::processing::types::TRealIRF::value_type)tRefDatDBL[tI];
    }

    return(sRefIRF);
  }

  /**  load pre-computed frequency domain filtering window that can be used during correlation */
  ilmsens::processing::types::TRealIRF& getFilterWindowFD(std::string pTypeID, unsigned int pOrder, unsigned int pOV, unsigned int pLOIdx)
  {
    static ilmsens::processing::types::TRealIRF sWin;

    // prepare extended file name for loading pre-computed reference data
    std::string tFName = std::string(REF_PATH) + "filter_window_MLBS" + std::to_string(pOrder) + "_OV" + std::to_string(pOV) + "_LO" + std::to_string(pLOIdx) + "_" + pTypeID + ".dat";

    //get file size and prepare buffer for doubles
    struct stat tStat;
    int tErg = stat(tFName.c_str(), &tStat);
    if (tErg < 0)
    {
      // an error occurred
      std::cerr << "getFilterWindowFD::ERROR: Cannot find or get size of filter window file: '" << tFName << "'." << std::endl;
      throw ilmsens::processing::Error("getFilterWindowFD: cannot find or get size of filter window file!", ILMSENS_ERROR_IO);
    }
    else
    {
      std::cout << "getFilterWindowFD::INFO: loading pre-computed filter window file: '" << tFName << "'." << std::endl;
    }

    off_t tSizeBytes = tStat.st_size;
    size_t tNumDouble = (size_t)tSizeBytes / sizeof(double);
    std::vector<double> tRefDatDBL(tNumDouble, 0.0);

    // open file
    FILE *tFID = fopen(tFName.c_str(), "rb");
    if (tFID == NULL)
    {
      // an error occurred
      std::cerr << "getFilterWindowFD::ERROR: Cannot open pre-computed M-sequence reference file: '" << tFName << "'." << std::endl;
      throw ilmsens::processing::Error("getFilterWindowFD: cannot open pre-computed M-sequence reference file!", ILMSENS_ERROR_IO);
    }
    
    // read in reference data
    size_t tNumRead = fread((void *)tRefDatDBL.data(), sizeof(double), tNumDouble, tFID);
    if (tNumRead != tNumDouble)
    {
      // an error occurred
      std::string tErrMsg = "<unkown IO error>";
      if (ferror(tFID))
      {
        tErrMsg = std::string(std::strerror(errno));
      }

      fclose(tFID);
      std::cerr << "getFilterWindowFD::ERROR: Cannot read pre-computed M-sequence reference file: '" << tFName << "'. Error was: " << tErrMsg << std::endl;
      throw ilmsens::processing::Error("getFilterWindowFD: cannot read pre-computed M-sequence reference file!", ILMSENS_ERROR_IO);
    }
    //close the file
    fclose(tFID);

    // alloate output buffer
    sWin.resize(tNumDouble);

    //copy and convert
    for (size_t tI = 0; tI < tNumDouble; tI++)
    {
      sWin[tI] = (ilmsens::processing::types::TRealIRF::value_type)tRefDatDBL[tI];
    }

    return(sWin);
  }

#ifdef _MSC_VER
  #undef _CRT_SECURE_NO_WARNINGS 
  #pragma warning(pop)
#endif

  /**  normalize given reference spectrum to obtain reference only using the phases */
  void getNormalisedSpectrumKeepPhases(fftw_complex *pSpec, size_t pLen)
  {
    //check inputs
    if (!pLen) return;

    if (pSpec == NULL)
    {
      std::cerr << "getNormalisedSpectrumKeepPhases::ERROR: provided pointer at spectrum is NULL!" << std::endl;
      throw ilmsens::processing::Error("getNormalisedSpectrumKeepPhases::ERROR: provided pointer at spectrum is NULL!", ILMSENS_ERROR_INVALID_PARAM);
    }
    
#ifndef WIN32
    std::cout << "getNormalisedSpectrumKeepPhases::INFO: MATH_ERRNO is "
              << (math_errhandling & MATH_ERRNO ? "set" : "not set") << '\n'
              << "MATH_ERREXCEPT is "
              << (math_errhandling & MATH_ERREXCEPT ? "set" : "not set") << '\n';
    std::feclearexcept(FE_ALL_EXCEPT);
#endif

    for (size_t tI = 0; tI < pLen; tI++)
    {
      // get angle of complex number
      double tAngRad = std::atan2(pSpec[tI][1], pSpec[tI][0]);

      //create complex number normalized to magbitude 1.0 with obtain angle
#ifndef WIN32
      bool tIsNan = std::isnan(tAngRad);
#else
      int tIsNan = _isnan(tAngRad);
#endif

      if (tIsNan)
      {
        //undefined value
        pSpec[tI][0] = 1.0;
        pSpec[tI][1] = 0.0;
      }
      else
      {
        //regular value
        pSpec[tI][0] = std::cos(tAngRad);
        pSpec[tI][1] = std::sin(tAngRad);
      }
    }
  }

} //namespace references
} //namespace processing
} //namespace ilmsens
