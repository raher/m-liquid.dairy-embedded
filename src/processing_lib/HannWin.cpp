/* HANNWIN.CPP
 * 
 * Calculates the Hanning-Window for short time fouriertransformation
 * Code is taken from MatLab R2018a (combination of hann() and genocoswin())
 *
 */

#include <iostream>
#include <cmath>
#define _USE_MATH_DEFINES
#include <math.h>

#include "ilmsens/processing/HannWin.hpp"
  
using namespace ilmsens::processing::types;

namespace ilmsens
{
  namespace processing
  {
    
    /* Constructor */
    HannWin::HannWin(unsigned int pWinLen)
      : mWinLen   (pWinLen),
        mHannVec  (pWinLen)
    {
      if (mWinLen % 2 )     // odd windowLength
      {
        unsigned int half = mWinLen / 2;
        TSampleVec mHalfHannVec = calcHalfHann(half, mWinLen);
        mHalfHannVec.insert(mHalfHannVec.end(), 
                            mHalfHannVec.end(), 
                            mHalfHannVec.begin());
        mHannVec = mHalfHannVec;
      }
      else                  // even windowLength
      {
        unsigned int half = (mWinLen - 1) / 2;
        TSampleVec mHalfHannVec = calcHalfHann(half, mWinLen);
        mHalfHannVec.insert(mHalfHannVec.end(),
                            mHalfHannVec.end()-1,
                            mHalfHannVec.begin());
        mHannVec = mHalfHannVec;
      }
    }
    
    /* Destructor */
    HannWin::~HannWin()
    {
    }

    /* Getter */
    const TSampleVec& HannWin::getHannVec(void)
    {
      return(mHannVec);
    }

    TSampleVec HannWin::calcHalfHann(unsigned int pHalfWinLen, unsigned int pWinLen)
    {
      TSampleVec halfHannVec(pHalfWinLen);
      for (unsigned int i = 0; i < pHalfWinLen; i++)
      {
        halfHannVec[i] = 0.5 - 0.5 * cos(2 * M_PI * i / (pWinLen - 1));
      }
      return halfHannVec;
    }
  
  } //namespace processing
} //namespace ilmsens
