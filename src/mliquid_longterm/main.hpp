/// @file main.hpp
/// @brief App definitions, declarations, and function prototypes.
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>


#ifndef MAIN_HPP
#define MAIN_HPP

// Logging & Debug
#define LOG_LEVEL_DEFAULT  Poco::Message::Priority::PRIO_INFORMATION      ///< Default Poco log level of app

// sensor setup
#define LT_SENSOR_SETUP_TIMEOUT 10000                                     ///< Timeout while waiting for sensor setup by dataReader thread

// App configuration
#define LT_AUTO_START_MEAS false                                          ///< automatically start a measurement at application start?

// Measurement timing
#define LT_SW_AVG_DEFAULT  32                                             ///< default software averages
#define LT_WC_DEFAULT      0                                              ///< default wait cycles

// raw data storage
#define LT_ACT_STORAGE     false                                          ///< activate storage of raw measued data by default?

// streaming configuration
#define LT_STREAM_IRFS     false                                          ///< Stream IRF data alongside info vector via TCP server?
#define LT_STREAM_WAIT_INT 1000                                           ///< Waiting interval [ms] between forwarding IRFs to TCP stream

#endif //MAIN_HPP
