/// @file hal_wrap.hpp
/// @description C++ wrapper classes header for Ilmsens HAL API
/// @author pavol.kurina@gmail.com and Ralf.Herrmann@ilmsens.com

#include <stdexcept>
#include <vector>
#include <chrono>

//include UWBAPI HAL
#include "ilmsens/hal/ilmsens_hal_defn.h"
#include "ilmsens/hal/ilmsens_hal_types.h"

#ifndef ILMSENS_HAL_WRAP_HPP
#define ILMSENS_HAL_WRAP_HPP

namespace ilmsens 
{
  namespace hal 
  {
    
    namespace utils 
    {
    
      class NotCopyable 
      {
      public:
        NotCopyable() {}
      private:
        NotCopyable(const NotCopyable&);
        NotCopyable& operator=(const NotCopyable&);
      };

    }

    class Error : public std::runtime_error 
    {
      typedef std::runtime_error Base;
    public:
      Error(const char* message, int status_code = ILMSENS_DEB_INFO)
        : Base(message), _status_code(status_code) {}
      int status_code() const { return _status_code; }
    private:
      int _status_code;
    };

    class Context : public utils::NotCopyable 
    {
    public:
      Context(unsigned debugLevel);
      ~Context();
      void setDebugLevel(unsigned pDebLevel);
      unsigned sensorCount() const { return _sensorCount; }
      const ilmsens_hal_Version& getVersion() const { return(_version); }
    private:
      unsigned _sensorCount;
      ilmsens_hal_Version _version;
    };

    typedef unsigned SensorIndex;
    typedef std::vector<SensorIndex> SensorIndexes;

    class SensorGroup : public utils::NotCopyable 
    {
    public:
      SensorGroup(const SensorIndexes& indexes);
      ~SensorGroup();

      const SensorIndexes::reference& front() { return _indexes.front(); }
      const SensorIndexes::reference& at(unsigned n) { return _indexes.at(n); }
      unsigned size() const { return static_cast<unsigned>(_indexes.size()); }
      
      void setMaster(unsigned masterPosition);
      void setSWAvgerages(unsigned pSWAvg, unsigned pWC = 0);
      
    private:
      SensorIndexes _indexes;
    };

    typedef ilmsens_hal_SampleType SampleType;
    typedef std::vector<SampleType> ImpulseResponse;

    class Measurement : public utils::NotCopyable 
    {
    public:
      Measurement(SensorGroup& sensors, size_t bufferSize, bool bufferedMode = false);
      ~Measurement();
      ImpulseResponse next(std::chrono::milliseconds timeout);
    private:
      SensorGroup& _sensors;
      size_t _bufferSize;
    };

    /// @returns the minimum number of elements for the ImpulseResponse's buffer
    unsigned configure(SensorGroup& sensors, unsigned mlbsOrder, double masterClk, unsigned receiverCount, unsigned softwareAvg, unsigned masterPosition);
    
    /// @returns HAL version structure
    const ilmsens_hal_Version& getHALVersion();
  }
}

#endif // ILMSENS_HAL_WRAP_HPP
