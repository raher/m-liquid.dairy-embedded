/* strmTCPServer.cpp
 * 
 * Thread function for TCP server which will accept a single client connection
 * and subsequently forward measured & info data to the client. When no connection is made, 
 * data is discarded.
 */


/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */


// Std. C++ library types / STL 

// File, IO, and Strings
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

/*
// Poco logging includes
#include "Poco/Logger.h"
#include "Poco/LogStream.h"
#include "Poco/Message.h"
*/

// Timing and threading includes
#include <pthread.h>                    // POSIX threads
#include <sched.h>                      // for Realtime Priority

// Shared variables, memories, and synchronisation ressources
#include "shared.hpp"

// TCP stuff & server class
#ifndef WIN32
  #include <arpa/inet.h> // for htonl() call
#endif
#include "ilmsens/networking/TCP_server_single.hpp"
#include "ilmsens/networking/ilmsens_networking_defn.h"
#include "ilmsens/networking/ilmsens_networking_error.hpp"

// Other Ilmsens stuff
#include "ilmsens/ilmsens_error.h"      // Ilmsens error codes

// main header for module
#include "strmTCPServer.hpp" 


/* -------------------------------------------------------------------------------------------------
 * Using clauses
 * -------------------------------------------------------------------------------------------------
 */

using namespace ilmsens::networking;


/* -------------------------------------------------------------------------------------------------
 * Local defines
 * -------------------------------------------------------------------------------------------------
 */


/* -------------------------------------------------------------------------------------------------
 * Local helper functions
 * -------------------------------------------------------------------------------------------------
 */

uint32_t float2uint32(float pVal)
{
  union
  {
    float uFloat;
    uint32_t uUInt32;
  };
  uFloat = pVal;

  return(uUInt32);
}


namespace ilmsens 
{
namespace mliquid
{
namespace longterm 
{
namespace streaming
{


/* -------------------------------------------------------------------------------------------------
 * Logging & logging helpers
 * 
 * Powered by Poco:-)
 * -------------------------------------------------------------------------------------------------
 */

/*
// Logger for main app
Poco::Logger& log()
{
  static Poco::Logger& sLog = Poco::Logger::get("ilmsens.mliquid.longterm.strm");
  return(sLog);
}

// Logstream for main app
Poco::LogStream& logStream()
{
  // REVISIT: needs protection?
  static Poco::LogStream sStream(log());
  return(sStream);
}
*/


/* -------------------------------------------------------------------------------------------------
 * Streaming thread
 * 
 * Creates TCP server socket for streaming processed data to client.
 * 
 * Accesses a cache shared with processing thread. 
 * 
 * If no client is connected, the data is just discarded. If a client is connected, data
 * is forwarded to the client as fast as possible. Rx1 data and Rx2 data are sent if requested by a
 * global flag, an additional vector containing various status & processing information is 
 * sent for each IRF.
 * 
 * -------------------------------------------------------------------------------------------------
 */

void* strmTCPServer(void*)
{
  bool tStrmExit = false;

  unsigned int tStrmPort = TCP_STREAM_PORT;
  TCP_server_single tServer;

  try
  {
    std::cout << ANSI_COLOR_MAGENTA << "Streaming: Thread created." << ANSI_COLOR_RESET << std::endl;
    
    /* Bind Thread to single CPU Core */
#ifndef WIN32
    cpu_set_t tCPUSet;
    CPU_ZERO(&tCPUSet);

    CPU_SET(2, &tCPUSet); // CPU #2
    if(pthread_setaffinity_np(pthread_self(), sizeof(tCPUSet), &tCPUSet) != 0) 
    {
      std::cerr << ANSI_COLOR_RED "Streaming::ERROR: pthread_setaffinity_np did not succeed: " << ANSI_COLOR_RESET 
                << strerror(errno) << std::endl;
    }
#endif
    
    /* start server (listening socket) */
    tServer.startServer(tStrmPort, TCP_STR_SEND_TO_MS);
  }
  catch(ilmsens::mliquid::longterm::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "Streaming::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what()  << std::endl 
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;

    // not successful
    tStrmExit = true;
  }
  catch(ilmsens::networking::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "Streaming::Net-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what()  << std::endl 
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;

    // not successful
    tStrmExit = true;
  }
  catch(std::exception& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "Streaming::Std-EXCEPTION: " << ANSI_COLOR_RESET 
              << tErr.what() << std::endl;

    // not successful
    tStrmExit = true;
  }
  catch(...) 
  {
    std::cerr << ANSI_COLOR_RED "Streaming::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;

    // not successful
    tStrmExit = true;
  }

  // streaming thread is always running (always waiting for client)
  // until the EXIT command has been received from client or signal
  while(!tStrmExit) 
  {
    try 
    {
      // give time for synchronisation after a client disconnected
      delay_ms(10);

      std::cout << ANSI_COLOR_MAGENTA << "Streaming: Waiting for Client at port " << std::dec << tStrmPort << " ..." << ANSI_COLOR_RESET << std::endl;

      // accept client connection with timeout
      while (!tStrmExit)
      {
        bool tClient = tServer.acceptClient(TCP_STR_WAIT_TO_MS);
        
        if (tClient)
        {
          // a new client was accepted
          std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: connection initiated with client " << tServer.getClientIP() << " ..." << ANSI_COLOR_RESET << std::endl;
          break;  
        }
        else
        {
          // no client was connected, so update the exit flag
          mMtxExit.lock();
            tStrmExit = mExitShutdwn;
          mMtxExit.unlock();
          /*
          mMtxSharedData.lock();
            tStrmExit = mState.mExitFlag;
          mMtxSharedData.unlock();
          mMtxStreaming.lock();
            tStrmExit = mStrmShutdwn;
          mMtxStreaming.unlock();
          */
          
          // try to drain PR cache, if no client is there or the previous one was disconnected
          mMtxPRCache.lock();
          while(!mPRCache.empty()) 
          {
            mPRCache.pop();
          }
          mMtxPRCache.unlock();
        }
      } // waiting for client
      
      // a client is here?
      if (!tStrmExit)
      {
        /* DATA TRANSFER */

        // count packages streamed
        unsigned int tIdx = 0;
        
        /* Create data tansfer variables */
        TFIFOCacheEntry tTXData;

        TStatusInfo tStatusData(TCP_STR_STATUS_VEC_LEN, 0);
        unsigned int tStatSize = sizeof(TStatusInfo::value_type) * TCP_STR_STATUS_VEC_LEN;
        
        // output debugging messages?
        unsigned int tDebug = TCP_STR_DEBUG_OUTPUT;

        mMtxSharedData.lock();
          tDebug        = mState.mDebFlag;
          //tStrmExit     = mState.mExitFlag;
          bool tSendIRF = mState.mSendIRF;
        mMtxSharedData.unlock();
        /*
        mMtxStreaming.lock();
          mStrmShutdwn     = false;
        mMtxStreaming.unlock();
        */

        /* message output timing */
        auto tStart = std::chrono::system_clock::now();
        //bool tExitStream = false;
        //while(!tStrmExit && !tExitStream) 
        while(!tStrmExit)
        {
          // Waiting for new data in PR cache
          mMtxPRCache.lock();
            bool tEmpty = mPRCache.empty();
          
          if (!tEmpty)
          {
            // new data has arrived from processing thread, so copy and forward it
              tTXData = mPRCache.front();
              mPRCache.pop();
            mMtxPRCache.unlock();

            auto tEnd = std::chrono::system_clock::now();
            auto tElapsed = std::chrono::duration_cast< std::chrono::milliseconds >(tEnd - tStart);
            if (tElapsed.count() >= 200)
            {
              tStart = tEnd;
              std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: sending frame #" << std::dec << tIdx << ANSI_COLOR_RESET << std::endl;
            }

            // fill in status data from cache entry
#if 0
            tStatusData[ 0] = tTXData.mIRFNum;
            tStatusData[ 1] = tTXData.mADCLevel1;
            tStatusData[ 2] = tTXData.mADCLevel2;
            tStatusData[ 3] = tTXData.mCacheSize;
            tStatusData[ 4] = tTXData.mCacheSize;
            tStatusData[ 5] = tTXData.mSenTempVal;
            tStatusData[ 6] = tTXData.mXU4MaxTemp;
            tStatusData[ 7] = float2uint32(tTXData.mSenTemp);
            tStatusData[ 8] = float2uint32(tTXData.mModTemp);
            tStatusData[ 9] = float2uint32(tTXData.mXU4Temp);
            tStatusData[10] = tTXData.mPulseDel;
            tStatusData[11] = float2uint32(tTXData.mPulseAmp);
#endif
//Parameter #     Datentyp        Beschreibung
//-----------     --------        ------------
//0               uint32          Sequenznummer Datensatz oder Timestamp
//1               float           Probentemperatur [°C] gemessen an der Eintauchsonde
//2               float           Sensortemperatur [°C] (UWB-Elektronik)
//3               float           Temperatur [°C] des embedded Computer
//4               float           Maximale Pulsamplitude [rel.]
//5               float           Laufzeit des Pulses [sample index]
//6               uint32          Time stamp seconds (unix time: gettimeofday -> seconds)
//7               uint32          Time stamp microseconds (unix time: gettimeofday -> microseconds)
            tStatusData[0] = tTXData.mIRFNum;
            tStatusData[1] = float2uint32(tTXData.mSenTemp);
            tStatusData[2] = float2uint32(tTXData.mModTemp);
            tStatusData[3] = float2uint32(tTXData.mXU4Temp);
#if TCP_STR_SEND_PULS_PARA
            tStatusData[4] = float2uint32(tTXData.mPulseAmp);
            tStatusData[5] = float2uint32(tTXData.mPulseDel);
#else
            tStatusData[4] = tTXData.mADCLevel1;
            tStatusData[5] = tTXData.mADCLevel2;
#endif
            tStatusData[6] = tTXData.mTS_sec;
            tStatusData[7] = tTXData.mTSusec;

            bool tExitStream = false;
            // send the IRF data to client?
            if (tSendIRF)
            {
              // calculate transmit sizes
              unsigned int tTxSize = static_cast<unsigned int>(sizeof(TRealIRF::value_type) * tTXData.mProcRx1.size());

              // send Rx 1 data
              if (tDebug)
              {
                // for debugging
                std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: sending Rx1 (" << std::dec << tTxSize << " Bytes)" << ANSI_COLOR_RESET << std::endl;
                std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: 1. value of Rx1: " << tTXData.mProcRx1[0] << ANSI_COLOR_RESET << std::endl;
              }

              unsigned int tTxLen = 0;
              char *tBuf = (char *)tTXData.mProcRx1.data();
              while ( tTxLen < tTxSize )
              {
                int tLen = tServer.sendTO((void *)(tBuf + tTxLen), tTxSize - tTxLen);
                if(tLen < 0) 
                {
                  // client connection has been lost
                  tExitStream = true;
                  /*
                  mMtxStreaming.lock();
                    mStrmShutdwn = true;
                    tExitStream  = true;
                  mMtxStreaming.unlock();
                  */
                  break;
                }
                else
                {
                  // count the bytes sent
                  tTxLen += tLen;
                }
              }
              if (tExitStream) break;

              // send Rx 2 data
              if (tDebug)
              {
                // for debugging
                std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: sending Rx2 (" << std::dec << tTxSize << " Bytes)" << ANSI_COLOR_RESET << std::endl;
                std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: 1. value of Rx2: " << tTXData.mProcRx2[0] << ANSI_COLOR_RESET << std::endl;
              }
              tTxLen = 0;
              tBuf = (char *)tTXData.mProcRx2.data();
              while ( tTxLen < tTxSize )
              {
                int tLen = tServer.sendTO((void *)(tBuf + tTxLen), tTxSize - tTxLen);
                if(tLen < 0) 
                {
                  // client connection has been lost
                  tExitStream = true;
                  /*
                  mMtxStreaming.lock();
                    mStrmShutdwn = true;
                    tExitStream  = true;
                  mMtxStreaming.unlock();
                  */
                  break;
                }
                else
                {
                  // count the bytes sent
                  tTxLen += tLen;
                }
              }
              if (tExitStream) break;
            }

            // send status data
            if (tDebug)
            {
              // for debugging
              std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: sending status data (" << std::dec << tStatSize << " Bytes)" << ANSI_COLOR_RESET << std::endl;
              std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: IRF #       : " << tTXData.mIRFNum << std::endl
                                              << "Streaming::INFO: sensor temp.: " << tTXData.mSenTempVal
                                              << ANSI_COLOR_RESET << std::endl;
            }

            unsigned int tTxLen = 0;
            char *tBuf = (char *)tStatusData.data();
            while ( tTxLen < tStatSize )
            {
              int tLen = tServer.sendTO((void *)(tBuf + tTxLen), tStatSize - tTxLen);
              if(tLen < 0) 
              {
                // client connection has been lost
                tExitStream = true;
                /*
                mMtxStreaming.lock();
                  mStrmShutdwn = true;
                  tExitStream  = true;
                mMtxStreaming.unlock();
                */
                break;
              }
              else
              {
                // count the bytes sent
                tTxLen += tLen;
              }
            }
            if (tExitStream) break;

            // package has been sent, so it counts
            tIdx++;
          }
          else
          {
            // cache was empty
            mMtxPRCache.unlock();
            
            // test, if connection is still alive...do non-blocking read with short timeout
            char tBuf[TCP_STR_MSG_SIZE];
            int tLen = tServer.receiveTO((void *)tBuf, TCP_STR_MSG_SIZE, 1);
            if (tLen < 0)
            {
              // client connection has been closed,
              // need to stop streaming TCP connection
              break;
              //tExitStream = true;
              /*
              mMtxStreaming.lock();
                mStrmShutdwn = true;
              mMtxStreaming.unlock();
              */
            }
            else
            {
              // wait some time to lower CPU burden
              delay_ms(10);
            }
          }

          // update configuration and shutdown flags 
          mMtxSharedData.lock();
            tDebug    = mState.mDebFlag;
            //tStrmExit = mState.mExitFlag;
            tSendIRF  = mState.mSendIRF;
          mMtxSharedData.unlock();
          mMtxExit.lock();
            tStrmExit = mExitShutdwn;
          mMtxExit.unlock();
          /*
          mMtxStreaming.lock();
            tExitStream = mStrmShutdwn;
          mMtxStreaming.unlock();
          */
        }
      }

      if (tStrmExit)
      {
        std::cout << ANSI_COLOR_MAGENTA << "Streaming: received EXIT command, ending thread ..." << ANSI_COLOR_RESET << std::endl;
      }
      else
      {
        std::cout << ANSI_COLOR_MAGENTA << "Streaming: no or bad client connection on streaming socket ..." << ANSI_COLOR_RESET << std::endl;
      }
    } 
    catch(ilmsens::mliquid::longterm::Error& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "Streaming::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
                << " Description     : " << tErr.what()  << std::endl 
                << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
                << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
    }
    catch(ilmsens::networking::Error& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "Streaming::Net-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
                << " Description     : " << tErr.what()  << std::endl 
                << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
                << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
    }
    catch(std::exception& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "Streaming::Std-EXCEPTION: " << ANSI_COLOR_RESET 
                << tErr.what() << std::endl;
    }
    catch(...) 
    {
      std::cerr << ANSI_COLOR_RED << "Streaming::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;
    }
    
    // close client's TCP connection
    tServer.closeClient();
  }

  // can go now, TCP server will be shutdown by destructor
  std::cout << ANSI_COLOR_MAGENTA << "Streaming: shutdown complete, exiting thread." << ANSI_COLOR_RESET << std::endl;
  return(NULL);
}

} //namespace streaming
} //namespace longterm
} //namespace mliquid
} //namespace ilmsens
