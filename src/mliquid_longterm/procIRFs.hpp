/// @file procIRFs.hpp
/// @brief Main and support funtions for a basic data processing thread.
///
/// The thread takes raw meaured data from a cache memory and pre-processes it.
/// MLBS-correlation will be done as well as other basic processing steps.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef PROCIRFS_HPP
#define PROCIRFS_HPP


/** Sensor-related processing constants */
#define PR_MLBS_ORDER         12        ///< M-Sequence order of m:liquid sensor
#define PR_MLBS_CLOCK         13.312    ///< RF system clock of m:liquid sensor [GHz]
#define PR_MLBS_NUM_RX        2         ///< number of Rx per sensor module

/** IRF processing constants */
#define PR_IP_FAK             8        ///< interpolation factor for pulse parameter extraction

/** Data forwarding constants */
//#define PR_MIN_UPDATE_WAIT_MS 1000      ///< minimum time interval [ms] between forwarding data to streaming process


/** Thread configuration constants */
#define PR_DEBUG_OUTPUT       1         ///< show debug info about processing operation


namespace ilmsens 
{
  namespace mliquid
  {
    namespace longterm 
    {
      namespace processing
      {

        /** Entry point for processing thread */
        void* procIRFs(void*);

      } //namespace processing
    } //namespace longterm
  } //namespace mliquid
} //namespace ilmsens

#endif //PROCIRFS_HPP
