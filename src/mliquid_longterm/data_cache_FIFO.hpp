/// @file data_cache_FIFO.hpp
/// @brief Definition of buffers and structures for measured data.
///
/// The types defined here can be used for data exchange between functions and threads.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>


#ifndef PROCESSING_BUFFERS_HPP
#define PROCESSING_BUFFERS_HPP

#include <queue>
#include <vector>

#include "ilmsens/processing/IRF_FRF.hpp"

using namespace ilmsens::processing::types;

/**   
 * @defgroup processing_cache_type Cache memories for exchange between threads.
 * @brief Types for exchanging measured data along with additional information.
 * @{
 */  


/** Cache FIFO entry structure */
struct sFIFOCacheEntry
{
  TRealIRF mCorrRx1;        ///< correlated IRF of Rx1
  TRealIRF mCorrRx2;        ///< correlated IRF of Rx2
  TRealIRF mProcRx1;        ///< processed IRF of Rx1
  TRealIRF mProcRx2;        ///< processed IRF of Rx2
  
  TAddInfo mIRFNum;         ///< number/index of IRF in current run (info attached to Rx1 data from sensor)
  TAddInfo mTS_sec;         ///< time stamp: seconds
  TAddInfo mTSusec;         ///< time stamp: microseconds

  TAddInfo mADCLevel1;      ///< ADC Level Rx1 [%]
  TAddInfo mADCLevel2;      ///< ADC Level Rx2 [%]
  TAddInfo mSenTempVal;     ///< Temperature info from sensor (info attached to Rx2 data)
  TAddInfo mXU4MaxTemp;     ///< max. (CPU-) Temperature of XU4 embedded board
  TAddInfo mCacheSize;      ///< current FIFO cache size

  TSampleSmall mSenTemp;    ///< probe temperature [°C]
  TSampleSmall mModTemp;    ///< sensor electronics temperature  [°C]
  TSampleSmall mXU4Temp;    ///< XU4 temperature  [°C]

  TSampleSmall mPulseAmp;   ///< amplitude of measured pulse
  TSampleSmall mPulseDel;   ///< delay of measured pulse [ns]
};

/** Cache FIFO entry type */
typedef struct sFIFOCacheEntry TFIFOCacheEntry; 

/** Cache FIFO type */
typedef std::queue<TFIFOCacheEntry> TFIFOCache;

/** @} processing_cache_type */

#endif //PROCESSING_BUFFERS_HPP
