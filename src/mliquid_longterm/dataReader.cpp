/* dataReader.cpp
 * 
 * Thread function handling Ilmsens m:liquid sensors. Ilmsens HAL library is used
 * to interface with one sensor module (the first one found). Measured data is pushed
 * into a FIFO cache for further processing. Raw data is also stored on disk for further
 * offline analysis.
 */


/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */


// Std. C++ library types / STL 

// File, IO, and Strings
#include <iostream>
#include <sstream>
#include <iomanip>
#include <cstring>
#include <ctime>

#include <cmath>

// String analysis
#include <regex>
#include <string>

// File IO
#include <sys/stat.h>
//windows workaround
#if !defined(S_ISDIR) && defined(S_IFMT) && defined(S_IFDIR)
  #define S_ISDIR(pD) (((pD) & S_IFMT) == S_IFDIR)
#endif

#include <cstdio>

// Time
#include <chrono>                         // For execution time measurements
#ifdef WIN32

  // For gettimeofday
  #define WIN32_LEAN_AND_MEAN
  #include <Windows.h>
  #include <stdint.h> // portable: uint64_t   MSVC: __int64 

  // MSVC defines this in winsock2.h!?
  typedef struct timeval 
  {
    long tv_sec;
    long tv_usec;
  } timeval;

  int gettimeofday(struct timeval * pT, struct timezone * /*pTZ*/)
  {
      // Note: some broken versions only have 8 trailing zero's, the correct epoch has 9 trailing zero's
      // This magic number is the number of 100 nanosecond intervals since January 1, 1601 (UTC)
      // until 00:00:00 January 1, 1970 
      static const uint64_t EPOCH = ((uint64_t) 116444736000000000ULL);

      SYSTEMTIME  system_time;
      FILETIME    file_time;
      uint64_t    time;

      GetSystemTime( &system_time );
      SystemTimeToFileTime( &system_time, &file_time );
      time =  ((uint64_t)file_time.dwLowDateTime )      ;
      time += ((uint64_t)file_time.dwHighDateTime) << 32;

      pT->tv_sec  = (long) ((time - EPOCH) / 10000000L);
      pT->tv_usec = (long) (system_time.wMilliseconds * 1000);
      return 0;
  }

#else
  #include <sys/time.h>                   // For gettimeofday
#endif

/*
// Poco logging includes
#include "Poco/Logger.h"
#include "Poco/LogStream.h"
#include "Poco/Message.h"
* */

// Timing and threading includes
#include <pthread.h>                    // POSIX threads
#include <sched.h>                      // for Realtime Priority

// Shared variables, memories, and synchronisation ressources
#include "shared.hpp"

// Ilmsens HAL library
#include "ilmsens/hal/ilmsens_hal.h"    // Ilmsens HAL library API

// Other Ilmsens stuff
#include "ilmsens/ilmsens_error.h"      // Ilmsens error codes

// XU4 board tools
#include "XU4_tools.hpp"

// processing thread
#include "procIRFs.hpp" 

// main header for module
#include "dataReader.hpp" 


/* -------------------------------------------------------------------------------------------------
 * Defines
 * -------------------------------------------------------------------------------------------------
 */

#define DR_SWAP_RX 0      // swap Rx1 and Rx2 data for processing thread?


namespace ilmsens 
{
namespace mliquid
{
namespace longterm 
{
namespace datareader
{

/* -------------------------------------------------------------------------------------------------
 * Local helper functions
 * -------------------------------------------------------------------------------------------------
 */

#ifdef _MSC_VER
  #define _CRT_SECURE_NO_WARNINGS 

  #pragma warning(push)
  #pragma warning(disable:4996)
#endif

FILE *createRawDataFile(std::string &pID, const char *pExt, unsigned int pSWAvg,  unsigned int pWC, unsigned int pMID)
{
  // check, if given file
  // generate new filename from all tokens found
  std::time_t tCurTime = std::time(NULL); // get current time
  std::stringstream tFileName;
  tFileName << "measurement_of_" << pID
            << "_from_" << std::put_time(std::localtime(&tCurTime), "%F_%H_%M_%S") 
            << std::dec << "_measID_" << std::setw(10) << std::setfill('0') << pMID 
            << "_SWavg_" << std::setw(5) << std::setfill('0') << pSWAvg 
            << "_WC_" << std::setw(5) << pWC
            << std::setfill(' ') << "." << pExt;
  std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: new file name will be '" << tFileName.str() << "'." << ANSI_COLOR_RESET << std::endl;

  std::string tFullName = DR_STOARGE_PATH + tFileName.str();
  //tFD = open(tFullName.c_str(), O_WRONLY | O_CREAT | O_TRUNC | O_NOATIME, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
  FILE *tFID = fopen(tFullName.c_str(), "wb");
  if (tFID == NULL)
  {
    // an error occurred
    std::cerr << ANSI_COLOR_RED << "DataReader::ERROR: file ''" << tFileName.str() 
              << "' could not be created or opened, data will not be stored! Error was: " 
              << strerror(errno) << ANSI_COLOR_RESET << std::endl;
  }
  else
  {
    // new file has been opened
    std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: new file '" << tFullName << "' created ..." << ANSI_COLOR_RESET << std::endl;
  }
  
  return(tFID);
}

#ifdef _MSC_VER
  #undef _CRT_SECURE_NO_WARNINGS 

  #pragma warning(pop)
#endif


// convert int to uint without MSB loss
uint32_t int2uint32(TAddInfo pVal)
{
  union
  {
    TAddInfo uInt32;
    uint32_t uUInt32;
  };
  uInt32 = pVal;

  return(uUInt32);
}


/* -------------------------------------------------------------------------------------------------
 * DataReader thread
 * 
 * Setup attached sensor when started, and begin continuous measurement. Forwards measured data
 * to processing thread (after amplitude scaling) via FIFO cache buffer. Stops measurement
 * upon stop signal. Shutdown of sensor/HAL when signalled (client sends the EXIT command).
 * 
 * -------------------------------------------------------------------------------------------------
 */

#ifdef _MSC_VER
  #define _CRT_SECURE_NO_WARNINGS 

  #pragma warning(push)
  #pragma warning(disable:4996)
#endif

void* dataReader(void*)
{
  // file descriptor for raw data storage
  FILE *tFD = NULL;

  try 
  {
    // bind thread to fixed CPU cores
#ifndef WIN32
    cpu_set_t tCPUSet;
    CPU_ZERO(&tCPUSet);

    CPU_SET(5, &tCPUSet); // CPU #5
    //CPU_SET(6, &tCPUSet); // CPU #6
    if(pthread_setaffinity_np(pthread_self(), sizeof(tCPUSet), &tCPUSet) != 0) 
    {
      std::cerr << ANSI_COLOR_RED "DataReader::ERROR: pthread_setaffinity_np did not succeed: " << ANSI_COLOR_RESET 
                << strerror(errno) << std::endl;
    }
#endif
    
    /*
    // Set Realtime Priority
    struct sched_param tParam;
    sched_getparam(0, &tParam);
    tParam.sched_priority = 99; // Realtime
    
    if(sched_setscheduler(0, SCHED_RR, &tParam) != 0) 
    {
      // Fatal: no real-time priority could be set
      throw ilmsens::mliquid::longterm::Error("DataReader::ERROR: set scheduling priority to realtime failed!", ILMSENS_ERROR_NO_MEMORY, errno);
    }
    sched_getparam(0, &tParam);
    */

    std::cout << ANSI_COLOR_CYAN << "DataReader: thread created" << ANSI_COLOR_RESET << std::endl << std::endl;
    
    //use C++ wrapper around HAL library to ensure no memory leaks will occur 
    //in the thread and take care of proper HAL unloading
    std::cout << ANSI_COLOR_CYAN << "DataReader: initializing m:liquid sensor..." << ANSI_COLOR_RESET << std::endl;

    /* Create sensor group with only first device activated */
    ilmsens::hal::SensorIndexes tSenIdxs;
    tSenIdxs.push_back(1); //only use first sensor attached to the XU4

    /* Initialise HAL library and enumerate sensors */
    ilmsens::hal::Context tIlmsensHAL(HAL_LOG_LEVEL_INIT);
    
    /* Add the sensors to a group and activate them */
    ilmsens::hal::SensorGroup tSenGroup(tSenIdxs);

    /* Get and check sensor ID */
    char tID[ILMSENS_HAL_MOD_ID_BUF_SIZE]; //max length allocated
    int tHALRes = ilmsens_hal_getModId(tSenGroup.at(0), tID, sizeof(tID));
    if (tHALRes < ILMSENS_SUCCESS)
    { 
      throw ilmsens::hal::Error("DataReader::ERROR: getModId failed.", tHALRes);
    }
    if (strlen(tID) == 0) 
    {
      std::cout << ANSI_COLOR_RED << "DataReader::ERROR: Sensor-ID is empty!" << ANSI_COLOR_RESET << std::endl;
      throw ilmsens::hal::Error("DataReader::ERROR: Sensor-ID is empty!", ILMSENS_ERROR_IO);
    }
    else 
    {
      std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: sensor-ID is '" << tID << "'." << ANSI_COLOR_RESET << std::endl;
    }

    /* MLBS sensor properties */
    unsigned tMLBSOrder = DR_MLBS_ORDER;  // M-sequence order
    double   tFClk      = DR_MLBS_CLOCK;  // rf system clock f_0 [GHz]

    // analyse device ID to read MLBS order and clock
    std::string tIDStr(tID);
    std::regex  tParaRegExp("-(\\d\\d)-(\\d\\d)[KMGkmg](\\d\\d\\d)");
    std::smatch tMatch;
    if (std::regex_search(tIDStr, tMatch, tParaRegExp))
    {
      // analyse the pattern match for system parameters
      //for (auto tSub:tMatch) std::cout << ANSI_COLOR_RED << tSub << ANSI_COLOR_RESET << std::endl;
      //std::cout << ANSI_COLOR_RED << tMatch[0] << ANSI_COLOR_RESET << std::endl;
      tMLBSOrder = (unsigned)std::stoi(tMatch[1]);
      tFClk      = (double)std::stoi(tMatch[2]) + ((double)std::stoi(tMatch[3]))/1000.0;
    }    


    /* prepare sensor configuration */
    const unsigned tMasterIdx = 0;                    // Index of master sensor: make first (and only) sensor to be master
    const unsigned tRxCnt = DR_MLBS_NUM_RX;           // Number of Rx-Channels per module

    // ask for SW averaging & debug setting (may be changed by TCP client)
    mMtxSharedData.lock();
      unsigned int tSWAvg = mState.mSW_AVG;
      unsigned int tWC    = mState.mWaitCyc;
      bool         tDebug = mState.mDebFlag;
    mMtxSharedData.unlock();
        
    /* configure the sensor(s) */
    if (tDebug) std::cout << ANSI_COLOR_CYAN << "DataReader::DEBUG: configuring sensor..." << ANSI_COLOR_RESET << std::endl;
    auto tResponseSize = ilmsens::hal::configure(tSenGroup, tMLBSOrder, tFClk, tRxCnt, tSWAvg, tMasterIdx);

    // get sensor properties after setup, e.g. for data processing
    ilmsens_hal_ModInfo tMInfo;
    tHALRes = ilmsens_hal_getModInfo (tSenGroup.at(0), &tMInfo);
    if (tHALRes != ILMSENS_SUCCESS)
    { 
      throw ilmsens::hal::Error("DataReader::ERROR: getModInfo failed.", tHALRes);
    }

    // update/calculate the sensor parameters and output them
    tMLBSOrder = tMInfo.mConfig.mOrder;     // MLBS Order
    tFClk      = tMInfo.mConfig.mClk * 1e9; // RF clock frequency [Hz]
    unsigned int   tMLBSLen = (1 << tMLBSOrder) - 1;  // Length of M-Sequence in samples

    std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: configuration of sensor is :" << std::endl << std::endl; 
    std::cout << "* RF system clock    [GHz]: " << std::dec << std::setprecision(6) << tMInfo.mConfig.mClk << std::endl;
    std::cout << "* MLBS order              : " << std::dec << tMInfo.mConfig.mOrder << std::endl;
    std::cout << "* Prescaler           1/  : " << std::dec << tMInfo.mConfig.mSub << std::endl;
    std::cout << "* Oversampling        x   : " << std::dec << tMInfo.mConfig.mOV << std::endl;
    std::cout << "* Number of Tx            : " << std::dec << tMInfo.mConfig.mTx << std::endl;
    std::cout << "* Number of Rx            : " << std::dec << tMInfo.mConfig.mRx << std::endl;
    std::cout << "* Number of samples per Rx: " << std::dec << tMInfo.mNumSamp << std::endl;
    std::cout << "* MLBS-Length             : " << std::dec << tMLBSLen << std::endl;
    std::cout << "* Hardware averages       : " << std::dec << tMInfo.mHWAvg << std::endl;
    std::cout << "* Software avg. limits    : [" << std::dec << tMInfo.mAvgLim[0] << " .. " << tMInfo.mAvgLim[1] << "]" << std::endl;
    std::cout << "* Software averages       : " << std::dec << tMInfo.mAvg << std::endl;
    std::cout << "* Wait cycle limits       : [" << std::dec << tMInfo.mWaitLim[0] << " .. " << tMInfo.mWaitLim[1] << "]" << std::endl;
    std::cout << "* Wait cycles             : " << std::dec << tMInfo.mWait << std::endl;
    std::cout << "* ADC full scale range [V]: [" << std::dec << std::setprecision(6) << tMInfo.mFSR[0] << " .. " << tMInfo.mFSR[1] << "]" << std::endl;
    std::cout << "* ADC LSB voltage     [mV]: " << std::dec << std::setprecision(6) << tMInfo.mLSB_Volt*1000.0 << std::endl;
    std::cout << "* Int. temperature    [" << '\370' << "C]: " << std::dec << std::setprecision(2) << tMInfo.mTemp << ANSI_COLOR_RESET << std::endl;

    //std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: MLBS-order of device is " << std::dec << tMInfo.mConfig.mOrder << ANSI_COLOR_RESET << std::endl;
    //std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: MLBS-Length will be set to " << std::dec << tMLBSLen << ANSI_COLOR_RESET << std::endl;
    //std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: RF System Clock will be set to " << std::dec << tMInfo.mConfig.mClk << " GHz" << ANSI_COLOR_RESET << std::endl;

    // Make MLBS-Length & other device properties globally known
    mMtxSharedData.lock();
      mState.mMLBSLen = tMLBSLen;
      mState.mClk     = tMInfo.mConfig.mClk;
#ifdef _MSC_VER
      strcpy_s(mState.mID, 1024, tID);
#else
      strcpy(mState.mID, tID);
#endif
      mState.mModInfo = tMInfo;
    mMtxSharedData.unlock();

    /* create memories and caches */
    TFIFOCacheEntry tNewData;

    tNewData.mCorrRx1.resize(tMLBSLen);
    tNewData.mCorrRx2.resize(tMLBSLen);

    /* Create Processing-Thread (only needs to live when sensor has been activated) */
    pthread_t tProcThread;

    mMtxProcessing.lock();
      mProcShutdwn = false;
    mMtxProcessing.unlock();

    // clear processing ready flag
    mMtxSharedData.lock();
      mState.mProcRdy = false;
    mMtxSharedData.unlock();

    int tErg = pthread_create(&tProcThread, NULL, &ilmsens::mliquid::longterm::processing::procIRFs, 0); // processing thread

    if(tErg != 0) 
    {
      std::cout << ANSI_COLOR_RED << "Datareader::ERROR: cannot create processing thread!" << ANSI_COLOR_RESET << std::endl;
      throw ilmsens::mliquid::longterm::Error("DataReader::ERROR: cannot create processing thread!", ILMSENS_ERROR_NO_MEMORY, errno);
    }

    // init is complete, so output fewer log messages
    tIlmsensHAL.setDebugLevel(HAL_LOG_LEVEL_NORM);

    /* Generate ressources for raw-data storage */
    size_t tStoSize   = DR_IRF_HEAD_SIZE; 
    size_t tStoNumBuf = DR_STO_NUM_BUF;
    bool tStoAll = DR_STO_ALL;
    if (tStoAll)
    {
      tStoSize   = tResponseSize;       // not only header, but all IRF data must be stored
      tStoNumBuf = DR_STO_NUM_BUF / 32; // reduce number of buffers in case of full IRFs to make storage chunks smaller
    }
    ilmsens::hal::ImpulseResponse tStoBuf(tStoSize * tStoNumBuf, 0); // buffer for raw data/header data
    unsigned int tNextRow = 0;
    
    // get parts of sensor ID 
    std::string tIDDelim = "::";
    std::vector< std::string > tIDParts;

    std::string::size_type tSubStart = 0;
    auto tSubEnd   = tIDStr.find(tIDDelim);
    while (tSubEnd != std::string::npos)
    {
      tIDParts.push_back(tIDStr.substr(tSubStart, tSubEnd - tSubStart));
      tSubStart = tSubEnd + tIDDelim.length();
      tSubEnd = tIDStr.find(tIDDelim, tSubStart);
    }
    
    //for (auto tSubStr : tIDParts)
    //{
      //std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: found ID part '" << tSubStr << "'." << ANSI_COLOR_RESET << std::endl;
    //}

    // check, if storage is activated and storage path exists
    bool tActStore = DR_ACT_RAW_STORAGE;
    struct stat tStat;
    tErg = stat(DR_STOARGE_PATH, &tStat);
    if ((tErg < 0) || !S_ISDIR(tStat.st_mode))
    {
      // not a valid folder
      tActStore = false;
      std::cerr << ANSI_COLOR_RED "DataReader::ERROR: path for raw data storage '" << DR_STOARGE_PATH << "' cannot be accessed or is not a folder. Error was: " << ANSI_COLOR_RESET 
                << strerror(errno) << std::endl;
    }
    if (tActStore)
    {
      std::cout << ANSI_COLOR_YELLOW << "DataReader::INFO: path for raw data storage will be '" << DR_STOARGE_PATH << "'." << ANSI_COLOR_RESET << std::endl;
    }
    else
    {
      std::cout << ANSI_COLOR_YELLOW << "DataReader::INFO: storage of raw data storage not activated!" << ANSI_COLOR_RESET << std::endl;
    }

    /* sensor & infra-structure has been activated */
    mMtxSharedData.lock();
      mState.mSenAct = true;
    mMtxSharedData.unlock();

    /* wait for initialising of processing thread */
    std::cout << ANSI_COLOR_CYAN << "DataReader::MEAS: waiting for processing thread to become ready ..." << ANSI_COLOR_RESET << std::endl;
    bool tProcRdy = false;
    bool tEndDR   = false;
    while (!tProcRdy && !tEndDR)
    {
      // get processing ready flag
      mMtxSharedData.lock();
        tProcRdy = mState.mProcRdy;
      mMtxSharedData.unlock();

      mMtxExit.lock();
        tEndDR = mExitShutdwn;
      mMtxExit.unlock();

       // sleep some time to decrease CPU burden
      delay_ms(10);
    }

    /* main loop of DataReader - wait for run flag and do measurement if appropriate */

    /*
    mMtxDataReader.lock();
      bool tEndDR = mDtRdShutdwn;
    mMtxDataReader.unlock();
    */
    while(!tEndDR) 
    {
      // check, if measurement request has been received & copy flags and info
      mMtxSharedData.lock();
        bool tRunFlag     = mState.mRunFlag;
        
        tSWAvg            = mState.mSW_AVG;
        tWC               = mState.mWaitCyc;

        unsigned tMID     = mState.mMID;
        
        bool tDoStore     = mState.mSaveFlag && tActStore;
      mMtxSharedData.unlock();
      
      if(tRunFlag) 
      {
        /* new measurement run started */
        std::cout << ANSI_COLOR_CYAN << "DataReader::MEAS: starting new measurement ..." << ANSI_COLOR_RESET << std::endl;
        
        // set the newly retrieved software averages & wait cycles
        tSenGroup.setSWAvgerages(tSWAvg, tWC);
        std::cout << ANSI_COLOR_CYAN << "DataReader::MEAS: set SW averages to " << std::dec << tSWAvg << " and wait cycles to " << tWC << "." << ANSI_COLOR_RESET << std::endl;

        // prepare query of sensor temperature
        //float tModTemp = -300.0;

        // get scaling of measured data because of averages
        double tAvgScale  = (double) tSWAvg * (double) tMInfo.mHWAvg;
        double tMeasScale = 1/tAvgScale * tMInfo.mLSB_Volt; 
        
        // calculate the measurement rate & time between IRFs
        double tMeasRate = tFClk / (double) tMInfo.mConfig.mSub / (double) tMInfo.mNumSamp / (double) (tMInfo.mHWAvg + 1) / (double) (tSWAvg+tWC);
        unsigned tTOMillis = 10 * (unsigned)(1000.0 / tMeasRate);
        std::chrono::milliseconds tTOVal(tTOMillis);

        std::cout << ANSI_COLOR_CYAN << "DataReader::MEAS: measurement rate will be " << std::dec << tMeasRate << " Hz (IRF timeout is set to " << tTOMillis << " ms)." << ANSI_COLOR_RESET << std::endl;

        /* start raw data storage */

        // try to open new file (permissions 0666 ???)
        if (tDoStore)
        {
          // generate new filename from all tokens found and open the file (if possible)
          if (tStoAll)
          {
            tFD = createRawDataFile(tIDParts[2], "raw", tSWAvg,  tWC, tMID);
          }
          else
          {
            tFD = createRawDataFile(tIDParts[2], "head", tSWAvg,  tWC, tMID);
          }
          
          /*
          std::time_t tCurTime = std::time(NULL); // get current time
          std::stringstream tFileName;
          tFileName << "measurement_of_" << tIDParts[2]
                    << "_from_" << std::put_time(std::localtime(&tCurTime), "%F_%H_%M_%S") 
                    << std::dec << "_SWavg_" << std::setw(5) << std::setfill('0') << tSWAvg 
                    << "_WC_" << std::setw(5) << tWC
                    << std::setfill(' ') << ".raw";
          std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: new file name will be '" << tFileName.str() << "'." << ANSI_COLOR_RESET << std::endl;

          std::string tFullName = DR_STOARGE_PATH + tFileName.str();
          //tFD = open(tFullName.c_str(), O_WRONLY | O_CREAT | O_TRUNC | O_NOATIME, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
          tFD = fopen(tFullName.c_str(), "wb");
          if (tFD == NULL)
          {
            // an error occurred
            std::cerr << ANSI_COLOR_RED << "DataReader::ERROR: file ''" << tFileName.str() 
                      << "' could not be created or opened, data will not be stored! Error was: " 
                      << strerror(errno) << ANSI_COLOR_RESET << std::endl;
          }
          else
          {
            // new file has been opened
            std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: new file '" << tFullName << "' created ..." << ANSI_COLOR_RESET << std::endl;
          }
          */
        }
        else
        {
          std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: raw data will not be stored to disk." << ANSI_COLOR_RESET << std::endl;
        }

        /* create buffers for measurement */
        ilmsens::hal::ImpulseResponse tIRF(tResponseSize, 0);
        ilmsens::hal::ImpulseResponse tHead(DR_IRF_HEAD_SIZE, 0);
        tNextRow = 0;
        void *tSrcBuf = (void *)tHead.data();
        if (tStoAll)
        {
          tSrcBuf = (void *)tIRF.data();
        }

        TRealIRF tBuf1(tMLBSLen, 0);
        TRealIRF tBuf2(tMLBSLen, 0);

        /* create new measurement */
        ilmsens::hal::Measurement tMeas(tSenGroup, tResponseSize, HAL_MEAS_MODE);

        // counting variable
        unsigned int tIdx    = 0;
        unsigned int tRunIdx = 0;
        unsigned int tLostWarnCnt = 10;

        // initialize execution-time-measurement
        auto tStart     = std::chrono::system_clock::now();
        //auto tStartTemp = std::chrono::system_clock::now();
        auto tEnd       = std::chrono::system_clock::now();
        
        // detect a new day
        std::time_t tActTimeSec = std::time(NULL); // get current time
        std::tm     tActTM      = *localtime(&tActTimeSec);
        
        /* main measurement loop */
        mMtxSharedData.lock();
          tRunFlag = mState.mRunFlag;
          tDebug   = mState.mDebFlag;
          
          // get number of IRFs before automatic stop of measurement (0 = do not stop automatically)
          unsigned int tNumIRF = mState.mNumIRF;
        mMtxSharedData.unlock();
        while(tRunFlag && !tEndDR) 
        {
          if (tDebug) std::cout << ANSI_COLOR_CYAN << "DataReader: reading dataset #" << std::dec << tRunIdx << " from sensor" << ANSI_COLOR_RESET << std::endl;

          // Get next IRF
          tIRF = tMeas.next(tTOVal);

          //auto tIRFTime = std::chrono::high_resolution_clock::now();
          timeval tNowTS;
          if (gettimeofday(&tNowTS, NULL) != 0)
          {
            std::cout << ANSI_COLOR_RED << "DataReader::WARNING: could not get time stamp, error was: " << strerror(errno) << "." << ANSI_COLOR_RESET << std::endl;

            tNewData.mTS_sec = 0;
            tNewData.mTSusec = 0;
          }
          else
          {
            if (tDebug) std::cout << ANSI_COLOR_CYAN << "DataReader: time stamp is: " << std::ctime((const std::time_t*)&tNowTS.tv_sec) << " | microseconds is: " << tNowTS.tv_usec << " us." << ANSI_COLOR_RESET << std::endl;

            tNewData.mTS_sec = (TAddInfo) tNowTS.tv_sec;
            tNewData.mTSusec = (TAddInfo) tNowTS.tv_usec;
          }

          // Check for lost data
          unsigned int tIdxDiff = tIRF[tMLBSLen] - tIdx;
          if ((tIdxDiff > 1) && tLostWarnCnt)
          {
            std::cout << ANSI_COLOR_RED << "DataReader::WARNING: lost " << std::dec << tIdxDiff-1 << " IRFs" << ANSI_COLOR_RESET << std::endl;
            tLostWarnCnt --; //limit log outputs
          }
          else
          {
            if (tLostWarnCnt < 10) tLostWarnCnt++;
          }

          // Save new index
          tIdx = tIRF[tMLBSLen];
          
          // Copy the IRF to buffer (convert to double)
          for(unsigned int tK = 0; tK < tMLBSLen; tK++) 
          {
#if DR_SWAP_RX
            tBuf2[tK] = (double)tIRF[tK];
            tBuf1[tK] = (double)tIRF[tK + tMLBSLen + 1];
#else
            tBuf1[tK] = (double)tIRF[tK];
            tBuf2[tK] = (double)tIRF[tK + tMLBSLen + 1];
#endif
          }

#if 0
          // Get module temperature every once in a while
          auto tEndTemp = std::chrono::system_clock::now();
          auto tElTemp  = std::chrono::duration_cast< std::chrono::milliseconds > (tEndTemp - tStartTemp);
          if (tElTemp.count() > DR_TEMP_UPDATE_MS)
          {
            // get module info
            tHALRes = ilmsens_hal_getModInfo (tSenGroup.at(0), &tMInfo);
            if (tHALRes != ILMSENS_SUCCESS)
            { 
              std::cout << ANSI_COLOR_RED << "DataReader::WARNING: could not read current module info/sensor tmemperature!" << ANSI_COLOR_RESET << std::endl;
            }
            else
            {
              tModTemp = (float) tMInfo.mTemp;
            }
            
            // reset starting time
            tStartTemp = tEndTemp;
          }
#endif

          // Time Measurement for debugging
          if (tDebug)
          {
            tEnd = std::chrono::system_clock::now();
            std::chrono::duration<double> tElSec = tEnd - tStart;
            
            std::cout << ANSI_COLOR_CYAN << "Datareader::DEBUG: calc. current meas. freq.: " << std::dec << 1/tElSec.count() 
                      << " Hz | nominal meas. freq.: " << tMeasRate 
                      << " Hz" << ANSI_COLOR_RESET << std::endl;
            tStart = std::chrono::system_clock::now();
          }

          // Scale data according to averages and copy to cache entry
          // Also mind the maximum value for ADC level estimation
          double tRx1Max = -1.0;
          double tRx2Max = -1.0;
          double tRx1Cur;
          double tRx2Cur;
          for(unsigned int tJ = 0; tJ < tMLBSLen; tJ++) 
          {
            tRx1Cur = tNewData.mCorrRx1[tJ] = tBuf1[tJ] * tMeasScale;
            tRx2Cur = tNewData.mCorrRx2[tJ] = tBuf2[tJ] * tMeasScale;
            
            // Mind maximum value
            tRx1Cur = fabs(tRx1Cur);
            tRx2Cur = fabs(tRx2Cur);
            if (tRx1Cur > tRx1Max) tRx1Max = tRx1Cur;
            if (tRx2Cur > tRx2Max) tRx2Max = tRx2Cur;
          }

          /* Additional Information */
          tNewData.mIRFNum     = int2uint32(tIRF[tMLBSLen]);                                        // current IRF index
          tNewData.mADCLevel1  = (unsigned int) ceil(tRx1Max / tMInfo.mFSR[1] * 100.0); // ADC level Rx1
          tNewData.mADCLevel2  = (unsigned int) ceil(tRx2Max / tMInfo.mFSR[1] * 100.0); // ADC level Rx2
          tNewData.mSenTempVal = int2uint32(tIRF[tResponseSize-1]);                     // Copy Temperatures (additional IRF info from Rx2 of sensor)
          tNewData.mModTemp    = DR_DEF_TEMP_INVALID; //tModTemp;

          // Reading temperature(s) of XU4 Board
          TTemperatures tXU4Temps = readXU4Temperature();
          
          tNewData.mXU4MaxTemp = *std::max_element(tXU4Temps.begin(), tXU4Temps.end()); // max. Temp. of XU4
          tNewData.mXU4Temp    = (float)tNewData.mXU4MaxTemp;                           // Copy XU4 Temp.


          // get/convert sensor temperatures
          unsigned int tTempRaw  = tNewData.mSenTempVal & DR_PTEMP_VAL_MASK;
          float        tTempVal1 = float(tTempRaw >> DR_PTEMP_VAL_SHIFT) / (float)DR_PTEMP_VAL_SCALE - (float)DR_PTEMP_VAL_OFF;

                       tTempRaw  = (tNewData.mSenTempVal >> 16) & DR_TEMP_VAL_MASK;
          float        tTempVal2 = DR_DEF_TEMP_INVALID;
          if (tTempRaw & DR_TEMP_VAL_SIGN) 
          {
            tTempVal2 = (((float) tTempRaw) - (float)DR_TEMP_VAL_OFF) / (float)DR_TEMP_VAL_SCALE;
          } 
          else 
          {
            tTempVal2 = ((float) tTempRaw) / (float)DR_TEMP_VAL_SCALE;
          }

          // save updated sensor temperatures
          tNewData.mSenTemp    = tTempVal1;                    // Copy probe Temp.
          tNewData.mModTemp    = tTempVal2;                    // Copy module Temp.

          // also copy current header info
          tHead[0] = tIRF[tMLBSLen];
          tHead[1] = tIRF[tResponseSize-1];
          tHead[2] = tNewData.mXU4MaxTemp;
          tHead[3] = (tNewData.mADCLevel2 << 16) + tNewData.mADCLevel1;
          
          /* Store new entry in cache */
          mMtxDRCache.lock();
            tNewData.mCacheSize = (unsigned int)mDRCache.size(); // current size of cache memory (DR cache)
            mDRCache.push(tNewData);               // push data into DR cache
          mMtxDRCache.unlock();
          
          tRunIdx++; // counting finished cycle
          
          //check for automatic stop of measurement
          if (tNumIRF)
          {
            if (tRunIdx >= tNumIRF)
            {
              //stop measuring now
              mMtxSharedData.lock();
                mState.mRunFlag = false;
              mMtxSharedData.unlock();
            }
          }
          
          // --------- DEBUGGING -----------
          if (tDebug) 
          {
            mMtxDRCache.lock();
              size_t tDRCacheSize = mDRCache.size();
            mMtxDRCache.unlock();

            mMtxPRCache.lock();
              size_t tPRCacheSize = mPRCache.size();
            mMtxPRCache.unlock();

            std::cout << ANSI_COLOR_CYAN << "DataReader::DEBUG: DR-cache-size: " 
                      << std::dec << tDRCacheSize 
                      << ", PR-cache-size: " << tPRCacheSize
                      << ANSI_COLOR_RESET << std::endl;
          }
          //--------------------------------
        
          /* Storing raw data */
          if (tFD)
          {
            // update source pointer only in case of storing full IRFs
            if (tStoAll)
            {
              tSrcBuf = (void *)tIRF.data();
            }
            // copy to buffer
            if (tDebug) std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: storing raw data in buffer ..." << ANSI_COLOR_RESET << std::endl;
            memcpy((void *) (tStoBuf.data() + tNextRow*tStoSize), (const void *)tSrcBuf, tStoSize * sizeof(ilmsens::hal::ImpulseResponse::value_type));
            tNextRow ++;
            
            // check, if buffer is full
            if (tNextRow == tStoNumBuf)
            {
              std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: committing raw data to disc ..." << ANSI_COLOR_RESET << std::endl;
              size_t tWritten = fwrite((const void *)(tStoBuf.data()), sizeof(ilmsens::hal::ImpulseResponse::value_type), tNextRow*tStoSize, tFD);
              if (tWritten < (tNextRow*tStoSize))
              {
                std::cerr << ANSI_COLOR_RED << "DataReader::ERROR: could not commit all samples to disc, only" 
                                            << std::dec << tWritten << " / " 
                                            << (tNextRow*tStoSize) << " samples were written!"
                                            << ANSI_COLOR_RESET << std::endl;
                if (ferror(tFD))
                {
                  std::cerr << ANSI_COLOR_RED << "DataReader: error was: " << strerror(errno) << "." << ANSI_COLOR_RESET << std::endl;
                }
              }
              tNextRow = 0;
        
              // detect a new day
              tActTimeSec = std::time(NULL); // get current time
              std::tm tCurTM  = *localtime(&tActTimeSec);
              
              if (tCurTM.tm_mday != tActTM.tm_mday)
              {
                // a new day started, so close old file and create new one
                fclose(tFD);
                if (tStoAll)
                {
                  tFD = createRawDataFile(tIDParts[2], "raw", tSWAvg,  tWC, tMID);
                }
                else
                {
                  tFD = createRawDataFile(tIDParts[2], "head", tSWAvg,  tWC, tMID);
                }

                // mind day for next time
                tActTM = tCurTM;
              }
              
            }
          }
          
          //update state information for measurement loop
          mMtxSharedData.lock();
            tRunFlag = mState.mRunFlag;
            tDebug   = mState.mDebFlag;
          mMtxSharedData.unlock();
          // update exit flag
          mMtxExit.lock();
            tEndDR = mExitShutdwn;
          mMtxExit.unlock();
        } //measurement loop

        // close raw data file and commit rest of buffered data, if still open
        if (tFD)
        {
          if (tNextRow)
          {
            std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: committing remaining raw data to disc ..." << ANSI_COLOR_RESET << std::endl;
            size_t tWritten = fwrite((const void *)(tStoBuf.data()), sizeof(ilmsens::hal::ImpulseResponse::value_type), tNextRow*tStoSize, tFD);
            if (tWritten < (tNextRow*tStoSize))
            {
              std::cerr << ANSI_COLOR_RED << "DataReader::ERROR: could not commit all samples to disc, only" 
                                          << std::dec << tWritten << " / " 
                                          << (tNextRow*tStoSize) << " samples were written!"
                                          << ANSI_COLOR_RESET << std::endl;
              if (ferror(tFD))
              {
                std::cerr << ANSI_COLOR_RED << "DataReader: error was: " << strerror(errno) << "." << ANSI_COLOR_RESET << std::endl;
              }
            }
            tNextRow = 0;
          }
          
          // finally close the file
          fclose(tFD);
          tFD = NULL;
        }
      } 
      else 
      {
        // sleep some time to wait for measurement request and decrease CPU burden
        delay_ms(10);
      }

      //update state information for DataReader
      mMtxExit.lock();
        tEndDR = mExitShutdwn;
      mMtxExit.unlock();
      /*
      mMtxDataReader.lock();
        tEndDR = mDtRdShutdwn;
      mMtxDataReader.unlock();
      */
    } //end of main DataReader while loop

    std::cout << ANSI_COLOR_CYAN << "Datareader: preparing for shutdown ..." << ANSI_COLOR_RESET << std::endl;

    mMtxProcessing.lock();
      mProcShutdwn = true;
    mMtxProcessing.unlock();

    //give time for synchronisation
    delay_ms(10);
  
    std::cout << ANSI_COLOR_CYAN << "DataReader: Waiting for termination of processing-thread ..." << ANSI_COLOR_RESET << std::endl;
    pthread_join(tProcThread, NULL);    // Waiting for Processing-Thread

    // deinit will shorty start in context destructor, so output more log messages again
    tIlmsensHAL.setDebugLevel(HAL_LOG_LEVEL_DEIN);
  }
  catch(ilmsens::mliquid::longterm::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "DataReader::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what()  << std::endl 
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
  }
  catch(ilmsens::hal::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "DataReader::HAL-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what()  << std::endl 
              << " Code was        : " << std::dec << tErr.status_code() << std::endl;
  }
  catch(std::exception& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "DataReader::Std-EXCEPTION: " << ANSI_COLOR_RESET 
              << tErr.what() << std::endl;
  }
  catch(...) 
  {
    std::cerr << ANSI_COLOR_RED << "DataReader::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;
  }

  /* sensor & infra-structure has been closed */
  mMtxSharedData.lock();
    mState.mSenAct = false;
  mMtxSharedData.unlock();

  // close raw data file, if still open
  if (tFD)
  {
    fclose(tFD);
    tFD = NULL;
  }

  // can go now
  std::cout << ANSI_COLOR_CYAN << "DataReader: shutdown complete, exiting thread." ANSI_COLOR_RESET << std::endl;
  return(NULL);
}

#ifdef _MSC_VER
  #undef _CRT_SECURE_NO_WARNINGS 

  #pragma warning(pop)
#endif

} //namespace datareader
} //namespace longterm
} //namespace mliquid
} //namespace ilmsens
