/// @file strmTCPServer.hpp
/// @brief Main and support funtions for a data streaming TCP server thread.
///
/// The TCP server running in a separate thread will accept a single client connection
/// and forward measured data & info presented at a shared FIFO cache to that client. If no client
/// is connected, FIFO data is simply discarded.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef STRMTCPSERVER_HPP
#define STRMTCPSERVER_HPP


/** TCP constants */
#define TCP_STREAM_PORT         8101        ///< TCP port of streaming connection
#define TCP_STR_MSG_SIZE        4           ///< size of streaming dummy message (for checking client alive)
#define TCP_STR_SEND_TO_MS      2000        ///< TCP send timeout [ms]
#define TCP_STR_WAIT_TO_MS      1000        ///< TCP waiting timeout for select() [s]

#define TCP_STR_STATUS_VEC_LEN  8           ///< length of status vector to be sent for each measured frame

#define TCP_STR_SEND_PULS_PARA  0           ///< send pulse parameter (amplitude, delay) or Rx ADC levels?

#define TCP_STR_DEBUG_OUTPUT    0           ///< show debug info about streamed data 


namespace ilmsens 
{
  namespace mliquid
  {
    namespace longterm 
    {
      namespace streaming
      {

        /** Entry point for streaming TCP server thread */
        void* strmTCPServer(void*);

      } //namespace streaming
    } //namespace longterm
  } //namespace mliquid
} //namespace ilmsens

#endif //STRMTCPSERVER_HPP
