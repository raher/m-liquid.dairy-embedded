/* dataReader.cpp
 * 
 * Thread function handling Ilmsens m:explore sensors. Ilmsens HAL library is used
 * to interface with one sensor module (the first one found). Measured data is pushed
 * into a FIFO cache for further processing. Raw data is also stored on disk for further
 * offline analysis.
 */


/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */


// Std. C++ library types / STL 

// File, IO, and Strings
#include <iostream>
#include <sstream>
#include <iomanip>
#include <cstring>
#include <ctime>

// math stuff
#include <cmath>
#include <climits>

#include <numeric>      // std::iota
#include <algorithm>    // std::sort

// String analysis
#include <regex>
#include <string>

// File IO
#include <sys/stat.h>
//windows workaround
#if !defined(S_ISDIR) && defined(S_IFMT) && defined(S_IFDIR)
  #define S_ISDIR(pD) (((pD) & S_IFMT) == S_IFDIR)
#endif

#include <cstdio>

// Time
#include <chrono>                         // For execution time measurements
#ifdef WIN32

  // For gettimeofday
  #define WIN32_LEAN_AND_MEAN
  #include <Windows.h>
  #include <stdint.h> // portable: uint64_t   MSVC: __int64 

  // MSVC defines this in winsock2.h!?
  typedef struct timeval 
  {
    long tv_sec;
    long tv_usec;
  } timeval;

  int gettimeofday(struct timeval * pT, struct timezone * /*pTZ*/)
  {
      // Note: some broken versions only have 8 trailing zero's, the correct epoch has 9 trailing zero's
      // This magic number is the number of 100 nanosecond intervals since January 1, 1601 (UTC)
      // until 00:00:00 January 1, 1970 
      static const uint64_t EPOCH = ((uint64_t) 116444736000000000ULL);

      SYSTEMTIME  system_time;
      FILETIME    file_time;
      uint64_t    time;

      GetSystemTime( &system_time );
      SystemTimeToFileTime( &system_time, &file_time );
      time =  ((uint64_t)file_time.dwLowDateTime )      ;
      time += ((uint64_t)file_time.dwHighDateTime) << 32;

      pT->tv_sec  = (long) ((time - EPOCH) / 10000000L);
      pT->tv_usec = (long) (system_time.wMilliseconds * 1000);
      return 0;
  }

#else
  #include <sys/time.h>                   // For gettimeofday
#endif

/*
// Poco logging includes
#include "Poco/Logger.h"
#include "Poco/LogStream.h"
#include "Poco/Message.h"
*/

// Timing and threading includes
#include <pthread.h>                    // POSIX threads
#include <sched.h>                      // for Realtime Priority

// Shared variables, memories, and synchronisation ressources
#include "shared.hpp"

// Ilmsens HAL library
#include "ilmsens/hal/ilmsens_hal.h"    // Ilmsens HAL library API

// Other Ilmsens stuff
#include "ilmsens/ilmsens_error.h"      // Ilmsens error codes

// XU4 board tools
#include "ilmsens/tools/embedded_MCU_tools.hpp"

// processing thread
#include "procIRFs.hpp" 

// main header for module
#include "dataReader.hpp" 


/* -------------------------------------------------------------------------------------------------
 * Defines
 * -------------------------------------------------------------------------------------------------
 */

#define DR_SWAP_RX 0      // swap Rx1 and Rx2 data for processing thread?

#define DR_HAS_STORAGE 0  // activate raw data storage capabilities?


/* -------------------------------------------------------------------------------------------------
 * Local types
 * -------------------------------------------------------------------------------------------------
 */

typedef std::vector< size_t > TIdxList;

namespace ilmsens 
{
namespace mexplore
{
namespace tomography 
{
namespace datareader
{

/* -------------------------------------------------------------------------------------------------
 * Local helper functions
 * -------------------------------------------------------------------------------------------------
 */

#if DR_HAS_STORAGE

#ifdef _MSC_VER
  #define _CRT_SECURE_NO_WARNINGS 

  #pragma warning(push)
  #pragma warning(disable:4996)
#endif

  FILE *createRawDataFile(std::string &pID, const char *pExt, unsigned int pSWAvg,  unsigned int pWC, unsigned int pMID)
{
  // check, if given file
  // generate new filename from all tokens found
  std::time_t tCurTime = std::time(NULL); // get current time
  std::stringstream tFileName;
  tFileName << "measurement_with_" << pID
            << "_(and_others)_from_" << std::put_time(std::localtime(&tCurTime), "%F_%H_%M_%S") 
            << std::dec << "_measID_" << std::setw(10) << std::setfill('0') << pMID 
            << "_SWavg_" << std::setw(5) << std::setfill('0') << pSWAvg 
            << "_WC_" << std::setw(5) << pWC
            << std::setfill(' ') << "." << pExt;
  std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: new file name will be '" << tFileName.str() << "'." << ANSI_COLOR_RESET << std::endl;

  std::string tFullName = DR_STOARGE_PATH + tFileName.str();
  //tFD = open(tFullName.c_str(), O_WRONLY | O_CREAT | O_TRUNC | O_NOATIME, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);
  FILE *tFID = fopen(tFullName.c_str(), "wb");
  if (tFID == NULL)
  {
    // an error occurred
    std::cerr << ANSI_COLOR_RED << "DataReader::ERROR: file ''" << tFileName.str() 
              << "' could not be created or opened, data will not be stored! Error was: " 
              << strerror(errno) << ANSI_COLOR_RESET << std::endl;
  }
  else
  {
    // new file has been opened
    std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: new file '" << tFullName << "' created ..." << ANSI_COLOR_RESET << std::endl;
  }
  
  return(tFID);
}

#ifdef _MSC_VER
  #undef _CRT_SECURE_NO_WARNINGS 

  #pragma warning(pop)
#endif

#endif


// convert int to uint without MSB loss
uint32_t int2uint32(SampleType pVal)
{
  union
  {
    SampleType uSamp32;
    uint32_t uUInt32;
  };
  uSamp32 = pVal;

  return(uUInt32);
}

// get measurement rate  (only valid for baseband sensors!)
double getMeasRate(ilmsens_hal_ModInfo pSenInfo, unsigned int pSWAvg, unsigned int pWC)
{
  double tSampClk  = (pSenInfo.mConfig.mClk * (double)pSenInfo.mConfig.mOV) * 1e9;
  double tMeasRate = tSampClk / (double)pSenInfo.mConfig.mSub / (double)pSenInfo.mNumSamp / (double)(pSenInfo.mHWAvg + 1) / (double) (pSWAvg+pWC);

  return(tMeasRate);
}

// sort string and keep track of resorted indices
TIdxList sortStrWithIdx(const TMultiModID &pStrings)
{
  // initialize original index locations
  TIdxList tIdxs(pStrings.size());
  std::iota(tIdxs.begin(), tIdxs.end(), 0);

  // sort indexes based on comparing values in pStrings
  std::sort(tIdxs.begin(), tIdxs.end(),
            [&pStrings](size_t tI1, size_t tI2) {return (pStrings[tI1] < pStrings[tI2]);});

  return (tIdxs);
}

/* -------------------------------------------------------------------------------------------------
 * DataReader thread
 * 
 * Setup attached sensor when started, and begin continuous measurement. Forwards measured data
 * to processing thread (after amplitude scaling) via FIFO cache buffer. Stops measurement
 * upon stop signal. Shutdown of sensor/HAL when signalled (client sends the EXIT command).
 * 
 * -------------------------------------------------------------------------------------------------
 */

#ifdef _MSC_VER
  #define _CRT_SECURE_NO_WARNINGS 

  #pragma warning(push)
  #pragma warning(disable:4996)
#endif

void* dataReader(void*)
{

#if DR_HAS_STORAGE
  // file descriptor for raw data storage
  FILE *tFD = NULL;
#endif

  try 
  {
    // bind thread to fixed CPU cores
#ifndef WIN32
    cpu_set_t tCPUSet;
    CPU_ZERO(&tCPUSet);

    CPU_SET(5, &tCPUSet); // CPU #5
    if(pthread_setaffinity_np(pthread_self(), sizeof(tCPUSet), &tCPUSet) != 0) 
    {
      std::cerr << ANSI_COLOR_RED "DataReader::ERROR: pthread_setaffinity_np did not succeed: " << ANSI_COLOR_RESET 
                << strerror(errno) << std::endl;
    }
#endif
    
    std::cout << ANSI_COLOR_CYAN << "DataReader: thread created" << ANSI_COLOR_RESET << std::endl << std::endl;

    // get sensor config defaults from global storage as well as initial SW avg and wait cycles
    mMtxSharedData.lock();
      std::string tMasterID = mState.mMasterID;

      bool tProcFlag        = mState.mProcFlag; 

      unsigned int tSWAvg   = mState.mSW_AVG;
      unsigned int tWC      = mState.mWaitCyc;
      bool         tDebug   = mState.mDebFlag;
    mMtxSharedData.unlock();
    
    //use C++ wrapper around HAL library to ensure no memory leaks will occur 
    //in the thread and take care of proper HAL init & unloading
    std::cout << ANSI_COLOR_CYAN << "DataReader: initializing m:explore.tomography sensors..." << ANSI_COLOR_RESET << std::endl;

    /* Initialise HAL library and enumerate sensors */
    ilmsens::hal::Context tHALSession(HAL_LOG_LEVEL_INIT);

    // get module ID strings and sort them
    //const TMultiModID& tIDs = tHALSession.getIDs();
    TMultiModID tIDs(tHALSession.getIDs());
    TIdxList    tModIdxs    = sortStrWithIdx(tIDs);
    std::sort(tIDs.begin(), tIDs.end());

    /* Create sensor group with all enumerated sensors and identify master module as well as configuration from ID strings */
    ilmsens::hal::SensorIndexes tSenIdxs;

    int      tMasterIdx = -1;             // default: no sensors
    unsigned tMLBSOrder = DR_MLBS_ORDER;  // default M-sequence order
    unsigned tOV        = DR_OV_DEF;      // default oversampling factor for m:explore.tomography sensors
    double   tFClk      = DR_MLBS_CLOCK;  // default RF system clock f_0 [GHz]
    std::regex  tParaRegExp("-(\\d{1,2})-(\\d+)[KMGkmg](\\d{2,})");
    std::smatch tMatch;
    std::string tConfigSensorID = "<unknown>";

    // iterate over all received sensor IDs
    unsigned int tNumSen = tHALSession.sensorCount();
    unsigned int tS;
    bool tFirstConfig = true;
    for (tS = 0; tS < tNumSen; tS ++)
    {
      // populate sensor index list
      //tSenIdxs.push_back(tS+1);
      ilmsens::hal::SensorIndex tCurSenIdx = static_cast< ilmsens::hal::SensorIndex >(tModIdxs[tS]+1);
      tSenIdxs.push_back(tCurSenIdx);

      // get current sensor ID for various analysis
      //std::string tCurID = tIDs[tS];
      std::string tCurID = tIDs[tS];
      std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: sensor-ID of sensor #" << std::dec << tS << " is '" << tCurID << "'." << ANSI_COLOR_RESET << std::endl;

      // also save middle ID of this sensor because it defined the config
      std::string tIDDelim = "::";
      std::vector< std::string > tIDParts;

      std::string::size_type tSubStart = 0;
      auto tSubEnd   = tCurID.find(tIDDelim);
      while (tSubEnd != std::string::npos)
      {
        tIDParts.push_back(tCurID.substr(tSubStart, tSubEnd - tSubStart));
        tSubStart = tSubEnd + tIDDelim.length();
        tSubEnd = tCurID.find(tIDDelim, tSubStart);
      }

      // also analyse sensor ID to identify the master module
      if ((tCurID.find(tMasterID) != std::string::npos) && (tMasterIdx <= 0))
      {
        std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: this sensor will be declared as master module of the group!" << ANSI_COLOR_RESET << std::endl;
        tMasterIdx = tS + 1;
        // replace master ID search string with actual master ID
        tMasterID  = tIDParts[2];
      }

      // get MLBS sensor properties from ID
      if (std::regex_search(tCurID, tMatch, tParaRegExp))
      {
        // analyse the pattern match for system parameters
        //for (auto tSub:tMatch) std::cout << ANSI_COLOR_RED << tSub << ANSI_COLOR_RESET << std::endl;
        //std::cout << ANSI_COLOR_RED << tMatch[0] << ANSI_COLOR_RESET << std::endl;
        unsigned tCurOrder = (unsigned)std::stoi(tMatch[1]);
        double   tCurFClk  = (double)std::stoi(tMatch[2]) + ((double)std::stoi(tMatch[3]))/1000.0;

        //is it an ECC sensor?
        unsigned tCurOV = 1;
        if (tCurID.find("ECC") != std::string::npos)
        {
          // has 3x oversampling
          tCurOV = 3;
        }
        std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: extracted config from current ID: MLBS order " << std::dec << tCurOrder << " | OV " << tCurOV << " | RF clock " << std::setprecision(6) << tCurFClk << " GHz." << ANSI_COLOR_RESET << std::endl;

        // save or compare extracted config
        if (tFirstConfig)
        {
          // just apply it
          tMLBSOrder = tCurOrder;
          tOV        = tCurOV;
          tFClk      = tCurFClk;

          //save middle part of sensor ID for sensor that sets config
          tConfigSensorID = tIDParts[2];

          //next time compare config to current settings
          tFirstConfig = false;
        }
        else
        {
          // compare with previous config (should be the same)
          if ((tMLBSOrder != tCurOrder) ||
              (tOV        != tCurOV) ||
              (tFClk      != tCurFClk)
             )
          {
            // not the same, so either ID or its analysis were flawed or the sensor will not work as expected
            std::cout << ANSI_COLOR_CYAN << "DataReader::WARNING: current config does not match previous sensor (MLBS order " << std::dec << tMLBSOrder << " | OV " << tOV << " | RF clock " << std::setprecision(6) << tFClk << " GHz)!" << ANSI_COLOR_RESET << std::endl;
          }
        } // first config
      }
      else
      {
        // could not analyse the ID
        std::cout << ANSI_COLOR_CYAN << "DataReader::WARNING: this sensor's ID could not be analysed for sensor configuration! Keeping current settings ..." << ANSI_COLOR_RESET << std::endl;
      } // reg exp analysis

    } // sensor ID iteration

    /* Add the sensors to a group and activate them */
    ilmsens::hal::SensorGroup tSenGroup(tSenIdxs);

    /* setup all sensors with the same config, synch them and reset transmitters */
    std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: configuring sensor(s) ..." << ANSI_COLOR_RESET << std::endl;

    auto tResponseSize = tSenGroup.setupSensors(tMLBSOrder, tOV, tFClk);
    tSenGroup.setMaster(tMasterIdx);
    tSenGroup.resetTx();
    tSenGroup.setTxPowerDown(false);
    tSenGroup.setSWAvgerages(tSWAvg, tWC);

    // finally get module infos
    auto tModInfos = tSenGroup.getModInfos();

    // update/calculate the sensor parameters (take first sensor) and output them
    tMLBSOrder = tModInfos[0].mConfig.mOrder;     // MLBS Order
    tFClk      = tModInfos[0].mConfig.mClk * 1e9; // RF clock frequency [Hz]
    unsigned int   tMLBSLen = ((1 << tMLBSOrder) - 1) * tModInfos[0].mConfig.mOV;  // Length of M-Sequence in samples

    std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: configuration of first sensor is :" << std::endl << std::endl; 
    std::cout << "* RF system clock    [GHz]: " << std::dec << std::setprecision(6) << tModInfos[0].mConfig.mClk << std::endl;
    std::cout << "* MLBS order              : " << std::dec << tModInfos[0].mConfig.mOrder << std::endl;
    std::cout << "* Prescaler           1/  : " << std::dec << tModInfos[0].mConfig.mSub << std::endl;
    std::cout << "* Oversampling        x   : " << std::dec << tModInfos[0].mConfig.mOV << std::endl;
    std::cout << "* Number of Tx            : " << std::dec << tModInfos[0].mConfig.mTx << std::endl;
    std::cout << "* Number of Rx            : " << std::dec << tModInfos[0].mConfig.mRx << std::endl;
    std::cout << "* Number of samples per Rx: " << std::dec << tModInfos[0].mNumSamp << std::endl;
    std::cout << "* MLBS-Length             : " << std::dec << tMLBSLen << std::endl;
    std::cout << "* Hardware averages       : " << std::dec << tModInfos[0].mHWAvg << std::endl;
    std::cout << "* Software avg. limits    : [" << std::dec << tModInfos[0].mAvgLim[0] << " .. " << tModInfos[0].mAvgLim[1] << "]" << std::endl;
    std::cout << "* Software averages       : " << std::dec << tModInfos[0].mAvg << std::endl;
    std::cout << "* Wait cycle limits       : [" << std::dec << tModInfos[0].mWaitLim[0] << " .. " << tModInfos[0].mWaitLim[1] << "]" << std::endl;
    std::cout << "* Wait cycles             : " << std::dec << tModInfos[0].mWait << std::endl;
    std::cout << "* ADC full scale range [V]: [" << std::dec << std::setprecision(6) << tModInfos[0].mFSR[0] << " .. " << tModInfos[0].mFSR[1] << "]" << std::endl;
    std::cout << "* ADC LSB voltage     [mV]: " << std::dec << std::setprecision(6) << tModInfos[0].mLSB_Volt*1000.0 << std::endl;
    std::cout << "* Int. sensor temp.   [" << '\370' << "C]: " << std::dec << std::setprecision(2) << tModInfos[0].mTemp << ANSI_COLOR_RESET << std::endl;

    double tSampClk  = (tFClk * (double)tModInfos[0].mConfig.mOV);
    double tMeasRate = getMeasRate(tModInfos[0], tSWAvg, tWC);
    unsigned int tNumRx = tNumSen * tModInfos[0].mConfig.mRx;

    // Make MLBS-Length & other device properties globally known
    mMtxSharedData.lock();
      // save sensor config to global state
      mState.mMLBSLen   = tMLBSLen;
      mState.mModIDs    = tIDs;
      mState.mModInfos  = tModInfos;
      mState.mMasterID  = tMasterID;
      mState.mMeasRate  = tMeasRate;
      mState.mSampClk   = tSampClk / 1e9;

      // sensors have been activated
      mState.mNumSen    = tNumSen;
      mState.mSenAct    = true;
    mMtxSharedData.unlock();

    /* create memories and caches */
    TFIFOCacheEntry tNewData;
    tNewData.mRawIRFs.resize(tResponseSize, 0);
    tNewData.mSclIRFs.resize(tResponseSize, 0.0);
    tNewData.mRxOffs.resize(tNumRx, 0);

//    TRealIRF tSingleRx(tMLBSLen, 0.0);
//    tNewData.mIRFs.resize(tNumRx, tSingleRx);

    // additional info buffers
    tNewData.mIRFNums.resize(tNumSen, 0);
    tNewData.mADCLevels.resize(tNumRx, 0);
    tNewData.mSenTempVals.resize(tNumSen, 0);
    tNewData.mSenTemps.resize(tNumSen, 0.0);
    tNewData.mModTemps.resize(tNumSen, 0.0);

    /* Processing-Thread (only needs to live when sensors have been activated) */
    pthread_t tProcThread;
    // clear it (type-independent)
    memset((void *)&tProcThread, 0, sizeof(tProcThread));

    mMtxProcessing.lock();
      mProcShutdwn = false;
    mMtxProcessing.unlock();

    // clear processing ready flag
    mMtxSharedData.lock();
      mState.mProcRdy = false;
    mMtxSharedData.unlock();

    // create processing thread only if configured to do so
    if (tProcFlag)
    {
      int tErg = pthread_create(&tProcThread, NULL, &ilmsens::mexplore::tomography::processing::procIRFs, 0); // processing thread

      if(tErg != 0) 
      {
        std::cout << ANSI_COLOR_RED << "Datareader::ERROR: cannot create processing thread!" << ANSI_COLOR_RESET << std::endl;
        throw ilmsens::mexplore::tomography::Error("DataReader::ERROR: cannot create processing thread!", ILMSENS_ERROR_NO_MEMORY, errno);
      }
    }

    // init is complete, so output fewer log messages
    tHALSession.setDebugLevel(HAL_LOG_LEVEL_NORM);

#if DR_HAS_STORAGE

    /* Generate ressources for raw-data storage */
    size_t tStoSize   = DR_IRF_HEAD_SIZE; 
    size_t tStoNumBuf = DR_STO_NUM_BUF;
    bool tStoAll = DR_STO_ALL;
    if (tStoAll)
    {
      tStoSize   = tResponseSize;       // not only header, but all IRF data must be stored
      tStoNumBuf = DR_STO_NUM_BUF / 32; // reduce number of buffers in case of full IRFs to make storage chunks smaller
    }
    ilmsens::hal::ImpulseResponse tStoBuf(tStoSize * tStoNumBuf, 0); // buffer for raw data/header data
    unsigned int tNextRow = 0;
    
    // check, if storage is activated and storage path exists
    bool tActStore = DR_ACT_RAW_STORAGE;
    struct stat tStat;
    tErg = stat(DR_STOARGE_PATH, &tStat);
    if ((tErg < 0) || !S_ISDIR(tStat.st_mode))
    {
      // not a valid folder
      tActStore = false;
      std::cerr << ANSI_COLOR_RED "DataReader::ERROR: path for raw data storage '" << DR_STOARGE_PATH << "' cannot be accessed or is not a folder. Error was: " << ANSI_COLOR_RESET 
                << strerror(errno) << std::endl;
    }
    if (tActStore)
    {
      std::cout << ANSI_COLOR_YELLOW << "DataReader::INFO: path for raw data storage will be '" << DR_STOARGE_PATH << "'." << ANSI_COLOR_RESET << std::endl;
    }
    else
    {
      std::cout << ANSI_COLOR_YELLOW << "DataReader::INFO: storage of raw data storage not activated!" << ANSI_COLOR_RESET << std::endl;
    }

#endif

    /* wait for initialising of processing thread */
    bool tEndDR   = false;
    if (tProcFlag)
    {
      std::cout << ANSI_COLOR_CYAN << "DataReader::MEAS: waiting for processing thread to become ready ..." << ANSI_COLOR_RESET << std::endl;
      bool tProcRdy = false;
      while (!tProcRdy && !tEndDR)
      {
        // get processing ready flag
        mMtxSharedData.lock();
          tProcRdy = mState.mProcRdy;
        mMtxSharedData.unlock();

        mMtxExit.lock();
          tEndDR = mExitShutdwn;
        mMtxExit.unlock();

         // sleep some time to decrease CPU burden
        delay_ms(10);
      } //waiting for processing to start up
    } //procflag

    /* main loop of DataReader - wait for run flag and do measurement if requested */
    while(!tEndDR) 
    {
      // check, if measurement request has been received & copy flags and info
      mMtxSharedData.lock();
        bool tRunFlag     = mState.mRunFlag;
        bool tSyncFlag    = mState.mSyncFlag;
        bool tCalSyncFlag = mState.mCalSyncFlag;

        tSWAvg            = mState.mSW_AVG;
        tWC               = mState.mWaitCyc;

        unsigned tMID     = mState.mMID;

        size_t tSelSen       = mState.mSelSen;
        unsigned int tPDMode = mState.mPDMode;
        unsigned int tPDLen  = mState.mPDPatLen;
        TPatRegs tPDPat      = mState.mPDPattern;

#if DR_HAS_STORAGE
        bool tDoStore     = mState.mSaveFlag && tActStore;
#endif
      mMtxSharedData.unlock();

      // check for a synchronisation calibration request
      if(tCalSyncFlag)
      {
        // calibrate all sensors' synch interfaces
        std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: starting master/slave synchronisation calibration ..." << ANSI_COLOR_RESET << std::endl;
        tSenGroup.doSynchCal();

        // once synchronisation is done, we can unset the synch request flag
        tCalSyncFlag = false;
        mMtxSharedData.lock();
          mState.mCalSyncFlag = false;
        mMtxSharedData.unlock();
      }

      // check for a synchronisation request
      if(tSyncFlag)
      {
        // (re-)syncronise all sensors (using internal synch and run flags)
        std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: starting master/slave synchronisation ..." << ANSI_COLOR_RESET << std::endl;
        tSenGroup.doMasterSlaveSynch();

        // once synchronisation is done, we can unset the synch request flag
        tSyncFlag = false;
        mMtxSharedData.lock();
          mState.mSyncFlag = false;
        mMtxSharedData.unlock();
      }

      // check for a power down pattern change request
      if(tPDMode != PD_DO_NOTHING)
      {
        //depending on mode, different action should be taken
        std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: applying requested power down operation ..." << ANSI_COLOR_RESET << std::endl;
        switch(tPDMode)
        {
          // mute Tx on all sensors
          case PD_ALL_TX_OFF:
            tSenGroup.setTxPowerDown(true);
            break;

          // unmute Tx on all sensors
          case PD_ALL_TX_ON:
            tSenGroup.setTxPowerDown(false);
            break;

          // mute Tx on selected sensor
          case PD_MOD_TX_OFF:
            tSenGroup.setTxPowerDown(true, (unsigned int)tSelSen+1);
            break;

          // unmute Tx on selected sensor
          case PD_MOD_TX_ON:
            tSenGroup.setTxPowerDown(false, (unsigned int)tSelSen+1);
            break;

          // apply Tx pattern to selected sensor
          case PD_MOD_TX_PAT:
            tSenGroup.setTxPowerDownPattern((unsigned int)tSelSen+1, tPDLen, tPDPat);
            break;

          // unknown PD mode
          default:
            std::cout << ANSI_COLOR_YELLOW << "DataReader::WARNING: received request for unknown power down action (request mode was " << tPDMode << ")! Ignoring the request." << ANSI_COLOR_RESET << std::endl;

        }

        // once power down feature was changed, reset the request mode
        tPDMode = PD_DO_NOTHING;
        mMtxSharedData.lock();
          mState.mPDMode = PD_DO_NOTHING;
        mMtxSharedData.unlock();
      }

      // shall we start a new measurement?
      if(tRunFlag) 
      {
        /* new measurement run started */
        std::cout << ANSI_COLOR_CYAN << "DataReader::MEAS: starting new measurement (ID " << std::dec << tMID <<") ..." << ANSI_COLOR_RESET << std::endl;

        // set the newly retrieved software averages & wait cycles
        tSenGroup.setSWAvgerages(tSWAvg, tWC);
        std::cout << ANSI_COLOR_CYAN << "DataReader::MEAS: set SW averages to " << std::dec << tSWAvg << " and wait cycles to " << tWC << "." << ANSI_COLOR_RESET << std::endl;

        // get scaling of measured data as influenced by total averages
        double tAvgScale  = (double) tSWAvg * (double) tModInfos[0].mHWAvg;
        double tMeasScale = 1/tAvgScale * tModInfos[0].mLSB_Volt; 

        // calculate the new measurement rate & time between IRFs
        tMeasRate = getMeasRate(tModInfos[0], tSWAvg, tWC);
        unsigned tTOMillis = 100 * (unsigned)(1000.0 / tMeasRate);
        std::chrono::milliseconds tTOVal(tTOMillis);

        std::cout << ANSI_COLOR_CYAN << "DataReader::MEAS: measurement rate will be " << std::dec << tMeasRate << " Hz (IRF timeout is set to " << tTOMillis << " ms)." << ANSI_COLOR_RESET << std::endl;

        // output current sensor configuration if debug output is enabled
        if (tDebug)
        {
          ilmsens_hal_ModInfo tSenInfo;
          int tHALRes = ilmsens_hal_getModInfo (tSenGroup.at(0), &tSenInfo);
          if (tHALRes != ILMSENS_SUCCESS)
          { 
            throw ilmsens::hal::Error("DataReader::ERROR: getModInfo failed.", tHALRes);
          }
          std::cout << ANSI_COLOR_CYAN << "DataReader::DEBUG: configuration of 1st sensor is currently :" << std::endl << std::endl; 
          std::cout << "* RF system clock    [GHz]: " << std::dec << std::setprecision(6) << tSenInfo.mConfig.mClk << std::endl;
          std::cout << "* MLBS order              : " << std::dec << tSenInfo.mConfig.mOrder << std::endl;
          std::cout << "* Prescaler           1/  : " << std::dec << tSenInfo.mConfig.mSub << std::endl;
          std::cout << "* Oversampling        x   : " << std::dec << tSenInfo.mConfig.mOV << std::endl;
          std::cout << "* Number of Tx            : " << std::dec << tSenInfo.mConfig.mTx << std::endl;
          std::cout << "* Number of Rx            : " << std::dec << tSenInfo.mConfig.mRx << std::endl;
          std::cout << "* Number of samples per Rx: " << std::dec << tSenInfo.mNumSamp << std::endl;
          std::cout << "* Hardware averages       : " << std::dec << tSenInfo.mHWAvg << std::endl;
          std::cout << "* Software avg. limits    : [" << std::dec << tSenInfo.mAvgLim[0] << " .. " << tSenInfo.mAvgLim[1] << "]" << std::endl;
          std::cout << "* Software averages       : " << std::dec << tSenInfo.mAvg << std::endl;
          std::cout << "* Wait cycle limits       : [" << std::dec << tSenInfo.mWaitLim[0] << " .. " << tSenInfo.mWaitLim[1] << "]" << std::endl;
          std::cout << "* Wait cycles             : " << std::dec << tSenInfo.mWait << std::endl;
          std::cout << "* Total averages          : " << std::dec << tAvgScale << std::endl;
          std::cout << "* ADC full scale range [V]: [" << std::dec << std::setprecision(6) << tSenInfo.mFSR[0] << " .. " << tSenInfo.mFSR[1] << "]" << std::endl;
          std::cout << "* ADC LSB voltage     [mV]: " << std::dec << std::setprecision(6) << tSenInfo.mLSB_Volt*1000.0 << std::endl;
          std::cout << "* Sample scaling factor   : " << std::dec << std::setprecision(6) << tMeasScale << std::endl;
          std::cout << "* Int. temperature    [" << '\370' << "C]: " << std::dec << std::setprecision(2) << tSenInfo.mTemp << ANSI_COLOR_RESET << std::endl;
        }

#if DR_HAS_STORAGE
        /* start raw data storage */

        // try to open new file (permissions 0666 ???)
        if (tDoStore)
        {
          // generate new filename from all tokens found and open the file (if possible)
          if (tStoAll)
          {
            tFD = createRawDataFile(tConfigSensorID, "raw", tSWAvg,  tWC, tMID);
          }
          else
          {
            tFD = createRawDataFile(tConigSensorID, "head", tSWAvg,  tWC, tMID);
          }
        }
        else
        {
          std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: raw data will not be stored to disk." << ANSI_COLOR_RESET << std::endl;
        }
#endif

        /* create buffers for measurement */
        ilmsens::hal::ImpulseResponse tRawData(tResponseSize, 0);

#if DR_HAS_STORAGE
        ilmsens::hal::ImpulseResponse tHead(DR_IRF_HEAD_SIZE, 0);

        tNextRow = 0;
        void *tSrcBuf = (void *)tHead.data();
        if (tStoAll)
        {
          tSrcBuf = (void *)tRawData.data();
        }
#endif

        /* create & start new measurement */
        ilmsens::hal::Measurement tMeas(tSenGroup, tResponseSize, HAL_MEAS_MODE);

        // counting variable
        unsigned int tIdx    = 0;
        unsigned int tRunIdx = 0;
        unsigned int tLostWarnCnt = 10;

        // initialize execution-time-measurement
        auto tStart     = std::chrono::system_clock::now();
        auto tEnd       = tStart;

#if DR_HAS_STORAGE
        // detect a new day
        std::time_t tActTimeSec = std::time(NULL); // get current time
        std::tm     tActTM      = *localtime(&tActTimeSec);
#endif        

        // get initial state of flags and timing variables
        mMtxSharedData.lock();
          tRunFlag = mState.mRunFlag;
          tDebug   = mState.mDebFlag;

          // get number of IRFs before automatic stop of measurement (0 = do not stop automatically)
          unsigned int tNumIRF = mState.mNumIRF;
        mMtxSharedData.unlock();

        /* main measurement loop */
        while(tRunFlag && !tEndDR) 
        {
          // try to get get next IRF
//          //tRawData = tMeas.next(tTOVal);
//          if (!tMeas.try_next(tRawData, tTOVal))
//          {
//            //no data returned
//            std::cout << ANSI_COLOR_RED << "DataReader::WARNING: could not get new measurement data within timeout of " << tTOMillis << "ms. Trying again ..." << ANSI_COLOR_RESET << std::endl;
          if (!tMeas.measurementReady())
          {
            // no data so far, so wait a bit
            delay_ms(1);

            // update state information for measurement loop and continue
            mMtxSharedData.lock();
              //flags
              tRunFlag = mState.mRunFlag;
              tDebug   = mState.mDebFlag;
            mMtxSharedData.unlock();

            // update exit flag
            mMtxExit.lock();
              tEndDR = mExitShutdwn;
            mMtxExit.unlock();

            //try again
            continue;
          }
          else
          {
            // read out the next raw IRFs
            tMeas.readMeasurement(tRawData);
          }
          if (tDebug) std::cout << ANSI_COLOR_CYAN << "DataReader: received dataset #" << std::dec << tRunIdx << " from sensor" << ANSI_COLOR_RESET << std::endl;

          timeval tNowTS;
          if (gettimeofday(&tNowTS, NULL) != 0)
          {
            std::cout << ANSI_COLOR_RED << "DataReader::WARNING: could not get time stamp, error was: " << strerror(errno) << "." << ANSI_COLOR_RESET << std::endl;

            tNewData.mTS_sec = 0;
            tNewData.mTSusec = 0;
          }
          else
          {
            if (tDebug)
            {
              std::time_t tTimeSec =  static_cast< std::time_t>(tNowTS.tv_sec);
              std::cout << ANSI_COLOR_CYAN << "DataReader: time stamp is: " << std::ctime((const std::time_t*)&tTimeSec) << " | microseconds is: " << tNowTS.tv_usec << " us." << ANSI_COLOR_RESET << std::endl;
            }

            tNewData.mTS_sec = (TAddInfo) tNowTS.tv_sec;
            tNewData.mTSusec = (TAddInfo) tNowTS.tv_usec;
          }

          // Check for lost data
          unsigned int tIdxDiff = tRawData[tMLBSLen] - tIdx;
          if ((tIdxDiff > 1) && tLostWarnCnt)
          {
            std::cout << ANSI_COLOR_RED << "DataReader::WARNING: lost " << std::dec << tIdxDiff-1 << " IRFs" << ANSI_COLOR_RESET << std::endl;
            tLostWarnCnt --; //limit log outputs
          }
          else
          {
            if (tLostWarnCnt < 10) tLostWarnCnt++;
          }

          // Save new index
          tIdx = tRawData[tMLBSLen];

          // Time Measurement for debugging
          if (tDebug)
          {
            tEnd = std::chrono::system_clock::now();
            std::chrono::duration<double> tElSec = tEnd - tStart;

            std::cout << ANSI_COLOR_CYAN << "Datareader::DEBUG: calc. current meas. freq.: " << std::dec << 1/tElSec.count() 
                      << " Hz | nominal meas. freq.: " << tMeasRate 
                      << " Hz" << ANSI_COLOR_RESET << std::endl;
            tStart = std::chrono::system_clock::now();
          }

          /* Copy the raw data to buffer */
          tNewData.mRawIRFs = tRawData;

          /* Scaled data and extract extra infos for all Rxs and put it into buffer */

          // calculate sample values (convert to double) and separate Rx channels
          // - get ADC level for all Rxs
          // - get temperature values from all sensors
          unsigned int tRxOff = 0;
          for (unsigned int tR = 0; tR < tNumRx; tR ++)
          {
            //store index of current Rx
            tNewData.mRxOffs[tR] = tRxOff;
            double tRxCur;
            double tRxMax = -1.0; // maximum magnitude found for current Rx
            for(tS = 0; tS < tMLBSLen; tS ++)
            {
              // Scale data according to averages and copy to cache entry
              tRxCur = tNewData.mSclIRFs[tRxOff] = (double)tRawData[tRxOff] * tMeasScale;

              // Mind maximum value
              tRxCur = fabs(tRxCur);
              if (tRxCur > tRxMax) 
              {
                tRxMax    = tRxCur;
              }
              tRxOff ++;
            }

            //calculate ADC level of current Rx
            tNewData.mADCLevels[tR] = (unsigned int) ceil(tRxMax / tModInfos[0].mFSR[1] * 100.0);

            //store additional info sample value depending on Rx number
            uint32_t tCurAddVal  = int2uint32(tRawData[tRxOff]);
            unsigned int tSenNum = tR >> 1;
            if (tR & 0x00000001)
            {
              // 2nd Rx has raw temperature values
              tNewData.mSenTempVals[tSenNum] = tCurAddVal;

              // also convert value to actual temperatures
              unsigned int tTempRaw  = tCurAddVal & DR_PTEMP_VAL_MASK;
              TSample      tTempVal1 = TSample(tTempRaw >> DR_PTEMP_VAL_SHIFT) / (TSample)DR_PTEMP_VAL_SCALE - (TSample)DR_PTEMP_VAL_OFF;

                           tTempRaw  = (tCurAddVal >> 16) & DR_TEMP_VAL_MASK;
              TSample      tTempVal2 = DR_DEF_TEMP_INVALID;
              if (tTempRaw & DR_TEMP_VAL_SIGN) 
              {
                tTempVal2 = (((TSample) tTempRaw) - (TSample)DR_TEMP_VAL_OFF) / (TSample)DR_TEMP_VAL_SCALE;
              } 
              else 
              {
                tTempVal2 = ((TSample) tTempRaw) / (TSample)DR_TEMP_VAL_SCALE;
              }

              // save updated sensor temperatures
              tNewData.mSenTemps[tSenNum] = tTempVal1;  // Copy probe Temp.
              tNewData.mModTemps[tSenNum] = tTempVal2;  // Copy module electronics Temp.
            }
            else
            {
              // 1st Rx has IRF sequence counter
              tNewData.mIRFNums[tSenNum] = tCurAddVal;
            }

            // adjust offset to skip additional info samples in raw data
            tRxOff += tModInfos[0].mConfig.mOV;
          }

          /* Get and store additional information in cache entry*/
#if 0
          tNewData.mIRFNum     = int2uint32(tIdx);                                           // current IRF index
          tNewData.mADCLevel1  = (unsigned int) ceil(tRxMax / tModInfos[0].mFSR[1] * 100.0); // maximum ADC level of all Rxs
          tNewData.mADCLevel2  = tRxMaxNum;

          tNewData.mSenTempVal = int2uint32(tRawData[tResponseSize-1]);                      // Copy Temperatures (additional IRF info from Rx2 of sensor)
          tNewData.mModTemp    = DR_DEF_TEMP_INVALID;
#endif

          // Reading temperature(s) of MCU Board
          TTemperatures tMCUTemps = readMCUTemperature();

          tNewData.mMCUMaxTemp = *std::max_element(tMCUTemps.begin(), tMCUTemps.end()); // max. Temp. of MCU
          tNewData.mMCUTemp    = (float)tNewData.mMCUMaxTemp;                           // Copy MCU Temp.


#if DR_HAS_STORAGE
          /* create header info */
          tHead[0] = tIRF[tMLBSLen];
          tHead[1] = tIRF[tResponseSize-1];
          tHead[2] = tNewData.mMCUMaxTemp;
          tHead[3] = (tNewData.mADCLevel2 << 16) + tNewData.mADCLevel1;
#endif

          /* Store new entry in cache */
          if (tProcFlag)
          {
            // put new data into dataReader cache which is drained by processing thread
            mMtxDRCache.lock();
              tNewData.mCacheSize = (TAddInfo) mDRCache.size(); // current size of cache memory (DR cache)
              mDRCache.push(tNewData);               // push data into DR cache
            mMtxDRCache.unlock();
          }
          else
          {
            // put new data directly into processing cache which is drained by streaming server thread
            mMtxPRCache.lock();
              tNewData.mCacheSize = (TAddInfo) mPRCache.size(); // current size of cache memory (DR cache)
              mPRCache.push(tNewData);               // push data into DR cache
            mMtxPRCache.unlock();
          }


          /* check for automatic stop of measurement */
          tRunIdx++; // counting finished cycle

          if (tNumIRF)
          {
            if (tRunIdx >= tNumIRF)
            {
              //stop measuring now
              mMtxSharedData.lock();
                mState.mRunFlag = false;
              mMtxSharedData.unlock();
            }
          }

          // --------- DEBUGGING -----------
          if (tDebug) 
          {
            mMtxDRCache.lock();
              size_t tDRCacheSize = mDRCache.size();
            mMtxDRCache.unlock();

            mMtxPRCache.lock();
              size_t tPRCacheSize = mPRCache.size();
            mMtxPRCache.unlock();

            std::cout << ANSI_COLOR_CYAN << "DataReader::DEBUG: DR-cache-size: " 
                      << std::dec << tDRCacheSize 
                      << ", PR-cache-size: " << tPRCacheSize
                      << ANSI_COLOR_RESET << std::endl;
          }
          //--------------------------------

#if DR_HAS_STORAGE
          /* Storing raw data */
          if (tFD)
          {
            // update source pointer only in case of storing full IRFs
            if (tStoAll)
            {
              tSrcBuf = (void *)tRawData.data();
            }
            // copy to buffer
            if (tDebug) std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: storing raw data in buffer ..." << ANSI_COLOR_RESET << std::endl;
            memcpy((void *) (tStoBuf.data() + tNextRow*tStoSize), (const void *)tSrcBuf, tStoSize * sizeof(ilmsens::hal::ImpulseResponse::value_type));
            tNextRow ++;
            
            // check, if buffer is full
            if (tNextRow == tStoNumBuf)
            {
              std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: committing raw data to disc ..." << ANSI_COLOR_RESET << std::endl;
              size_t tWritten = fwrite((const void *)(tStoBuf.data()), sizeof(ilmsens::hal::ImpulseResponse::value_type), tNextRow*tStoSize, tFD);
              if (tWritten < (tNextRow*tStoSize))
              {
                std::cerr << ANSI_COLOR_RED << "DataReader::ERROR: could not commit all samples to disc, only" 
                                            << std::dec << tWritten << " / " 
                                            << (tNextRow*tStoSize) << " samples were written!"
                                            << ANSI_COLOR_RESET << std::endl;
                if (ferror(tFD))
                {
                  std::cerr << ANSI_COLOR_RED << "DataReader: error was: " << strerror(errno) << "." << ANSI_COLOR_RESET << std::endl;
                }
              }
              tNextRow = 0;
        
              // detect a new day
              tActTimeSec = std::time(NULL); // get current time
              std::tm tCurTM  = *localtime(&tActTimeSec);
              
              if (tCurTM.tm_mday != tActTM.tm_mday)
              {
                // a new day started, so close old file and create new one
                fclose(tFD);
                if (tStoAll)
                {
                  tFD = createRawDataFile(tConfigSensorID, "raw", tSWAvg,  tWC, tMID);
                }
                else
                {
                  tFD = createRawDataFile(tConfigSensorID, "head", tSWAvg,  tWC, tMID);
                }

                // mind day for next time
                tActTM = tCurTM;
              }
            }
          }
#endif
          //update state information for measurement loop
          mMtxSharedData.lock();
            //flags
            tRunFlag = mState.mRunFlag;
            tDebug   = mState.mDebFlag;
          mMtxSharedData.unlock();

          // update exit flag
          mMtxExit.lock();
            tEndDR = mExitShutdwn;
          mMtxExit.unlock();

        } //measurement loop

#if DR_HAS_STORAGE
        // close raw data file and commit rest of buffered data, if still open
        if (tFD)
        {
          if (tNextRow)
          {
            std::cout << ANSI_COLOR_CYAN << "DataReader::INFO: committing remaining raw data to disc ..." << ANSI_COLOR_RESET << std::endl;
            size_t tWritten = fwrite((const void *)(tStoBuf.data()), sizeof(ilmsens::hal::ImpulseResponse::value_type), tNextRow*tStoSize, tFD);
            if (tWritten < (tNextRow*tStoSize))
            {
              std::cerr << ANSI_COLOR_RED << "DataReader::ERROR: could not commit all samples to disc, only" 
                                          << std::dec << tWritten << " / " 
                                          << (tNextRow*tStoSize) << " samples were written!"
                                          << ANSI_COLOR_RESET << std::endl;
              if (ferror(tFD))
              {
                std::cerr << ANSI_COLOR_RED << "DataReader: error was: " << strerror(errno) << "." << ANSI_COLOR_RESET << std::endl;
              }
            }
            tNextRow = 0;
          }
          
          // finally close the file
          fclose(tFD);
          tFD = NULL;
        }
#endif

      } // RUN flag 
      else 
      {
        // sleep some time to wait for measurement request and decrease CPU burden
        delay_ms(10);
      }

      //update state information for DataReader
      mMtxExit.lock();
        tEndDR = mExitShutdwn;
      mMtxExit.unlock();

    } //end of main DataReader while loop

    std::cout << ANSI_COLOR_CYAN << "Datareader: preparing for shutdown ..." << ANSI_COLOR_RESET << std::endl;

    /* shut down processing thread */
    if (tProcFlag)
    {
      mMtxProcessing.lock();
        mProcShutdwn = true;
      mMtxProcessing.unlock();

      //give time for synchronisation
      delay_ms(10);

      std::cout << ANSI_COLOR_CYAN << "DataReader: Waiting for termination of processing-thread ..." << ANSI_COLOR_RESET << std::endl;
      pthread_join(tProcThread, NULL);    // Waiting for Processing-Thread
    }

    // deinit will shortly start in context destructor, so output more log messages again
    tHALSession.setDebugLevel(HAL_LOG_LEVEL_DEIN);
  }
  catch(ilmsens::mexplore::tomography::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "DataReader::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what()  << std::endl 
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
  }
  catch(ilmsens::hal::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "DataReader::HAL-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what()  << std::endl 
              << " Code was        : " << std::dec << tErr.status_code() << std::endl;
  }
  catch(std::exception& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "DataReader::Std-EXCEPTION: " << ANSI_COLOR_RESET 
              << tErr.what() << std::endl;
  }
  catch(...) 
  {
    std::cerr << ANSI_COLOR_RED << "DataReader::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;
  }

  /* sensor & infra-structure has been closed */
  mMtxSharedData.lock();
    mState.mSenAct = false;
  mMtxSharedData.unlock();

#if DR_HAS_STORAGE
  // close raw data file, if still open
  if (tFD)
  {
    fclose(tFD);
    tFD = NULL;
  }
#endif

  // can go now
  std::cout << ANSI_COLOR_CYAN << "DataReader: shutdown complete, exiting thread." ANSI_COLOR_RESET << std::endl;
  return(NULL);
}

#ifdef _MSC_VER
  #undef _CRT_SECURE_NO_WARNINGS 

  #pragma warning(pop)
#endif

} //namespace datareader
} //namespace tomography
} //namespace mexplore
} //namespace ilmsens
