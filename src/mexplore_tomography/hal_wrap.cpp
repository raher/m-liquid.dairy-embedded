/// @file hal_wrap.cpp
/// @description C++ wrapper classes for Ilmsens HAL API
/// @author pavol.kurina@gmail.com and Ralf.Herrmann@ilmsens.com

#include <iostream>
#include <cassert>
#include <cmath>
#include <chrono>
#include <thread>
#include <algorithm>
#include <cstring>

#include "hal_wrap.hpp"
#include "ilmsens/hal/ilmsens_hal.h"

using namespace ilmsens::hal;


//**************************************************************
//* Local utilities
//**************************************************************

template<class T> 
std::chrono::milliseconds as_milliseconds(const T& duration)
{
  return std::chrono::duration_cast<std::chrono::milliseconds>(duration);
}


//**************************************************************
//* HAL library context and session management
//**************************************************************

Context::Context(unsigned debugLevel)
  : _sensorCount(0)
  , _modIDs(0)
{

  //init the HAL
  int status = ilmsens_hal_initHAL ();
  if (status < 0) 
  {
    ilmsens_hal_deinitHAL ();
    throw Error("initHAL failed.", status);
  }

  _sensorCount = status;

  //set the debug Level
  setDebugLevel(debugLevel);

  if (0 == sensorCount()) 
  {
    ilmsens_hal_deinitHAL ();
    throw Error("No sensors found.");
  }

  //get library version
  _version = getHALVersion();

  //retrieve module IDs of all sensors
  _modIDs.resize(_sensorCount);
  for (unsigned int tS=1; tS <= _sensorCount; tS ++)
  {
    // open the sensor connection
    status = ilmsens_hal_openSensors(&tS, 1);
    if (status < 0) 
    {
      ilmsens_hal_closeSensors(&tS, 1);
      throw Error("openSensors failed during sensor ID readout.", status);
    }

    // read out ID
    char tCurID[ILMSENS_HAL_MOD_ID_BUF_SIZE];
    status = ilmsens_hal_getModId(tS, tCurID, ILMSENS_HAL_MOD_ID_BUF_SIZE);
    if (status < 0) 
    {
      ilmsens_hal_closeSensors(&tS, 1);
      throw Error("getModID failed during sensor ID readout.", status);
    }

    // close sensor connection again
    ilmsens_hal_closeSensors(&tS, 1);

    //store ID
    _modIDs[tS-1].assign(tCurID);
  }
}

Context::~Context()
{
  ilmsens_hal_deinitHAL();
}

void Context::setDebugLevel(unsigned pDebLevel)
{
  int status = ilmsens_hal_setDEBLevel (pDebLevel);
  if (status < 0) 
  {
    throw Error("setDEBLevel failed.", status);
  }
}


//**************************************************************
//* Sensor and sensor group handling
//**************************************************************

// Crearte sensor group instance and open all sensors inside that group
SensorGroup::SensorGroup(const SensorIndexes& indexes) 
  : _indexes(indexes)
  , _modInfos(indexes.size())
  , _updateInfos(true)
{
  assert(size() > 0);
  // open the sensor connections
  int status = ilmsens_hal_openSensors (&front(), size());
  if (status != ILMSENS_SUCCESS) 
  {
    _updateInfos = false;
    throw Error("openSensors failed.", status);
  }
}

// Destroy sensor group, i.e. make sure all sensors are closed
SensorGroup::~SensorGroup()
{
  ilmsens_hal_closeSensors(&front(), size());
}

// apply sensor configuration to all sensors in list
// @returns the minimum number of elements required for the ImpulseResponse's buffer 
size_t  SensorGroup::setupSensors(unsigned pOrder, unsigned pOV, double pClk)
{
  //check inputs
  if (pOrder < 1)
  {
    throw Error("setupSensors got invalid MLBS order (must be > 0).", ILMSENS_ERROR_INVALID_PARAM);
  }
  if (pOV < 1)
  {
    throw Error("setupSensors got invalid oversampling factor (must be > 0).", ILMSENS_ERROR_INVALID_PARAM);
  }
  if (pClk == 0.0)
  {
    throw Error("setupSensors got invalid RF clock (must be > 0).", ILMSENS_ERROR_INVALID_PARAM);
  }

  //make basic sensor setup
  ilmsens_hal_ModConfig tMCnf;

  tMCnf.mClk   = pClk;   // RF system clock [GHz]
  tMCnf.mOrder = pOrder; // order of MLBS: typically 9, 12, or 15
  tMCnf.mSub   = 0;      // clock divider for real sampling rate: use default
  tMCnf.mOV    = pOV;    // number of oversampling: use provided value
  tMCnf.mTx    = 0;      // number of transmitters: use default
  tMCnf.mRx    = 0;      // number of Rx: use default

  int status = ilmsens_hal_setupSensors (&front(), size(), &tMCnf);
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("setupSensors failed.", status);
  }

  // query actual sensor configs valid after setup
  const TMultiModInfo& tModInfos = getModInfos(true);

  // sensors are assumed to have the same config....so get the config of sensor #1
  // to calculate required Imnpulse response buffer size required for measRead/Get
  tMCnf = tModInfos[0].mConfig;
  return (static_cast<size_t>(size() * static_cast<unsigned int>(1 << tMCnf.mOrder) * tMCnf.mOV * tMCnf.mRx));
}

const TMultiModInfo& SensorGroup::getModInfos(bool pDoUpdate)
{
  // get latest module infos?
  if (pDoUpdate || _updateInfos)
  {
    // auto-update only done the first time
    _updateInfos = false;

    // iterate over all sensors in the group to get current module infos
    for (size_t tS = 0; tS < size(); tS ++)
    {
      ilmsens_hal_ModInfo tInfo;
      int status = ilmsens_hal_getModInfo(_indexes[tS], &tInfo);
      if (status != ILMSENS_SUCCESS) 
      {
        throw Error("getModInfo failed.", status);
      }
      //copy data
      _modInfos[tS] = tInfo;
    }
  }

  // return result
  return(_modInfos);
}


// declare master sensor in the group
// * masterPosition < 0: all sensors remain (or are reset) to be slaves
// * masterPosition = 0: all sensors will be masters (sensors must be separate without any physical synchronisation connection)
// * masterPosition > 0: single sensor at the given position (1-based index!) will be master, all others will be slaves
void SensorGroup::setMaster(int masterPosition)
{
  // switch to master mode.....do this only on stand-alone modules or on the
  // master in a chain of modules (there can only be one master in each chain!)
  assert(masterPosition <= (int)size());

  //de-synchronise all sensors
  int status = ilmsens_hal_synchMS (&front(), size(), 0);
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("synchMS(0) failed.", status);
  }

  // in any case reset master-flag on all sensors to make them slaves
  status = ilmsens_hal_setMaster (&front(), size(), 0 /* SLAVE */);
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("setMaster(0) failed.", status);
  }

  //if masterPosition is 0 -> all sensors shall be masters
  //if masterPosition > 0 -> single sensor at the position (1-based index!) will be master
  //otherwise -> all remains slaves
  if (masterPosition > 0)
  {
    // set master-flag on one sensor only (position is 1-based but list-index is 0-based!)
    status = ilmsens_hal_setMaster (&at((unsigned int)masterPosition-1), 1, 1 /* MASTER */);
    if (status != ILMSENS_SUCCESS) 
    {
      throw Error("setMaster(1) failed.", status);
    }
  }
  else if (masterPosition == 0)
  {
    // set master-flag on all sensors
    status = ilmsens_hal_setMaster (&front(), size(), 1 /* MASTER */);
    if (status != ILMSENS_SUCCESS) 
    {
      throw Error("setMaster(1) failed.", status);
    }
  }

  //re-synchronise all sensors
  status = ilmsens_hal_synchMS (&front(), size(), 1);
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("synchMS(1) failed.", status);
  }
}


// calibrate synchronisation interface fo attached sensors
void SensorGroup::doSynchCal(void)
{
  std::cout << "HAL::INFO: starting synch. calibration with " << size() << " modules ... " << std::endl;
  //iterate over all sensors
  for (unsigned int tM = 0; tM < size(); tM ++)
  {
    //read in system register
    ilmsens_hal_MemoryType tVal;
    int tStatus = ilmsens_hal_readReg(&at((unsigned int)tM), 1, 0x00800000, &tVal, sizeof(ilmsens_hal_MemoryType));
    if (tStatus < ILMSENS_SUCCESS) 
    {
      //display warning
      std::cout << "HAL::WARNING: could not read register from module #" << tM << ". Result code was " << tStatus << "." << std::endl;
      //try next module
      continue;
    }
    else
    {
      std::cout << "HAL::WARNING: calibrating synch. interface of module #" << tM << " ..." << std::endl;
    }

    //get current synch status
    uint32_t tCurStat  = (tVal >> 6) & 0x00000003;
    //get synch interface infos
    uint32_t tSyncInfo = (tVal >> 8) & 0x000000FF;

    //master or slave?
    if (tVal & 0x00000004)
    {
      //first output info
      std::cout << "HAL::INFO: master with module #" << tM << " reported a synch. status of " << std::dec << tCurStat << "." << std::endl;
      std::cout << "HAL::INFO: master with module #" << tM << " reported a synch. interface info of 0x" << std::hex << tSyncInfo << "." << std::endl;
      //nothing to do for master
    }
    else
    {
      //first output info
      std::cout << "HAL::INFO: slave with module #" << tM << " reported a synch. status of " << std::dec << tCurStat << "." << std::endl;
      std::cout << "HAL::INFO: slave with module #" << tM << " reported a synch. interface info of 0x" << std::hex << tSyncInfo << "." << std::endl;

      //analyse synch. interface info and select correct input

      //check, which synch input is valid
      if (tSyncInfo & 0x10)
      {
        // Q1 input is valid, so select it
        tVal &= ~(0x00000100);
        std::cout << "HAL::INFO: selecting Q1 for synch input." << std::endl;
      }
      else
      {
        if (tSyncInfo & 0x20)
        {
          // only Q2 input is valid, so select it
          tVal |= 0x00000100;
          std::cout << "HAL::INFO: selecting Q2 for synch input." << std::endl;
        }
      }

      //check, which run input is valid
      if (tSyncInfo & 0x40)
      {
        // Q1 input is valid, so select it
        tVal &= ~(0x00000400);
        std::cout << "HAL::INFO: selecting Q1 for run input." << std::endl;
      }
      else
      {
        if (tSyncInfo & 0x80)
        {
          // only Q2 input is valid, so select it
          tVal |= 0x00000400;
          std::cout << "HAL::INFO: selecting Q2 for run input." << std::endl;
        }
      }

      //apply new setting
      tStatus = ilmsens_hal_writeReg(&at((unsigned int)tM), 1, 0x00800000, tVal);
      if (tStatus < ILMSENS_SUCCESS) 
      {
        //display warning
        std::cout << "HAL::WARNING: could not write register from module #" << tM << ". Result code was " << tStatus << "." << std::endl;
        //try next module
        continue;
      }
    } // slave module
  } // loop over all modules
}

// synchronise master/slave modules by unsetting run and synch flags and then raising synch flag again on masters
void SensorGroup::doMasterSlaveSynch(void)
{
  //synchronise all sensors at frist (may not be needed in all cases, but makes sure, internal synch flag is high regardless of its power-on state)
  int status = ilmsens_hal_synchMS (&front(), size(), 1);
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("synchMS(1) failed.", status);
  }

  //de-synchronise all sensors to create high->low transition on internal synch flag -> make sure synch flag of slaves is really low
  status = ilmsens_hal_synchMS (&front(), size(), 0);
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("synchMS(0) failed.", status);
  }

  //start raise and lower run flag to make sure its state is low
  status = ilmsens_hal_measRun (&front(), size(), ILMSENS_HAL_RUN_RAW);
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("measRun failed.", status);
  }
  status = ilmsens_hal_measStop (&front(), size());
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("measStop failed.", status);
  }

  //finally re-synchronise all sensors again to make salves aligned to masters -> all synch flags should be high now
  status = ilmsens_hal_synchMS (&front(), size(), 1);
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("synchMS(1) failed.", status);
  }

  //finally reset Tx wrt to newly synchronised sensors
  resetTx();

  //show results and status of synchronisation interface
  for (unsigned int tM = 0; tM < size(); tM ++)
  {
    //read in system register
    ilmsens_hal_MemoryType tVal;
    int tStatus = ilmsens_hal_readReg(&at((unsigned int)tM), 1, 0x00800000, &tVal, sizeof(ilmsens_hal_MemoryType));
    if (tStatus < ILMSENS_SUCCESS) 
    {
      //display warning
      std::cout << "HAL::WARNING: could not read register from module #" << tM << ". Result code was " << tStatus << "." << std::endl;
      //try next module
      continue;
    }

    //get current synch status
    uint32_t tCurStat  = (tVal >> 6) & 0x00000003;
    //get synch interface infos
    uint32_t tSyncInfo = (tVal >> 8) & 0x000000FF;

    //output info
    std::cout << "HAL::INFO: module #" << tM << " reported a synch. status of " << std::dec << tCurStat << " and an synch. interface info of 0x" << std::hex << tSyncInfo << "." << std::endl;
  } // loop over all modules
}

// reset MLBS TX (should be done after setting master module) to start and synch it
void SensorGroup::resetTx(void)
{
  //MLBS transmitter reset
  int status = ilmsens_hal_setMLBS (&front(), size());
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("setMLBS failed.", status);
  }
}

// activate or deactivate power down feature of Tx
// * pModIdx = 0: all sensors will be treated
// * pModIdx > 0: single sensor at the given position (1-based index!) will be treated
void SensorGroup::setTxPowerDown(bool pTxPD, unsigned int pModIdx)
{
  //check, which sensor(s) should be treated
  if (pModIdx)
  {
    //MLBS transmitter power down set on a single module
    if (pModIdx <= (unsigned int)size())
    {
      //given index is valid
      int status = ilmsens_hal_setPD (&at((unsigned int)pModIdx-1), 1, (pTxPD) ? ILMSENS_HAL_TX_OFF : ILMSENS_HAL_TX_ON);
      if (status != ILMSENS_SUCCESS) 
      {
        throw Error("setPD for single sensor failed.", status);
      }
      //also disable PD pattern by setting its length to 0
      status = ilmsens_hal_setPDPattern (&at((unsigned int)pModIdx-1), 1, 0, NULL, 0);
      if (status != ILMSENS_SUCCESS) 
      {
        throw Error("setPDPattern for single sensor failed.", status);
      }
    }
  }
  else
  {
    //MLBS transmitter power down set on all modules
    int status = ilmsens_hal_setPD (&front(), size(), (pTxPD) ? ILMSENS_HAL_TX_OFF : ILMSENS_HAL_TX_ON);
    if (status != ILMSENS_SUCCESS) 
    {
      throw Error("setPD for all sensors failed.", status);
    }
    //also disable PD pattern by setting its length to 0
    status = ilmsens_hal_setPDPattern (&front(), size(), 0, NULL, 0);
    if (status != ILMSENS_SUCCESS) 
    {
      throw Error("setPDPattern for all sensors failed.", status);
    }
  }
}

// apply power down pattern
// * pModIdx = 0: all sensors will be treated
// * pModIdx > 0: single sensor at the given position (1-based index!) will be treated
void SensorGroup::setTxPowerDownPattern(unsigned int pModIdx, unsigned int pPDLen, TPDPattern& pPattern)
{
  //check, which sensor(s) should be treated
  if (pModIdx)
  {
    //MLBS transmitter power down pattern set on a single module
    if (pModIdx <= (unsigned int)size())
    {
      //given index is valid
      int status = ilmsens_hal_setPDPattern (&at((unsigned int)pModIdx-1), 1, pPDLen, pPattern.data(), pPattern.size()*sizeof(TPDPattern::value_type));
      if (status != ILMSENS_SUCCESS) 
      {
        throw Error("setPDPattern for single sensor failed.", status);
      }
    }
  }
  else
  {
    //MLBS transmitter power down pattern set on all modules
    int status = ilmsens_hal_setPDPattern (&front(), size(), pPDLen, pPattern.data(), pPattern.size()*sizeof(TPDPattern::value_type));
    if (status != ILMSENS_SUCCESS) 
    {
      throw Error("setPDPattern for all sensors failed.", status);
    }
  }
}

// apply measurement configuration
void SensorGroup::setSWAvgerages(unsigned pSWAvg, unsigned pWC)
{
  //set new software averages with 0 wait cycles
  int status = ilmsens_hal_setAvg (&front(), size(), pSWAvg, pWC);
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("setAvg failed.", status);
  }
}


//**************************************************************
//* Measurement handling
//**************************************************************

// create measurement instance and start a measurement run
Measurement::Measurement(SensorGroup& group, size_t bufferSize, bool bufferedMode)
  : _sensors(group)
  , _bufferSize(bufferSize)
{
  assert(bufferSize > 0);
  int status = ilmsens_hal_measRun (&_sensors.front(), _sensors.size(), bufferedMode ? ILMSENS_HAL_RUN_BUF : ILMSENS_HAL_RUN_RAW);
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("measRun failed.", status);
  }
}

// destroy measurement instance after stopping a measurement run
Measurement::~Measurement()
{
  int status = ilmsens_hal_measStop (&_sensors.front(), _sensors.size());
  if (status != ILMSENS_SUCCESS) 
  {
    std::cerr << "measStop failed with " << std::dec << status << "." << std::endl;
  }
}

// check, if new measured data is available
bool Measurement::measurementReady(void)
{
  int status = ilmsens_hal_measRdy (&_sensors.front(), _sensors.size());
  if (status < 0) 
  {
    throw Error("measRdy failed", status);
  }
  return(status > 0);
}

// extract measurement data from each sensor in group
size_t Measurement::readMeasurement(ImpulseResponse& buffer)
{
  void* start = &buffer.front();
  int status = ilmsens_hal_measRead (&_sensors.front(), _sensors.size(), static_cast<int*>(start), buffer.size()*sizeof(SampleType));
  if (status < 0) 
  {
    throw Error("measRead failed", status);
  }
  return status;
}

ImpulseResponse Measurement::next(std::chrono::milliseconds timeout)
{
  using namespace std::chrono;
  auto start = steady_clock::now();
  //auto ready = measurementReady(_sensors);
  auto ready = measurementReady();
  auto diff = as_milliseconds(steady_clock::now() - start);
  while (!ready && diff < timeout) 
  {
    // REVISIT: this assumes that timeout is less than 10x of min(measurement-period) of the group!
    std::this_thread::sleep_for((std::max)(milliseconds(1), diff / 10));
    //ready = measurementReady(_sensors);
    ready = measurementReady();
    diff = as_milliseconds(steady_clock::now() - start);
  }
  if (!ready) 
  {
    throw Error("Timeout on measRdy expired.");
  }
  ImpulseResponse buffer(_bufferSize);
  //buffer.resize(readMeasurement(_sensors, buffer));
  buffer.resize(readMeasurement(buffer));
  return buffer;
}

// try to get new data within next timeout but do not throw when timeout expired
bool Measurement::try_next(ImpulseResponse& pBuffer, std::chrono::milliseconds pTO)
{
  using namespace std::chrono;
  auto start = steady_clock::now();
  auto ready = measurementReady();
  auto diff = as_milliseconds(steady_clock::now() - start);
  while (!ready && diff < pTO) 
  {
    // REVISIT: this assumes that timeout is less than 10x of min(measurement-period) of the group!
    std::this_thread::sleep_for((std::max)(milliseconds(1), diff / 10));
    ready = measurementReady();
    diff  = as_milliseconds(steady_clock::now() - start);
  }
  if (!ready) 
  {
    //no data arrived within timeout, nothing more to do
    return(false);
  }
  //read in new data
  pBuffer.resize(readMeasurement(pBuffer));
  //all was fine
  return(true);
}


//**************************************************************
//* HAL support functions
//**************************************************************

// HAL library version
const ilmsens_hal_Version& ilmsens::hal::getHALVersion()
{
  static ilmsens_hal_Version sHalVersion;

  int tRes = ilmsens_hal_getVersion(&sHalVersion);
  if (tRes != ILMSENS_SUCCESS) 
  {
    throw Error("getVersion failed.", tRes);
  }

  return(sHalVersion);
}


//implement sensor setup and calculate buffer size for meaured data retrieval
unsigned ilmsens::hal::configure(SensorGroup& sensors, unsigned mlbsOrder, unsigned ov, double masterClk, unsigned receiverCount, unsigned softwareAvg, int masterPosition)
{
  //default measurement time configuration
  const unsigned int mNum_SW_AVG = softwareAvg; // number of software averages to use: min. 0, max. 2^16-1
  const unsigned int mNum_Wait = 0;             // number of wait cycles between acquisitions (i.e. for use in snapshot mode): min. 0, max. 2^16-1

  //read out ID of each attached sensor
  char tID[ILMSENS_HAL_MOD_ID_BUF_SIZE] = {0}; //max length allocated
  for(unsigned int tS = 0; tS < sensors.size(); tS++)
  {
    int count = 0;
    tID[0] = 0; //empty string
    while (strlen(tID) == 0 && count < 10) 
    {  
      int tErg = ilmsens_hal_getModId (sensors.at(tS), tID, sizeof(tID));

      std::cout << "Sensor #" << std::dec << sensors.at(tS) << " has ID '" << tID
              <<   "' (result was " << std::dec << tErg << ")." << std::endl;
              count++;
    }
  }
  if (strlen(tID) == 0) 
  {
    throw Error("Cannot read Sensor-ID", ILMSENS_ERROR_UNKNOWN);
  }

  //make basic sensor setup
  ilmsens_hal_ModConfig tMCnf;

  tMCnf.mClk = masterClk;        // RF system clock [GHz]
  tMCnf.mOrder = mlbsOrder;      // order of MLBS: 9, 12, or 15
  tMCnf.mSub = 0;                // clock divider for real sampling rate: use default
  tMCnf.mOV = ov;                // number of oversampling: use provided value
  tMCnf.mTx = 0;                 // number of transmitters: use default
  tMCnf.mRx = receiverCount;     // number of Rx: usually 2 per sensor

  int status = ilmsens_hal_setupSensors (&sensors.front(), sensors.size(), &tMCnf);
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("setupSensors failed.", status);
  }

  //averaging

  //read out HW averages for each sensor
  ilmsens_hal_ModInfo tMInfo;
  for(unsigned int tS=0; tS < sensors.size(); tS++)
  {
    status = ilmsens_hal_getModInfo (sensors.at(tS), &tMInfo);
    if (status != ILMSENS_SUCCESS) 
    {
      throw Error("getModInfo failed.", status);
    }
    std::cout << "Sensor #" << std::dec << sensors.at(tS) << " has HW averages set to: "
              << std::dec << tMInfo.mHWAvg << "." << std::endl;
    //store actual module basic parameters
    tMCnf = tMInfo.mConfig;
  }

  //new set SW averages and wait cycles
  sensors.setSWAvgerages(mNum_SW_AVG, mNum_Wait);
  /*
  status = ilmsens_hal_setAvg (&sensors.front(), sensors.size(), mNum_SW_AVG, mNum_Wait);
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("setAvg failed.", status);
  }
  */

  //set the master sensor and do digital synchronisation
  sensors.setMaster(masterPosition);

  //MLBS transmitter
  status = ilmsens_hal_setMLBS (&sensors.front(), sensors.size()); //reset
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("setMLBS failed.", status);
  }
  status = ilmsens_hal_setPD (&sensors.front(), sensors.size(), 0); //unmute
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("setPD failed.", status);
  }


  //Poll the first sensor for configuration (number if receivers/channels etc). 
  //Must be the same for all active nodes after above steps have completed successfully!
  return sensors.size() * static_cast<unsigned>(1 << tMCnf.mOrder) * tMCnf.mOV * tMCnf.mRx; //return required buffer size for measRead/Get
}

#if 0

// check, if new measured data is available
static bool measurementReady(const SensorGroup& sensors)
{
  SensorGroup& nonconst_sensors = const_cast<SensorGroup&>(sensors);
  int status = ilmsens_hal_measRdy (&nonconst_sensors.front(), nonconst_sensors.size());
  if (status < 0) 
  {
    throw Error("measRdy failed", status);
  }
  return status > 0;
}

// extract measurement data from each sensor in group
static size_t readMeasurement(const SensorGroup& sensors, ImpulseResponse& buffer)
{
  SensorGroup& nonconst_sensors = const_cast<SensorGroup&>(sensors);
  void* start = &buffer.front();
  int status = ilmsens_hal_measRead (&nonconst_sensors.front(), nonconst_sensors.size(), static_cast<int*>(start), buffer.size()*sizeof(SampleType));
  if (status < 0) 
  {
    throw Error("measRead failed", status);
  }
  return status;
}

#endif
