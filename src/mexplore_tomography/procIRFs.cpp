/* procIRFs.cpp
 * 
 * Thread function doing basic processing of raw M-sequence data retrieved from a cache buffer.
 * MLBS-correlation is performed by using cross-correlation method. Pulse parameters are extracted.
 * Processed data is then forwarded into another cache buffer.
 */


/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */


// Std. C++ library types / STL 

// IO, and Strings
#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <deque>
#include <chrono>

#include <cstring>

#include <cmath>

// Math
#include <algorithm>                    // for max_element

// Timing and threading includes
#include <pthread.h>                    // POSIX threads
#include <sched.h>                      // for Realtime Priority


// Shared variables, memories, and synchronisation ressources
#include "shared.hpp"

// Ilmsens processing library
#include "ilmsens/processing/ilmsens_processing_error.hpp"
#include "ilmsens/processing/KKF_interp_ECC.hpp"   // Ilmsens MLBS-KKF class for ECC-sensors
#include "ilmsens/processing/vector_iteration_functions.hpp"
#include "ilmsens/processing/simple_math_functions.hpp"

// Other Ilmsens stuff
#include "ilmsens/ilmsens_error.h"      // Ilmsens error codes

// mean header for module
#include "procIRFs.hpp" 

using namespace ilmsens::processing;

namespace ilmsens 
{
namespace mexplore
{
namespace tomography 
{
namespace processing
{


/* -------------------------------------------------------------------------------------------------
 * Processing types
 */

// none

/* -------------------------------------------------------------------------------------------------
 * Processing buffers and variables
 */

// none

/* -------------------------------------------------------------------------------------------------
 * Processing functions
 */

// none


/* -------------------------------------------------------------------------------------------------
 * Processing thread
 * 
 * Receives data during running measurement from DataReader and process it according to requested
 * processing flags from client. The thread is created and shutdown by the DataReader.
 * 
 * By default, MLBS correlation by KKF is performed, the data interpolated, and pulse parameters
 * are extracted. 
 * 
 * -------------------------------------------------------------------------------------------------
 */

void* procIRFs(void*)
{
  try 
  {
    std::cout << ANSI_COLOR_YELLOW << "Processing: thread created" << ANSI_COLOR_RESET << std::endl;

    /* Bind Thread to single CPU Core */
#ifndef WIN32
    cpu_set_t tCPUSet;
    CPU_ZERO(&tCPUSet);

    CPU_SET(7, &tCPUSet); // CPU #7
    CPU_SET(8, &tCPUSet); // CPU #8
    if(pthread_setaffinity_np(pthread_self(), sizeof(tCPUSet), &tCPUSet) != 0) 
    {
      std::cerr << ANSI_COLOR_RED "Processing::ERROR: pthread_setaffinity_np did not succeed: " << ANSI_COLOR_RESET 
                << strerror(errno) << std::endl;
    }
#endif

    /* Get required configuration information for processing */
    mMtxSharedData.lock();
      unsigned int tMLBSLen = mState.mMLBSLen;
      bool tDebug           = mState.mDebFlag;

      ilmsens_hal_ModInfo tModInfo = mState.mModInfos[0];
      double tSampClk       = mState.mSampClk;

      unsigned int tStrWait = mState.mStrWait;
    mMtxSharedData.unlock();

    // mind number of samples before interpolation
    double tNumSamp = (double) tMLBSLen;

    // apply some interpolation
    unsigned int tInterpLen = tMLBSLen * PR_IP_FAK;
    //round it up to next power of 2
    tInterpLen = (unsigned int) ilmsens::processing::getNextPow2_32((uint32_t) tInterpLen);

    // mind number of samples after interpolation and get final ROI definition
    double tNumSampIP = (double) tInterpLen;

    //update (virtual) sampling clock according to interpolation
    tSampClk *= tNumSampIP / tNumSamp;


    /* Acquire pre-processing ressources as needed */
    KKF_interp_ECC myKKF(tModInfo.mConfig.mOrder, tModInfo.mConfig.mOV, tInterpLen, true);
    std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: KKF handler created with MLBS order " << tModInfo.mConfig.mOrder 
                                   << " | oversampling factor " << tModInfo.mConfig.mOV 
                                   << " | IRF length (incl. interpolation) " << tInterpLen << ANSI_COLOR_RESET << std::endl;

    
    // get sample spacing in time by using f0 and interpolated length
    TSampleSmall tTs = (TSampleSmall)1.0 / ((TSampleSmall)tSampClk); // sampling time spacing with interpolation [ns]


    // additional vectors/buffers pre-allocation
    TFIFOCacheEntry tRawDat;

    TRealIRF tIRF(tMLBSLen, 0);
    TRealIRF tBG (tMLBSLen, 0);

    /* prepare processing states */
    unsigned int tZeroIdx = 0;      // index of time-zero for milk analysis
    bool tGetRefPulsePos  = true;   // get current reference index of pulse from 1st IRF


    /* main loop is executed until shutdown flag is received */

    // initialize execution-time-measurement
    auto tStart = std::chrono::steady_clock::now();
    auto tEnd   = tStart;

    // set processing ready flag and get related processing flags
    mMtxSharedData.lock();
      double tBGR_a   = mState.mBGR_a; // exponential BGR
      mState.mProcRdy = true;
      bool tDoTimeZero = mState.mDoTimeZero;
    mMtxSharedData.unlock();

    // get state of processing
    std::cout << ANSI_COLOR_YELLOW << "Processing: waiting for data to process ..." << ANSI_COLOR_RESET << std::endl;
    mMtxProcessing.lock();
      bool tEndProc = mProcShutdwn;
    mMtxProcessing.unlock();
    while(!tEndProc) 
    {
      // Check if new data is available in DataReader cache
      mMtxDRCache.lock();
        bool tDRCEmpty = mDRCache.empty();

      if (!tDRCEmpty)
      {
        /* new data is there, so get & process it */

        // pop data from cache
          tRawDat = mDRCache.front(); // Get new Data
          mDRCache.pop();             // Delete new Data in Cache
        mMtxDRCache.unlock();

        if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: received new data ..." << ANSI_COLOR_RESET << std::endl;

        // get number of Rxs in the entry
        size_t tNumRx = tRawDat.mRxOffs.size();


        /* process new data for tomography analysis */
        auto tStartProc = std::chrono::steady_clock::now();

        // iterate over data from each sensor separately
        for (size_t tR = 0; tR < tNumRx; tR ++)
        {
          //copy data samples for current Rx
          auto tFirstSamp = tRawDat.mSclIRFs.begin() + static_cast< size_t >(tRawDat.mRxOffs[tR]);
          auto tLastSamp  = tRawDat.mSclIRFs.begin() + static_cast< size_t >(tRawDat.mRxOffs[tR] + tMLBSLen);
          TRealIRF tCurDat(tFirstSamp, tLastSamp);

          // rotate vectors to time-zero (ATTENTION: will not be valid for first IRF!)
          if (tDoTimeZero)
          {
            if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: time-zero correction ..." << ANSI_COLOR_RESET << std::endl;
            std::rotate(tCurDat.begin(), tCurDat.begin() + tZeroIdx, tCurDat.end());
          }
          auto tElRot = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::steady_clock::now() - tStartProc);

          /* do MLBS correlation */
          if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: starting KKF ..." << ANSI_COLOR_RESET << std::endl;
          tIRF = myKKF.calculateKKF(tCurDat);
          if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: KKF done ..." << ANSI_COLOR_RESET << std::endl;
          auto tElKKF = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::steady_clock::now() - tStartProc);

          // copy correlated and aligned data to cache entry (raw data before processing)
          std::copy(tIRF.begin(), tIRF.end(), tFirstSamp);

          /* extract pulse parameters for time-zero and energy analysis */       
          if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: starting raw pulse analysis ..." << ANSI_COLOR_RESET << std::endl;

          // analyse maximum of current channel
          TIRFPara tVoltPara = getRlIRFPara(tIRF); 
          tRawDat.mPulseAmp =  (TSampleSmall) tVoltPara.mMax.mVal;        // pulse maximum amplitude
          tRawDat.mPulseDel = ((TSampleSmall) tVoltPara.mMax.mIdx) * tTs; // pulse maximum delay [ns]
          if (tDebug)
          {
            std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: pulse parameters Rx1:" << std::endl 
                      << "total energy  : " << std::dec << tVoltPara.mEn << std::endl
                      << "max. val | idx: " << std::dec << tVoltPara.mMax.mVal << " | " << tVoltPara.mMax.mIdx << std::endl
                      << "min. val | idx: " << std::dec << tVoltPara.mMin.mVal << " | " << tVoltPara.mMin.mIdx << std::endl
                      << "Processing::INFO: temperatures (probe/module/MCU): " << std::dec << tRawDat.mSenTemps[0] << "°C | " << tRawDat.mModTemps[0] << "°C | " << tRawDat.mMCUTemp << "°C" << std::endl 
                      << ANSI_COLOR_RESET << std::endl;
          }

          // get time-zero reference ?
          if (tGetRefPulsePos && tDoTimeZero)
          {
            // get current pulse position of reference channel for time-zero
            TMinMax tRefMinMax = getRlMinMax(tIRF);
            if (fabs(tRefMinMax.mMin.mVal) > tRefMinMax.mMax.mVal)
            {
              tZeroIdx = tRefMinMax.mMin.mIdx;
            }
            else
            {
              tZeroIdx = tRefMinMax.mMax.mIdx;
            }

            tGetRefPulsePos = false;
          }

          auto tElPara = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::steady_clock::now() - tStartProc);

          /* --- do processing steps as requested by client--- */

          // exponential BGR
          if (tBGR_a != 0.0)
          {
            // handle first Rx
            expBGRRl(tIRF, tBG, tIRF, tBGR_a);
          }

          /* --- end of processing steps as requested by client--- */
          auto tElStdProc = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::steady_clock::now() - tStartProc);

          /* --- analyse pulse parameters - tomography processing --- */
          if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: starting analysis ..." << ANSI_COLOR_RESET << std::endl;

          /* --- Do additonal processing --- */

          /*
            if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: starting additional processing ..." << ANSI_COLOR_RESET << std::endl;
           *
           *
           *
           *
           *
            if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: additional processing done ..." << ANSI_COLOR_RESET << std::endl;
           */

          /* --- End of additonal processing --- */       

          auto tElAddProc = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::steady_clock::now() - tStartProc);

          // output timing info
          if (tDebug)
          {
            std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: durations of different processing stages: " << std::endl
                      << "Rotation       : " << std::dec << tElRot.count() << " us" << std::endl
                      << "KKF            : " << std::dec << tElKKF.count() << " us" << std::endl
                      << "Raw pulse para.: " << std::dec << tElPara.count() << " us" << std::endl
                      << "Std. processing: " << std::dec << tElStdProc.count() << " us" << std::endl
                      << "Add. processing: " << std::dec << tElAddProc.count() << " us" << std::endl
                      << ANSI_COLOR_RESET << std::endl;
          }

        } //looping over data from each Rx


        /* forward new data to streaming, if suitable time interval has elapsed */

        // get time elpased since last data retrieval
        tEnd = std::chrono::steady_clock::now();
        auto tElapsed = std::chrono::duration_cast< std::chrono::milliseconds > (tEnd - tStart);
        if (tElapsed.count() >= tStrWait)
        {
          // enough time has passed, so forward data to streaming process
          tStart = tEnd;

          /* Store data in streaming cache */
          mMtxPRCache.lock();
            tRawDat.mCacheSize = (TAddInfo) mPRCache.size();
            mPRCache.push(tRawDat);
          mMtxPRCache.unlock();
        }
      }
      else
      {
        // unlock DR cache
        mMtxDRCache.unlock();

        // wait some time to lower CPU burden
        delay_ms(10);

        // Update processing state - only when cache is empty.
        // This should ensure, that all received data is being processed, before the thread is closed
        mMtxProcessing.lock();
          tEndProc = mProcShutdwn;
        mMtxProcessing.unlock();
      }

      // Update debugging and run state, update processing parameters
      double tOldBGR_a = tBGR_a;
      mMtxSharedData.lock();
        //flags
        tDebug = mState.mDebFlag;

        //parameters
        tBGR_a      = mState.mBGR_a;
        tDoTimeZero = mState.mDoTimeZero;
      mMtxSharedData.unlock();

      // reset background, if BGR is switch off
      if ((tOldBGR_a != 0.0) && (tBGR_a == 0.0))
      {
        tBG.clear();
        tBG.resize(tMLBSLen, 0.0);
      }
    }

    //give time for synchronisation
    delay_ms(10);
  } 
  catch(ilmsens::mexplore::tomography::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "Processing::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl 
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
  }
  catch(ilmsens::processing::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "Processing::Prc-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl 
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl;
  }
  catch(std::exception& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "Processing::Std-EXCEPTION: " << ANSI_COLOR_RESET 
              << tErr.what() << std::endl;
  }
  catch(...) 
  {
    std::cerr << ANSI_COLOR_RED << "Processing::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;
  }
  
  // can go now
  std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: shutdown complete, exiting thread" << ANSI_COLOR_RESET << std::endl;
  return(NULL);
}

} //namespace processing
} //namespace tomography
} //namespace mexplore
} //namespace ilmsens
