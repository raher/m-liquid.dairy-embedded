/// @file shared.hpp
/// @brief Global variables shared by all threads.
///
/// The variables defined here can be accessed by all threads of the program. 
/// Mutex mechanism is used to synchronise access.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>


#ifndef SHARED_HPP
#define SHARED_HPP

// C++ STL and std.-libs
#include <vector>
#include <stdexcept>
#include <mutex>
#include <string>

// processing-related cache buffer types
#include "data_cache_FIFO.hpp"

// exceptions & error handling
#include "ilmsens/ilmsens_error.h"

// Ilmsens HAL constants and types
#include "ilmsens/hal/ilmsens_hal.h"

// actual HAL C++ wrapper
#include <hal_wrap.hpp>

using namespace ilmsens::hal;

/**   
 * @defgroup mexplore_tomography_global_defn Common definitions used by the app.
 * @brief Defintions used by all threads, e.g. ANSI color codes, etc.
 * @{
 */  


/** Defining colored outputs in terminal */
#ifdef WIN32
  // no console colouring available by default
  #define ANSI_COLOR_RED     "| [R] | "
  #define ANSI_COLOR_GREEN   "| [G] | "
  #define ANSI_COLOR_YELLOW  "| [Y] | "
  #define ANSI_COLOR_BLUE    "| [B] | "
  #define ANSI_COLOR_MAGENTA "| [M] | "
  #define ANSI_COLOR_CYAN    "| [C] | "
  #define ANSI_COLOR_RESET   " |"
#else
  #define ANSI_COLOR_RED     "\x1b[31m"
  #define ANSI_COLOR_GREEN   "\x1b[32m"
  #define ANSI_COLOR_YELLOW  "\x1b[33m"
  #define ANSI_COLOR_BLUE    "\x1b[34m"
  #define ANSI_COLOR_MAGENTA "\x1b[35m"
  #define ANSI_COLOR_CYAN    "\x1b[36m"
  #define ANSI_COLOR_RESET   "\x1b[0m"
#endif

/** Defining power down control constants */
#define PD_DO_NOTHING 0             ///< do not change power down of transmitters
#define PD_ALL_TX_ON  1             ///< switch on Tx power on all attached sensors
#define PD_ALL_TX_OFF 2             ///< switch off Tx power on all attached sensors
#define PD_MOD_TX_ON  3             ///< switch on Tx power on currently selected sensor
#define PD_MOD_TX_OFF 4             ///< switch off Tx power on currently selected sensor
#define PD_MOD_TX_PAT 5             ///< apply Tx power switching pattern to currently selected sensor

#define PD_PATTERN_MAX_LENGTH 256                           ///< maximum length of PD pattern
#define PD_PATTERN_REG_LENGTH (PD_PATTERN_MAX_LENGTH >> 5)  ///< max number of uint32 registers used to store PD pattern

typedef std::vector< uint32_t > TPatRegs;                   ///< type for PD pattern registers 



/** @} mexplore_tomography_global_defn */


/**   
 * @defgroup mexplore_tomography_global_types Type definitions used by the app.
 * @brief Types for shared program states.
 * @{
 */  


/** Sensor configuration and state data structure for sharing between threads */
struct sSharedData 
{
//  bool mExitFlag = false;                         ///< Application want to exit?
  bool mSenAct;                                   ///< dataReader could activate the sensor?
  bool mCalSyncFlag;                              ///< Calibrate master/slave synchronisation interface
  bool mSyncFlag;                                 ///< Re-Synchronise all sensors (master-slave synchronisation)?
  bool mRunFlag;                                  ///< Measurement is running?

  bool mDebFlag;                                  ///< Output debug messages?
  bool mSaveFlag;                                 ///< Save raw data to storage?
  bool mSendIRF;                                  ///< Send IRF alongside IRF info via streaming?
  
  bool mProcFlag;                                 ///< Processing activated?
  bool mProcRdy;                                  ///< Processing is ready?

  size_t       mSelSen;                           ///< index of currently selected sensor
  unsigned int mPDMode;                           ///< apply power down to transmitters of attached sensors in different ways
  unsigned int mPDPatLen;                         ///< length of power down switching pattern

  unsigned int mMLBSLen;                          ///< Length of M-Sequence (Samples)
  double       mClk;                              ///< RF system clock f0 [GHz]
  unsigned int mACK;                              ///< Acknoledge Code (ASCII "ACK ")
  unsigned int mNACK;                             ///< Not-Acknoledge Code (ASCII "NACK")
  unsigned int mSW_AVG;                           ///< Software averages
  unsigned int mWaitCyc;                          ///< Wait cycles between averaging cycles
  unsigned int mHW_AVG;                           ///< Hardware averages
  double       mSampClk;                          ///< equivalent sampling rate of UWB sensor [GHz]
  double       mMeasRate;                         ///< measurement rate of sensor

  unsigned int mNumIRF;                           ///< Number of IRFs to acquire per run, 0 = infinite run

  unsigned int mStrWait;                          ///< min. wait interval between streaming IRFs via TCP server
  
  unsigned int mMID;                              ///< Measurement ID (to be set by client)

  bool         mDoTimeZero;                       ///< do time-zero alignment to maxmum amplitude?
  double       mBGR_a;                            ///< exponential dynamic background removal alpha constant ([0.0 .. 1.0[, 0.0 means BGR off)

  size_t         mNumSen;                         ///< number of attached sensors

  std::string    mMasterID;                       ///< device ID of master module among multiple sensors
  std::string    mServerName;                     ///< reader-friendly name of server running this app

  TPatRegs       mPDPattern;                      ///< registers holding current PD pattern

  TMultiModID    mModIDs;                         ///< device IDs of all attached sensors
  TMultiModInfo  mModInfos;                       ///< module infos of all attached sensors

  //default constructor
  sSharedData():
    mSenAct(false),
    mCalSyncFlag(false),
    mSyncFlag(false),
    mRunFlag(false),
    mDebFlag(false),
    mSaveFlag(false),
    mSendIRF(true),
    mProcFlag(false),
    mProcRdy(false),
    mSelSen(0),
    mPDMode(PD_DO_NOTHING),
    mPDPatLen(0),
    mMLBSLen(511),
    mClk(13.312),
    mACK(0x41434b20),
    mNACK(0x4e41434b),
    mSW_AVG(4),
    mWaitCyc(0),
    mHW_AVG(48),
    mSampClk(13.312),
    mMeasRate(70.58),
    mNumIRF(0),
    mStrWait(1000),
    mMID(0),
    mDoTimeZero(false),
    mBGR_a(0.0),
    mNumSen(0),
    mMasterID("<NO_MASTER_ID>"),
    mServerName("<NO_SERVER_NAME"),
    mPDPattern(PD_PATTERN_REG_LENGTH,0),
    mModIDs(0),
    mModInfos(0)
  {}
};

/** Configuration and state data type */
typedef struct sSharedData TSharedData; 

/** @} mexplore_tomography_global_types */

  
/**   
 * @defgroup mexplore_tomography_global_synch Mutex definitions for synchronisation of threads.
 * @brief Mutexes for shared program states and memory caches.
 * @{
 */  

// Synchronisation mutexes for configuration and measurement state 
extern std::mutex mMtxExit;                       ///< Mutex for App's EXIT-Flag
extern std::mutex mMtxLogging;                    ///< Mutex for Logging Shutdown-Flag
extern std::mutex mMtxProcessing;                 ///< Mutex for Processing Shutdown-Flag

// Synchronisation mutexes for configuration and measurement state 
extern std::mutex mMtxSharedData;                 ///< Mutex for access to shared data structure

// Synchronisation mutexes for shared memories
extern std::mutex mMtxDRCache;                    ///< Synchronisation mutex for DR cache (datareader & processing)
extern std::mutex mMtxPRCache;                    ///< Synchronisation mutex for PR cache (processing & streaming)

/** @} mexplore_tomography_global_synch */

 
/**   
 * @defgroup mexplore_tomography_global_state State and sensor configuration information.
 * @brief Variables representing program or thread state and sensor configuration.
 * @{
 */  

extern TSharedData mState;                        ///< global state flags and sensor configuration

extern bool mExitShutdwn;                         ///< Application shutdown flag
extern bool mLogMShutdwn;                         ///< Logging thread shutdown flag
extern bool mProcShutdwn;                         ///< Processing thread shutdown flag

/** @} mexplore_tomography_global_state */

 
/**   
 * @defgroup mexplore_tomography_global_memory Global memory caches or buffers.
 * @brief Memories for transferring measured data between different threads.
 * @{
 */  

extern TFIFOCache mDRCache;                       ///< Cache for DataReader to buffer data for processing thread
extern TFIFOCache mPRCache;                       ///< Cache for Processing to buffer data for streaming thread

/** @} mexplore_tomography_global_memory */

 
/**   
 * @defgroup mexplore_tomography_exception Exceptions & safety tools used by this app.
 * @brief Exceptions only used by this app and its threads. Tools for exception safe programming.
 * @{
 */  

namespace ilmsens 
{
  namespace mexplore
  {
    namespace tomography 
    {

      /**  Application-specific error class storing an Ilmsens error code along with a message */
      class Error : public std::runtime_error 
      {
        typedef std::runtime_error Base;

        public:
          Error(const char* pMsg, int pCode = ILMSENS_ERROR_UNKNOWN, int pErrorNum = 0)
            : Base(pMsg), mErrorCode(pCode), mErrorNum(pErrorNum) {}
          
          int getErrorCode() const { return mErrorCode; }
          int getErrorNum () const { return mErrorNum; }

        private:
          int mErrorCode;
          int mErrorNum;
      };

    } //namespace tomography
  } //namespace mexplore
} //namespace ilmsens

/** @} mexplore_tomography_exception */

 
/**   
 * @defgroup mexplore_tomography_tools Common tools used by this app.
 * @brief Tools used by this app and its threads, e.g. sleep function.
 * @{
 */  

namespace ilmsens 
{
  namespace mexplore
  {
    namespace tomography 
    {

      void delay_ms(unsigned int pDel_ms);

    } //namespace tomography
  } //namespace mexplore
} //namespace ilmsens

/** @} mexplore_tomography_tools */

#endif //SHARED_HPP
