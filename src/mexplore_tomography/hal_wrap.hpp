/// @file hal_wrap.hpp
/// @description C++ wrapper classes header for Ilmsens HAL API
/// @author pavol.kurina@gmail.com and Ralf.Herrmann@ilmsens.com

#include <stdexcept>
#include <vector>
#include <chrono>

//include UWBAPI HAL
#include "ilmsens/hal/ilmsens_hal_defn.h"
#include "ilmsens/hal/ilmsens_hal_types.h"

#ifndef ILMSENS_HAL_WRAP_HPP
#define ILMSENS_HAL_WRAP_HPP

namespace ilmsens 
{
  namespace hal 
  {
    
    /*** HAL utility classes and defintions ***/
    namespace utils 
    {
    
      /* base class that prohibts copying instances */
      class NotCopyable 
      {
        public:
          NotCopyable() {}

        private:
          NotCopyable(const NotCopyable&);
          NotCopyable& operator=(const NotCopyable&);
      };

    } // namespace utils

    /*** Error class for HAL exceptions ***/
    class Error : public std::runtime_error 
    {
      typedef std::runtime_error Base;

      public:
        Error(const char* message, int status_code = ILMSENS_DEB_INFO)
          : Base(message), _status_code(status_code) {}
        int status_code() const { return _status_code; }

      private:
        int _status_code;
    };

    /*** Sensors and sensor groups for setup and handling of UWB Sensors ***/
    typedef unsigned                            SensorIndex;      // index in enumerated sensor's list
    typedef std::vector< SensorIndex >          SensorIndexes;    // list of indices, i.e. addressing a group of sensors
    typedef std::vector< ilmsens_hal_ModInfo >  TMultiModInfo;    // list of sensor/modfule infos (e.g. as retrieved from all attached sensors)
    typedef std::vector< uint32_t >             TPDPattern;       // list of registers carrying power down pattern


    class SensorGroup : public utils::NotCopyable 
    {
      public:
        SensorGroup(const SensorIndexes& indexes);
        ~SensorGroup();

        const SensorIndexes::reference& front() { return _indexes.front(); }
        const SensorIndexes::reference& at(unsigned n) { return _indexes.at(n); }
        unsigned size() const { return static_cast<unsigned>(_indexes.size()); }
      
        size_t setupSensors(unsigned pOrder, unsigned pOV, double pClk);
        const TMultiModInfo& getModInfos(bool pDoUpdate = false);
        void setMaster(int masterPosition);
        void doMasterSlaveSynch(void);
        void doSynchCal(void);
        void resetTx(void);
        void setTxPowerDown(bool pTxPD = false, unsigned int pModIdx = 0);
        void setTxPowerDownPattern(unsigned int pModIdx, unsigned int mPDLen, TPDPattern& pPattern);
        void setSWAvgerages(unsigned pSWAvg, unsigned pWC = 0);
      
      private:
        SensorIndexes _indexes;
        TMultiModInfo _modInfos;
        bool          _updateInfos;
    };


    /*** Context class for general HAL session management ***/
    typedef std::vector< std::string         >  TMultiModID;      // list of sensors ID strings (e.g. as retrieved from all attached sensors)

    class Context : public utils::NotCopyable 
    {
      public:
        Context(unsigned debugLevel);
        ~Context();

        void setDebugLevel(unsigned pDebLevel);
        unsigned sensorCount() const { return _sensorCount; }

        const ilmsens_hal_Version& getVersion() const { return(_version); }
        const TMultiModID&         getIDs()     const { return(_modIDs); }

      private:
        unsigned            _sensorCount;
        TMultiModID         _modIDs;
        ilmsens_hal_Version _version;
    };

    /*** Types and classes for handling measuremnents ***/
    typedef ilmsens_hal_SampleType  SampleType;       // type representing a single sample
    typedef std::vector<SampleType> ImpulseResponse;  // type representing a vector of samples

    class Measurement : public utils::NotCopyable 
    {
      public:
        Measurement(SensorGroup& sensors, size_t bufferSize, bool bufferedMode = false);
        ~Measurement();
        ImpulseResponse next(std::chrono::milliseconds timeout);
        bool            try_next(ImpulseResponse& pBuffer, std::chrono::milliseconds pTO);

        bool measurementReady(void);
        size_t readMeasurement(ImpulseResponse& buffer);

      private:

        SensorGroup& _sensors;
        size_t    _bufferSize;
    };


    /*** HAL support functions ***/

    /// @returns the minimum number of elements for the ImpulseResponse's buffer
    unsigned configure(SensorGroup& sensors, unsigned mlbsOrder, unsigned ov, double masterClk, unsigned receiverCount, unsigned softwareAvg, int masterPosition);
    
    /// @returns HAL version structure
    const ilmsens_hal_Version& getHALVersion();
  }
}

#endif // ILMSENS_HAL_WRAP_HPP
