﻿//============================================================================
// Name        : m:explore.tomography measurement and processing application
// Author      : Ralf Herrmann
// Version     : 0.1.0
// Copyright   : (C) Ilmsens GmbH 2019
// Description : TCP control+streaming server & tomography measurement application  
//               for embedded ARM-Systems using multiple Ilmsens m:explore UWB-Sensor.
// Changelog:
//
// Rev. 0.1.0 (07/2019, HER)
//
//  + initial version based on m:liquid.humidity application V0.1 with removed processing
//  + added support for using multiple Ilmsens UWB-sensors with master/slave configuration
//
//============================================================================


/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */

// Std. C++ library types / STL 
#include <csignal>     // signal handling
#include <cerrno>      // IO/system call error handling

// Poco logging includes
#include "Poco/AutoPtr.h"
#include "Poco/Logger.h"
#include "Poco/ConsoleChannel.h"
#include "Poco/PatternFormatter.h"
#include "Poco/FormattingChannel.h"
#include "Poco/LogStream.h"
#include "Poco/Message.h"

// Timing and threading includes
#include <chrono>                       // For execution time measurements
#include <pthread.h>                    // POSIX threads
#include <sched.h>                      // for Realtime Priority

// synchronisation
#include <mutex>

// File, IO, and Strings
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstring>

// Ilmsens includes
#include "ilmsens/ilmsens_error.h"      // Ilmsens error codes

// Ilmsens m:explore.tomography includes
#include "ilmsens/mexplore_tomography/ilmsens_mexplore_tomography_version.h" //project version & revision

// Shared variables, memories, and synchronisation ressources
#include "shared.hpp"

// TCP server threads
#include "logTCPServer.hpp"
#include "ctrlTCPServer.hpp"
#include "strmTCPServer.hpp"

//dataReader for handling the UWB sensor
#include "dataReader.hpp"

// embedded MCU board tools
#include "ilmsens/tools/embedded_MCU_tools.hpp"

// Header for main program
#include "main.hpp" 

/* -------------------------------------------------------------------------------------------------
 * Using name spaces
 * -------------------------------------------------------------------------------------------------
 */

using namespace std;


/* -------------------------------------------------------------------------------------------------
 * Local definitions
 * -------------------------------------------------------------------------------------------------
 */


/* -------------------------------------------------------------------------------------------------
 * Local types and classes
 * -------------------------------------------------------------------------------------------------
 */


/* -------------------------------------------------------------------------------------------------
 * Local variables 
 * -------------------------------------------------------------------------------------------------
 */

// logging
static Poco::Message::Priority sLogLevel = LOG_LEVEL_DEFAULT;

// console redirects
static bool sDoRedir = false;

// app setup
static std::string  sFriendlyName(TOMO_FRIENDLY_NAME);

// measurement timing and confiuration
static unsigned int sSWAvg = TOMO_SW_AVG_DEFAULT;
static unsigned int sWC    = TOMO_WC_DEFAULT;
static std::string  sMasterID(TOMO_MASTER_DEFAULT_ID);

// measurement configuration
static bool sMeasAutoStart  = TOMO_AUTO_START_MEAS;
static unsigned int sNumIRF = TOMO_NUM_IRF;

//processing configuration
static bool sActProcessing  = TOMO_ACT_PROC;

// streaming configuration
static bool sStreamIRFs    = TOMO_STREAM_IRFS;
static unsigned int sStreamWait = TOMO_STREAM_WAIT_INT;

// raw data storage
static bool sActStorage    = TOMO_ACT_STORAGE;


// thread handles
static pthread_t sLogThread;          // Logging redirect TCP server
static pthread_t sDRThread;           // DataReader thread controlling the sensor
static pthread_t sCtrlThread;         // Controlling TCP server
static pthread_t sStrmThread;         // Streaming TP server


/* -------------------------------------------------------------------------------------------------
 * Logging & logging helpers
 * 
 * Powered by Poco:-)
 * -------------------------------------------------------------------------------------------------
 */

// Configure Poco logging: create a formatted console output
void configureLogging(int pRootLevel)
{
  using Poco::AutoPtr;
  AutoPtr< Poco::ConsoleChannel >   tConsole  (new Poco::ConsoleChannel);
  AutoPtr< Poco::PatternFormatter > tFormatter(new Poco::PatternFormatter);
  
  tFormatter->setProperty("pattern", "%E.%i %N:%I %s:%q: %t");
  
  AutoPtr< Poco::FormattingChannel > tChannel(new Poco::FormattingChannel(tFormatter, tConsole));

  // set newly created formatted console channel as default channel at Poco root
  Poco::Logger::root().setChannel(tChannel);
  Poco::Logger::root().setLevel(pRootLevel);
}

// Logger for main app
Poco::Logger& log()
{
  static Poco::Logger& sLog = Poco::Logger::get("ilmsens.mexplore.tomography");
  return(sLog);
}

// Logstream for main app
Poco::LogStream& logStream()
{
  // REVISIT: needs protection?
  static Poco::LogStream sStream(log());
  return(sStream);
}


/* -------------------------------------------------------------------------------------------------
 * Command line helpers
 * 
 * -------------------------------------------------------------------------------------------------
 */

// apply defaults to all config variables that can be changed from command line
void resetDefaults(void)
{
  // logging
  sLogLevel = LOG_LEVEL_DEFAULT;

  // app setup
  sFriendlyName = TOMO_FRIENDLY_NAME;

  // console redirects
  sDoRedir = false;

  // measurement timing
  sSWAvg    = TOMO_SW_AVG_DEFAULT;
  sWC       = TOMO_WC_DEFAULT;
  sMasterID = TOMO_MASTER_DEFAULT_ID;

  // measurement configuration
  sMeasAutoStart = TOMO_AUTO_START_MEAS;
  sNumIRF        = TOMO_NUM_IRF;

  // raw data storage
  sActStorage    = TOMO_ACT_STORAGE;

  // streaming configuration
  sStreamIRFs    = TOMO_STREAM_IRFS;
  sStreamWait    = TOMO_STREAM_WAIT_INT;

  // processing configuration
  sActProcessing = TOMO_ACT_PROC;
}

// show help message
void printCommandLineHelp(void)
{
  //output help text and exit
  std::cout << ANSI_COLOR_GREEN << "m:explore.tomography::HELP: The following command line options are supported:" << std::endl
            << "--logLevel <Level>  : set verbosity of logging output (0 = off .. 8 = trace)"<< std::endl
            << std::endl
            << "--serverName <name> : reader-friendly name uniquely identifiying this server"<< std::endl
            << std::endl
            << "--SWAvg    <Avg>    : set default software averages of measurement (1 .. 4095)"<< std::endl
            << "--WaitCyc  <WC>     : set default wait cycles of measurement (0 .. 1023)"<< std::endl
            << "--MasterID <ID>     : set ID of master module (full ID string or unique part thereof)"<< std::endl
            << std::endl
            << "--noAutoMeas        : do not automatically start a measurement"<< std::endl
            << "--startMeas         : automatically start a measurement after sensor setup"<< std::endl
            << "--numIRF   <num>    : number of IRFs per measurement run (0 = run continuously)"<< std::endl
            << std::endl
            << "--enableProcessing  : enable the processing thread (disabled by default) for doing, e.g. MLBS correlation"<< std::endl
            << std::endl
            << "--noStreamIRF       : do not stream IRF data alongside info vector"<< std::endl
            << "--streamIRF         : stream IRF data alongside info vector"<< std::endl
            << "--streamWait <int>  : min .wait interval [ms] between streaming IRFs via TCP server to client"<< std::endl
            << std::endl
            << "--storeRaw          : activate saving of raw measured data to mass storage"<< std::endl
            << std::endl
            << "--redirCon          : redirect/copy console output (cout & cerr) to TCP server at port 8200 for remote supervision"<< std::endl
            << "--help / -?         : show this help screen"<< std::endl
            << ANSI_COLOR_RESET << std::endl;
}

// parse command line to set various parameters
void parseCommandLine(int argc, char* argv[])
{
  //apply defaults first
  resetDefaults();

  for (int tI = 1; tI < argc; ++tI) 
  {
    if (0 == std::string("--logLevel").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sLogLevel = (Poco::Message::Priority)std::stoi(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mexplore::tomography::Error("Missing log level argument.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--serverName").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sFriendlyName = argv[tI + 1];
        tI ++;
      } 
      else
      {
        throw ilmsens::mexplore::tomography::Error("Missing server name value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--SWAvg").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sSWAvg = (unsigned int)std::stoi(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mexplore::tomography::Error("Missing software averages value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--WaitCyc").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sWC = (unsigned int)std::stoi(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mexplore::tomography::Error("Missing wait cycles value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--MasterID").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sMasterID = argv[tI + 1];
        tI ++;
      } 
      else
      {
        throw ilmsens::mexplore::tomography::Error("Missing wait cycles value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--streamWait").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sStreamWait = (unsigned int)std::stoi(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mexplore::tomography::Error("Missing wait interval value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--numIRF").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sNumIRF = (unsigned int)std::stoi(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mexplore::tomography::Error("Missing wait interval value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--noAutoMeas").compare(argv[tI])) 
    {
      sMeasAutoStart = false;
    } else if (0 == std::string("--startMeas").compare(argv[tI])) 
    {
      sMeasAutoStart = true;
    } else if (0 == std::string("--noStreamIRF").compare(argv[tI])) 
    {
      sStreamIRFs = false;
    } else if (0 == std::string("--streamIRF").compare(argv[tI])) 
    {
      sStreamIRFs = true;
    } else if (0 == std::string("--storeRaw").compare(argv[tI])) 
    {
      sActStorage = true;
    } else if (0 == std::string("--enableProcessing").compare(argv[tI])) 
    {
      sActProcessing = true;
    } else if (0 == std::string("--redirCon").compare(argv[tI])) 
    {
      sDoRedir = true;
    } else if ( (0 == std::string("--help").compare(argv[tI])) || (0 == std::string("-?").compare(argv[tI])) )
    {
      printCommandLineHelp();
      throw ilmsens::mexplore::tomography::Error("Command line help requested.", ILMSENS_SUCCESS);
    }
    else
    {
      //output warning
      std::cerr << ANSI_COLOR_YELLOW << "m:explore.tomography::WARNING: unknown command line option '" << argv[tI] << "'. Use '--help' to get a list of valid options." << ANSI_COLOR_RESET << std::endl;
    }
  }
}


/* -------------------------------------------------------------------------------------------------
 * Signal handling
 * -------------------------------------------------------------------------------------------------
 */

// initiate a shutdown
void doShutDown(void)
{
  // make sure all other threads are signalled to end
  mMtxExit.lock();
    mExitShutdwn = true;
  mMtxExit.unlock();
  
  //wait some time for synchronisation
  ilmsens::mexplore::tomography::delay_ms(200);

  /* Wait until all threads are ended */
  pthread_join(sCtrlThread, NULL);
  pthread_join(sStrmThread, NULL);
  pthread_join(sDRThread, NULL);
  
  /* Finally, wait until logging thread has ended */
  if (sDoRedir)
  {
    mMtxLogging.lock();
      mLogMShutdwn = true;
    mMtxLogging.unlock();
    pthread_join(sLogThread, NULL);
  }

  ilmsens::mexplore::tomography::delay_ms(10);
}

// system signal handler
void handleTermSigs(int pSigNum) 
{
  /* output signal just received */
  std::cerr << "m:explore.tomography::SIGNAL: app received signal " << std::dec << pSigNum << "." << std::endl;
  
  switch (pSigNum)
  {
    // caused by abort()
    case SIGABRT:
      std::cerr << "m:explore.tomography::SIGNAL: signal was SIGABRT." << std::endl;
      exit(EXIT_FAILURE);

    // floating point error (e.g. divide by zero)
    case SIGFPE:
      std::cerr << "m:explore.tomography::SIGNAL: signal was SIGFPE." << std::endl;
      break;

    // illegal instruction
    case SIGILL:
      std::cerr << "m:explore.tomography::SIGNAL: signal was SIGILL." << std::endl;
      break;

    // interrupt, e.g. CTRL+C
    case SIGINT:
      std::cerr << "m:explore.tomography::SIGNAL: signal was SIGINT." << std::endl;
      doShutDown();
      exit(EXIT_SUCCESS);

    // segmentation violation fault
    case SIGSEGV:
      std::cerr << "m:explore.tomography::SIGNAL: signal was SIGSEGV." << std::endl;
      exit(EXIT_FAILURE);

    // termination request
    case SIGTERM:
      std::cerr << "m:explore.tomography::SIGNAL: signal was SIGTERM." << std::endl;
      doShutDown();
      exit(EXIT_FAILURE);

    default:
      std::cerr << "m:explore.tomography::SIGNAL: this unknown signal is unhandled!" << std::endl;
  }
}


/* -------------------------------------------------------------------------------------------------
 * Main program entry point
 * 
 * Checks command line options (if required), and dispatches the controlling and streaming thread.
 * 
 * When the EXIT command has been received from a client, the corresponding threads shut down
 * and main() cleans up any remaining ressources.
 * -------------------------------------------------------------------------------------------------
 */


int main(int argc, char* argv[])
{

  /* setup signal handlers */
  
#ifdef _MSC_VER
  #define _CRT_SECURE_NO_WARNINGS 

  #pragma warning(push)
  #pragma warning(disable:4996)
#endif

  if (signal(SIGABRT, handleTermSigs) == SIG_ERR)
  {
    std::cerr << "m:explore.tomography::SIGNAL: could not set sognal handler for SIGABRT. Error was: " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  } 
  else if (signal(SIGFPE, handleTermSigs) == SIG_ERR)
  {
    std::cerr << "m:explore.tomography::SIGNAL: could not set sognal handler for SIGFPE. Error was: " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  } 
  else if (signal(SIGILL, handleTermSigs) == SIG_ERR)
  {
    std::cerr << "m:explore.tomography::SIGNAL: could not set sognal handler for SIGILL. Error was: " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  } 
  else if (signal(SIGINT, handleTermSigs) == SIG_ERR)
  {
    std::cerr << "m:explore.tomography::SIGNAL: could not set sognal handler for SIGINT. Error was: " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  } 
  else if (signal(SIGSEGV, handleTermSigs) == SIG_ERR)
  {
    std::cerr << "m:explore.tomography::SIGNAL: could not set sognal handler for SIGSEGV. Error was: " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  } 
  else if (signal(SIGTERM, handleTermSigs) == SIG_ERR)
  {
    std::cerr << "m:explore.tomography::SIGNAL: could not set sognal handler for SIGTERM. Error was: " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  }

#ifdef _MSC_VER
  #undef _CRT_SECURE_NO_WARNINGS 

  #pragma warning(pop)
#endif

  int tExitCode = EXIT_SUCCESS;

  /* Main part - create threads and wait for their termination */
  try 
  {
    /* print greeting */
    std::cout << std::endl << ANSI_COLOR_GREEN << "**** Welcome to m:explore.tomography V" << std::dec 
              << ILMSENS_MEXPLORE_TOMOGRAPHY_VER_MAJOR << "."
              << ILMSENS_MEXPLORE_TOMOGRAPHY_VER_MINOR << "."
              << ILMSENS_MEXPLORE_TOMOGRAPHY_VER_BUILD
              << "! ****" << ANSI_COLOR_RESET << std::endl << std::endl;

    /* parse command line */
    parseCommandLine(argc, argv);

    /* setup Poco logging */
    configureLogging(sLogLevel);

    /* check, if cout and cerr should be redirected to TCP server */
    signed int tRes;
    if (sDoRedir)
    {
      /*** Create TCP server thread for log messages ***/
      tRes = pthread_create(&sLogThread, NULL, &ilmsens::mexplore::tomography::logging::logTCPServer, NULL);
      if(tRes != 0) 
      {
        std::cerr << ANSI_COLOR_RED << "m:explore.tomography::ERROR: cannot create logging TCP server thread! Logging will be local only." << ANSI_COLOR_RESET << std::endl;
        sDoRedir = false;
      }
      else
      {
        std::cout << ANSI_COLOR_GREEN << "m:explore.tomography::INFO: created thread for logging TCP server." << ANSI_COLOR_RESET << std::endl;
      }
    }

    // get & print current XU4 temperatures
    const TTemperatures& tMCUTemps = readMCUTemperature();
    std::cout << ANSI_COLOR_CYAN << "Received " << std::dec << tMCUTemps.size() << " temperature values from MCU board:" << std::endl;

    for (unsigned int tI = 0; tI < tMCUTemps.size(); tI++)
    {
      std::cout << "Temperature # " << std::dec << tI << ": " << tMCUTemps[tI] << "°C" << std::endl;
    }

    std::cout << ANSI_COLOR_RESET << std::endl;

    /*** Create always running DataReader thread ***/

    // configure measurement run and processing
    mMtxSharedData.lock();
      mState.mSenAct   = false;
      mState.mRunFlag  = false;

      mState.mProcRdy  = false;
      mState.mProcFlag = sActProcessing;

      mState.mSW_AVG   = sSWAvg;
      mState.mWaitCyc  = sWC;
      mState.mMasterID = sMasterID;

      mState.mSaveFlag = sActStorage;

      mState.mSendIRF  = sStreamIRFs;
      mState.mStrWait  = sStreamWait;
      mState.mNumIRF   = sNumIRF;

      mState.mServerName = sFriendlyName;
    mMtxSharedData.unlock();

    /* Thread for sensor's DataReader */
    tRes = pthread_create(&sDRThread, NULL, &ilmsens::mexplore::tomography::datareader::dataReader, 0);
    if(tRes != 0) 
    {
      //make sure all other running threads are ended
      if (sDoRedir)
      {
        mMtxLogging.lock();
          mLogMShutdwn = true;
        mMtxLogging.unlock();

        pthread_join(sLogThread, NULL);
      }

      throw ilmsens::mexplore::tomography::Error("m:explore.tomography::ERROR: cannot create DataReader thread!", ILMSENS_ERROR_NO_MEMORY, tRes);
    }
    else
    {
      std::cout << ANSI_COLOR_GREEN << "m:explore.tomography::INFO: created DataReader thread." << ANSI_COLOR_RESET << std::endl;
    }

    // wait, until dataReader has activated the sensor(s)
    mMtxSharedData.lock();
      bool tSenAct = mState.mSenAct;
    mMtxSharedData.unlock();
    auto tStartWait = std::chrono::system_clock::now();
    while (!tSenAct)
    {
      // wait some time to lower CPU burden
      ilmsens::mexplore::tomography::delay_ms(100);

      //mind the timeout
      auto tElWait = std::chrono::duration_cast< std::chrono::milliseconds > (std::chrono::system_clock::now() - tStartWait);
      if (tElWait.count() > TOMO_SENSOR_SETUP_TIMEOUT)
      {
        // need to stop waiting, could not activate sensor in time
        std::cout << std::endl << ANSI_COLOR_GREEN << "m:explore.tomography::ERROR: sensor setup could not be finished within timeout, exiting m:explore.tomography." << ANSI_COLOR_RESET << std::endl;
        // app not successful
        tExitCode = EXIT_FAILURE;
        break;
      }

      //update sensor status
      mMtxSharedData.lock();
        tSenAct = mState.mSenAct;
      mMtxSharedData.unlock();
    }

    if (tSenAct)
    {
      /*** Create always running TCP server threads ***/

      /* Thread for controlling TCP server */
      tRes = pthread_create(&sCtrlThread, NULL, &ilmsens::mexplore::tomography::controlling::ctrlTCPServer, NULL);
      if(tRes != 0) 
      {
        //make sure all other running threads are ended
        mMtxExit.lock();
          mExitShutdwn = true;
        mMtxExit.unlock();
        pthread_join(sDRThread, NULL);

        if (sDoRedir)
        {
          mMtxLogging.lock();
            mLogMShutdwn = true;
          mMtxLogging.unlock();

          pthread_join(sLogThread, NULL);
        }

        throw ilmsens::mexplore::tomography::Error("m:explore.tomography::ERROR: cannot create controlling TCP server thread!", ILMSENS_ERROR_NO_MEMORY, tRes);
      }
      else
      {
        std::cout << ANSI_COLOR_GREEN << "m:explore.tomography::INFO: created thread for controlling TCP server." << ANSI_COLOR_RESET << std::endl;
      }

      /* Thread for streaming TCP server */
      tRes = pthread_create(&sStrmThread, NULL, &ilmsens::mexplore::tomography::streaming::strmTCPServer, NULL);
      if(tRes != 0) 
      {
        //make sure all other running threads are ended
        mMtxExit.lock();
          mExitShutdwn = true;
        mMtxExit.unlock();
        pthread_join(sDRThread, NULL);
        pthread_join(sCtrlThread, NULL);

        if (sDoRedir)
        {
          mMtxLogging.lock();
            mLogMShutdwn = true;
          mMtxLogging.unlock();

          pthread_join(sLogThread, NULL);
        }

        throw ilmsens::mexplore::tomography::Error("m:explore.tomography::ERROR: cannot create streaming TCP server thread!", ILMSENS_ERROR_NO_MEMORY, tRes);
      }
      else
      {
        std::cout << ANSI_COLOR_GREEN << "m:explore.tomography::INFO: created thread for streaming TCP server." << ANSI_COLOR_RESET << std::endl;
      }

      /* start a continuous measurement run (?) */
      mMtxSharedData.lock();
        mState.mRunFlag = sMeasAutoStart;
      mMtxSharedData.unlock();

      /* Wait until all threads are ended (upon EXIT command from client) */
      pthread_join(sCtrlThread, NULL);
      pthread_join(sStrmThread, NULL);
      pthread_join(sDRThread, NULL);
    }
    else
    {
      //could not finish sensor setup in time
      mMtxExit.lock();
        mExitShutdwn = true;
      mMtxExit.unlock();
      pthread_join(sDRThread, NULL);
    }


    /* Finally, trigger and wait until logging threads are ended */
    if (sDoRedir)
    {
      mMtxLogging.lock();
        mLogMShutdwn = true;
      mMtxLogging.unlock();

      pthread_join(sLogThread, NULL);
    }

    std::cout << std::endl << ANSI_COLOR_GREEN << "m:explore.tomography::INFO: All Threads closed, exiting m:explore.tomography." << ANSI_COLOR_RESET << std::endl;
  }
  catch(ilmsens::mexplore::tomography::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "m:explore.tomography::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;

    // not successful (?)
    if (tErr.getErrorCode() != ILMSENS_SUCCESS) tExitCode = EXIT_FAILURE;
  }
  catch(std::exception& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "m:explore.tomography::Std-EXCEPTION: " << ANSI_COLOR_RESET 
              << tErr.what() << std::endl;

    // not successful
    tExitCode = EXIT_FAILURE;
  }
  catch(...) 
  {
    std::cerr << ANSI_COLOR_RED "m:explore.tomography::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;

    // not successful
    tExitCode = EXIT_FAILURE;
  }


  /* all was fine, we can go now */
  std::cout << std::endl << ANSI_COLOR_GREEN << "*** Good Bye! ***" << ANSI_COLOR_RESET << std::endl;
  return (tExitCode);
}
