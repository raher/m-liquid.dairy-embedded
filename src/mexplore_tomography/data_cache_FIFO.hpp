/// @file data_cache_FIFO.hpp
/// @brief Definition of buffers and structures for measured data.
///
/// The types defined here can be used for data exchange between functions and threads.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>


#ifndef PROCESSING_BUFFERS_HPP
#define PROCESSING_BUFFERS_HPP

#include <queue>
#include <vector>

#include "ilmsens/processing/IRF_FRF.hpp"
#include "hal_wrap.hpp"

using namespace ilmsens::processing::types;
using namespace ilmsens::hal;

/**   
 * @defgroup processing_cache_type Cache memories for exchange between threads.
 * @brief Types for exchanging measured data along with additional information.
 * @{
 */  


/** Cache FIFO entry structure */
typedef std::vector< TRealIRF > TRealIRFs;
typedef std::vector< unsigned int > TIndexes;

struct sFIFOCacheEntry
{
  ImpulseResponse mRawIRFs; ///< impulse response buffer holding raw int32 data from all channels
  TRealIRF        mSclIRFs; ///< impulse response buffer holding scaled/correlated double data from all channels
//  TRealIRFs mIRFs;          ///< buffer for ORFs from all active receivers
  TIndexes        mRxOffs;  ///< offsets in the above data vectors where respective Rx data starts
  
  TAddInfos mIRFNums;       ///< number/index of IRF in current run (info attached to Rx1 data from sensor)
  TAddInfo mTS_sec;         ///< time stamp: seconds
  TAddInfo mTSusec;         ///< time stamp: microseconds

  TAddInfos mADCLevels;     ///< ADC Levels for all Rxs
  TAddInfos mSenTempVals;   ///< Temperature info from sensor (info attached to Rx2 data)
  TAddInfo mMCUMaxTemp;     ///< max. (CPU-) Temperature of XU4 embedded board
  TAddInfo mCacheSize;      ///< current FIFO cache size

  TSampleVec   mSenTemps;    ///< probe temperature [°C]
  TSampleVec   mModTemps;   ///< sensor electronics temperatures  [°C]

  TSampleSmall mMCUTemp;    ///< XU4 temperature  [°C]

  TSampleSmall mPulseAmp;   ///< amplitude of measured pulse
  TSampleSmall mPulseDel;   ///< delay of measured pulse [ns]
};

/** Cache FIFO entry type */
typedef struct sFIFOCacheEntry TFIFOCacheEntry; 

/** Cache FIFO type */
typedef std::queue<TFIFOCacheEntry> TFIFOCache;

/** @} processing_cache_type */

#endif //PROCESSING_BUFFERS_HPP
