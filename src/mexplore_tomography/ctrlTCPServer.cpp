/* ctrlTCPServer.cpp
 * 
 * Thread function for TCP server which will accept a single client connection
 * and subsequently receive control commands and info requests. According to the commands,
 * other working threads (Ilmsens DataReader) will be influenced/stopped and the app's state changed.
 */



/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */


// Std. C++ library types / STL 

// File, IO, and Strings
#include <iostream>
#include <iomanip>
#include <cstring>

/*

// Poco logging includes
#include "Poco/Logger.h"
#include "Poco/LogStream.h"
#include "Poco/Message.h"
*/

// Timing and threading includes
#include <pthread.h>                    // POSIX threads
#include <sched.h>                      // for Realtime Priority


// Shared variables, memories, and synchronisation ressources
#include "shared.hpp"

// TCP stuff & server class
#ifndef WIN32
  #include <arpa/inet.h> // for htonl() call
#endif
#include "ilmsens/networking/TCP_server_single.hpp"
#include "ilmsens/networking/ilmsens_networking_defn.h"
#include "ilmsens/networking/ilmsens_networking_error.hpp"

// Ilmsens stuff
#include "ilmsens/ilmsens_error.h"      // Ilmsens error codes

// Sensor working threads
//#include "dataReader.hpp"

// main header for module
#include "ctrlTCPServer.hpp" 


/* -------------------------------------------------------------------------------------------------
 * Using clauses
 * -------------------------------------------------------------------------------------------------
 */

using namespace ilmsens::networking;


/* -------------------------------------------------------------------------------------------------
 * Local defines
 * -------------------------------------------------------------------------------------------------
 */


/* -------------------------------------------------------------------------------------------------
 * Local variables
 * -------------------------------------------------------------------------------------------------
 */

static size_t mSenIdx = 0;        // index of current sensor, e.g. used for ID or property queries

/* -------------------------------------------------------------------------------------------------
 * Local helper functions
 * -------------------------------------------------------------------------------------------------
 */

/* converting floats to network byte order */
#ifndef WIN32
  unsigned int htonf(float pVal)
  {
    union
    {
      float uFloat;
      uint32_t uUInt32;
    };
    uFloat = pVal;
    return(htonl(uUInt32));
  }

/* converting floats from network byte order */
float ntohf(uint32_t pVal)
{
  union
  {
    float    uFloat;
    uint32_t uUInt32;
  };
  uUInt32 = ntohl(pVal);
  return(uFloat);
}

#else
 //build into winsock2.h!
#endif


/* -------------------------------------------------------------------------------------------------
 * TCP command/data & system helpers
 * 
 * -------------------------------------------------------------------------------------------------
 */

// process & execute commands received from TCP client
void processCommand(TCP_server_single& pServer, unsigned int pCmd)
{
  // for receiving parameter values from client (e.g. software averages)
  unsigned int tMsgSize = sizeof(unsigned int) * TCP_MSG_SIZE;
  unsigned int tMsgBuf[TCP_MSG_SIZE];

  // for receiving power down pattern
  unsigned int tPatLen = tMsgSize * (1 + PD_PATTERN_REG_LENGTH);
  unsigned int tPatBuf[1 + PD_PATTERN_REG_LENGTH];

  // result for sending/receiving

  //before a command can be processed, lock access to global state structure.
  //The lock will only be removed, after the ACK has been sent to the client
  //to avoid races due to asynch processing on server and client machine
  mMtxSharedData.lock();

  unsigned int tACK  = htonl(mState.mACK);
  unsigned int tNACK = htonl(mState.mNACK);

  unsigned int tRepl = tACK; // by default, send ACK as reply
  int tLen; // return value for send/receive operations

  //select command
  switch(pCmd) 
  {
    // Enable DEBUG messages: ASCII "DEB1"
    case 0x44454231 : 
        
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received DEBUG-ENABLE-Command" << ANSI_COLOR_RESET << std::endl;
        mState.mDebFlag = true;
        break;

    // Disable DEBUG messages: ASCII "DEB0"
    case 0x44454230 : 
        
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received DEBUG-DISABLE-Command" << ANSI_COLOR_RESET << std::endl;
        mState.mDebFlag = false;
        break;
        
    // Calibrate master/slave synchronisation: ASCII "SCAL"
    case 0x5343414C :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received SCAL-Command" << ANSI_COLOR_RESET << std::endl;
        mState.mCalSyncFlag = true;
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: calibration of master/slave synchronisation triggered" << ANSI_COLOR_RESET << std::endl;
        break;
        
    // Snych. master/slaves: ASCII "SYNC"
    case 0x53594e43 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received SYNC-Command" << ANSI_COLOR_RESET << std::endl;
        mState.mSyncFlag = true;
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Master/slave synchronisation triggered" << ANSI_COLOR_RESET << std::endl;
        break;

    // Start: ASCII "RUN "
    case 0x52554e20 : 
        
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received START-Command" << ANSI_COLOR_RESET << std::endl;
        mState.mRunFlag = true;
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Measurement start triggered" << ANSI_COLOR_RESET << std::endl;
        break;
        
    // Stop: ASCII "STOP"
    case 0x53544f50 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received STOP-Command" << ANSI_COLOR_RESET << std::endl;
        mState.mRunFlag = false;
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Measurement stop triggered" << ANSI_COLOR_RESET << std::endl;
        break;

    // Save raw data: ASCII "STO1"
    case 0x53544f31 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received STORAGE-ENABLE-Command" << ANSI_COLOR_RESET << std::endl;
        mState.mSaveFlag = true;
        break;

    // Do not save raw data: ASCII "STO0"
    case 0x53544f30 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received STORAGE-DISABLE-Command" << ANSI_COLOR_RESET << std::endl;
        mState.mSaveFlag = false;
        break;

    // Stream IRF data: ASCII "IRF1"
    case 0x49524631 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received IRF-STREAM-ENABLE-Command" << ANSI_COLOR_RESET << std::endl;
        mState.mSendIRF = true;
        break;
        
    // Do not stream IRF data: ASCII "IRF0"
    case 0x49524630 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received IRF-STREAM-DISABLE-Command" << ANSI_COLOR_RESET << std::endl;
        mState.mSendIRF = false;
        break;

    // switch off Tx on all attached modules: ASCII "TXA0"
    case 0x54584130 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received TX OFF for all modules command" << ANSI_COLOR_RESET << std::endl;
        mState.mPDMode = PD_ALL_TX_OFF;
        break;

    // switch on Tx on all attached modules: ASCII "TXA1"
    case 0x54584131 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received TX ON for all modules command" << ANSI_COLOR_RESET << std::endl;
        mState.mPDMode = PD_ALL_TX_ON;
        break;

    // switch off Tx on selected module: ASCII "TXM0"
    case 0x54584D30 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received TX OFF for selected module command" << ANSI_COLOR_RESET << std::endl;
        mState.mPDMode = PD_MOD_TX_OFF;
        break;

    // switch on Tx on selected module: ASCII "TXM1"
    case 0x54584D31 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received TX ON for selected module command" << ANSI_COLOR_RESET << std::endl;
        mState.mPDMode = PD_MOD_TX_ON;
        break;

    // Exit: ASCII "EXIT"
    case 0x45584954 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received EXIT-Command" << ANSI_COLOR_RESET << std::endl;
        mState.mRunFlag  = false;
        //mState.mExitFlag = true;
        mMtxExit.lock();
          mExitShutdwn = true;
        mMtxExit.unlock();
        break;

    // Select current module: ASCII "MOD#"
    case 0x4D4F4423 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received MOD#-Command" << ANSI_COLOR_RESET << std::endl;

        //read value from client
        tLen = pServer.receiveTO((void *) tMsgBuf, tMsgSize, TCP_CMD_RECV_TO_MS);
        // data could be received?
        if (tLen < 0)
        {
          // client connection lost
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: client connection failed while receiving module select number!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else if (tLen < (int)tMsgSize)
        {
          // not enough data received
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: did not receive enough bytes for module select number: " << std::dec << tLen << "/" << tMsgSize << " bytes only!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else
        {
          // check, if the index value is valid
          size_t tIdx = ntohl(tMsgBuf[0]);
          if (tIdx < mState.mNumSen)
          {
            // all was fine, so store new value
            mSenIdx = tIdx;
            std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Storing module select number to be " << std::dec << mSenIdx << "." << ANSI_COLOR_RESET << std::endl;
          }
          else
          {
            // index out of range
            std::cout << ANSI_COLOR_RED << "Controlling::ERROR: module select number out of range: " << std::dec << tIdx << " but should be smaller than " << mState.mNumSen << "!" << ANSI_COLOR_RESET << std::endl;
            tRepl = tNACK;
          }
        }
        break;

    // current measurement ID: ASCII "MID#"
    case 0x4D494423 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received MID#-Command" << ANSI_COLOR_RESET << std::endl;

        //read value from client
        tLen = pServer.receiveTO((void *) tMsgBuf, tMsgSize, TCP_CMD_RECV_TO_MS);
        // data could be received?
        if (tLen < 0)
        {
          // client connection lost
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: client connection failed while receiving new measurement ID!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else if (tLen < (int)tMsgSize)
        {
          // not enough data received
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: did not receive enough bytes for measurement ID: " << std::dec << tLen << "/" << tMsgSize << " bytes only!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else
        {
          // all was fine, so store new value
          mState.mMID = ntohl(tMsgBuf[0]);
          std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Storing measurement ID to be " << std::dec << mState.mMID << "." << ANSI_COLOR_RESET << std::endl;
        }
        break;

    // Software Averages: ASCII "SAVG"
    case 0x53415647 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received SOFT-AVG-Command" << ANSI_COLOR_RESET << std::endl;
        
        //read value from client
        tLen = pServer.receiveTO((void *) tMsgBuf, tMsgSize, TCP_CMD_RECV_TO_MS);
        // data could be received?
        if (tLen < 0)
        {
          // client connection lost
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: client connection failed while receiving new SOFT-AVG value!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else if (tLen < (int)tMsgSize)
        {
          // not enough data received
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: did not receive enough bytes for SOFT-AVG value: " << std::dec << tLen << "/" << tMsgSize << " bytes only!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else
        {
          // all was fine, so store new value
          mState.mSW_AVG = ntohl(tMsgBuf[0]);
          std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Storing Soft_AVG to be " << std::dec << mState.mSW_AVG << "." << ANSI_COLOR_RESET << std::endl;
        }
        break;

    // Wait cycles: ASCII "WCYC"
    case 0x57435943 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received WAIT-CYC-Command" << ANSI_COLOR_RESET << std::endl;
        
        //read value from client
        tLen = pServer.receiveTO((void *) tMsgBuf, tMsgSize, TCP_CMD_RECV_TO_MS);
        // data could be received?
        if (tLen < 0)
        {
          // client connection lost
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: client connection failed while receiving new WAIT-CYC value!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else if (tLen < (int)tMsgSize)
        {
          // not enough data received
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: did not receive enough bytes for WAIT-CYC value: " << std::dec << tLen << "/" << tMsgSize << " bytes only!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else
        {
          // all was fine, so store new value
          mState.mWaitCyc = ntohl(tMsgBuf[0]);
          std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Storing Wait_Cyc to be " << std::dec << mState.mWaitCyc << "." << ANSI_COLOR_RESET << std::endl;
        }
        break;

    // Number of IRFs: ASCII "#IRF"
    case 0x23495246 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received IRF-per-Run-Command" << ANSI_COLOR_RESET << std::endl;
        
        //read value from client
        tLen = pServer.receiveTO((void *) tMsgBuf, tMsgSize, TCP_CMD_RECV_TO_MS);
        // data could be received?
        if (tLen < 0)
        {
          // client connection lost
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: client connection failed while receiving new IRF per run value!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else if (tLen < (int)tMsgSize)
        {
          // not enough data received
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: did not receive enough bytes for IRF per Run value: " << std::dec << tLen << "/" << tMsgSize << " bytes only!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else
        {
          // all was fine, so store new value
          mState.mNumIRF = ntohl(tMsgBuf[0]);
          std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Storing IRF per run to be " << std::dec << mState.mNumIRF << "." << ANSI_COLOR_RESET << std::endl;
        }
        break;

    // apply Tx pattern to currently selected module: ASCII "TXMP"
    case 0x54584D50 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received TX pattern command for module #" << mSenIdx << ANSI_COLOR_RESET << std::endl;
        
        //read pattern length and also actual pattern from client
        tLen = pServer.receiveTO((void *) tPatBuf, tPatLen, TCP_CMD_RECV_TO_MS);
        // data could be received?
        if (tLen < 0)
        {
          // client connection lost
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: client connection failed while receiving TX pattern data!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else if (tLen < (int)tPatLen)
        {
          // not enough data received
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: did not receive enough bytes for pattern data: " << std::dec << tLen << "/" << tPatLen << " bytes only!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else
        {
          // all was fine, so store new values
          mState.mPDPatLen = ntohl(tPatBuf[0]);
          std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Storing pattern length to be " << std::dec << mState.mPDPatLen << "." << ANSI_COLOR_RESET << std::endl;
          for (unsigned int tP = 0; tP < PD_PATTERN_REG_LENGTH; tP++)
          {
            mState.mPDPattern[tP] = static_cast< uint32_t >(ntohl(tPatBuf[tP+1]));
            std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Storing pattern register #" << std::dec << tP << " to be 0x" 
                      << std::hex << std::setfill('0') << std::setw(8) << mState.mPDPattern[tP] << "." << ANSI_COLOR_RESET << std::endl;
          }

          mState.mPDMode = PD_MOD_TX_PAT;
        }
        break;

    // Exponential BGR alpha: ASCII "BGRA"
    case 0x42475241 :

        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received exponential-BGR-Alpha-Command" << ANSI_COLOR_RESET << std::endl;
        
        //read value from client
        tLen = pServer.receiveTO((void *) tMsgBuf, tMsgSize, TCP_CMD_RECV_TO_MS);
        // data could be received?
        if (tLen < 0)
        {
          // client connection lost
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: client connection failed while receiving new exponential BGR alpha value!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else if (tLen < (int)tMsgSize)
        {
          // not enough data received
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: did not receive enough bytes for exponential BGR alpha value: " << std::dec << tLen << "/" << tMsgSize << " bytes only!" << ANSI_COLOR_RESET << std::endl;
          tRepl = tNACK;
        }
        else
        {
          // all was fine, so store new value
          mState.mBGR_a = (double)ntohf(tMsgBuf[0]);
          std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Storing exponential BGR alpha parameter to be " << std::dec << mState.mBGR_a << "." << ANSI_COLOR_RESET << std::endl;
        }
        break;

    // Get app status: ASCII "STA?"
    case 0x5354413F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-STATE-Command" << ANSI_COLOR_RESET << std::endl;
        break;

    // Get software averages: ASCII "SAV?"
    case 0x5341563F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-SOFT-AVG-Command" << ANSI_COLOR_RESET << std::endl;
        break;

    // Get hardware averages: ASCII "HAV?"
    case 0x4841563F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-HARD-AVG-Command" << ANSI_COLOR_RESET << std::endl;
        break;

    // Wait cycles: ASCII "WCY?"
    case 0x5743593F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-WAIT-CYC-Command" << ANSI_COLOR_RESET << std::endl;
        break;

    // Number of IRFs: ASCII "IRF?"
    case 0x4952463F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-NUM-IRF-Command" << ANSI_COLOR_RESET << std::endl;
        break;

    // Get MLBS order: ASCII "MLO?"
    case 0x4D4C4F3F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-MLBS-Order-Command" << ANSI_COLOR_RESET << std::endl;
        break;

    // Get MLBS system clock: ASCII "CLK?"
    case 0x434C4B3F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-RF-Clk-Command" << ANSI_COLOR_RESET << std::endl;
        break;

    // Get measurement ID: ASCII "MID?"
    case 0x4D49443F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-MID-Command" << ANSI_COLOR_RESET << std::endl;
        break;

    // Get current module ID: ASCII "MOD?"
    case 0x4D4F443F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-MOD#-Command" << ANSI_COLOR_RESET << std::endl;
        break;

    // Get sensor ID string: ASCII "SID?"
    case 0x5349443F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-ID-Command" << ANSI_COLOR_RESET << std::endl;
        break;

    // Get server's friendly name string: ASCII "SFN?"
    case 0x53464E3F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-SERVER-FRIENDLY-NAME-Command" << ANSI_COLOR_RESET << std::endl;
        break;

    // Get exponential BGR alpha: ASCII "BGR?"
    case 0x4247523F :
        std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: Received GET-exponential-BGR-Alpha-Command" << ANSI_COLOR_RESET << std::endl;
        break;

    default:
        std::cout << ANSI_COLOR_CYAN << "Controlling::WARNING: Unknown command: 0x" << std::hex << std::setfill('0') << std::setw(8) << pCmd << std::setfill(' ') << ", please try again..." << ANSI_COLOR_RESET << std::endl;
        tRepl = tNACK; // Send NACK
        break;
  }

  //finally find out, if current module info is valid wrt attached sensor
  bool tIdxValid = (mSenIdx < mState.mNumSen);
  if (tIdxValid)
  {
    // also update selected sensor in global state structure
    mState.mSelSen = mSenIdx;
  }
  mMtxSharedData.unlock();

  // send reply first
  unsigned int tReplSize = sizeof(tRepl);
  tLen = pServer.sendTO((void *) &tRepl, tReplSize);

  // reply could be sent?
  if (tLen < 0)
  {
    // client connection lost
    std::cout << ANSI_COLOR_RED << "Controlling::ERROR: client connection failed while sending reply!" << ANSI_COLOR_RESET << std::endl;
  }
  else if (tLen < (int)tReplSize)
  {
    // not enough data sent
    std::cout << ANSI_COLOR_RED << "Controlling::ERROR: could not send full reply: " << std::dec << tLen << "/" << tReplSize << " bytes only!" << ANSI_COLOR_RESET << std::endl;
  }

  // finally send additional data, when required by command
  unsigned int tVal;
  float        tFVal;
  char         *tID  = NULL;
  void         *tMsg = NULL;
  tMsgSize           = 0;


  switch(pCmd) 
  {
    // Get app status: ASCII "STA?"
    case 0x5354413F :

        tVal = 0;
        mMtxSharedData.lock();
          // encode flags
          if (mState.mSenAct)       tVal |= 0x01;
          if (mState.mRunFlag)      tVal |= 0x02;
          if (mState.mProcRdy)      tVal |= 0x04;
          if (mState.mDebFlag)      tVal |= 0x08;
          if (mState.mSaveFlag)     tVal |= 0x10;
          if (mState.mSyncFlag)     tVal |= 0x20;
          if (mState.mSendIRF)      tVal |= 0x40;
          if (mState.mCalSyncFlag)  tVal |= 0x80;
        mMtxSharedData.unlock();

        tMsgBuf[0] = htonl(tVal);
        std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending STATE value to client: " << std::dec << tVal << ANSI_COLOR_RESET << std::endl;

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;

    // Get software averages: ASCII "SAV?"
    case 0x5341563F :

        mMtxSharedData.lock();
          tVal = mState.mSW_AVG; 
        mMtxSharedData.unlock();

        tMsgBuf[0] = htonl(tVal);
        std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending SOFT-AVG value to client: " << std::dec << tVal << ANSI_COLOR_RESET << std::endl;

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;

    // Get hardware averages: ASCII "HAV?"
    case 0x4841563F :

        mMtxSharedData.lock();
          tVal = mState.mHW_AVG; 
        mMtxSharedData.unlock();

        tMsgBuf[0] = htonl(tVal);
        std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending HARD-AVG value to client: " << std::dec << tVal << ANSI_COLOR_RESET << std::endl;

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;

    // Wait cycles: ASCII "WCY?"
    case 0x5743593F :

        mMtxSharedData.lock();
          tVal = mState.mWaitCyc;
        mMtxSharedData.unlock();

        tMsgBuf[0] = htonl(tVal);
        std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending WAIT-CYC value to client: " << std::dec << tVal << ANSI_COLOR_RESET << std::endl;

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;

    // Number of IRFs: ASCII "IRF?"
    case 0x4952463F :

        mMtxSharedData.lock();
          tVal = mState.mNumIRF;
        mMtxSharedData.unlock();

        tMsgBuf[0] = htonl(tVal);
        std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending NUM-IRF value to client: " << std::dec << tVal << ANSI_COLOR_RESET << std::endl;

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;

    // Get MLBS order: ASCII "MLO?"
    case 0x4D4C4F3F :

        if (tIdxValid)
        {
          mMtxSharedData.lock();
            tVal = mState.mModInfos[mSenIdx].mConfig.mOrder; 
          mMtxSharedData.unlock();
        }
        else
        {
          // no valid sensor, so return invalid value
          tVal = 0;
        }

        tMsgBuf[0] = htonl(tVal);
        std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending MLBS-Order value to client: " << std::dec << tVal << ANSI_COLOR_RESET << std::endl;

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;

    // Get MLBS system clock: ASCII "CLK?"
    case 0x434C4B3F :


        if (tIdxValid)
        {
          mMtxSharedData.lock();
            tFVal = (float)mState.mModInfos[mSenIdx].mConfig.mClk; 
          mMtxSharedData.unlock();
        }
        else
        {
          // no valid sensor, so return invalid value
          tFVal = 0;
        }

        tMsgBuf[0] = htonf(tFVal);
        std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending f0 clock value to client: " << std::dec << std::setprecision(6) << tFVal << " GHz" << ANSI_COLOR_RESET << std::endl;

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;

    // Get measurement ID: ASCII "MID?"
    case 0x4D4f443F :

        mMtxSharedData.lock();
          tVal = (unsigned int)mState.mNumSen; 
        mMtxSharedData.unlock();

        tMsgBuf[0] = htonl(tVal);
        std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending # of sensors to client: '" << std::dec << tVal << "'" << ANSI_COLOR_RESET << std::endl;

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;

    // Get number of attach sensors: ASCII "MOD?"
    case 0x4D49443F :

        mMtxSharedData.lock();
          tVal = mState.mMID; 
        mMtxSharedData.unlock();

        tMsgBuf[0] = htonl(tVal);
        std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending measurement ID to client: '" << std::dec << tVal << "'" << ANSI_COLOR_RESET << std::endl;

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;

    // Get sensor ID string: ASCII "SID?"
    case 0x5349443F :

        if (tIdxValid)
        {
          mMtxSharedData.lock();
            tVal = (unsigned int)mState.mModIDs[mSenIdx].length(); 
            tID  = (char *)mState.mModIDs[mSenIdx].c_str();
          mMtxSharedData.unlock();
          std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending sensor ID to client: '" << std::dec << tID << "'" << ANSI_COLOR_RESET << std::endl;
          if (!tVal)
          {
            // no string can be sent
            tID = NULL;
          }
        }
        else
        {
          // no valid sensor, so return invalid value
          tVal = 0;
          tID  = NULL;
          std::cout << ANSI_COLOR_RED << "Controlling::ERROR: no sensor found, sending no ID to client!" << ANSI_COLOR_RESET << std::endl;
        }

        tMsgBuf[0] = htonl(tVal);

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;

    // Get server's friendly name string: ASCII "SFN?"
    case 0x53464E3F :
        // get friendly name from global structure
        mMtxSharedData.lock();
          tVal = (unsigned int)mState.mServerName.length();
          tID  = (char *)mState.mServerName.c_str();
        mMtxSharedData.unlock();
        std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending server friendly name to client: '" << std::dec << tID << "'" << ANSI_COLOR_RESET << std::endl;

        tMsgBuf[0] = htonl(tVal);

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;

    // Get exponential BGR alpha: ASCII "BGR?"
    case 0x4247523F :

        mMtxSharedData.lock();
          tFVal = (float)mState.mBGR_a;
        mMtxSharedData.unlock();

        tMsgBuf[0] = htonf(tFVal);
        std::cout << ANSI_COLOR_MAGENTA << "Controlling::INFO: sending exp. BGR alpha value to client: " << std::dec << std::setprecision(6) << tFVal << ANSI_COLOR_RESET << std::endl;

        tMsg     = (void *) tMsgBuf;
        tMsgSize = sizeof(unsigned int);
        break;
        
    default:
        break;
  }

  /* send requested value, if any */
  if (tMsgSize)
  {
    tLen = pServer.sendTO(tMsg, tMsgSize);

    // value could be sent?
    if (tLen < 0)
    {
      // client connection lost
      std::cerr << ANSI_COLOR_RED << "Controlling::ERROR: client connection failed while sending requested value!" << ANSI_COLOR_RESET << std::endl;
    }
    else if (tLen < (int)tMsgSize)
    {
      // not enough data sent
      std::cerr << ANSI_COLOR_RED << "Controlling::ERROR: could not send requested value fully: " << std::dec << tLen << "/" << tMsgSize << " bytes only!" << ANSI_COLOR_RESET << std::endl;
    }

    // additionally send a string?
    if (tID)
    {
      tLen = pServer.sendTO((void *)tID, (unsigned int)strlen(tID));

      // value could be sent?
      if (tLen < 0)
      {
        // client connection lost
        std::cerr << ANSI_COLOR_RED << "Controlling::ERROR: client connection failed while sending string!" << ANSI_COLOR_RESET << std::endl;
      }
      else if (tLen < (int)tMsgSize)
      {
        // not enough data sent
        std::cerr << ANSI_COLOR_RED << "Controlling::ERROR: could not send requested string fully: " << std::dec << tLen << "/" << strlen(tID) << " bytes only!" << ANSI_COLOR_RESET << std::endl;
      }
    }
  }
}


namespace ilmsens 
{
namespace mexplore
{
namespace tomography
{
namespace controlling
{


/* -------------------------------------------------------------------------------------------------
 * Logging & logging helpers
 * 
 * Powered by Poco:-)
 * -------------------------------------------------------------------------------------------------
 */

/*
// Logger for main app
Poco::Logger& log()
{
  static Poco::Logger& sLog = Poco::Logger::get("ilmsens.mexplore.tomography.ctrl");
  return(sLog);
}

// Logstream for main app
Poco::LogStream& logStream()
{
  // REVISIT: needs protection?
  static Poco::LogStream sStream(log());
  return(sStream);
}
*/


/* -------------------------------------------------------------------------------------------------
 * Controlling thread
 * 
 * Creates TCP server socket for command connection and accepts one client at a time.
 * 
 * When a client is successfully connected, the DataReader thread is controlled. 
 * Received commands are dispatched by the process_command function
 * via gloably defined flags.
 * 
 * When the client closes the connection, flags are kept to their last state, e.g. 
 * unattended measurements will continue.
 * 
 * -------------------------------------------------------------------------------------------------
 */

void* ctrlTCPServer(void*)
{
  bool tCtrlExit = false;

  unsigned int tCtrlPort = TCP_COMMAND_PORT;
  TCP_server_single tServer;

  try
  {
    std::cout << ANSI_COLOR_CYAN << "Controlling: Thread created." << ANSI_COLOR_RESET << std::endl;
    
#ifndef WIN32
    /* Bind Thread to single CPU Core */
    cpu_set_t tCPUSet;
    CPU_ZERO(&tCPUSet);

    CPU_SET(1, &tCPUSet); // CPU #1
    if(pthread_setaffinity_np(pthread_self(), sizeof(tCPUSet), &tCPUSet) != 0) 
    {
      std::cerr << ANSI_COLOR_RED "Controlling::ERROR: pthread_setaffinity_np did not succeed: " << ANSI_COLOR_RESET 
                << strerror(errno) << std::endl;
    }
#endif

    /* start server (listening socket) */
    tServer.startServer(tCtrlPort, TCP_CMD_SEND_TO_MS);
    
  }
  catch(ilmsens::mexplore::tomography::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "Controlling::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what()  << std::endl 
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;

    // not successful
    tCtrlExit = true;
  }
  catch(ilmsens::networking::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "Controlling::Net-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what()  << std::endl 
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;

    // not successful
    tCtrlExit = true;
  }
  catch(std::exception& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "Controlling::Std-EXCEPTION: " << ANSI_COLOR_RESET 
              << tErr.what() << std::endl;

    // not successful
    tCtrlExit = true;
  }
  catch(...) 
  {
    std::cerr << ANSI_COLOR_RED "Controlling::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;

    // not successful
    tCtrlExit = true;
  }

  // serve TCP command connection until the EXIT command has been received
  while(!tCtrlExit) 
  {
    try 
    {
      // give time for synchronisation after a client disconnected
      delay_ms(10);

      std::cout << ANSI_COLOR_CYAN << "Controlling: Waiting for Client at port " << std::dec << tCtrlPort << " ..." << ANSI_COLOR_RESET << std::endl;

      // accept client connection with timeout
      while (!tCtrlExit)
      {
        bool tClient = tServer.acceptClient(TCP_CMD_WAIT_TO_MS);
        
        if (tClient)
        {
          // a new client was accepted
          std::cout << ANSI_COLOR_CYAN << "Controlling::INFO: connection initiated with client " << tServer.getClientIP() << " ..." << ANSI_COLOR_RESET << std::endl;
          break;  
        }
        else
        {
          // no client was connected, so update the exit flag
          mMtxExit.lock();
            tCtrlExit = mExitShutdwn;
          mMtxExit.unlock();
        }
      } // waiting for client
      
      // a client is here?
      if (!tCtrlExit)
      {
        // Check connection on socket and subsequently serve commands received
        unsigned int tBuf[TCP_CMD_SIZE];
        char         *tBufBytes = (char *) tBuf; // byte-wise access
        unsigned int tCmdSize = sizeof(unsigned int) * TCP_CMD_SIZE;
        bool tCmdRcvd = false;
        unsigned int tRcvd = 0;
        std::cout << ANSI_COLOR_CYAN << "Controlling: waiting for command ..." << ANSI_COLOR_RESET << std::endl;

        while(!tCtrlExit) 
        {
          //did we process a new command last time?
          if (tCmdRcvd)
          {
            std::cout << ANSI_COLOR_CYAN << "Controlling: waiting for command ..." << ANSI_COLOR_RESET << std::endl;
            tCmdRcvd = false;
          }

          /* Reading command */
          
          /* do non-blocking read to allow asynchr. ending of program*/
          int tLen = tServer.receiveTO((void *)(tBufBytes+tRcvd), tCmdSize-tRcvd, TCP_CMD_RECV_TO_MS);

          if (tLen < 0)
          {
            //client connection has been closed
            // need to stop TCP connection, but at the same time close DR thread
            break; // inner loop
          }
          else
          {
            // at least some data may have been read
            tRcvd += tLen;
          }
          
          if (tRcvd == tCmdSize)
          {
            // a complete command has been received, so process it
            processCommand(tServer, ntohl(tBuf[0]));

            // start to wait for a new command
            tRcvd    = 0;
            tCmdRcvd = true;
          }

          // update exit flag
          mMtxExit.lock();
            tCtrlExit = mExitShutdwn;
          mMtxExit.unlock();
        }
      }
        
      if (tCtrlExit)
      {
        // ends working threads and also exits the control thread!
        std::cout << ANSI_COLOR_CYAN << "Controlling: received EXIT command, closing threads and application ..." << ANSI_COLOR_RESET << std::endl;
      }
      else
      {
        // only closes client connections
        std::cout << ANSI_COLOR_CYAN << "Controlling: no or bad client connection on command socket ..." << ANSI_COLOR_RESET << std::endl;
      }
    } 
    catch(ilmsens::mexplore::tomography::Error& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "Controlling::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
                << " Description     : " << tErr.what()  << std::endl 
                << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
                << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
    }
    catch(ilmsens::networking::Error& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "Controlling::Net-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
                << " Description     : " << tErr.what()  << std::endl 
                << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
                << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
    }
    catch(std::exception& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "Controlling::Std-EXCEPTION: " << ANSI_COLOR_RESET 
                << tErr.what() << std::endl;
    }
    catch(...) 
    {
      std::cerr << ANSI_COLOR_RED << "Controlling::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;
    }
    
    // close client's TCP connection
    tServer.closeClient();
  }

  // can go now, TCP server will be shutdown by destructor
  std::cout << ANSI_COLOR_CYAN << "Controlling: shutdown complete, exiting thread." << ANSI_COLOR_RESET << std::endl;
  return(NULL);
}

} //namespace controlling
} //namespace tomography
} //namespace mexplore
} //namespace ilmsens
