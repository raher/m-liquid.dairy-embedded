/* strmTCPServer.cpp
 * 
 * Thread function for TCP server which will accept a single client connection
 * and subsequently forward measured & info data to the client. When no connection is made, 
 * data is discarded.
 */


/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */


// Std. C++ library types / STL 

// File, IO, and Strings
#include <iostream>
#include <iomanip>
#include <cstring>
#include <chrono>

// math stuff
#include <climits>

/*
// Poco logging includes
#include "Poco/Logger.h"
#include "Poco/LogStream.h"
#include "Poco/Message.h"
*/

// Timing and threading includes
#include <pthread.h>                    // POSIX threads
#include <sched.h>                      // for Realtime Priority

// Shared variables, memories, and synchronisation ressources
#include "shared.hpp"

// TCP stuff & server class
#ifndef WIN32
  #include <arpa/inet.h> // for htonl() call
#endif
#include "ilmsens/networking/TCP_server_single.hpp"
#include "ilmsens/networking/ilmsens_networking_defn.h"
#include "ilmsens/networking/ilmsens_networking_error.hpp"

// Other Ilmsens stuff
#include "ilmsens/ilmsens_error.h"      // Ilmsens error codes

// main header for module
#include "strmTCPServer.hpp" 


/* -------------------------------------------------------------------------------------------------
 * Using clauses
 * -------------------------------------------------------------------------------------------------
 */

using namespace ilmsens::networking;


/* -------------------------------------------------------------------------------------------------
 * Local defines
 * -------------------------------------------------------------------------------------------------
 */


/* -------------------------------------------------------------------------------------------------
 * Local helper functions
 * -------------------------------------------------------------------------------------------------
 */

uint32_t float2uint32(float pVal)
{
  union
  {
    float uFloat;
    uint32_t uUInt32;
  };
  uFloat = pVal;

  return(uUInt32);
}


namespace ilmsens 
{
namespace mexplore
{
namespace tomography 
{
namespace streaming
{


/* -------------------------------------------------------------------------------------------------
 * Logging & logging helpers
 * 
 * Powered by Poco:-)
 * -------------------------------------------------------------------------------------------------
 */

/*
// Logger for main app
Poco::Logger& log()
{
  static Poco::Logger& sLog = Poco::Logger::get("ilmsens.mexplore.tomography.strm");
  return(sLog);
}

// Logstream for main app
Poco::LogStream& logStream()
{
  // REVISIT: needs protection?
  static Poco::LogStream sStream(log());
  return(sStream);
}
*/


/* -------------------------------------------------------------------------------------------------
 * Streaming thread
 * 
 * Creates TCP server socket for streaming processed data to client.
 * 
 * Accesses a cache shared with processing thread. 
 * 
 * If no client is connected, the data is just discarded. If a client is connected, data
 * is forwarded to the client as fast as possible. Rx1 data and Rx2 data are sent if requested by a
 * global flag, an additional vector containing various status & processing information is 
 * sent for each IRF.
 * 
 * -------------------------------------------------------------------------------------------------
 */

void* strmTCPServer(void*)
{
  bool tStrmExit = false;

  unsigned int tStrmPort = TCP_STREAM_PORT;
  TCP_server_single tServer;

  try
  {
    std::cout << ANSI_COLOR_MAGENTA << "Streaming: Thread created." << ANSI_COLOR_RESET << std::endl;
    
    /* Bind Thread to single CPU Core */
#ifndef WIN32
    cpu_set_t tCPUSet;
    CPU_ZERO(&tCPUSet);

    CPU_SET(2, &tCPUSet); // CPU #2
    if(pthread_setaffinity_np(pthread_self(), sizeof(tCPUSet), &tCPUSet) != 0) 
    {
      std::cerr << ANSI_COLOR_RED "Streaming::ERROR: pthread_setaffinity_np did not succeed: " << ANSI_COLOR_RESET 
                << strerror(errno) << std::endl;
    }
#endif
    
    /* start server (listening socket) */
    tServer.startServer(tStrmPort, TCP_STR_SEND_TO_MS);
  }
  catch(ilmsens::mexplore::tomography::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "Streaming::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what()  << std::endl 
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;

    // not successful
    tStrmExit = true;
  }
  catch(ilmsens::networking::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "Streaming::Net-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what()  << std::endl 
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;

    // not successful
    tStrmExit = true;
  }
  catch(std::exception& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "Streaming::Std-EXCEPTION: " << ANSI_COLOR_RESET 
              << tErr.what() << std::endl;

    // not successful
    tStrmExit = true;
  }
  catch(...) 
  {
    std::cerr << ANSI_COLOR_RED "Streaming::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;

    // not successful
    tStrmExit = true;
  }

  // streaming thread is always running (always waiting for client)
  // until the EXIT command has been received from client or signal
  while(!tStrmExit) 
  {
    try 
    {
      // give time for synchronisation after a client disconnected
      delay_ms(10);

      std::cout << ANSI_COLOR_MAGENTA << "Streaming: Waiting for Client at port " << std::dec << tStrmPort << " ..." << ANSI_COLOR_RESET << std::endl;

      // accept client connection with timeout
      while (!tStrmExit)
      {
        bool tClient = tServer.acceptClient(TCP_STR_WAIT_TO_MS);

        if (tClient)
        {
          // a new client was accepted
          std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: connection initiated with client " << tServer.getClientIP() << " ..." << ANSI_COLOR_RESET << std::endl;
          break;  
        }
        else
        {
          // no client was connected, so update the exit flag
          mMtxExit.lock();
            tStrmExit = mExitShutdwn;
          mMtxExit.unlock();

          // try to drain PR cache, if no client is there or the previous one was disconnected
          mMtxPRCache.lock();
          while(!mPRCache.empty()) 
          {
            mPRCache.pop();
          }
          mMtxPRCache.unlock();
        }
      } // waiting for client

      // a client is here?
      if (!tStrmExit)
      {
        /* DATA TRANSFER */

        // count packages streamed
        unsigned int tIdx = 0;

        /* Create data tansfer variables */
        TFIFOCacheEntry tTXData;

        TStatusInfo tStatusData(TCP_STR_STATUS_VEC_LEN, 0);

        // output debugging messages?
        unsigned int tDebug = TCP_STR_DEBUG_OUTPUT;

        mMtxSharedData.lock();
          tDebug         = mState.mDebFlag;

          bool tSendIRF  = mState.mSendIRF;

          bool tProcFlag = mState.mProcFlag; 
        mMtxSharedData.unlock();

        /* message output timing */
        auto tStart = std::chrono::system_clock::now();
        while(!tStrmExit)
        {
          // Waiting for new data in PR cache
          mMtxPRCache.lock();
            bool tEmpty = mPRCache.empty();

          if (!tEmpty)
          {
            // new data has arrived from processing or dataReader thread, so copy and forward it
              tTXData = mPRCache.front();
              mPRCache.pop();
            mMtxPRCache.unlock();

            auto tEnd = std::chrono::system_clock::now();
            auto tElapsed = std::chrono::duration_cast< std::chrono::milliseconds >(tEnd - tStart);
            if (tElapsed.count() >= TCP_STR_LOG_TO_MS)
            {
              tStart = tEnd;
              std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: sending frame #" << std::dec << tIdx << ANSI_COLOR_RESET << std::endl;
            }

            /* Fill up status vector from new frame infos */
            unsigned int tNumSen     = static_cast<unsigned int>(tTXData.mIRFNums.size());
            unsigned int tStatVecLen = tNumSen * TCP_STR_STATUS_PER_SEN;
            tStatusData.resize(tStatVecLen);

            //Parameter mapping per sensor:
            //Parameter #     Datentyp        Beschreibung
            //-----------     --------        ------------
            //0               uint32          Sequenznummer Datensatz oder Timestamp
            //1               float           Sensortemperatur [°C] (UWB-Elektronik)
            //7               uint32          ADC level Rx 1 [%]
            //8               uint32          ADC level Rx 2 [%]

            unsigned int tStatIdx     = 0;
            unsigned int tADCLevelIdx = 0;
            for (unsigned int tS = 0; tS < tNumSen; tS ++)
            {
              // IRF sequence number
              tStatusData[tStatIdx] = tTXData.mIRFNums[tS];
              tStatIdx ++;
              // sensor electronics temperature
              tStatusData[tStatIdx] = float2uint32((float)tTXData.mModTemps[tS]);
              tStatIdx ++;
              // ADC level for Rx1
              tStatusData[tStatIdx] = tTXData.mADCLevels[tADCLevelIdx];
              tStatIdx ++;
              tADCLevelIdx ++;
              // ADC level for Rx2
              tStatusData[tStatIdx] = tTXData.mADCLevels[tADCLevelIdx];
              tStatIdx ++;
              tADCLevelIdx ++;
            }

#if 0
            // fill in status data from cache entry
            tStatusData[ 0] = tTXData.mIRFNum;
            tStatusData[ 1] = float2uint32(tTXData.mSenTemp);
            tStatusData[ 2] = float2uint32(tTXData.mModTemp);
            tStatusData[ 3] = float2uint32(tTXData.mMCUTemp);
            tStatusData[ 4] = float2uint32(tTXData.mPulseAmp);
            tStatusData[ 5] = float2uint32(tTXData.mPulseDel);
            tStatusData[ 6] = UINT_MAX; //std::numeric_limits< TStatusInfo::value_type >::max(); //float2uint32(tTXData.mLaserDist);
            tStatusData[ 7] = tTXData.mADCLevel1;
            tStatusData[ 8] = tTXData.mADCLevel2;
            tStatusData[ 9] = tTXData.mTS_sec;
            tStatusData[10] = tTXData.mTSusec;
#endif

            bool tExitStream = false;
            // send the IRF data to client?
            if (tSendIRF)
            {
              // calculate transmit sizes
              unsigned int tTxSize = 0;
              if (tProcFlag)
              {
                // considering double values (potentially already correlated
                tTxSize = static_cast<unsigned int>(sizeof(TRealIRF::value_type) * tTXData.mSclIRFs.size());
              }
              else
              {
                // considering int32 values (raw data from sensors)
                tTxSize = static_cast<unsigned int>(sizeof(ImpulseResponse::value_type) * tTXData.mRawIRFs.size());
              }

              // send raw Rx data
              if (tDebug)
              {
                // for debugging
                if (tProcFlag)
                {
                  std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: sending pre-processed Rx data (" << std::dec << tTxSize << " Bytes)" << ANSI_COLOR_RESET << std::endl;
                  std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: 1. value of Rx1: " << tTXData.mSclIRFs[0] << ANSI_COLOR_RESET << std::endl;
                }
                else
                {
                  std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: sending raw Rx data (" << std::dec << tTxSize << " Bytes)" << ANSI_COLOR_RESET << std::endl;
                  std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: 1. raw value of Rx1: " << tTXData.mRawIRFs[0] << ANSI_COLOR_RESET << std::endl;
                }
              }

              unsigned int tTxLen = 0;
              char *tBuf = NULL;
              if (tProcFlag)
              {
                // considering double values (potentially already correlated
                tBuf = (char *)tTXData.mSclIRFs.data();
              }
              else
              {
                // considering int32 values (raw data from sensors)
                tBuf = (char *)tTXData.mRawIRFs.data();
              }

              while ( tTxLen < tTxSize )
              {
                int tLen = tServer.sendTO((void *)(tBuf + tTxLen), tTxSize - tTxLen);
                if(tLen < 0) 
                {
                  // client connection has been lost
                  tExitStream = true;
                  break;
                }
                else
                {
                  // count the bytes sent
                  tTxLen += tLen;
                }
              }
              if (tExitStream) break;
            }

            // send status data
            unsigned int tStatSize = sizeof(TStatusInfo::value_type) * static_cast<unsigned int>(tStatusData.size());
            if (tDebug)
            {
              // for debugging
              std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: sending status data (" << std::dec << tStatSize << " Bytes)" << ANSI_COLOR_RESET << std::endl;
              std::cout << ANSI_COLOR_MAGENTA << "Streaming::INFO: IRF #       : " << tTXData.mIRFNums[0] << std::endl
                                              << "Streaming::INFO: sensor temp.: " << tTXData.mSenTempVals[0]
                                              << ANSI_COLOR_RESET << std::endl;
            }

            unsigned int tTxLen = 0;
            char *tBuf = (char *)tStatusData.data();
            while ( tTxLen < tStatSize )
            {
              int tLen = tServer.sendTO((void *)(tBuf + tTxLen), tStatSize - tTxLen);
              if(tLen < 0) 
              {
                // client connection has been lost
                tExitStream = true;
                break;
              }
              else
              {
                // count the bytes sent
                tTxLen += tLen;
              }
            }
            if (tExitStream) break;

            // package has been sent, so it counts
            tIdx++;
          }
          else
          {
            // cache was empty
            mMtxPRCache.unlock();

            // test, if connection is still alive...do non-blocking read with short timeout
            char tBuf[TCP_STR_MSG_SIZE];
            int tLen = tServer.receiveTO((void *)tBuf, TCP_STR_MSG_SIZE, 1);
            if (tLen < 0)
            {
              // client connection has been closed,
              // need to stop streaming TCP connection
              break;
            }
            else
            {
              // wait some time to lower CPU burden
              delay_ms(10);
            }
          }

          // update configuration and shutdown flags 
          mMtxSharedData.lock();
            tDebug    = mState.mDebFlag;
            tSendIRF  = mState.mSendIRF;
          mMtxSharedData.unlock();
          mMtxExit.lock();
            tStrmExit = mExitShutdwn;
          mMtxExit.unlock();
        }
      }

      if (tStrmExit)
      {
        std::cout << ANSI_COLOR_MAGENTA << "Streaming: received EXIT command, ending thread ..." << ANSI_COLOR_RESET << std::endl;
      }
      else
      {
        std::cout << ANSI_COLOR_MAGENTA << "Streaming: no or bad client connection on streaming socket ..." << ANSI_COLOR_RESET << std::endl;
      }
    } 
    catch(ilmsens::mexplore::tomography::Error& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "Streaming::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
                << " Description     : " << tErr.what()  << std::endl 
                << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
                << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
    }
    catch(ilmsens::networking::Error& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "Streaming::Net-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
                << " Description     : " << tErr.what()  << std::endl 
                << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
                << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
    }
    catch(std::exception& tErr) 
    {
      std::cerr << ANSI_COLOR_RED << "Streaming::Std-EXCEPTION: " << ANSI_COLOR_RESET 
                << tErr.what() << std::endl;
    }
    catch(...) 
    {
      std::cerr << ANSI_COLOR_RED << "Streaming::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;
    }
    
    // close client's TCP connection
    tServer.closeClient();
  }

  // can go now, TCP server will be shutdown by destructor
  std::cout << ANSI_COLOR_MAGENTA << "Streaming: shutdown complete, exiting thread." << ANSI_COLOR_RESET << std::endl;
  return(NULL);
}

} //namespace streaming
} //namespace tomography
} //namespace mexplore
} //namespace ilmsens
