/// @file main.hpp
/// @brief App definitions, declarations, and function prototypes.
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>


#ifndef MAIN_HPP
#define MAIN_HPP

// Logging & Debug
#define LOG_LEVEL_DEFAULT  Poco::Message::Priority::PRIO_INFORMATION        ///< Default Poco log level of app

// app setup
#define TOMO_FRIENDLY_NAME        "TOMO_SERVER"                             ///< default reader-friendly name of server running this app (should be set by command line to something unique)

// sensor setup
#define TOMO_SENSOR_SETUP_TIMEOUT 10000                                     ///< Timeout while waiting for sensor setup by dataReader thread

// App configuration
#define TOMO_AUTO_START_MEAS false                                          ///< automatically start a measurement at application start?
#define TOMO_NUM_IRF         0                                              ///< default number of IRFs to measure per run (0 = infinite until user stops)

// Measurement timing
#define TOMO_SW_AVG_DEFAULT  32                                             ///< default software averages
#define TOMO_WC_DEFAULT      0                                              ///< default wait cycles

// Measurement configuration
#define TOMO_MASTER_DEFAULT_ID    "MASTER"                                  ///< default ID of master module

// raw data storage
#define TOMO_ACT_STORAGE     false                                          ///< activate storage of raw measued data by default?

// data processing
#define TOMO_ACT_PROC        false                                          ///< activate processing thread?

// streaming configuration
#define TOMO_STREAM_IRFS     true                                           ///< Stream IRF data alongside info vector via TCP server?
#define TOMO_STREAM_WAIT_INT 0                                              ///< Waiting interval [ms] between forwarding IRFs to TCP stream

// external sensor config
#define TOMO_DIST_LASER_COM     0                                           ///< default COM port number of laser distance metre; 0 = don't use it
#define TOMO_EXT_SEN_QUERY_INT  200                                         ///< default refresh interval for querying external sensors

#endif //MAIN_HPP
