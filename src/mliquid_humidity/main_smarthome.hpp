/// @file main_smarthome.hpp
/// @brief App definitions, declarations, and function prototypes.
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>


#ifndef MAIN_SMARTHOME_HPP
#define MAIN_SMARTHOME_HPP

// Logging & Debug
#define LOG_LEVEL_DEFAULT  Poco::Message::Priority::PRIO_INFORMATION      ///< Default Poco log level of app

// sensor setup
#define SH_SENSOR_SETUP_TIMEOUT 10000                                     ///< Timeout while waiting for sensor setup by dataReader thread

// App configuration
#define SH_AUTO_START_MEAS true                                           ///< automatically start a measurement at application start?
#define SH_NUM_IRF         0                                              ///< default number of IRFs to measure per run (0 = infinite until user stops)

// Measurement timing
#define SH_SW_AVG_DEFAULT  4                                              ///< default software averages
#define SH_WC_DEFAULT      0                                              ///< default wait cycles

// raw data storage
#define SH_ACT_STORAGE     false                                          ///< activate storage of raw measued data by default?

// streaming configuration
#define SH_STREAM_IRFS     true                                           ///< Stream IRF data alongside info vector via TCP server?
#define SH_STREAM_WAIT_INT 0                                              ///< Waiting interval [ms] between forwarding IRFs to TCP stream

#endif //MAIN_SMARTHOME_HPP
