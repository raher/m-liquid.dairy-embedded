/// @file main.hpp
/// @brief App definitions, declarations, and function prototypes.
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>


#ifndef MAIN_HPP
#define MAIN_HPP

// Logging & Debug
#define LOG_LEVEL_DEFAULT  Poco::Message::Priority::PRIO_INFORMATION      ///< Default Poco log level of app

// sensor setup
#define HM_SENSOR_SETUP_TIMEOUT 10000                                     ///< Timeout while waiting for sensor setup by dataReader thread

// App configuration
#define HM_AUTO_START_MEAS false                                          ///< automatically start a measurement at application start?
#define HM_NUM_IRF         0                                              ///< default number of IRFs to measure per run (0 = infinite until user stops)

// Measurement timing
#define HM_SW_AVG_DEFAULT  32                                             ///< default software averages
#define HM_WC_DEFAULT      0                                              ///< default wait cycles

// raw data storage
#define HM_ACT_STORAGE     false                                          ///< activate storage of raw measued data by default?

// streaming configuration
#define HM_STREAM_IRFS     true                                           ///< Stream IRF data alongside info vector via TCP server?
#define HM_STREAM_WAIT_INT 0                                              ///< Waiting interval [ms] between forwarding IRFs to TCP stream

// external sensor config
#define HM_DIST_LASER_COM     0                                           ///< default COM port number of laser distance metre; 0 = don't use it
#define HM_EXT_SEN_QUERY_INT  200                                         ///< default refresh interval for querying external sensors

#endif //MAIN_HPP
