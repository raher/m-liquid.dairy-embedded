/// @file hal_wrap.cpp
/// @description C++ wrapper classes for Ilmsens HAL API
/// @author pavol.kurina@gmail.com and Ralf.Herrmann@ilmsens.com

#include <iostream>
#include <cassert>
#include <cmath>
#include <chrono>
#include <thread>
#include <algorithm>
#include <cstring>

#include "hal_wrap.hpp"
#include "ilmsens/hal/ilmsens_hal.h"

using namespace ilmsens::hal;


const ilmsens_hal_Version& ilmsens::hal::getHALVersion()
{
  static ilmsens_hal_Version sHalVersion;

  int tRes = ilmsens_hal_getVersion(&sHalVersion);
  if (tRes != ILMSENS_SUCCESS) 
  {
    throw Error("getVersion failed.", tRes);
  }

  return(sHalVersion);
}

Context::Context(unsigned debugLevel)
{

  //init the HAL
  int status = ilmsens_hal_initHAL ();
  if (status < 0) 
  {
    ilmsens_hal_deinitHAL ();
    throw Error("initHAL failed.", status);
  }

  _sensorCount = status;

  //set the debug Level
  setDebugLevel(debugLevel);

  //get library version
/*  status = ilmsens_hal_getVersion (&_version);
  
  if (status < 0) 
  {
    ilmsens_hal_deinitHAL ();
    throw Error("getVersion failed.", status);
  }
  
  std::cout << "Version: " << std::dec << _version.mMajor << "." << _version.mMinor << "." << _version.mBuild << " / Status " << std::hex << status << std::endl;
*/  
  if (0 == sensorCount()) 
  {
    throw Error("No sensors found.");
  }
}

Context::~Context()
{
  ilmsens_hal_deinitHAL();
}

void Context::setDebugLevel(unsigned pDebLevel)
{
  int status = ilmsens_hal_setDEBLevel (pDebLevel);
  if (status < 0) 
  {
    throw Error("setDEBLevel failed.", status);
  }
}

SensorGroup::SensorGroup(const SensorIndexes& indexes) : _indexes(indexes)
{
  assert(size() > 0);
  int status = ilmsens_hal_openSensors (&front(), size());
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("openSensors failed.", status);
  }
}

SensorGroup::~SensorGroup()
{
  ilmsens_hal_closeSensors(&front(), size());
}

void SensorGroup::setMaster(unsigned masterPosition)
{
  // switch to master mode.....do this only on stand-alone modules or on the
  // master in a chain of modules (there can only be one master in each chain!)
  assert(masterPosition <= size());

  //de-synchronise all sensors
  int status = ilmsens_hal_synchMS (&front(), size(), 0);
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("synchMS(0) failed.", status);
  }

  //if masterPosition is 0 -> all sensors shall be masters
  //otherwise -> only the sensor at the position (1-based index!) will be master
  if (masterPosition)
  {
    // reset master-flag on all sensors to make them slaves
    status = ilmsens_hal_setMaster (&front(), size(), 0/* SLAVE */);
    if (status != ILMSENS_SUCCESS) 
    {
      throw Error("setMaster(0) failed.", status);
    }

    // set master-flag on one sensor only (position is 1-based but list-index is 0-based!)
    status = ilmsens_hal_setMaster (&at(masterPosition-1), 1, 1/* MASTER */);
    if (status != ILMSENS_SUCCESS) 
    {
      throw Error("setMaster(1) failed.", status);
    }
  }
  else
  {
    // set master-flag on all sensors
    status = ilmsens_hal_setMaster (&front(), size(), 1 /* MASTER */);
    if (status != ILMSENS_SUCCESS) 
    {
      throw Error("setMaster(1) failed.", status);
    }
  }

  //re-synchronise all sensors
  status = ilmsens_hal_synchMS (&front(), size(), 1);
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("synchMS(1) failed.", status);
  }
}

void SensorGroup::setSWAvgerages(unsigned pSWAvg, unsigned pWC)
{
  //set new software averages with 0 wait cycles
  int status = ilmsens_hal_setAvg (&front(), size(), pSWAvg, pWC);
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("setAvg failed.", status);
  }
}

Measurement::Measurement(SensorGroup& group, size_t bufferSize, bool bufferedMode)
  : _sensors(group)
  , _bufferSize(bufferSize)
{
  assert(bufferSize > 0);
  int status = ilmsens_hal_measRun (&_sensors.front(), _sensors.size(), bufferedMode ? ILMSENS_HAL_RUN_BUF : ILMSENS_HAL_RUN_RAW);
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("measRun failed.", status);
  }
}

Measurement::~Measurement()
{
  int status = ilmsens_hal_measStop (&_sensors.front(), _sensors.size());
  if (status != ILMSENS_SUCCESS) 
  {
    std::cerr << "measStop failed with " << std::dec << status << "." << std::endl;
  }
}

static bool measurementReady(const SensorGroup& sensors)
{
  SensorGroup& nonconst_sensors = const_cast<SensorGroup&>(sensors);
  int status = ilmsens_hal_measRdy (&nonconst_sensors.front(), nonconst_sensors.size());
  if (status < 0) 
  {
    throw Error("measRdy failed", status);
  }
  return status > 0;
}

static size_t readMeasurement(const SensorGroup& sensors, ImpulseResponse& buffer)
{
  SensorGroup& nonconst_sensors = const_cast<SensorGroup&>(sensors);
  void* start = &buffer.front();
  int status = ilmsens_hal_measRead (&nonconst_sensors.front(), nonconst_sensors.size(), static_cast<int*>(start), buffer.size()*sizeof(SampleType));
  if (status < 0) 
  {
    throw Error("measRead failed", status);
  }
  return status;
}

template<class T> 
std::chrono::milliseconds as_milliseconds(const T& duration)
{
  return std::chrono::duration_cast<std::chrono::milliseconds>(duration);
}

ImpulseResponse Measurement::next(std::chrono::milliseconds timeout)
{
  using namespace std::chrono;
  auto start = steady_clock::now();
  auto ready = measurementReady(_sensors);
  auto diff = as_milliseconds(steady_clock::now() - start);
  while (!ready && diff < timeout) 
  {
    // REVISIT: this assumes that timeout is less than 10x of min(measurement-period) of the group!
    std::this_thread::sleep_for((std::max)(milliseconds(1), diff / 10));
    ready = measurementReady(_sensors);
    diff = as_milliseconds(steady_clock::now() - start);
  }
  if (!ready) 
  {
    throw Error("Timeout on measRdy expired.");
  }
  ImpulseResponse buffer(_bufferSize);
  buffer.resize(readMeasurement(_sensors, buffer));
  return buffer;
}

unsigned ilmsens::hal::configure(SensorGroup& sensors, unsigned mlbsOrder, unsigned ov, double masterClk, unsigned receiverCount, unsigned softwareAvg, unsigned masterPosition)
{
  //default measurement time configuration
  const unsigned int mNum_SW_AVG = softwareAvg; // number of software averages to use: min. 0, max. 2^16-1
  const unsigned int mNum_Wait = 0;             // number of wait cycles between acquisitions (i.e. for use in snapshot mode): min. 0, max. 2^16-1

  //read out ID of each attached sensor
  char tID[ILMSENS_HAL_MOD_ID_BUF_SIZE] = {0}; //max length allocated
  for(unsigned int tS = 0; tS < sensors.size(); tS++)
  {
    int count = 0;
    tID[0] = 0; //empty string
    while (strlen(tID) == 0 && count < 10) 
    {  
      int tErg = ilmsens_hal_getModId (sensors.at(tS), tID, sizeof(tID));

      std::cout << "Sensor #" << std::dec << sensors.at(tS) << " has ID '" << tID
              <<   "' (result was " << std::dec << tErg << ")." << std::endl;
              count++;
    }
  }
  if (strlen(tID) == 0) 
  {
    throw Error("Cannot read Sensor-ID", ILMSENS_ERROR_UNKNOWN);
  }

  //make basic sensor setup
  ilmsens_hal_ModConfig tMCnf;

  tMCnf.mClk = masterClk;        // RF system clock [GHz]
  tMCnf.mOrder = mlbsOrder;      // order of MLBS: 9, 12, or 15
  tMCnf.mSub = 0;                // clock divider for real sampling rate: use default
  tMCnf.mOV = ov;                // number of oversampling: use provided value
  tMCnf.mTx = 0;                 // number of transmitters: use default
  tMCnf.mRx = receiverCount;     // number of Rx: usually 2 per sensor

  int status = ilmsens_hal_setupSensors (&sensors.front(), sensors.size(), &tMCnf);
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("setupSensors failed.", status);
  }

  //averaging

  //read out HW averages for each sensor
  ilmsens_hal_ModInfo tMInfo;
  for(unsigned int tS=0; tS < sensors.size(); tS++)
  {
    status = ilmsens_hal_getModInfo (sensors.at(tS), &tMInfo);
    if (status != ILMSENS_SUCCESS) 
    {
      throw Error("getModInfo failed.", status);
    }
    std::cout << "Sensor #" << std::dec << sensors.at(tS) << " has HW averages set to: "
              << std::dec << tMInfo.mHWAvg << "." << std::endl;
    //store actual module basic parameters
    tMCnf = tMInfo.mConfig;
  }

  //new set SW averages and wait cycles
  sensors.setSWAvgerages(mNum_SW_AVG, mNum_Wait);
  /*
  status = ilmsens_hal_setAvg (&sensors.front(), sensors.size(), mNum_SW_AVG, mNum_Wait);
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("setAvg failed.", status);
  }
  */

  //set the master sensor and do digital synchronisation
  sensors.setMaster(masterPosition);

  //MLBS transmitter
  status = ilmsens_hal_setMLBS (&sensors.front(), sensors.size()); //reset
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("setMLBS failed.", status);
  }
  status = ilmsens_hal_setPD (&sensors.front(), sensors.size(), 0); //unmute
  if (status != ILMSENS_SUCCESS) 
  {
    throw Error("setPD failed.", status);
  }


  //Poll the first sensor for configuration (number if receivers/channels etc). 
  //Must be the same for all active nodes after above steps have completed successfully!
  return sensors.size() * static_cast<unsigned>(1 << tMCnf.mOrder) * tMCnf.mOV * tMCnf.mRx; //return required buffer size for measRead/Get
}
