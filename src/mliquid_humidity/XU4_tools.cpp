/* XU4_tools.cpp
 * 
 * Functions related to Odroid XU4 board, e.g. read board temperatures.
 */


/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */

// File, IO, and String-Streams
#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdlib>

// Time measurement
#include <chrono>

// Own header
#include "XU4_tools.hpp"


/* -------------------------------------------------------------------------------------------------
 * Using name spaces
 * -------------------------------------------------------------------------------------------------
 */

using namespace std;

/* -------------------------------------------------------------------------------------------------
 * Local variables
 * -------------------------------------------------------------------------------------------------
 */

/* -------------------------------------------------------------------------------------------------
 * XU4 functions
 * -------------------------------------------------------------------------------------------------
 */

// Reading temperature(s) of XU4 board from sys-file (updates max. once per second)
#if defined(WIN32)
const TTemperatures& readXU4Temperature(void)
{
  // storage for XU4 tempertures: vector holding temperatures read from sys file
  static TTemperatures sTemps(5, 0);

  // all was fine, return the unaltered results
  return (sTemps);
}

#else

//ordoid XU4 on linux
const TTemperatures& readXU4Temperature(void)
{
  // storage for XU4 tempertures: vector holding temperatures read from sys file
  static TTemperatures sTemps(5, 0);

  // mind the first call to this function
  static bool sFirstTime = true;
  // mind the system time when the function was last called
  static std::chrono::system_clock::time_point sTempTime = std::chrono::system_clock::now();

  // check, if enough time has elapsed and an update of temperature is needed
  std::chrono::system_clock::time_point tCurTime = std::chrono::system_clock::now();
  auto tElapsed = std::chrono::duration_cast< std::chrono::milliseconds >(tCurTime - sTempTime);
  if ((tElapsed.count() > TEMP_UPDATE_INT_MS) || sFirstTime)
  {
    // time interval elapsed, so do an update and store current time
    sFirstTime = false;
    sTempTime  = tCurTime;
  
    // Open system file with temperature data
    FILE* tFD = fopen("/sys/devices/10060000.tmu/temp", "r"); 
    if(tFD == NULL) 
    {
      //TODO: make it an exception or ignore error?
      perror("readXU4Temperature::ERROR: could open temperature file");
    }
    else
    {
      //string buffer
      char tTempXU4[XU4_TEMP_MAX_LEN];
      
      // Read Temperature from file, then close it
      size_t tLen = fread(tTempXU4, sizeof(char), XU4_TEMP_MAX_LEN, tFD);
      fclose(tFD);

      if (tLen < XU4_TEMP_MIN_LEN)
      {
        std::cerr << "readXU4Temperature::ERROR: could not read enough data from XU4 temperature file, fread returned: " << std::dec << tLen << "!" << std::endl;
      }
      else
      {
        // Seperate single values out of total string
        
        //string buffer
        char tCurTemp[3];

        tCurTemp[2] = 0; //terminate string
        
        // 1. temperature
        tCurTemp[0] = tTempXU4[10]; 
        tCurTemp[1] = tTempXU4[11];
        sTemps[0]   = atoi(tCurTemp);
        
        // 2. temperature
        tCurTemp[0] = tTempXU4[26]; 
        tCurTemp[1] = tTempXU4[27];
        sTemps[1]   = atoi(tCurTemp);
        
        // 3. temperature
        tCurTemp[0] = tTempXU4[42]; 
        tCurTemp[1] = tTempXU4[43];
        sTemps[2]   = atoi(tCurTemp);
        
        // 4. temperature
        tCurTemp[0] = tTempXU4[58]; 
        tCurTemp[1] = tTempXU4[59];
        sTemps[3]   = atoi(tCurTemp);
        
        // 5. temperature
        tCurTemp[0] = tTempXU4[74]; 
        tCurTemp[1] = tTempXU4[75];
        sTemps[4]   = atoi(tCurTemp);
      }
    }
  }
  
  // all was fine, return the results
  return (sTemps);
}

#endif