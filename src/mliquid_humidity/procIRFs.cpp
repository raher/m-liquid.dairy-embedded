/* procIRFs.cpp
 * 
 * Thread function doing basic processing of raw M-sequence data retrieved from a cache buffer.
 * MLBS-correlation is performed by using cross-correlation method. Pulse parameters are extracted.
 * Processed data is then forwarded into another cache buffer.
 */


/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */


// Std. C++ library types / STL 

// IO, and Strings
#include <iostream>
#include <iomanip>
#include <vector>
#include <algorithm>
#include <deque>
#include <chrono>

#include <cstring>

#include <cmath>

// Math
#include <algorithm>                    // for max_element

/*
// Poco logging includes
#include "Poco/Logger.h"
#include "Poco/LogStream.h"
#include "Poco/Message.h"
*/

// Timing and threading includes
#include <pthread.h>                    // POSIX threads
#include <sched.h>                      // for Realtime Priority


// Shared variables, memories, and synchronisation ressources
#include "shared.hpp"

// Ilmsens processing library
#include "ilmsens/processing/ilmsens_processing_error.hpp"
#include "ilmsens/processing/KKF_interp_ECC.hpp"   // Ilmsens MLBS-KKF class for ECC-sensors
#include "ilmsens/processing/vector_iteration_functions.hpp"
#include "ilmsens/processing/simple_math_functions.hpp"

// Other Ilmsens stuff
#include "ilmsens/ilmsens_error.h"      // Ilmsens error codes

// mean header for module
#include "procIRFs.hpp" 

using namespace ilmsens::processing;

namespace ilmsens 
{
namespace mliquid
{
namespace humidity 
{
namespace processing
{


/* -------------------------------------------------------------------------------------------------
 * Processing types
 */


/* -------------------------------------------------------------------------------------------------
 * Processing buffers and variables
 */

static TIdxSample sPulsePara;               // default entry for pulse parameters (amplitude/delay)

static TRealIRF   sInterpIRF;               // interpolated IRF for validity analysis


/* -------------------------------------------------------------------------------------------------
 * Online data analysis algorithm
 * 
 * Receives correlated and time-zero-aligned data to interpolate and extract pulse parameters.
 * 
 * It requires access to the FRF of the last correlation operation on the 'liquid' channel
 * to perform efficient FFT-interpolation
 * 
 * -------------------------------------------------------------------------------------------------
 */

TIdxSample procLiquidData (KKF_interp_ECC& pKKF)
{
  /* get parameters for current IRF */
  
  // get interpolated liquid channel IRF
  sInterpIRF = pKKF.calculateInterpolation(); // uses the correlated spectrum from last KKF call
  
  // find current milk pulse
  TMinMax tIPMinMax = getRlMinMax(sInterpIRF);
  
  // extract current main pulse parameters
  TIdxSample tMaxVal = tIPMinMax.mMax;
  if (fabs(tIPMinMax.mMin.mVal) > tIPMinMax.mMax.mVal)
  {
    tMaxVal = tIPMinMax.mMin;
  }
  
  return(tMaxVal);
}


/* -------------------------------------------------------------------------------------------------
 * Processing thread
 * 
 * Receives data during running measurement from DataReader and process it according to requested
 * processing flags from client. The thread is created and shutdown by the DataReader.
 * 
 * By default, MLBS correlation by KKF is performed, the data interpolated, and pulse parameters
 * are extracted. 
 * 
 * -------------------------------------------------------------------------------------------------
 */

void* procIRFs(void*)
{
  try 
  {
    std::cout << ANSI_COLOR_YELLOW << "Processing: thread created" << ANSI_COLOR_RESET << std::endl;

    /* Bind Thread to single CPU Core */
#ifndef WIN32
    cpu_set_t tCPUSet;
    CPU_ZERO(&tCPUSet);

    CPU_SET(7, &tCPUSet); // CPU #7
    CPU_SET(8, &tCPUSet); // CPU #8
    if(pthread_setaffinity_np(pthread_self(), sizeof(tCPUSet), &tCPUSet) != 0) 
    {
      std::cerr << ANSI_COLOR_RED "Processing::ERROR: pthread_setaffinity_np did not succeed: " << ANSI_COLOR_RESET 
                << strerror(errno) << std::endl;
    }
#endif

    /* Get required configuration information for processing */
    mMtxSharedData.lock();
      unsigned int tMLBSLen = mState.mMLBSLen;
      bool tDebug           = mState.mDebFlag;
      //double tFClk          = mState.mClk;

      ilmsens_hal_ModInfo tModInfo = mState.mModInfo;
      //double tMeasRate      = mState.mMeasRate;
      double tSampClk       = mState.mSampClk;

      unsigned int tStrWait = mState.mStrWait;
    mMtxSharedData.unlock();

    // mind number of samples before interpolation
    double tNumSamp = (double) tMLBSLen;

    // apply some interpolation
    unsigned int tInterpLen = tMLBSLen * PR_IP_FAK;
    //round it up to next power of 2
    tInterpLen = (unsigned int) ilmsens::processing::getNextPow2_32((uint32_t) tInterpLen);

    // mind number of samples after interpolation and get final ROI definition
    double tNumSampIP = (double) tInterpLen;

    //update (virtual) sampling clock according to interpolation
    tSampClk *= tNumSampIP / tNumSamp;


    /* Acquire pre-processing ressources as needed */
    KKF_interp_ECC myKKF(tModInfo.mConfig.mOrder, tModInfo.mConfig.mOV, tInterpLen, true);
    std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: KKF handler created with MLBS order " << tModInfo.mConfig.mOrder 
                                   << " | oversampling factor " << tModInfo.mConfig.mOV 
                                   << " | IRF length (incl. interpolation) " << tInterpLen << ANSI_COLOR_RESET << std::endl;

    
    // get sample spacing in time by using f0 and interpolated length
    TSampleSmall tTs = (TSampleSmall)1.0 / ((TSampleSmall)tSampClk); // sampling time spacing with interpolation [ns]


    // additional vectors/buffers
    TFIFOCacheEntry tRawDat;
    TFIFOCacheEntry tProcData;

    tProcData.mCorrRx1.resize(tMLBSLen);
    tProcData.mCorrRx2.resize(tMLBSLen);
    tProcData.mProcRx1.resize(tMLBSLen);
    tProcData.mProcRx2.resize(tMLBSLen);

    // buffers for local processing
    TRealIRF tIRF1(tMLBSLen, 0);
    TRealIRF tIRF2(tMLBSLen, 0);

    TRealIRF tBG1(tMLBSLen, 0);
    TRealIRF tBG2(tMLBSLen, 0);

    sInterpIRF.resize(tInterpLen); // adapt processing vector size to current IRF length

    /* prepare processing states */
    unsigned int tZeroIdx = 0;      // index of time-zero for milk analysis
    bool tGetRefPulsePos  = true;   // get current reference index of pulse from 1st IRF


    /* main loop is executed until shutdown flag is received */

    // initialize execution-time-measurement
    auto tStart = std::chrono::steady_clock::now();
    auto tEnd   = tStart;

    // set processing ready flag and get related processing flags
    mMtxSharedData.lock();
      double tBGR_a   = mState.mBGR_a; // exponential BGR
      mState.mProcRdy = true;
      bool tDoTimeZero = mState.mDoTimeZero;
    mMtxSharedData.unlock();

    // get state of processing
    std::cout << ANSI_COLOR_YELLOW << "Processing: waiting for data to process ..." << ANSI_COLOR_RESET << std::endl;
    mMtxProcessing.lock();
      bool tEndProc = mProcShutdwn;
    mMtxProcessing.unlock();
    while(!tEndProc) 
    {
      // Check if new data is available in DataReader cache
      mMtxDRCache.lock();
        bool tDRCEmpty = mDRCache.empty();
        
      if (!tDRCEmpty)
      {
        /* new data is there, so get & process it */
        
        // pop data from cache
          tRawDat = mDRCache.front(); // Get new Data
          mDRCache.pop();             // Delete new Data in Cache
        mMtxDRCache.unlock();

        if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: received new data ..." << ANSI_COLOR_RESET << std::endl;


        /* process new data for humidity analysis */
        auto tStartProc = std::chrono::steady_clock::now();
        
        // rotate vectors to time-zero (ATTENTION: will not be valid for first IRF!)
        if (tDoTimeZero)
        {
          if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: time-zero correction ..." << ANSI_COLOR_RESET << std::endl;
          std::rotate(tRawDat.mCorrRx1.begin(), tRawDat.mCorrRx1.begin() + tZeroIdx, tRawDat.mCorrRx1.end());
          std::rotate(tRawDat.mCorrRx2.begin(), tRawDat.mCorrRx2.begin() + tZeroIdx, tRawDat.mCorrRx2.end());
        }

        auto tElRot = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::steady_clock::now() - tStartProc);

        /* do MLBS correlation */
        if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: starting KKF ..." << ANSI_COLOR_RESET << std::endl;
        tIRF2 = myKKF.calculateKKF(tRawDat.mCorrRx2);
        tIRF1 = myKKF.calculateKKF(tRawDat.mCorrRx1);
        if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: KKF done ..." << ANSI_COLOR_RESET << std::endl;

        auto tElKKF = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::steady_clock::now() - tStartProc);

        // copy state and additional info
        tProcData.mIRFNum     = tRawDat.mIRFNum;     // Copy index
        tProcData.mADCLevel1  = tRawDat.mADCLevel1;  // Copy ADC Level
        tProcData.mADCLevel2  = tRawDat.mADCLevel2;  // Copy ADC Level

        tProcData.mSenTempVal = tRawDat.mSenTempVal;  // Copy Temp. (additional IRF info from Rx2 of sensor)
        tProcData.mXU4MaxTemp = tRawDat.mXU4MaxTemp;  // max. Temp. of XU4
        tProcData.mXU4Temp    = tRawDat.mXU4Temp;     // Copy XU4 Temp.
        tProcData.mSenTemp    = tRawDat.mSenTemp;     // probe temperature
        tProcData.mModTemp    = tRawDat.mModTemp;     // module electronic's temperature

        tProcData.mLaserDist  = tRawDat.mLaserDist;   // Laser distance information

        tProcData.mTS_sec     = tRawDat.mTS_sec;     // Time stamp seconds
        tProcData.mTSusec     = tRawDat.mTSusec;     // Time stamp microseconds

        // copy correlated and aligned data to cache entry (raw data before processing)
        tProcData.mCorrRx1 = tIRF1;
        tProcData.mCorrRx2 = tIRF2;

        /* extract pulse parameters for time-zero and energy analysis */       
        if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: starting raw pulse analysis ..." << ANSI_COLOR_RESET << std::endl;

        // analyse liquid channel parameters
        if (tDebug)
        {
          TIRFPara tVoltPara = getRlIRFPara(tIRF1); 
          std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: pulse parameters Rx1:" << std::endl 
                    << "total energy  : " << std::dec << tVoltPara.mEn << std::endl
                    << "max. val | idx: " << std::dec << tVoltPara.mMax.mVal << " | " << tVoltPara.mMax.mIdx << std::endl
                    << "min. val | idx: " << std::dec << tVoltPara.mMin.mVal << " | " << tVoltPara.mMin.mIdx << std::endl
                    << "Processing::INFO: temperatures (probe/module/XU4): " << std::dec << tProcData.mSenTemp << "°C | " << tProcData.mModTemp << "°C | " << tProcData.mXU4Temp << "°C" << std::endl 
                    << ANSI_COLOR_RESET << std::endl;
        }

        // get time-zero reference ?
        if (tGetRefPulsePos && tDoTimeZero)
        {
          // get current pulse position of reference channel for time-zero
          TMinMax tRefMinMax = getRlMinMax(tIRF2);
          if (fabs(tRefMinMax.mMin.mVal) > tRefMinMax.mMax.mVal)
          {
            tZeroIdx = tRefMinMax.mMin.mIdx;
          }
          else
          {
            tZeroIdx = tRefMinMax.mMax.mIdx;
          }

          tGetRefPulsePos = false;
        }

        auto tElPara = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::steady_clock::now() - tStartProc);

        /* --- do processing steps as requested by client--- */

        // exponential BGR
        if (tBGR_a != 0.0)
        {
          // handle Rx1
          expBGRRl(tIRF1, tBG1, tIRF1, tBGR_a);
          // handle Rx2
          expBGRRl(tIRF2, tBG2, tIRF2, tBGR_a);
        }

        /* --- end of processing steps as requested by client--- */

        auto tElStdProc = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::steady_clock::now() - tStartProc);


        /* --- analyse pulse parameters - humidity processing --- */
        if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: starting analysis ..." << ANSI_COLOR_RESET << std::endl;

        sPulsePara = procLiquidData (myKKF);
        tProcData.mPulseAmp = (TSampleSmall) sPulsePara.mVal; // pulse maximum amplitude
        tProcData.mPulseDel = ((TSampleSmall) sPulsePara.mIdx) * tTs; // pulse maximum delay [ns]

        auto tElProc = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::steady_clock::now() - tStartProc);

        /* --- Do additonal processing --- */

        /*
          if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: starting additional processing ..." << ANSI_COLOR_RESET << std::endl;
         *
         *
         *
         *
         *
          if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: additional processing done ..." << ANSI_COLOR_RESET << std::endl;
         */

        /* --- End of additonal processing --- */       

        auto tElAddProc = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::steady_clock::now() - tStartProc);

        // output timing info
        if (tDebug)
        {
          std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: durations of different processing stages: " << std::endl
                    << "Rotation       : " << std::dec << tElRot.count() << " us" << std::endl
                    << "KKF            : " << std::dec << tElKKF.count() << " us" << std::endl
                    << "Raw pulse para.: " << std::dec << tElPara.count() << " us" << std::endl
                    << "Std. processing: " << std::dec << tElStdProc.count() << " us" << std::endl
                    << "Lqd. processing: " << std::dec << tElProc.count() << " us" << std::endl
                    << "Add. processing: " << std::dec << tElAddProc.count() << " us" << std::endl
                    << ANSI_COLOR_RESET << std::endl;
        }


        /* forward new data to streaming, if suitable time interval has elapsed */

        // get time elpased since last data retrieval
        tEnd = std::chrono::steady_clock::now();
        auto tElapsed = std::chrono::duration_cast< std::chrono::milliseconds > (tEnd - tStart);
        if (tElapsed.count() >= tStrWait)
        {
          // enough time has passed, so forward data to streaming process
          tStart = tEnd;

          // copy processed data
          tProcData.mProcRx1 = tIRF1;
          tProcData.mProcRx2 = tIRF2;

          /* Store data in streaming cache */
          mMtxPRCache.lock();
            tProcData.mCacheSize = (TAddInfo) mPRCache.size();
            mPRCache.push(tProcData);
          mMtxPRCache.unlock();
        }
      }
      else
      {
        // unlock DR cache
        mMtxDRCache.unlock();

        // wait some time to lower CPU burden
        delay_ms(10);

        // Update processing state - only when cache is empty.
        // This should ensure, that all received data is being processed, before the thread is closed
        mMtxProcessing.lock();
          tEndProc = mProcShutdwn;
        mMtxProcessing.unlock();
      }

      // Update debugging and run state, update processing parameters
      double tOldBGR_a = tBGR_a;
      mMtxSharedData.lock();
        //flags
        tDebug = mState.mDebFlag;

        //parameters
        tBGR_a      = mState.mBGR_a;
        tDoTimeZero = mState.mDoTimeZero;
      mMtxSharedData.unlock();

      // reset background, if BGR is switch off
      if ((tOldBGR_a != 0.0) && (tBGR_a == 0.0))
      {
        tBG1.clear();
        tBG1.resize(tMLBSLen, 0.0);
        tBG2.clear();
        tBG2.resize(tMLBSLen, 0.0);
      }
    }

    //give time for synchronisation
    delay_ms(10);
  } 
  catch(ilmsens::mliquid::humidity::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "Processing::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl 
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
  }
  catch(ilmsens::processing::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "Processing::Prc-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl 
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl;
  }
  catch(std::exception& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "Processing::Std-EXCEPTION: " << ANSI_COLOR_RESET 
              << tErr.what() << std::endl;
  }
  catch(...) 
  {
    std::cerr << ANSI_COLOR_RED << "Processing::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;
  }
  
  // can go now
  std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: shutdown complete, exiting thread" << ANSI_COLOR_RESET << std::endl;
  return(NULL);
}

} //namespace processing
} //namespace humidity
} //namespace mliquid
} //namespace ilmsens
