/* pollExtSensors.cpp
 * 
 * Thread function polling external sensors like temperature or laser distance metres in a 
 * fixed refrsh interval. Data is stored in the global shared data structure.
 */


/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */


// Std. C++ library types / STL 

// IO, and Strings
#include <iostream>
#include <iomanip>

// errors and exepctions
#include "ilmsens/tools/ilmsens_tools_error.hpp"



// RS232 support library
//#include <unistd.h>
#include "rs232.h"

// Shared variables, memories, and synchronisation ressources
#include "shared.hpp"

// main header for module
#include "pollExtSensors.hpp" 

//using namespace ilmsens::processing;

namespace ilmsens 
{
namespace mliquid
{
namespace humidity 
{
namespace sensors
{

void* pollExtSensors(void*)
{
  unsigned int mPollInt  = 0;
  unsigned int mLaserCOM = 0;
  bool mUseLaser    = false; 
  bool mSerialError = false;

  try
  {
    std::cout << ANSI_COLOR_YELLOW << "ExtSensors: polling thread created" << ANSI_COLOR_RESET << std::endl;

    // get infos from shared structure
    mMtxSharedData.lock();
      mPollInt   = mState.mExtPollInt;
      mLaserCOM  = mState.mLaserCOM;

      bool tDebug = mState.mDebFlag;
    mMtxSharedData.unlock();

    //shall we use teh laser sensor at all?
    int tRS232Result = 0;
    if (mLaserCOM)
    {
      mUseLaser = true;

      // try to open connection to laser distance sensor
      char tUARTMode[]={'8','N','1',0};
      std::cout << ANSI_COLOR_YELLOW << "ExtSensors::INFO: Opening COM port " << std::dec << mLaserCOM << " with UART mode '" << tUARTMode << "'." << std::endl
                                     << "                  Polling interval will be " << mPollInt << " ms." << ANSI_COLOR_RESET << std::endl;

      tRS232Result = RS232_OpenComport((int)mLaserCOM, 38400, tUARTMode);
      if(tRS232Result)
      {
        std::cout << ANSI_COLOR_RED << "ExtSensors::ERROR: Error while opening COM port! Return code was: " << tRS232Result << ANSI_COLOR_RESET << std::endl;
        throw ilmsens::tools::Error("ExtSensors::ERROR: could not open COM port for laser distance metre!", ILMSENS_ERROR_IO, tRS232Result);
      }
    }
	
    // now do polling loop until exist flag has been raised
    mMtxExit.lock();
      bool tExitFlag = mExitShutdwn;
    mMtxExit.unlock();

    while(!tExitFlag)
    {
      //wait the given refresh interval
      delay_ms(mPollInt);

      //check, if external sensor shall be querried anyways
      mMtxSharedData.lock();
        bool tDoPoll = mState.mExtSenDoPoll;
        float tLaserDist = mState.mLaserDistance;
      mMtxSharedData.unlock();
      if (tDoPoll)
      {
        // poll the laser distance metre?
        if (mUseLaser)
        {
          //flush UART
          RS232_flushRXTX(mLaserCOM);

          //query laser distance: write command to laser
	        RS232_cputs(mLaserCOM, "/020D0059.");

          unsigned char tLaserBuf[32]; 

          //poll COM port until start of answer was detected
          unsigned int tPollCnt = 0;
          do
          {
            tRS232Result = RS232_PollComport(mLaserCOM, tLaserBuf, 1);
            if (!tRS232Result)
            {
              //wait some time
              delay_ms(1);
              tPollCnt ++;
            }
          } while(!tRS232Result && (tPollCnt < COM_ANSWER_TO_MS));

          //check, if result starts with correct indicator
          if(tRS232Result && (tLaserBuf[0] == '/'))
          {
            int tOffSet = 1;
            int tRetLen = 18; //expected remaining length of returned result
          
            tPollCnt = 0;
            do
            {
              tRS232Result = RS232_PollComport(mLaserCOM, tLaserBuf+tOffSet, tRetLen);
              if (!tRS232Result)
              {
                //wait some time
                delay_ms(1);
                tPollCnt ++;
              }
              else
              {
                tOffSet += tRS232Result;
                tRetLen -= tRS232Result;
              }
            } while((tRetLen > 0) && (tPollCnt < COM_ANSWER_TO_MS));

            //finish received string and show answer
            tLaserBuf[tOffSet] = 0;
            if (tDebug)
            {
              std::cout << ANSI_COLOR_YELLOW << "ExtSensors::INFO: laser distance metre returned '" << tLaserBuf << "' for distance query." << ANSI_COLOR_RESET << std::endl;
            }

            //check received length
            if (tRetLen == 0)
            {
              if(strchr((char *)tLaserBuf, '+') && strchr((char *)tLaserBuf, 'u')) 
              {
                char *tPart = strchr((char *)tLaserBuf, '+');
                //skip '+' character
                tPart ++;

                //search for number delimited by "u" token
  #ifdef _MSC_VER
    #pragma warning(disable:4996)
  #endif
                tPart = strtok(tPart, "u");
  #ifdef _MSC_VER
    #pragma warning(default:4996)
  #endif
                tLaserDist = (float)atof(tPart);
              }
              else
              {
                //did not get valid answer, so report invaid result
                tLaserDist = -1.0;
              }
            }
            else
            {
              //did not receive full answer within timeout
              std::cout << ANSI_COLOR_YELLOW << "ExtSensors::ERROR: laser distance metre returned incomplete answer '" << tLaserBuf << "'! Aborting query." << ANSI_COLOR_RESET << std::endl;
              //must report invalid result
              tLaserDist = -1.0;
            }
	        }
          else
          {
            //must report invalid result
            tLaserDist = -1.0;
            //show reason
            if (tDebug)
            {
              if (tRS232Result)
              {
                tLaserBuf[1] = 0;
                std::cout << ANSI_COLOR_YELLOW << "ExtSensors::ERROR: laser distance metre returned answer with wrong SOF'" << tLaserBuf << "'! Aborting query." << ANSI_COLOR_RESET << std::endl;
              }
              else
              {
                std::cout << ANSI_COLOR_YELLOW << "ExtSensors::ERROR: laser distance metre did not return answer within tiemout!" << ANSI_COLOR_RESET << std::endl;
              }
            }
          }
        } // poll laser distance metre

        //store data in global structure and update info
        mMtxSharedData.lock();
          mPollInt   = mState.mExtPollInt;

          mState.mLaserDistance = tLaserDist;

          tDebug = mState.mDebFlag;
        mMtxSharedData.unlock();
      } //polling activated

      //update exit flag
      mMtxExit.lock();
        tExitFlag = mExitShutdwn;
      mMtxExit.unlock();
    } // polling external sensors loop

  }
  catch(ilmsens::tools::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "ExtSensors::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl 
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
    mSerialError = true;
  }
  catch(std::exception& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "ExtSensors::Std-EXCEPTION: " << ANSI_COLOR_RESET 
              << tErr.what() << std::endl;
    mSerialError = true;
  }
  catch(...) 
  {
    std::cerr << ANSI_COLOR_RED << "ExtSensors::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;
    mSerialError = true;
  }

  //cleanup RS232 ressources
  RS232_CloseComport(mLaserCOM);

  //report possible error
  mMtxSharedData.lock();
    mState.mExtSenThreadError = mSerialError;
	mMtxSharedData.unlock();

  // can go now
  std::cout << ANSI_COLOR_YELLOW << "ExtSensors: shutdown complete, exiting thread" << ANSI_COLOR_RESET << std::endl;
  return(NULL);
} // pollExtSensors

} //namespace sensors
} //namespace humidity
} //namespace mliquid
} //namespace ilmsens
