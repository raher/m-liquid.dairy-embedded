/// @file procIRFs.hpp
/// @brief Main and support funtions for a basic data processing thread.
///
/// The thread takes raw meaured data from a cache memory and pre-processes it.
/// MLBS-correlation will be done as well as other basic processing steps.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef PROCIRFS_HPP
#define PROCIRFS_HPP


/** Sensor-related processing constants */
#define PR_MLBS_ORDER         9         ///< M-Sequence order of m:liquid.humidity sensor
#define PR_MLBS_CLOCK         3.619     ///< RF system clock of m:liquid.humidity sensor [GHz]
#define PR_MLBS_NUM_RX        2         ///< number of Rx per sensor module

/** IRF processing constants */
#define PR_IP_FAK             4         ///< interpolation factor for pulse parameter extraction



/** Thread configuration constants */
#define PR_DEBUG_OUTPUT       1         ///< show debug info about processing operation


namespace ilmsens 
{
  namespace mliquid
  {
    namespace humidity 
    {
      namespace processing
      {

        /** Entry point for processing thread */
        void* procIRFs(void*);

      } //namespace processing
    } //namespace humidity
  } //namespace mliquid
} //namespace ilmsens

#endif //PROCIRFS_HPP
