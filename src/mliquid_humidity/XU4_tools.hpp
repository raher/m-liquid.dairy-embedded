/// @file XU4_tools.hpp
/// @brief Tools, types, and helpers for Odroid XU4 embedded board.
///
/// XU4 specific functions such as reading CPU temperatures, etc.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef XU4_TOOLS_HPP
#define XU4_TOOLS_HPP

#include <vector>


/* -------------------------------------------------------------------------------------------------
 * XU4 definitions
 * -------------------------------------------------------------------------------------------------
 */

#define TEMP_UPDATE_INT_MS 1000           ///< Min. interval between temperature updates [ms]

#define XU4_TEMP_MAX_LEN   79             ///< Maximum buffer length for reading XU4 board temperatures
#define XU4_TEMP_MIN_LEN   76             ///< Minimum length for reading XU4 board temperatures


/* -------------------------------------------------------------------------------------------------
 * XU4 related types
 * -------------------------------------------------------------------------------------------------
 */

typedef std::vector<int> TTemperatures;   ///< Type holding temperature values read from XU4 board


/* -------------------------------------------------------------------------------------------------
 * XU4 functions
 * -------------------------------------------------------------------------------------------------
 */


/** Reading temperature(s) of XU4 board from sys-file */
const TTemperatures& readXU4Temperature(void);

#endif //XU4_TOOLS_HPP
