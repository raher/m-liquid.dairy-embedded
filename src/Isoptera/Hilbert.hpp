/// @file Hilbert.hpp
/// @brief Performing Hilbert transform of given IRF/FRF.
///
/// Hilbert transform for measured data (time or frequency domain).
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef HILBERT_HPP
#define HILBERT_HPP

#include <memory>

#include "global.hpp"
#include <fftw3.h>  // FFTW

class Hilbert
{
  public:

    Hilbert(unsigned int pWinLen = 511);
    ~Hilbert();
    
    const TRealIRF& calculateHilbTD (const TRealIRF& pTDData);     // Hilbert transform of given time domain data
    const TRealIRF& calculateHilbFD (const fftw_complex* pFDData); // Hilbert transform of given frequency data
    
  private:

    //Signal properties
    unsigned int mWinLen;
    
    //result of Hilbert transform
    TRealIRF mHilbIRF;
    
    //reference weight vector for Hilbert transform
    TRealIRF mH;

    /* replaced by smart pointers
    // Data variables for FFTW
    fftw_complex *mTDHilb; // Data vectors time domain
    fftw_complex *mFDHilb; // Data vectors frequency domain
    */

    std::unique_ptr< fftw_complex[], ilmsens::mliquid::dairy::sFFTwDelete > mTDHilb; // Data vectors time domain
    std::unique_ptr< fftw_complex[], ilmsens::mliquid::dairy::sFFTwDelete > mFDHilb; // Data vectors frequency domain

    // FFTW-Plans for KKF
    fftw_plan mFFTPlan, mIFFTPlan;  // FFT of signal and IFFT after transform

    //perform actual Hilbert transform using frequency domain data;
    const TRealIRF& doHilbert(void);
};

#endif // HILBERT_HPP
