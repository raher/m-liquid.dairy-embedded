/* KKF.CPP
 * Calculating cross correlation between stimulus and received signal. For better performance, 
 * we first convert the signals into frequency domain (FFT), calculate the cross correlation spectrum 
 * and after that we do a backtransformation (IFFT) into time domain..
 * 
 * For FFT/IFFT we are using FFTW-Library 3.3.4.
 */

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
//#include <sstream>

#include "KKF.hpp"

/* Constructor */
KKF::KKF(unsigned int pMLBSLen)
: mMLBSLen(pMLBSLen), 
  mFDRef ( fftw_alloc_complex((size_t) pMLBSLen) ),
  mTDRaw ( fftw_alloc_complex((size_t) pMLBSLen) ),
  mTDCorr( fftw_alloc_complex((size_t) pMLBSLen) ),
  mFDRaw ( fftw_alloc_complex((size_t) pMLBSLen) ),
  mFDCorr( fftw_alloc_complex((size_t) pMLBSLen) )
{
  /*
  mtx_sharedata.lock();
  int mlbslen = share.mlbsLen;
  mtx_sharedata.unlock();
  */

  //std::cout << "KKF::Info: MLBS length is set to " << std::dec << mMLBSLen << "." << std::endl;
    
  /* Reading ideal M-Sequence from File */
  mRefMLBS.resize(mMLBSLen);

  // guess file name
  std::string tFName;
  switch (mMLBSLen)  
  {
    case 511:     // 9th order 
      tFName = "/usr/include/ilmsens/processing/mlbs09.txt";
      break;
    
    case 4095:    // 12th order
      tFName = "/usr/include/ilmsens/processing/mlbs12.txt";
      break;
    
    case 32767:   // 15th order
      tFName = "/usr/include/ilmsens/processing/mlbs15.txt";
      break;
      
    default:
      std::cerr << "KKF::ERROR: Unknown MLBS-Length: " << std::dec << mMLBSLen << "!" << std::endl;
      throw ilmsens::mliquid::dairy::Error("KKF: unknown MLBS length!", ILMSENS_ERROR_INVALID_PARAM);
  }
  
  // read in the file
  std::ifstream tMLBSFile;
  tMLBSFile.open(tFName.c_str(), std::ifstream::in);
  if (tMLBSFile.is_open())
  {
    std::string tLine;
    for (unsigned int tI = 0; tI < mMLBSLen; tI++) 
    {
      getline(tMLBSFile, tLine);
      mRefMLBS[tI] = atof(tLine.c_str());
    }
    tMLBSFile.close();
  }
  else 
  {
    std::cerr << "KKF::ERROR: Cannot open reference M-sequence file: ''" << tFName << "'." << std::endl;
    throw ilmsens::mliquid::dairy::Error("KKF: cannot open M-sequence reference file!", ILMSENS_ERROR_IO);
  }
   
  /* Calculate Spectrum of ideal M-Sequence */
  fftw_complex *tTD_mseq = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * mMLBSLen); // Time-Domain ideal M-Sequence

  //mFDRef = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * mMLBSLen); // Frequ.-Domain ideal M-Sequence
  //fftw_plan tFFTPlan = fftw_plan_dft_1d(mMLBSLen, tTD_mseq, mFDRef, FFTW_FORWARD, FFTW_PATIENT);
  fftw_plan tFFTPlan = fftw_plan_dft_1d(mMLBSLen, tTD_mseq, mFDRef.get(), FFTW_FORWARD, FFTW_PATIENT);

  //fill ideal MLBS into vector
  for (unsigned int tI = 0; tI < mMLBSLen; tI++) 
  {
    tTD_mseq[tI][0] = mRefMLBS[tI];  // Real Part
    tTD_mseq[tI][1] = 0;            // Imaginary Part
  }
  
  // Calculate FFT of ideal MLBS
  fftw_execute(tFFTPlan);
  
  //clean up temporary FFTw ressources
  fftw_destroy_plan(tFFTPlan);
  fftw_free(tTD_mseq);
   
  /* Allocate all required FFTw memories for KKF calculation */
  mCorrIRF.resize(mMLBSLen);

  mFFTPlan  = fftw_plan_dft_1d(mMLBSLen, mTDRaw.get(), mFDRaw.get(), FFTW_FORWARD, FFTW_PATIENT);    //FFT plan
  mIFFTPlan = fftw_plan_dft_1d(mMLBSLen, mFDCorr.get(), mTDCorr.get(), FFTW_BACKWARD, FFTW_PATIENT); //IFFT plan

  /*
  mTDRaw  = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * mMLBSLen); // Time-Domain uncorr. Impulse Response
  mTDCorr = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * mMLBSLen); // Time-Domain corr. Impulse Response
  mFDRaw  = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * mMLBSLen); // Frequ.-Domain uncorr. Impulse Response
  mFDCorr = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * mMLBSLen); // Frequ.-Domain corr. Impulse Response
  
  mFFTPlan  = fftw_plan_dft_1d(mMLBSLen, mTDRaw, mFDRaw, FFTW_FORWARD, FFTW_PATIENT);    //FFT plan
  mIFFTPlan = fftw_plan_dft_1d(mMLBSLen, mFDCorr, mTDCorr, FFTW_BACKWARD, FFTW_PATIENT); //IFFT plan
  */

  /*
  std::cout << "KKF::INFO: size of FFTW complex is " << sizeof(fftw_complex) << "." << std::endl;

  std::cout << "KKF::INFO: mFDRef  pointer is 0x" << std::hex << mFDRef << std::endl;

  std::cout << "KKF::INFO: mTDRaw  pointer is 0x" << std::hex << mTDRaw << std::endl;
  std::cout << "KKF::INFO: mTDCorr pointer is 0x" << std::hex << mTDCorr << std::endl;
  std::cout << "KKF::INFO: mFDRaw  pointer is 0x" << std::hex << mFDRaw << std::endl;
  std::cout << "KKF::INFO: mFDCorr pointer is 0x" << std::hex << mFDCorr << std::endl;
  */
}

/* Destructor */
KKF::~KKF()
{
  /* -> now done by smart pointer destructors
  //destroy the allocated FFTw vectors
  fftw_free(mFDRef);

  fftw_free(mTDRaw);
  fftw_free(mTDCorr);

  fftw_free(mFDRaw);
  fftw_free(mFDCorr);
  */

  //destroy FFTw plans
  fftw_destroy_plan(mFFTPlan); //FFT
  fftw_destroy_plan(mIFFTPlan); //IFFT
}

/* Calculate Crosscorrelation between received impulse response and ideal m-sequence */
const TRealIRF& KKF::calculateKKF(const TRealIRF& pData) 
{
  /*
  mtx_sharedata.lock();
  int mlbslen = share.mlbsLen;
  mtx_sharedata.unlock();
  */

  /*
  std::cout << "KKF::INFO: doing KKF, MLBS length is set to " << std::dec << mMLBSLen << ", input data length is " << pData.size() << '.' << std::endl;

  std::cout << "KKF::INFO: size of FFTW complex is " << sizeof(fftw_complex) << "." << std::endl;

  std::cout << "KKF::INFO: mFDRef  pointer is 0x" << std::hex << mFDRef << std::endl;

  std::cout << "KKF::INFO: mTDRaw  pointer is 0x" << std::hex << mTDRaw << std::endl;
  std::cout << "KKF::INFO: mTDCorr pointer is 0x" << std::hex << mTDCorr << std::endl;
  std::cout << "KKF::INFO: mFDRaw  pointer is 0x" << std::hex << mFDRaw << std::endl;
  std::cout << "KKF::INFO: mFDCorr pointer is 0x" << std::hex << mFDCorr << std::endl;
  */

  fftw_complex *tTDRaw  = mTDRaw.get(); // get raw pointer (not required, but faster:-)
  for (unsigned int tI = 0; tI < mMLBSLen; tI++)
  {
    tTDRaw[tI][0] = pData[tI]; // Real part
    tTDRaw[tI][1] = 0.0;       // Imag. part
  }
  
  /* FFT: Calculate Spectrum of raw data */
  fftw_execute(mFFTPlan);

  //std::cout << "KKF::Info: pre-mult ..."<< std::endl;
  
  /* Calculate Cross-Spectrum FFT(x(t)) * conj(FFT(mlbs(t))) */
  fftw_complex *tFDRaw  = mFDRaw.get();  // get raw pointer (not required, but faster:-)
  fftw_complex *tFDRef  = mFDRef.get();  // get raw pointer (not required, but faster:-)
  fftw_complex *tFDCorr = mFDCorr.get(); // get raw pointer (not required, but faster:-)
  for (unsigned int tI = 0; tI < mMLBSLen; tI++)
  {
    //std::cout << "KKF::Info: doing mult ..." << std::dec << tI << std::endl;
    //complex multiply, ATTENTION: mFDRef is used in complex conjugate form!
    tFDCorr[tI][0] = (tFDRaw[tI][0] * tFDRef[tI][0]) + (tFDRaw[tI][1] * tFDRef[tI][1]); // Real part
    tFDCorr[tI][1] = (tFDRaw[tI][1] * tFDRef[tI][0]) - (tFDRaw[tI][0] * tFDRef[tI][1]); // Imag. part
  }

  //std::cout << "KKF::Info: doing KKF, MLBS length is set to " << std::dec << mMLBSLen << ", input data length is " << pData.size() << '.' << std::endl;
  
  /* Calculate IFFT of Cross-Spectrum */
  fftw_execute(mIFFTPlan);
  
  /* Normalize and copy */
  double tNormScale = (double)mMLBSLen * (double)mMLBSLen;
  fftw_complex *tTDCorr = mTDCorr.get(); // get raw pointer (not required, but faster:-)
  for (unsigned int tI = 0; tI < mMLBSLen; tI++)
  {
    mCorrIRF[tI] = tTDCorr[tI][0] / tNormScale;
  }
  
  return(mCorrIRF);
}

/* return FRF of last correlation operation */
const fftw_complex* KKF::getLastFRF(void)
{
  return (mFDCorr.get());
}             
