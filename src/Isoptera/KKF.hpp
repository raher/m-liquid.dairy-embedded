/// @file KKF.hpp
/// @brief Cross correlation with ideal M-sequence of given IRF.
///
/// Cross correleation is performed by using FFT/IFFT method.
/// Ideal M-sequence is the reference function for correlation.
/// It is loaded from /usr/include/ilmsens/processing/mlbsXX.txt
/// where XX is the MLBS order.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef KKF_HPP
#define KKF_HPP

#include <memory>

#include "global.hpp"
#include <fftw3.h>    // FFTW

class KKF
{
  public:

    KKF(unsigned int pMLBSLen = 511);
    ~KKF();

    const TRealIRF&     calculateKKF(const TRealIRF& pData); // calculate Cross Correlation Function of given data
    const fftw_complex* getLastFRF(void);                    // return FRF of last correlation operation

  private:

    //MLBS properties
    unsigned int mMLBSLen;
    
    //reference MLBS as read from file
    TRealIRF mRefMLBS;
    
    //result of correlation
    TRealIRF mCorrIRF;

    // Data variables for FFTW
    std::unique_ptr< fftw_complex[], ilmsens::mliquid::dairy::sFFTwDelete > mFDRef;  // Data vector for reference spectrum from ideal MLBS

    std::unique_ptr< fftw_complex[], ilmsens::mliquid::dairy::sFFTwDelete > mTDRaw;  // Data vectors time domain for KKF
    std::unique_ptr< fftw_complex[], ilmsens::mliquid::dairy::sFFTwDelete > mTDCorr; // Data vectors time domain for KKF
    
    std::unique_ptr< fftw_complex[], ilmsens::mliquid::dairy::sFFTwDelete > mFDRaw;  // Data vectors frequency domain for KKF
    std::unique_ptr< fftw_complex[], ilmsens::mliquid::dairy::sFFTwDelete > mFDCorr; // Data vectors frequency domain for KKF

    /*
    fftw_complex *mFDRef;           // Data vector for reference spectrum from ideal MLBS
    fftw_complex *mTDRaw, *mTDCorr; // Data vectors time domain for KKF
    fftw_complex *mFDRaw, *mFDCorr; // Data vectors frequency domain for KKF
    */
    
    // FFTW-Plans for KKF
    fftw_plan mFFTPlan, mIFFTPlan;  // FFT of measured data and IFFT of correleated data
};

#endif // KKF_HPP
