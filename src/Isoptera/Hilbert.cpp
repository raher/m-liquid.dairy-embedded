/* HILBERT.CPP
 * Calculating Hilbert transform of given data.
 * 
 * For FFT/IFFT we are using FFTW-Library 3.3.4.
 */

#include <memory>
#include <iostream>
#include <iomanip>
#include <cstring>

#include "Hilbert.hpp"


/* Constructor */
Hilbert::Hilbert(unsigned int pWinLen)
:mWinLen(pWinLen), mTDHilb( fftw_alloc_complex((size_t) pWinLen) ), mFDHilb( fftw_alloc_complex((size_t) pWinLen) ) 
{
  /* Allocate remaining FFTw memories for KKF calculation */
  mHilbIRF.resize(mWinLen);

  /* repalced by smart pointers
  mTDHilb = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * mWinLen); // Time-Domain 
  mFDHilb = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * mWinLen); // Frequ.-Domain 
  */
  
  mFFTPlan  = fftw_plan_dft_1d(mWinLen, mTDHilb.get(), mFDHilb.get(), FFTW_FORWARD, FFTW_PATIENT);  //FFT plan
  mIFFTPlan = fftw_plan_dft_1d(mWinLen, mFDHilb.get(), mTDHilb.get(), FFTW_BACKWARD, FFTW_PATIENT); //IFFT plan
    
  //generate reference weight vector for Hilbert transform
  mH[0] = 1.0;
  mH[mWinLen/2] = 1.0;
  for (unsigned int tI = 1; tI < mWinLen/2 - 1; tI++) 
  {
    mH[tI] = 2.0;
  }
  for (unsigned int tI = mWinLen/2 + 1; tI < mWinLen; tI++)
  {
    mH[tI] = 0.0;
  }

  /*
  td_hilbert = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * winlen);
  fd_hilbert = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * winlen);
    
  h1 = fftw_plan_dft_1d(winlen, td_hilbert, fd_hilbert, FFTW_FORWARD, FFTW_PATIENT);
  h2 = fftw_plan_dft_1d(winlen, fd_hilbert, td_hilbert, FFTW_BACKWARD, FFTW_PATIENT);
  */
}

/* Destructor */
Hilbert::~Hilbert()
{
  /* replaced by auto-destruct of smart pointer!
  //destroy the allocated FFTw vectors
  fftw_free(mTDHilb);
  fftw_free(mFDHilb);
  */

  //destroy FFTw plans
  fftw_destroy_plan(mFFTPlan); //FFT
  fftw_destroy_plan(mIFFTPlan); //IFFT
}

/* Hilbert transform for time domain data */
const TRealIRF& Hilbert::calculateHilbTD (const TRealIRF& pTDData)
{
  //check data length
  unsigned int tCurLen = pTDData.size();
  if (tCurLen != mWinLen)
  {
    std::cerr << "Hilbert::ERROR: length of input data (" << std::dec << tCurLen << ") does not match reference length (" << mWinLen << ")!" << std::endl;
    throw ilmsens::mliquid::dairy::Error("Hilbert: wrong signal length!", ILMSENS_ERROR_INVALID_PARAM);
  }

  //copy data into FFTw buffer
  for (unsigned int tI = 0; tI < mWinLen; tI++)
  {
    mTDHilb[tI][0] = pTDData[tI];
    mTDHilb[tI][1] = 0.0;
  }

  //perform FFT
  fftw_execute(mFFTPlan);

  //do the transform and return result
  return (doHilbert());
}

/* Hilbert transform for frequency domain data */
const TRealIRF& Hilbert::calculateHilbFD (const fftw_complex* pFDData)
{
  /* TODO: how to check data length?
  //check data length
  unsigned int tCurLen = pTDData.size();
  if (tCurLen != mWinLen)
  {
    std::cerr << "Hilbert::ERROR: length of input data (" << std::dec << tCurLen << ") does not match reference length (" << mWinLen << ")!" << std::endl;
    throw ilmsens::mliquid::dairy::Error("Hilbert: wrong signal length!", ILMSENS_ERROR_INVALID_PARAM);
  }
  */

  //copy data into FFTw buffer
  memcpy((void *) mFDHilb.get(), (const void *) pFDData, sizeof(fftw_complex) * mWinLen);
  /*
  for (unsigned int tI = 0; tI < mWinLen; tI++)
  {
    mFDHilb[tI][0] = pFDData[tI][0];
    mFDHilb[tI][1] = pFDData[tI][1];
  }
  */

  //do the transform and return result
  return (doHilbert());
}

/* Do the Hilbert transform and return result */
const TRealIRF& Hilbert::doHilbert(void)
{
  //calculate Hilbert product
  for (unsigned int tI = 0; tI < mWinLen; tI++)
  {
    mFDHilb[tI][0] = mFDHilb[tI][0] * mH[tI];
    mFDHilb[tI][1] = mFDHilb[tI][1] * mH[tI];
  }
  
  //do IFFT
  fftw_execute(mIFFTPlan);
  
  //copy data to output buffer
  for (unsigned int tI = 0; tI < mWinLen; tI++)
  {
    //TODO: hilbert transform is complex, the actual time domain result is in the imaginary part
    mHilbIRF[tI] = mTDHilb[tI][1];
  }
  
  return (mHilbIRF);
}

/*
std::vector<float>  Hilbert::Calculate (std::vector<float> input) 
{
    
    unsigned int len = input.size();
    std::vector<float> h, output;
    h.resize(len);
    output.resize(len);
    
    // h-Vector erstellen
    
    // FFT berechnen
    for (unsigned int i = 0; i < len; i++) {
        td_hilbert[i][0] = input[i];
        td_hilbert[i][1] = 0;
    }
    
    fftw_execute(h1);
    
    // Calculate Product
    for (unsigned int i = 0; i < len; i++) {
        fd_hilbert[i][0] = fd_hilbert[i][0] * h[i];
        fd_hilbert[i][1] = fd_hilbert[i][1] * h[i];
    }
    
    // IFFT berechnen
    fftw_execute(h2);
    
    for (unsigned int i = 0; i < len; i++) {
        output[i] = (float) td_hilbert[i][0];
    }
    
    return output;
}
*/
