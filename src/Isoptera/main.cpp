//============================================================================
// Name        : IsopteraScanner.cpp
// Author      : Johannes Zender / Ralf Herrmann
// Version     : 0.1.0
// Copyright   : (C) Ilmsens GmbH 2017
// Description : TCP control server & measurement application written for 
//               embedded ARM-Systems using an Ilmsens UWB-Sensor.
// Changelog:
//
// Rev. 0.1.0 (03/2017, HER)
//
//  = rework of support classes (enforcing minimal coding guide to enhance
//    clarity)
//  = BugFix: removed memory leaks in support classes for FFTw buffers
//  + added Poco logging facilities in main program 
//
// Rev. 0.12 (02/2017, ZEN)
//
//  = Changed state-machine: processing-thread will be created by 
//    Datareader-Thread (instead of Controlling-Thread)
//
//============================================================================


/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */

// Std. C++ library types / STL 
#include <cstdint>
#include <cerrno>
#include <vector>
#include <algorithm> // for max_element
//#include <mutex>
//#include <queue>

/*
// Poco logging includes
#include "Poco/AutoPtr.h"
#include "Poco/Logger.h"
#include "Poco/ConsoleChannel.h"
#include "Poco/PatternFormatter.h"
#include "Poco/FormattingChannel.h"
#include "Poco/LogStream.h"
*/

// FFTw & math includes
#include <cmath>

#include <fftw3.h>      // FFTW main library header
#include <complex.h>
//#include <math.h>

// TCP & network includes
#include <arpa/inet.h>                            // for TCP-Sockets
#include <netdb.h>                          // for TCP-Sockets
#include <netinet/in.h>                   // for TCP-Sockets
#include <sys/socket.h>                   // for TCP-Sockets
#include <sys/types.h>                    // for TCP-Sockets
#include <unistd.h>
//#include <errno.h>                        //  error definitions for socket functions...

// Timing and threading includes
#include <chrono>                         // For execution time measurements
#include <thread>                         // Platform portable sleep
#include <ctime> 

#include <pthread.h>                    //POSIX threads
#include <sched.h>                      // for Realtime Priority
#include <time.h>

// File, IO, and String-Streams
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>                      //File/IO descriptor manipulation

// Ilmsens HAL library includes
#include "ilmsens/hal/ilmsens_hal.h"

// Ilmsens m:liquid includes
#include "../include/ilmsens/mliquid_dairy/ilmsens_mliquid_dairy_version.h" //project version & revision

// Own support class includes
#include "Hilbert.hpp"
#include "KKF.hpp"
#include "datahandle.hpp"
#include "hal_wrap.hpp"  //wraps ilmsens HAL in C++ classes

// Global variables, memories, and synchronisation ressources
#include "global.hpp"

// Header for main program
#include "main.h" 


/* -------------------------------------------------------------------------------------------------
 * Using name spaces
 * -------------------------------------------------------------------------------------------------
 */

using namespace std;
using namespace std::chrono;
using namespace ilmsens::hal;


/* -------------------------------------------------------------------------------------------------
 * Local definitions
 * -------------------------------------------------------------------------------------------------
 */

// Defining colored outputs
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

// HAL configuration and measurement parameters
#define HAL_MEAS_MODE      false

// XU4 board
#define XU4_TEMP_MAX_LEN   79       //buffer length for reading XU4 board temperatures

/* -------------------------------------------------------------------------------------------------
 * Local types of main program
 * -------------------------------------------------------------------------------------------------
 */


//type holding tmperature values read from XU4 board
typedef std::vector<int> TTemperatures;


/* -------------------------------------------------------------------------------------------------
 * Local variables of main program
 * -------------------------------------------------------------------------------------------------
 */

#if 0
static bool bufferedMode = false;       // buffered measurement?
static unsigned responseCount = 10;     // acquire how many IRFs?
static milliseconds timeoutMillis(1000); // maximum time between IRFs?
#endif


/* -------------------------------------------------------------------------------------------------
 * Logging & logging helpers
 * 
 * Powered by Poco:-)
 * 
 * -------------------------------------------------------------------------------------------------
 */

// static int logLevel = ::utils::logging::parseLevel("warning");

/*
//configure Poco logging: create a formatted console output
void configureLogging(int rootLevel)
{
  using Poco::AutoPtr;
  AutoPtr<Poco::ConsoleChannel> console(new Poco::ConsoleChannel);
  AutoPtr<Poco::PatternFormatter> formatter(new Poco::PatternFormatter);
  formatter->setProperty("pattern", "%E.%i %N:%I %s:%q: %t");
  AutoPtr<Poco::FormattingChannel> channel(new Poco::FormattingChannel(formatter, console));

  Poco::Logger::root().setChannel(channel);
  Poco::Logger::root().setLevel(rootLevel);
}

//static logger
Poco::Logger& log()
{
  static Poco::Logger& log = Poco::Logger::get("ilmsens.mliquid.dairy.Isop");
  return log;
}

//static stream of static logger
Poco::LogStream& logstream()
{
  // REVISIT: needs protection?
  static Poco::LogStream stream(log());
  return stream;
}
*/

#if 0

//print content of impulse response to screen
void print(std::ostream& os, ImpulseResponse& irs, SensorGroup& sensors, unsigned mlbsOrder)
{
    SampleType* pos = &irs.front();
    for(unsigned i = 0; i < sensors.size(); ++i) {
        const size_t sizeOfSensorResponse = irs.size() / sensors.size();
        // FIXME: std::pow(2. mMLBS_Order) * mNum_OV --> now "1"
        const unsigned sizeOfReceiverResponse = static_cast<unsigned>(std::pow(2, mlbsOrder));
        for(unsigned j = 0; j < sizeOfSensorResponse / sizeOfReceiverResponse; ++j) {
            os << "Sensor-" << std::dec << sensors.at(i) << "/Receiver-" << std::dec << j << ": ";
            auto group_sample = 0;
            unsigned group_size = 0;
            for(unsigned k = 0; k < sizeOfReceiverResponse; ++k) {
                auto sample = *(pos + i * sizeOfSensorResponse + j * sizeOfReceiverResponse + k);
                if(group_sample != sample) {
                    if(group_size > 1) {
                        os << "(" << std::dec << group_size << ")";
                    }
                    os << " " << std::hex << sample;
                    group_size = 1;
                    group_sample = sample;
                } else {
                    ++group_size;
                }
            }
            if(group_size > 1) {
                os << "(" << std::dec << group_size << ")";
            }
            os << std::endl;
        }
    }
}

#endif


/* -------------------------------------------------------------------------------------------------
 * System helpers
 * 
 * All the fine goodies
 * 
 * -------------------------------------------------------------------------------------------------
 */

//Delay for 'pDel_ms'-Milliseconds
void delay_ms(unsigned int pDel_ms)
{
  std::this_thread::sleep_for(std::chrono::milliseconds(pDel_ms));

  //usleep(ms * 1000);
}

// Reading temperature(s)) of XU4 Board
TTemperatures readXU4Temperature(void)
{
  unsigned int tMaxLen = XU4_TEMP_MAX_LEN;

  //buffers
  char tempxu4[XU4_TEMP_MAX_LEN];
  
  char sensor0[3];
  char sensor1[3];
  char sensor2[3];
  char sensor3[3];
  char sensor4[3];
  TTemperatures tITemp;

  //nothing read so far
  tITemp.clear();
  
  FILE* tFD = fopen("/sys/devices/10060000.tmu/temp", "r"); // Open File with Temperature Data
  if(tFD == NULL) 
  {
    //TODO: make it an "ilmsens::mliquid::dairy::Error" exception?
    perror("readXU4Temp::ERROR: Cannot read Temperature File");
  }
  else
  {
    int tLen = fread(tempxu4, sizeof(char), tMaxLen, tFD); // Read Temperature from file
    fclose(tFD);
    
    if (tLen < 76)
    {
      std::cerr << "readXU4Temp::ERROR: could not read enough data from XU4 temperture file, fread returned: " << std::dec << tLen << "!" << std::endl;
    }
    else
    {
      sensor0[0] = tempxu4[10]; // Seperate single values out of string
      sensor0[1] = tempxu4[11];
      sensor0[2] = 0; //terminate C - string

      sensor1[0] = tempxu4[26];
      sensor1[1] = tempxu4[27];
      sensor1[2] = 0; //terminate C - string

      sensor2[0] = tempxu4[42];
      sensor2[1] = tempxu4[43];
      sensor2[2] = 0; //terminate C - string

      sensor3[0] = tempxu4[58];
      sensor3[1] = tempxu4[59];
      sensor3[2] = 0; //terminate C - string

      sensor4[0] = tempxu4[74];
      sensor4[1] = tempxu4[75];
      sensor4[2] = 0; //terminate C - string
      
      // Convert char into vector<int>
      tITemp.push_back(atoi(sensor0)); 
      tITemp.push_back(atoi(sensor1));
      tITemp.push_back(atoi(sensor2));
      tITemp.push_back(atoi(sensor3));
      tITemp.push_back(atoi(sensor4));
    }
  }
  
  return (tITemp);
}


/* -------------------------------------------------------------------------------------------------
 * TCP command/data & system helpers
 * 
 * -------------------------------------------------------------------------------------------------
 */

//process & execute commands received from TCP client
void process_command(int sock1_acpt, unsigned int pCmd)
{
  int len = 0;
  
  //for receiving parameter values from client (e.g. software averages)
  int rxsize = sizeof(int) * TCP_CMD_SIZE;
  int buffer[1] = { 0 };
  
  //before a command can be processed, lock access to global state structure.
  //The lock will only be removed, after the ACK has been sent to the client
  //to avoid races due to asynch processing on server and client machine
  mMtxSharedData.lock();

  unsigned int tACK  = htonl(mState.mACK);
  unsigned int tNACK = htonl(mState.mNACK);
  unsigned int tSAvg = htonl(mState.mSW_AVG);
  
  //select command
  switch(pCmd) 
  {
    // Enable DEBUG messages: ASCII "DEB1"
    case 0x44454231 : 
        
        printf(ANSI_COLOR_CYAN "Controlling: Received DEBUG-ENABLE-Command" ANSI_COLOR_RESET "\n");
        mState.mDebFlag = true;
        len = send(sock1_acpt, &tACK, rxsize, 0); // Send ACK
        break;

    // Disable DEBUG messages: ASCII "DEB0"
    case 0x44454230 : 
        
        printf(ANSI_COLOR_CYAN "Controlling: Received DEBUG-DISABLE-Command" ANSI_COLOR_RESET "\n");
        mState.mDebFlag = false;
        len = send(sock1_acpt, &tACK, rxsize, 0); // Send ACK
        break;

    // Start: ASCII "RUN "
    case 0x52554e20 : 
        
        printf(ANSI_COLOR_CYAN "Controlling: Received START-Command" ANSI_COLOR_RESET "\n");
        mState.mRunFlag = true;
        len = send(sock1_acpt, &tACK, rxsize, 0); // Send ACK
        printf(ANSI_COLOR_CYAN "Controlling: Measurement start triggered" ANSI_COLOR_RESET "\n");
        break;
        
    // Stop: ASCII "STOP"
    case 0x53544f50 :

        printf(ANSI_COLOR_CYAN "Controlling: Received STOP-Command" ANSI_COLOR_RESET "\n");
        mState.mRunFlag = false;
        len = send(sock1_acpt, &tACK, rxsize, 0); // Send ACK
        printf(ANSI_COLOR_CYAN "Controlling: Measurement stop triggered" ANSI_COLOR_RESET "\n");
        break;
        
    // Exit: ASCII "EXIT"
    case 0x45584954 :

        printf(ANSI_COLOR_CYAN "Controlling: Received EXIT-Command" ANSI_COLOR_RESET "\n");
        mState.mRunFlag  = false;
        mState.mExitFlag = true;
        len = send(sock1_acpt, &tACK, rxsize, 0); // Send ACK
        break;
        
    // Dynamic Background Removal Enable: ASCII "BGR1"
    case 0x42475231 :

        printf(ANSI_COLOR_CYAN "Controlling: Received BACKGROUND-REMOVAL-ENABLE-Command" ANSI_COLOR_RESET "\n");
        mState.mBGFlag = true;
        len = send(sock1_acpt, &tACK, rxsize, 0); // Send ACK
        break;
        
    // Dynamic Background Removal Disable: ASCII "BGR0"
    case 0x42475230 :

        printf(ANSI_COLOR_CYAN "Controlling: Received BACKGROUND-REMOVAL-DISABLE-Command" ANSI_COLOR_RESET "\n");
        mState.mBGFlag = false;
        len = send(sock1_acpt, &tACK, rxsize, 0); // Send ACK
        break;
        
    // Hilbert Transformation Enabled: ASCII "HIL1"
    case 0x48494c31 :

        printf(ANSI_COLOR_CYAN "Controlling: Received HILBERT-ENABLE-Command" ANSI_COLOR_RESET "\n");
        mState.mHilbFlag = true;
        len = send(sock1_acpt, &tACK, rxsize, 0); // Send ACK
        break;
        
    // Hilbert Transformation Disabled: ASCII "HIL0"
    case 0x48494c30 :

        printf(ANSI_COLOR_CYAN "Controlling: Received HILBERT-DISABLE-Command" ANSI_COLOR_RESET "\n");
        mState.mHilbFlag = false;
        len = send(sock1_acpt, &tACK, rxsize, 0); // Send ACK
        break;
        
     // Software Averages: ASCII "SAVG"
    case 0x53415647 :

        printf(ANSI_COLOR_CYAN "Controlling: Received SOFT-AVG-Command" ANSI_COLOR_RESET "\n");
        //read value from client
        len = recv(sock1_acpt, buffer, rxsize, 0);
        mState.mSW_AVG = ntohl(buffer[0]);
        len = send(sock1_acpt, &tACK, rxsize, 0); // Send ACK
        printf("Controlling: Storing Soft_AVG to be %u" ANSI_COLOR_RESET "\n", mState.mSW_AVG);
        break;
        
     // Get software averages: ASCII "SAV?"
    case 0x5341563F :

        printf(ANSI_COLOR_CYAN "Controlling: Received GET-SOFT-AVG-Command" ANSI_COLOR_RESET "\n");
        //send value to client
        len = send(sock1_acpt, &tACK, rxsize, 0); // Send ACK
        if (len == rxsize) len = send(sock1_acpt, &tSAvg, sizeof(tSAvg), 0); // Send averages
        printf("Controlling: sent software averages of %u to the client" ANSI_COLOR_RESET "\n", mState.mSW_AVG);
        break;
        
    default:
        printf(ANSI_COLOR_CYAN "Unknown Command: 0x%08X  Please try again..." ANSI_COLOR_RESET "\n", pCmd);
        len = send(sock1_acpt, &tNACK, rxsize, 0); // Send NACK
        break;
  }

  //check, if answer could be sent correctly
  if (len != rxsize)
  {
    printf(ANSI_COLOR_CYAN "Controlling:.ERROR: could not corretly acknowledge the command, sent only %d / %d bytes!" ANSI_COLOR_RESET "\n", len, rxsize);
  }

  //ACK/NACK has been sent to client, so unlock global state such that threads can respond
  //to any changes ...
  mMtxSharedData.unlock();
}

// Check, if socket has client connected
int isConnected(int socket)
{
  // Check if Socket is still connected
  int error = 0;
  socklen_t len = sizeof(error);
  int ret = getsockopt(socket, SOL_SOCKET, SO_ERROR, &error, &len);
  
  if(ret != 0) 
  {
    // Problem to get error code
    std::cerr << "isConnected::ERROR: getting socket error code: " << strerror(ret) << std::endl;
    return(0);
  }
  
  if(error != 0) 
  {
    // Socket has error code
    std::cerr << "isConnected::ERROR: on socket: " << strerror(ret) << std::endl;
    return(-error);
  }
  
  //socket is happily connected
  return(1);
}


/*--------------------------------------------------------------------------------------------------*/
/*****************************************  THREAD SECTION  *****************************************/
/*--------------------------------------------------------------------------------------------------*/


/* -------------------------------------------------------------------------------------------------
 * DataReader thread
 * 
 * Setup attached sensor when signalled (a client has connected to command port), 
 * wait for measurement run signal and perform measurement. Forwards measured data
 * to processing thread (after amplitude scaling) via FIFO cache buffer. Stops measurement
 * upon stop signal. Shutdown of sensor/HAL when signalled (client ends the command connection).
 * 
 * -------------------------------------------------------------------------------------------------
 */

void* datareader(void*)
{
  try 
  {
    //Bind Thread to single CPU Core
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(5, &cpuset); // CPU #5
    CPU_SET(6, &cpuset); // CPU #6
    if(pthread_setaffinity_np(pthread_self(), sizeof(cpuset), &cpuset) != 0) 
    {
      perror("DataReader::ERROR: pthread_setaffinity_np failed");
    }
    
    // Set Realtime Priority
    struct sched_param param;
    sched_getparam(0, &param);
    param.sched_priority = 99; // Realtime
    
    if(sched_setscheduler(0, SCHED_RR, &param) != 0) 
    {
      //Fatal: no real-time priority could be set
      perror("DataReader::ERROR: Set Scheduling Priority failed");
      throw ilmsens::mliquid::dairy::Error("DataReader::ERROR: Set Scheduling Priority failed", ILMSENS_ERROR_NO_MEMORY);
      //exit(-1);
    }
    sched_getparam(0, &param);

    printf(ANSI_COLOR_GREEN "Datareader: Thread created (bound to CPUs #5 and #6 with realtime priority)" ANSI_COLOR_RESET "\n\n");
    
    //use C++ wrapper around HAL library to ensure no memory leaks will occur 
    //in the thread and take care of proper HAL unloading
    printf(ANSI_COLOR_GREEN "Datareader: Initializing Radar-Devices..." ANSI_COLOR_RESET "\n");

    SensorIndexes groupMembers; // List of Radar Device Numbers
    groupMembers.push_back(1); //only use first sensor attached to the XU4

    Context ctx(ILMSENS_DEB_MOST); //init the HAL and enumerate the sensors
    SensorGroup sensors(groupMembers); //create sensor group and activate the sensors

    //get and check sensor ID
    char tID[ILMSENS_HAL_MOD_ID_BUF_SIZE]; //max length allocated
    int status = ilmsens_hal_getModId(sensors.at(0), tID, sizeof(tID));
    if (strlen(tID) == 0) 
    {
      printf(ANSI_COLOR_RED "DataReader::ERROR: Sensor-ID is empty!" ANSI_COLOR_RESET "\n");
      throw ilmsens::hal::Error("DataReader::ERROR: Sensor-ID is empty!", ILMSENS_ERROR_IO);
    }
    else 
    {
      printf(ANSI_COLOR_GREEN "Datareader: Sensor-ID is %s" ANSI_COLOR_RESET "\n", tID);
    }

    //TODO: find out actual MLBS order of sensor
    unsigned mlbsOrder = 12;
    double fclock = 13.312;     // [GHz]
  
    //prepare sensor configuration
    const unsigned masterIndex = 0;                       // Index of master sensor: make first (and only) sensor to be master
//    const unsigned mlbsOrder = tMInfo.mConfig.mOrder;     // MLBS Order
    unsigned int   mlbsLen = (1 << mlbsOrder) - 1;        // Length of M-Sequence
//    double         fclock = tMInfo.mConfig.mClk;          // Clock Frequency
    const unsigned receiverCount = 2;                     // Number of Rx-Channels
    printf(ANSI_COLOR_GREEN "Datareader: MLBS-Length will be set to %u" ANSI_COLOR_RESET "\n", mlbsLen);
    printf(ANSI_COLOR_GREEN "Datareader: RF System Clock will be set to %f GHz" ANSI_COLOR_RESET "\n", fclock);

    //ask for SW averaging & debug setting (may be changed by TCP client)
    mMtxSharedData.lock();
      unsigned int SftAVG = mState.mSW_AVG;
      bool         tDebug = mState.mDebFlag;
    mMtxSharedData.unlock();
        
    //configure the sensor(s))
    if (tDebug) printf(ANSI_COLOR_GREEN "DEBUG Datareader: Configuring sensor..." ANSI_COLOR_RESET "\n");
    auto responseSizeSamples = configure(sensors, mlbsOrder, fclock, receiverCount, SftAVG, masterIndex);

    //get sensor properties after setup, e.g. for data processing
    ilmsens_hal_ModInfo tMInfo;
    status = ilmsens_hal_getModInfo (sensors.at(0), &tMInfo);
    if (status != ILMSENS_SUCCESS)
    { 
      throw ilmsens::hal::Error("DataReader::Error: getModInfo failed.", status);
    }
    else
    {
      printf(ANSI_COLOR_GREEN "Datareader: MLBS-Order of Device is %u" ANSI_COLOR_RESET "\n", tMInfo.mConfig.mOrder);
    }

    //save/calculate the sensor parameters
    mlbsOrder = tMInfo.mConfig.mOrder;     // MLBS Order
    mlbsLen   = (1 << mlbsOrder) - 1;      // Length of M-Sequence
    fclock    = tMInfo.mConfig.mClk * 1e9;       // Clock Frequency [Hz]

    // Make MLBS-Length globally known
    mMtxSharedData.lock();
      mState.mMLBSLen = mlbsLen;      
    mMtxSharedData.unlock();

    //create memories and caches
    TFIFOCacheEntry newdata;
    //TRealIRF adc1;                      // Buffer for calculating ADC-Levels
    //TRealIRF adc2;                      // Buffer for calculating ADC-Levels        

    newdata.Channel1.resize(mlbsLen);
    newdata.Channel2.resize(mlbsLen);
    
    //adc1.resize(mlbsLen);
    //adc2.resize(mlbsLen);

    // Create Processing-Thread (only needs to live when sensor has been activated)
    pthread_t thread2; // Processing

    mMtxProcessing.lock();
      processing_shutdwn = false;
    mMtxProcessing.unlock();

    int rc = pthread_create(&thread2, NULL, &processing, 0); // Thread 2 (void * processing(void *))

    if(rc != 0) 
    {
      printf(ANSI_COLOR_RED "Datareader::ERROR: Cannot create Thread 2 (Processing)!" ANSI_COLOR_RESET "\n");
      throw ilmsens::mliquid::dairy::Error("DataReader::ERROR: Cannot create Thread 2 (Processing)!", ILMSENS_ERROR_NO_MEMORY);
    }

    // init is complete, so output fewer log messages
    ctx.setDebugLevel(ILMSENS_DEB_INFO);

    // main loop of DataReader - wait for run flag and do measurement if appropriate
    //unsigned errorCount = 0;

    mMtxDataReader.lock();
      bool tEndDR = datareader_shutdwn;
    mMtxDataReader.unlock();
    while(!tEndDR) 
    {
      //check, if measurement request has been received
      mMtxSharedData.lock();
        bool tRunFlag = mState.mRunFlag;
        SftAVG = mState.mSW_AVG;
      mMtxSharedData.unlock();
      if(tRunFlag) 
      {
        printf(ANSI_COLOR_BLUE "Datareader: starting new measurement ..." ANSI_COLOR_RESET "\n");
        
        //set the newly retrieved software averages
        sensors.setSWAvgerages(SftAVG);
        printf(ANSI_COLOR_BLUE "Datareader: set SW averages to %u" ANSI_COLOR_RESET "\n", SftAVG);
        
        // get scaling of measured data because of averages
        double tAvgScale  = (double) SftAVG * (double) tMInfo.mHWAvg;
        // TODO: add ADC lsb voltage to scaling
        double tMeasScale = 1/tAvgScale * tMInfo.mLSB_Volt; 
        
        // calculate the measurement rate & time between IRFs
        double tMeasRate = fclock / (double) tMInfo.mConfig.mSub / (double) tMInfo.mNumSamp / (double) (tMInfo.mHWAvg + 1) / (double) SftAVG;
        unsigned tTOMillis = 2 * (unsigned)(1000.0 / tMeasRate);
        std::chrono::milliseconds tTOVal(tTOMillis);

        printf(ANSI_COLOR_BLUE "Datareader: measurement rate will be %f Hz (IRF timeout is set to %u ms)." ANSI_COLOR_RESET "\n", tMeasRate, tTOMillis);

        // create buffers for measurement
        ImpulseResponse irs(responseSizeSamples, 0);

        /*
        float* buffer1 = new float[mlbsLen];
        float* buffer2 = new float[mlbsLen];
        */

        TRealIRF buffer1(mlbsLen, 0);
        TRealIRF buffer2(mlbsLen, 0);

        //create new measurement
        Measurement measurement(sensors, responseSizeSamples, HAL_MEAS_MODE);

        unsigned int ind = 0;
        unsigned int run = 0; // Counting variable

        // Initialize execution-time-measurement
        std::chrono::time_point<std::chrono::system_clock> start, end;
        start = std::chrono::system_clock::now();
        end   = std::chrono::system_clock::now();
        
        //main measurement loop
        mMtxSharedData.lock();
          tRunFlag = mState.mRunFlag;
          tDebug   = mState.mDebFlag;
        mMtxSharedData.unlock();
        while(tRunFlag) 
        {
          printf(ANSI_COLOR_GREEN "Datareader: Reading Dataset #%u from Radar-Device" ANSI_COLOR_RESET "\n", run);
          
                            
          // Get next IRF
          irs = measurement.next(tTOVal);
          
          // Check for lost data
          if((irs[mlbsLen] - ind) > 1) 
          {
            printf(ANSI_COLOR_YELLOW "Datareader: WARNING: Lost %u IRFs" ANSI_COLOR_RESET "\n",
                   irs[mlbsLen] - ind);
          }
          // Save new index
          ind = irs[mlbsLen];
          
          // Copy the IRF to buffer (convert to double)
          for(unsigned int k = 0; k < mlbsLen; k++) 
          {
            buffer1[k] = (double)irs[k];
            buffer2[k] = (double)irs[k + mlbsLen + 1];
          }
          
          // Time Measurement for debugging
          if (tDebug)
          {
            end = std::chrono::system_clock::now();
            std::chrono::duration<double> elapsed_seconds = end - start;
            printf(ANSI_COLOR_GREEN "DEBUG Datareader: Calc. meas. freq.: %f Hz | current meas. freq.: %f Hz" ANSI_COLOR_RESET "\n",
                   tMeasRate, 1 / elapsed_seconds.count());
            start = std::chrono::system_clock::now();
          }
          
          // Scale data according to averages and copy to cache entry
          // Also mind the maximum value for ADC level estimation
          double tRx1Max = -1.0;
          double tRx2Max = -1.0;
          double tRx1Cur;
          double tRx2Cur;
          for(unsigned int j = 0; j < mlbsLen; j++) 
          {
            tRx1Cur = newdata.Channel1[j] = buffer1[j] * tMeasScale;
            tRx2Cur = newdata.Channel2[j] = buffer2[j] * tMeasScale;
            
            // Mind maximum value
            tRx1Cur = fabs(tRx1Cur);
            tRx2Cur = fabs(tRx2Cur);
            if (tRx1Cur > tRx1Max) tRx1Max = tRx1Cur;
            if (tRx2Cur > tRx2Max) tRx2Max = tRx2Cur;
            
            //adc1[j] = std::abs(newdata->Channel1[j]);
            //adc2[j] = std::abs(newdata->Channel2[j]);
          }
          
          /* Additional Information */
          newdata.index        = irs[mlbsLen];                                      // current IR index
          newdata.ADClevel_Rx1 = (unsigned int) ceil(tRx1Max / tMInfo.mFSR[1] * 100.0); // ADC Level
          newdata.ADClevel_Rx2 = (unsigned int) ceil(tRx2Max / tMInfo.mFSR[1] * 100.0); // ADC Level
          newdata.temp_mexpl   = irs[responseSizeSamples-1];                        // Copy Temp. (additional IRF info from Rx2 of sensor)
          //newdata->ADClevel_Rx2 = *std::max_element(adc2.begin(), adc2.end()); // ADC Level
          
          // Store new entry in cache
          mMtxDRCache.lock();
            newdata.cache1size = mDRCache.size(); // current size of cache memory (DR cache)
            mDRCache.push(newdata);               // Push data into DataReader cache
          mMtxDRCache.unlock();
          
          run++; // Counting variable
          
          // --------- DEBUGGING -----------
          if (tDebug) 
          {
            mMtxDRCache.lock();
              unsigned int tDRCacheSize = mDRCache.size();
            mMtxDRCache.unlock();

            mMtxPRCache.lock();
              unsigned int tPRCacheSize = mPRCache.size();
            mMtxPRCache.unlock();

            printf(ANSI_COLOR_GREEN "DEBUG Datareader: DR-cache-size: %u, PR-cache-size: %u" ANSI_COLOR_RESET "\n",
                   tDRCacheSize, tPRCacheSize);
          }
          //--------------------------------
          
          //update state information for measurement loop
          mMtxSharedData.lock();
            tRunFlag = mState.mRunFlag;
            tDebug   = mState.mDebFlag;
          mMtxSharedData.unlock();
        } //measurement loop
      } 
      else 
      {
        //mtx_sharedata.unlock();
        //sleep some time to wait for measurement request and decrease CPU burden
        delay_ms(50);
      }

      //update state information for DataReader
      mMtxDataReader.lock();
        tEndDR = datareader_shutdwn;
      mMtxDataReader.unlock();
    } //end of main DataReader while loop

    //mtx_datareader.unlock();
    printf(ANSI_COLOR_GREEN "Datareader: preparing for shutdown ..." ANSI_COLOR_RESET "\n");
    
    mMtxProcessing.lock();
      processing_shutdwn = true;
    mMtxProcessing.unlock();

    //give time for synchronisation
    delay_ms(10);
  
    printf(ANSI_COLOR_GREEN "Datareader: Waiting for termination of processing-thread ..." ANSI_COLOR_RESET "\n");
    pthread_join(thread2, NULL);    // Waiting for Processing-Thread
  }
  catch(ilmsens::hal::Error& e) 
  {
    printf(ANSI_COLOR_RED "Datareader: HAL-exception: " ANSI_COLOR_RESET);
    cout << e.what() << std::dec << " Code was: " << e.status_code() << std::endl;
//         mtx_sharedata.lock();
//         share.exitflag = true;
//         mtx_sharedata.unlock();
  }
  catch(ilmsens::mliquid::dairy::Error& e) 
  {
    printf(ANSI_COLOR_RED "Datareader: APP-exception: " ANSI_COLOR_RESET);
    cout << e.what() << std::dec << " Code was: " << e.getErrorCode() << std::endl;
  }
  catch(std::exception& e) 
  {
    printf(ANSI_COLOR_RED "Datareader: std. exception: " ANSI_COLOR_RESET);
    cout << e.what() << endl;
  }
  catch(...) 
  {
    printf(ANSI_COLOR_RED "Datareader: unknown exception!" ANSI_COLOR_RESET);
  }

  // can go now
  printf(ANSI_COLOR_GREEN "Datareader: shutdown complete, exiting thread." ANSI_COLOR_RESET "\n");
  return(NULL);
}


/* -------------------------------------------------------------------------------------------------
 * Processing thread
 * 
 * Receives data during running measurement from DataReader and process it according to requested
 * processing flags from client. The thread is created and shutdown by the DataReader.
 * 
 * By default, MLBS correlation by KKF is performed. Optionally, a hilbert transform of the 
 * correlated data and/or exponential background removal may be performed, too.
 * 
 * -------------------------------------------------------------------------------------------------
 */

void* processing(void*)
{
  try 
  {
    printf(ANSI_COLOR_YELLOW "Processing: thread created" ANSI_COLOR_RESET "\n");
    
    /* Bind Thread to single CPU Core */
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(1, &cpuset); // CPU #1
    if(pthread_setaffinity_np(pthread_self(), sizeof(cpuset), &cpuset) != 0) 
    {
      perror("Processing::ERROR: pthread_setaffinity_np failed");
    }

    //get required information for processing
    mMtxSharedData.lock();
      unsigned int tMLBSLen = mState.mMLBSLen;
      bool tDoHilb          = mState.mHilbFlag;
      bool tDoBGR           = mState.mBGFlag;
      bool tDebug           = mState.mDebFlag;
    mMtxSharedData.unlock();

    // acquire processing ressources as needed
    // use exception-save smart pointers
    std::unique_ptr< KKF > myKKF(new KKF(tMLBSLen));
    printf(ANSI_COLOR_YELLOW "Processing: KKF handler created" ANSI_COLOR_RESET "\n");
    
    std::unique_ptr< Hilbert >    myHilbert;
    std::unique_ptr< datahandle > myData;
    if (tDoHilb)
    {
      myHilbert.reset(new Hilbert(tMLBSLen));
      printf(ANSI_COLOR_YELLOW "Processing: Hilbert handler created" ANSI_COLOR_RESET "\n");
    }
    if (tDoBGR)
    {
      myData.reset(new datahandle(tMLBSLen, 10));
      printf(ANSI_COLOR_YELLOW "Processing: BGR handler created" ANSI_COLOR_RESET "\n");
    }

    // additional vectors/buffers
    TFIFOCacheEntry rawdata;
    TFIFOCacheEntry procdata;
    procdata.Channel1.resize(tMLBSLen);
    procdata.Channel2.resize(tMLBSLen);
    procdata.IRS_ch1.resize(tMLBSLen);
    procdata.IRS_ch2.resize(tMLBSLen);
    TRealIRF IRS_ch1(tMLBSLen, 0);
    TRealIRF IRS_ch2(tMLBSLen, 0);

    /*
    fifocache* rawdata = new fifocache;
    fifocache* procdata = new fifocache;
    KKF* myKKF = new KKF(tMLBSLen);
    datahandle* myData = new datahandle;
    Hilbert* myHilbert = new Hilbert(tMLBSLen);
    std::vector<float> IRS_ch1, IRS_ch2;
    */

    //get state of processing
    printf(ANSI_COLOR_YELLOW "Processing: waiting for data to process ..." ANSI_COLOR_RESET "\n");
    mMtxProcessing.lock();
      bool tEndProc = processing_shutdwn;
    mMtxProcessing.unlock();
    while(!tEndProc) 
    {
      //mtx_processing.unlock();

      //mtx_processing.lock();
      
      // Check if new data is available in DataReader cache
      mMtxDRCache.lock();
        bool tDRCEmpty = mDRCache.empty();
        
      if (!tDRCEmpty)
      {
        // new data is there, so get & process it
        if (tDebug) printf(ANSI_COLOR_YELLOW "Processing: detected new data ..." ANSI_COLOR_RESET "\n");
        
        // pop data from cache
        rawdata = mDRCache.front(); // Get new Data
        //*rawdata = mDRCache.front(); // Get new Data
        mDRCache.pop();              // Delete new Data in Cache 1
        if (tDebug) printf(ANSI_COLOR_YELLOW "Processing: received new data ..." ANSI_COLOR_RESET "\n");

        // unlock DR cache
        mMtxDRCache.unlock();

        //printf(ANSI_COLOR_YELLOW "Processing: channel length is %u." ANSI_COLOR_RESET "\n", rawdata.Channel1.size());

        // do MLBS correlation & copy information
        if (tDebug) printf(ANSI_COLOR_YELLOW "Processing: doing KKF ..." ANSI_COLOR_RESET "\n");
        procdata.Channel1 = myKKF->calculateKKF(rawdata.Channel1);
        procdata.Channel2 = myKKF->calculateKKF(rawdata.Channel2);

        if (tDebug) printf(ANSI_COLOR_YELLOW "Processing: KKF calculated ..." ANSI_COLOR_RESET "\n");

        procdata.index        = rawdata.index;         // Copy index
        procdata.ADClevel_Rx1 = rawdata.ADClevel_Rx1;  // Copy ADC Level
        procdata.ADClevel_Rx2 = rawdata.ADClevel_Rx2;  // Copy ADC Level
        procdata.cache1size   = rawdata.cache1size;    // Copy Cache 1 size
        procdata.temp_mexpl   = rawdata.temp_mexpl;    // Copy Temp. (additional IRF info from Rx2 of sensor)

        //store current state of processing
        IRS_ch1 = procdata.Channel1;
        IRS_ch2 = procdata.Channel2;

        if (tDebug) printf(ANSI_COLOR_YELLOW "Processing: buffered IRF ..." ANSI_COLOR_RESET "\n");

        // get current processing flags
        mMtxSharedData.lock();
          bool tCurHilb = mState.mHilbFlag;
          bool tCurBGR  = mState.mBGFlag;
        mMtxSharedData.unlock();

        // Exponential BGR?
        if (tDoBGR && tCurBGR)
        {
          // Dynamic exponential Background Subtraction
          myData->rotateIRFs(1);
          myData->attachNewIRF(IRS_ch1, IRS_ch2);
          myData->dynamicBGR(0.05);

          IRS_ch1 = myData->getSingleIRF("rx1", 1);
          IRS_ch2 = myData->getSingleIRF("rx2", 2);

          if (tDebug) printf(ANSI_COLOR_YELLOW "Processing: BGR done ..." ANSI_COLOR_RESET "\n");
        }
       
        // Hilbert Transformation?
        if (tDoHilb && tCurHilb)
        {
          IRS_ch1 = myHilbert->calculateHilbTD(IRS_ch1);
          IRS_ch2 = myHilbert->calculateHilbTD(IRS_ch2);
          if (tDebug) printf(ANSI_COLOR_YELLOW "Processing: Hilbert done ..." ANSI_COLOR_RESET "\n");
        }
        
        // save processed data
        procdata.IRS_ch1 = IRS_ch1;
        procdata.IRS_ch2 = IRS_ch2;

        // Reading temperature of XU4 Board
        TTemperatures tXU4Temps = readXU4Temperature();
        
        procdata.temp_xu4 = *std::max_element(tXU4Temps.begin(), tXU4Temps.end()); // max. Temp. of XU4
    
        //store data in streaming cache
        mMtxPRCache.lock();
          procdata.cache2size = mPRCache.size();
          mPRCache.push(procdata);
        mMtxPRCache.unlock();
      }
      else
      {
        //unlock DR cache
        mMtxDRCache.unlock();

        //wait some time to lower CPU burden
        delay_ms(10);
      }
      
      /*
      while( && !processing_shutdwn) { 
        mtx_processing.unlock();
        locker1.unlock();
          mtx_processing.lock();
        locker1.lock();
      }
      if (processing_shutdwn) {
        mtx_processing.unlock();
        locker1.unlock();
        break;
      }
            mtx_processing.unlock();
      locker1.unlock();
      */

      // update debugging state
      mMtxSharedData.lock();
        tDebug = mState.mDebFlag;
      mMtxSharedData.unlock();
      
      // update processing state
      mMtxProcessing.lock();
        tEndProc = processing_shutdwn;
      mMtxProcessing.unlock();
    }
    //mtx_processing.unlock();

    //give time for synchronisation
    delay_ms(1);
  } 
  catch(ilmsens::mliquid::dairy::Error& e) 
  {
    printf(ANSI_COLOR_RED "Processing: APP-exception: " ANSI_COLOR_RESET);
    cout << e.what() << std::dec << " Code was: " << e.getErrorCode() << std::endl;
  }
  catch(std::exception& e) 
  {
    printf(ANSI_COLOR_RED "Processing: std. exception: " ANSI_COLOR_RESET);
    cout << e.what() << endl;
  }
  catch(...) 
  {
    printf(ANSI_COLOR_RED "Processing: unknown exception!" ANSI_COLOR_RESET);
  }

  /*
  catch(std::exception& ex) 
  {
    cout << "Processing: ERROR OCCURED!" << endl;
    cout << ex.what() << endl;
  }
  */
  
  printf(ANSI_COLOR_YELLOW "Processing: shutdown complete, exiting thread" ANSI_COLOR_RESET "\n");
  return(NULL);
}


/* -------------------------------------------------------------------------------------------------
 * Controlling thread
 * 
 * Creates TCP server socket for command connection and accepts one client at a time.
 * 
 * When a client is successfully connected, the DataReader thread is started (which deploys the
 * processing thread if needed). Received commands are dispatched by the process_command function
 * via gloably defined flags.
 * 
 * When the client closes the connection, flags are set to stop a running measurement and 
 * all other threads are told to end themselves.
 * 
 * -------------------------------------------------------------------------------------------------
 */

void* controlling(void*)
{
  bool tCtrlExit = false;
  
  int           sock1 = 0;
  unsigned int  port1;

  try
  {
    printf(ANSI_COLOR_CYAN "Controlling: Thread created " ANSI_COLOR_RESET "\n");
    
    /* Bind Thread to single CPU Core */
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(2, &cpuset); // CPU #2
    if(pthread_setaffinity_np(pthread_self(), sizeof(cpuset), &cpuset) != 0) 
    {
      perror("Controlling::ERROR: pthread_setaffinity_np");
    }

    /* Erzeugen des Server-Sockets */
    sock1 = socket(AF_INET, SOCK_STREAM, 0); // Socket 1 - command connection
    if(sock1 < 0) 
    {
      printf(ANSI_COLOR_RED "Controlling::ERROR: server socket for command connection could not be created: " ANSI_COLOR_RESET "\n");
      std::cout << strerror(errno) << std::endl;

      throw ilmsens::mliquid::dairy::Error("Controlling::ERROR: server socket for command connection could not be created!", ILMSENS_ERROR_NO_MEMORY);
    }
    
    // fill in socket properties
    port1 = TCP_COMMAND_PORT; // Portnummer
    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;                // IPv4
    server.sin_addr.s_addr = htonl(INADDR_ANY); // Jede Source IP-Adresse ist gültig (listen on any interface)
    server.sin_port = htons(port1);
    
    int option = TCP_CMD_SOCK_REUSE;
    setsockopt(sock1, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));
    
    // BINDING
    while(bind(sock1, (struct sockaddr*)&server, sizeof(server)) < 0) 
    { 
      printf(ANSI_COLOR_RED "Controlling::ERROR: cannot bind command connection socket: " ANSI_COLOR_RESET "\n");
      std::cout << strerror(errno) << std::endl;
      
      printf(ANSI_COLOR_GREEN "Will retry in 2 seconds..." ANSI_COLOR_RESET "\n");
      delay_ms(2000);
    }

    // LISTENING
    if(listen(sock1, 5) < 0) 
    { 
      printf(ANSI_COLOR_RED "Controlling::ERROR: cannot listen on command connection socket : " ANSI_COLOR_RESET "\n");
      std::cout << strerror(errno) << std::endl;

      throw ilmsens::mliquid::dairy::Error("Controlling::ERROR: cannot listen on command connection socket!", ILMSENS_ERROR_NO_MEMORY);
    }
  }
  catch(ilmsens::mliquid::dairy::Error& e) 
  {
    printf(ANSI_COLOR_RED "Controlling: APP-exception: " ANSI_COLOR_RESET);
    cout << e.what() << std::dec << " Code was: " << e.getErrorCode() << std::endl;

    tCtrlExit = true;
  }
  catch(std::exception& e) 
  {
    printf(ANSI_COLOR_RED "Controlling: std. exception: " ANSI_COLOR_RESET);
    cout << e.what() << endl;

    tCtrlExit = true;
  }
  catch(...) 
  {
    printf(ANSI_COLOR_RED "Controlling: unknown exception!" ANSI_COLOR_RESET);

    tCtrlExit = true;
  }

  // listen & serve TCP command connection until the EXIT command has been received
  while(!tCtrlExit) 
  {
    int sock1_acpt = 0;

    try 
    {
      // give time for synchronisation after a client disconnected
      delay_ms(10);

      printf(ANSI_COLOR_CYAN "Controlling: Waiting for Client at Port %u" ANSI_COLOR_RESET "\n", port1);

      // accept client connection
      struct sockaddr_in client;
      socklen_t socklen = sizeof(client);

      sock1_acpt = accept(sock1, (struct sockaddr*)&client, &socklen);
      if(sock1_acpt < 0) 
      {
        printf("Controlling::ERROR: cannot accept connection on command socket : " ANSI_COLOR_RESET "\n");
        std::cout << strerror(errno) << std::endl;
        
        throw ilmsens::mliquid::dairy::Error("Controlling::ERROR: cannot accept connection on command socket!", ILMSENS_ERROR_NO_MEMORY);
      }

      printf(ANSI_COLOR_CYAN "Controlling: Connection initiated with Client %s" ANSI_COLOR_RESET "\n",
             inet_ntoa(client.sin_addr));
      
      // Create Datareader-Thread since client is connected now
      mMtxDataReader.lock();
        datareader_shutdwn = false;
      mMtxDataReader.unlock();
        
      pthread_t thread1;
      signed int rc = 0;
      
      // Thread 1 (datareader)
      printf(ANSI_COLOR_CYAN "Controlling: creating Datareader-thread..." ANSI_COLOR_RESET "\n");
      rc = pthread_create(&thread1, NULL, &datareader, 0);
      if(rc != 0) 
      {
        perror("Controlling::ERROR: cannot create DataReader thread");

        // also exit the control thread 
        tCtrlExit = true;

        throw ilmsens::mliquid::dairy::Error("Controlling::ERROR: cannot create DataReader thread!", ILMSENS_ERROR_NO_MEMORY);
        
        /*
        // close the opened server/client socket, because the controlling thread will be ended
        close(sock1_acpt);

        break;
        */
      }

      int buffer[TCP_CMD_SIZE];
      char *tBufBytes = (char *)buffer; //access buffer byte-wise

      // Check connection on socket and subsequently serve commands received
      mMtxSharedData.lock();
        bool tExitFlag = mState.mExitFlag;
      mMtxSharedData.unlock();
      unsigned int rxsize = sizeof(int) * TCP_CMD_SIZE;
      unsigned int tRcvd  = 0;
      printf(ANSI_COLOR_CYAN "Controlling: Waiting for command... (length of %u bytes)" ANSI_COLOR_RESET "\n", rxsize);
      bool tCmdRcvd = false;
      while((isConnected(sock1_acpt) == 1) && !tExitFlag) 
      {
        //did we process a new command last time?
        if (tCmdRcvd)
        {
          printf(ANSI_COLOR_CYAN "Controlling: Waiting for command..." ANSI_COLOR_RESET "\n");
          tCmdRcvd = false;
        }

        /* Reading command */
        //unsigned int rxsize = sizeof(int) * TCP_CMD_SIZE;
        //unsigned int len = recv(sock1_acpt, buffer, rxsize, 0);
        //  if(len < 1) 
        //  {
        //  }
        
        /* do non-blocking read to allow asynchr. ending of program*/
        int len = recv(sock1_acpt, (void *)(tBufBytes+tRcvd), rxsize, MSG_DONTWAIT);
        //printf("len = %d\n", len);
        if (len < 0)
        {
          // an error occured, check for errors that indicate no data was there
          int tErrNo = errno;
          
          if ((tErrNo == EAGAIN) || (tErrNo == EWOULDBLOCK))
          {
            //there simply was no data, so wait some time
            delay_ms(10);
          }
          else
          {
            printf(ANSI_COLOR_RED "Controlling::ERROR: could not receive command word: " ANSI_COLOR_RESET "\n");
            std::cout << strerror(tErrNo) << std::endl;

            // need to stop TCP connection, but at the same time close DR thread
            break; // inner loop
            //throw ilmsens::mliquid::dairy::Error("Controlling::ERROR: could not receive command word!", ILMSENS_ERROR_NO_MEMORY);
          }
        }
        else
        {
          if (len)
          {
            // at least some data has been read
            rxsize -= len;
            tRcvd  += len;
          }
          else
          {
            // zero length means connection has been closed
            // need to stop TCP connection, but at the same time close DR thread
            break; // inner loop
          }
        }
        
        if (!rxsize)
        {
          // a complete command has been received, so process it
          process_command(sock1_acpt, ntohl(buffer[0]));

          // start to wait for a new command
          rxsize   = sizeof(int) * TCP_CMD_SIZE;
          tRcvd    = 0;
          tCmdRcvd = true;
        }

        mMtxSharedData.lock();
          tExitFlag = mState.mExitFlag;
        mMtxSharedData.unlock();
      }
        
      if (tExitFlag)
      {
        printf(ANSI_COLOR_CYAN "Controlling: received EXIT command, closing threads and application..." ANSI_COLOR_RESET "\n");
        
        // also exit the control thread 
        tCtrlExit = true;
      }
      else
      {
        printf(ANSI_COLOR_CYAN "Controlling: no or bad client connection on command socket, closing threads..." ANSI_COLOR_RESET "\n");
      }

      // set all flags for shutdown
      mMtxSharedData.lock();
        mState.mRunFlag    = false; // Stop measurement
      mMtxSharedData.unlock();
      
      mMtxDataReader.lock();
        datareader_shutdwn = true;  // Exit datareader thread
      mMtxDataReader.unlock();
        
      mMtxProcessing.lock();
        processing_shutdwn = true;  // Exit processing thread
      mMtxProcessing.unlock();

      /*
      mtx_datareader.lock();
      mtx_processing.lock();
      mtx_sharedata.lock();
      mtx_streaming.lock();
      mtx_streaming.unlock();
      mtx_sharedata.unlock();
      mtx_datareader.unlock();
      mtx_processing.unlock();
      */

      // wait until DataReader thread has gracefully ended
      pthread_join(thread1, NULL);
      
      // Exit streaming socket and empty PR cache
      mMtxStreaming.lock();
        streaming_shutdwn = true;
      mMtxStreaming.unlock();
        
      // Meanwhile, empty DR Cache
      mMtxDRCache.lock();
      while(!mDRCache.empty()) 
      {
        mDRCache.pop();
      }
      mMtxDRCache.unlock();
        
      // close the opened sockets
      //close(sock1_acpt);
      //close(sock1);
    } 
    catch(ilmsens::mliquid::dairy::Error& e) 
    {
      printf(ANSI_COLOR_RED "Controlling: APP-exception: " ANSI_COLOR_RESET);
      cout << e.what() << std::dec << " Code was: " << e.getErrorCode() << std::endl;
    }
    catch(std::exception& e) 
    {
      printf(ANSI_COLOR_RED "Controlling: std. exception: " ANSI_COLOR_RESET);
      cout << e.what() << endl;
    }
    catch(...) 
    {
      printf(ANSI_COLOR_RED "Controlling: unknown exception!" ANSI_COLOR_RESET);
    }
    
    // close server/client TCP connection
    if (sock1_acpt > 0) close(sock1_acpt);
  }

  // close server TCP connection
  if (sock1 > 0) close(sock1);

  // can go now
  printf(ANSI_COLOR_CYAN "Controlling: shutdown complete, exiting thread" ANSI_COLOR_RESET "\n");
  return(NULL);
}


/* -------------------------------------------------------------------------------------------------
 * Streaming thread
 * 
 * Creates TCP server socket for streaming processed data to client.
 * 
 * Accesses a cache shared with processing thread. 
 * 
 * If no client is connected, the cache is just discarded. If a client is connected, data
 * is forwarded to the client as fast as possible. Rx1 data and Rx2 data are sent directly,
 * an additional vector containing various status information is attached to each IRF.
 * 
 * -------------------------------------------------------------------------------------------------
 */

void* streaming(void*)
{
  bool tExitFlag = false;
  
  int           sock1 = 0;
  unsigned int  port1;
  
  try
  {
    printf(ANSI_COLOR_MAGENTA "Streaming: Thread created" ANSI_COLOR_RESET "\n");
    
    /* Bind Thread to single CPU Core */
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(3, &cpuset); // CPU #3
    if(pthread_setaffinity_np(pthread_self(), sizeof(cpuset), &cpuset) != 0) 
    {
      perror("pthread_setaffinity_np");
    }

    // create streaming server TCP socket
    sock1 = socket(AF_INET, SOCK_STREAM, 0); // Socket 1
    if(sock1 < 0) 
    {
      printf(ANSI_COLOR_RED "Streaming::ERROR: could not create socket: " ANSI_COLOR_RESET "\n");
      std::cout << strerror(errno) << std::endl;

      throw ilmsens::mliquid::dairy::Error("Streaming::ERROR: could not create socket!", ILMSENS_ERROR_NO_MEMORY);
    }
    
    // configure server socket
    int option = TCP_CMD_SOCK_REUSE;
    setsockopt(sock1, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));
    
    timeval tTO;
    tTO.tv_usec = 0;
    tTO.tv_sec = TCP_STR_SEND_TO;
    setsockopt(sock1, SOL_SOCKET, SO_SNDTIMEO, &tTO, sizeof(tTO)); // Setting send() timeout
    
    // prepare binding
    port1 = TCP_STREAM_PORT; // Portnumber for streaming channel
    struct sockaddr_in server1;
    memset(&server1, 0, sizeof(server1));
    server1.sin_family = AF_INET;                // IPv4
    server1.sin_addr.s_addr = htonl(INADDR_ANY); // bind to every IP-Adress available
    server1.sin_port = htons(port1);             // set TCP port
    
    // BINDING Socket
    while(bind(sock1, (struct sockaddr*)&server1, sizeof(server1)) < 0) 
    { 
      printf(ANSI_COLOR_RED "Streaming::ERROR: cannot bind socket on port/IP-Adress: ");
      std::cout << strerror(errno) << std::endl;

      printf("Will retry in 2 seconds..." ANSI_COLOR_RESET "\n");
      delay_ms(2000);
    }
    
    // LISTENING
    if(listen(sock1, 5) < 0) 
    { 
      printf(ANSI_COLOR_RED "STREAMING::ERROR: cannot listen on socket: " ANSI_COLOR_RESET "\n");
      std::cout << strerror(errno) << std::endl;

      throw ilmsens::mliquid::dairy::Error("Streaming::ERROR: cannot listen on socket!", ILMSENS_ERROR_NO_MEMORY);
    }
  }
  catch(ilmsens::mliquid::dairy::Error& e) 
  {
    printf(ANSI_COLOR_RED "Streaming: APP-exception: " ANSI_COLOR_RESET);
    cout << e.what() << std::dec << " Code was: " << e.getErrorCode() << std::endl;
    tExitFlag = true;
  }
  catch(std::exception& e) 
  {
    printf(ANSI_COLOR_RED "Streaming: std. exception: " ANSI_COLOR_RESET);
    cout << e.what() << endl;
    tExitFlag = true;
  }
  catch(...) 
  {
    printf(ANSI_COLOR_RED "Streaming: unknown exception!" ANSI_COLOR_RESET);
    tExitFlag = true;
  }

  //streaming thread is always running (always waiting for client)
  while(!tExitFlag) 
  {
    int sock1_acpt = 0;

    try 
    {
      // give time for synchronisation
      delay_ms(10);
      
      // prepare client connections
      printf(ANSI_COLOR_MAGENTA "Streaming: Waiting for Client at Port %u" ANSI_COLOR_RESET "\n", port1);

      //define timeout for select
      timeval tTO;
      tTO.tv_usec = 0;
      tTO.tv_sec = TCP_STR_WAIT_TO;
      
      // ACCEPTING with timeout using select
      while (!tExitFlag)
      {
        fd_set tReadFDS;
        
        FD_ZERO(&tReadFDS); //clear it
        FD_SET (sock1, &tReadFDS); //add listening socket to FD set
        int tFDMax = sock1; // maximum socket descriptor number in the set
        
        // call select with timeout for accepting new connections
        int tSelRes = select(tFDMax+1, &tReadFDS, NULL, NULL, &tTO);
        if (tSelRes < 0) 
        {
          printf(ANSI_COLOR_RED "STREAMING::ERROR: select did not succeed on server socket: " ANSI_COLOR_RESET "\n");
          std::cout << strerror(errno) << std::endl;

          //make sure to leave main loop
          tExitFlag = true;          

          throw ilmsens::mliquid::dairy::Error("Streaming::ERROR: could not accept client on server socket!", ILMSENS_ERROR_NO_MEMORY);

          // close the opened sockets
          //close(sock1);
          //break;
        }
      
        // test, if new client was connected
        if (!FD_ISSET(sock1, &tReadFDS))
        {
          // no client wants to connect, so update the exit flag
          mMtxSharedData.lock();
            tExitFlag   = mState.mExitFlag;
          mMtxSharedData.unlock();
          
          // try to drain PR cache, if no client is there or the previous one was disconnected
          mMtxPRCache.lock();
          while(!mPRCache.empty()) 
          {
            mPRCache.pop();
          }
          mMtxPRCache.unlock();
        }
        else
        {
          // a new client can be accepted
          break;  
        }
      } // waiting for client
      
      // a client can be connected?
      if (!tExitFlag)
      {
        // accept new client connection and do streaming
        struct sockaddr_in client1;
        socklen_t socklen1 = sizeof(client1);

        sock1_acpt = accept(sock1, (struct sockaddr*)&client1, &socklen1); 
        if(sock1_acpt < 0) 
        {
          printf(ANSI_COLOR_RED "Streaming::ERROR: cannot accept client on socket: " ANSI_COLOR_RESET "\n");
          std::cout << strerror(errno) << std::endl;
          
          throw ilmsens::mliquid::dairy::Error("Streaming::ERROR: could not accept client on socket!", ILMSENS_ERROR_NO_MEMORY);
          // close the opened sockets
          //close(sock1);
        }

        // apply socket options
        tTO.tv_usec = 0;
        tTO.tv_sec = TCP_STR_SEND_TO;
        setsockopt(sock1_acpt, SOL_SOCKET, SO_SNDTIMEO, &tTO, sizeof(tTO));
        
        printf(ANSI_COLOR_MAGENTA
               "Streaming: Connection initiated with Client %s at port %u " ANSI_COLOR_RESET "\n",
               inet_ntoa(client1.sin_addr), port1);

        // by default, streaming will be working as soon as a client is connected to TCP port
        mMtxStreaming.lock();
          streaming_shutdwn = false;
          bool tExitStream = streaming_shutdwn;
        mMtxStreaming.unlock();
        
        /* DATA TRANSFER */
        
        // ----- FOR DEBUGGING -------
        // save streaming data to file on disc
        /*  unsigned int fd1, fd2, fd3;
         *    remove("/home/odroid/Channel1_debug.bin");
         *    remove("/home/odroid/Channel2_debug.bin");
         *    fd1 = open("/home/odroid/Channel1_debug.bin", O_WRONLY | O_CREAT | O_APPEND, S_IRWXO | S_IRWXU);
         *    fd2 = open("/home/odroid/Channel2_debug.bin", O_WRONLY | O_CREAT | O_APPEND, S_IRWXO | S_IRWXU);
         *    std::vector <float> debugsignal(511);
         *    for (float u = 0; u < 511; u++) {
         *        debugsignal[u] = u-250;
         *    }
         */ 
        // ---------------------------
      
        // count packges streamed
        unsigned int tIdx = 0;
        
        mMtxSharedData.lock();
          //unsigned int tMLBSLen = mState.mMLBSLen;
          tExitFlag             = mState.mExitFlag;
        mMtxSharedData.unlock();
        
        TFIFOCacheEntry senddata;

        TStatusInfo tStatusData(TCP_STATUS_VEC_LEN, 0);
        unsigned int tStatSize = sizeof(TStatusInfo::value_type) * TCP_STATUS_VEC_LEN;
        
        unsigned int tDebug = TCP_DEBUG_OUTPUT;

        // do streaming while client connection is available, data arrives from processing
        while( !tExitStream && !tExitFlag )
        {
          // Waiting for new data in PR cache
          mMtxPRCache.lock();
            bool tEmpty = mPRCache.empty();
          mMtxPRCache.unlock();
          
          if (!tEmpty)
          {
            // new data has arrived from processing thread, so copy and forward it
            mMtxPRCache.lock();
              senddata = mPRCache.front();
              mPRCache.pop();
            mMtxPRCache.unlock();

            if (tDebug)
            {
              printf(ANSI_COLOR_MAGENTA "Streaming: Sending frame #%u" ANSI_COLOR_RESET "\n", tIdx);
            }

            // fill in status data from cache entry
            tStatusData[0] = senddata.index;
            tStatusData[1] = senddata.ADClevel_Rx1;
            tStatusData[2] = senddata.ADClevel_Rx2;
            tStatusData[3] = senddata.cache1size;
            tStatusData[4] = senddata.cache2size;
            tStatusData[5] = senddata.temp_mexpl;
            tStatusData[6] = senddata.temp_xu4;

            // calculate transmit sizes
            unsigned int tRxSize = sizeof(TRealIRF::value_type) * senddata.IRS_ch1.size();

            // get total size of frame including both Rx's data and status information
            //unsigned int txsize = 2 * tRxSize + tStatSize; // total size

            // send Rx 1 data
            if (tDebug)
            {
              printf(ANSI_COLOR_MAGENTA "Streaming: Sending Channel 1 (%u Bytes)" ANSI_COLOR_RESET "\n", tRxSize);
              printf(ANSI_COLOR_MAGENTA "Streaming: 1. Value of Channel 1: %f" ANSI_COLOR_RESET "\n", senddata.IRS_ch1[0]);      // for debugging
              //                                printf(ANSI_COLOR_MAGENTA "Streaming: 2. Value of Channel 1: %f"
              //                                ANSI_COLOR_RESET "\n", senddata->IRS_ch1[1]);      // for debugging
            }

            unsigned int txlen = 0;
            char *tBuf = (char *)senddata.IRS_ch1.data();
            while ( txlen < tRxSize )
            {
              signed int len = send(sock1_acpt, (void *)(tBuf + txlen), tRxSize - txlen, 0);
              if(len < 0) 
              {
                perror(ANSI_COLOR_RED "Streaming::ERROR: cannot send Rx 1 data" ANSI_COLOR_RESET);
                break;
              }
              else
              {
                // count the bytes sent
                txlen += len;
              }
            }

            // send Rx 2 data
            if (tDebug)
            {
              printf(ANSI_COLOR_MAGENTA "Streaming: Sending Channel 2 (%u Bytes)" ANSI_COLOR_RESET "\n", tRxSize);
            }
            txlen = 0;
            tBuf = (char *)senddata.IRS_ch2.data();
            while ( txlen < tRxSize )
            {
              signed int len = send(sock1_acpt, (void *)(tBuf + txlen), tRxSize - txlen, 0);
              if(len < 0) 
              {
                perror(ANSI_COLOR_RED "Streaming::ERROR: cannot send Rx 2 data" ANSI_COLOR_RESET);
                break;
              }
              else
              {
                // count the bytes sent
                txlen += len;
              }
            }

            // send status data
            if (tDebug)
            {
              printf(ANSI_COLOR_MAGENTA "Streaming: sending status data (%u Bytes)" ANSI_COLOR_RESET "\n", tStatSize);
            }
            txlen = 0;
            tBuf = (char *)tStatusData.data();
            while ( txlen < tStatSize )
            {
              signed int len = send(sock1_acpt, (void *)(tBuf + txlen), tStatSize - txlen, 0);
              if(len < 0) 
              {
                perror(ANSI_COLOR_RED "Streaming::ERROR: cannot send status data" ANSI_COLOR_RESET);
                break;
              }
              else
              {
                // count the bytes sent
                txlen += len;
              }
            }

            // package has been sent, so it counts
            tIdx++;
          }
          else
          {
            // test, if connection is still alive...do non-blocking read
            int tBuffer;
            int len = recv(sock1_acpt, (void *)&tBuffer, 4, MSG_DONTWAIT);
            if (len < 0)
            {
              // an error occured, check for errors that indicate no data was there
              int tErrNo = errno;
              
              if ((tErrNo != EAGAIN) && (tErrNo != EWOULDBLOCK))
              {
                printf(ANSI_COLOR_RED "Streaming::ERROR: could not do non-blocking read: " ANSI_COLOR_RESET "\n");
                std::cout << strerror(tErrNo) << std::endl;
              }
            }
            else
            {
              if (len == 0)
              {
                // zero length means connection has been closed
                // need to stop streaming TCP connection
                mMtxStreaming.lock();
                  streaming_shutdwn = true;
                mMtxStreaming.unlock();
              }
            }

            // wait some time to lower CPU burden
            delay_ms(10);
          }
          
          /*
          mtx_streaming.lock();
          locker2.lock();
          while( cache2.empty() && !streaming_shutdwn)      
          {
            mtx_streaming.unlock();
            locker2.unlock();
                    delay_ms(1);
            mtx_streaming.lock();
            locker2.lock();
          }
          mtx_streaming.unlock();
          locker2.unlock();
        
          mtx_streaming.lock();
          if (streaming_shutdwn) {
            mtx_streaming.unlock();
            break;
          }
          mtx_streaming.unlock();
              
          locker2.lock();
          *senddata = cache2.front();
          cache2.pop();
          locker2.unlock();
              
          mtx_sharedata.lock();
          txsize = sizeof(float) * share.mlbsLen;
          std::vector<float> status;
          status.resize(share.mlbsLen);
          mtx_sharedata.unlock();
        
          locker1.lock();
          locker2.lock();
          status[0] = senddata->index;
          status[1] = senddata->ADClevel_Rx1;
          status[2] = senddata->ADClevel_Rx2;
          status[3] = cache1.size();
          status[4] = cache2.size();
          status[5] = senddata->temp_mexpl;
          status[6] = senddata->temp_xu4;
          locker1.unlock();
          locker2.unlock();
          */
          
          //  /* ------ WRITE DATA TO LOCAL DISK FOR DEBUGGING -------*/
          //
          //  // Channel 1
          //    txsize = 511*sizeof(float);
          //    txlen = 0;
          //    printf("Streaming: Writing Channel 1 to disk\n");
          //    while (txlen < txsize) {
          //      txlen = write(fd1. &(senddata->Channel1[0]). txsize - txlen);
          //    }
          //    if (txlen < txsize) {
          //      printf("STREAMING ERROR: Writing Data to Disk\n");
          //    }
          //
          //  // Channel 2
          //    txsize = 511*sizeof(float);
          //    txlen = 0;
          //    printf("Streaming: Writing Channel 2 to disk\n");
          //    while (txlen < txsize) {
          //      txlen = write(fd2. &(senddata->Channel2[0]). txsize - txlen);
          //    }
          //    if (txlen < txsize) {
          //      printf("STREAMING ERROR: Writing Data to Disk\n");
          //    }
          //
          //  /* --------------------------------------------------------- */
          
          /*
          // Channel 1
          printf(ANSI_COLOR_MAGENTA "Streaming: Sending Channel 1 (%u Bytes)" ANSI_COLOR_RESET "\n", txsize);
          printf(ANSI_COLOR_MAGENTA "Streaming: 1. Value of Channel 1: %f" ANSI_COLOR_RESET "\n", senddata->IRS_ch1[0]);      // for debugging
          //                                printf(ANSI_COLOR_MAGENTA "Streaming: 2. Value of Channel 1: %f"
          //                                ANSI_COLOR_RESET "\n", senddata->IRS_ch1[1]);      // for debugging
          txlen = 0;
          mtx_streaming.lock();
          while( (txlen < txsize) && !streaming_shutdwn) {
            len = send(sock1_acpt, &(senddata->IRS_ch1[0]), txsize - txlen, 0);
            if(len < 0) {
              perror(ANSI_COLOR_RED "Streaming: ERROR: Cannot send Channel 1");
              printf(ANSI_COLOR_RESET "\n");
            }
            txlen = txlen + len;
          }
          
          // Channel 2
          printf(ANSI_COLOR_MAGENTA "Streaming: Sending Channel 2 (%u Bytes)" ANSI_COLOR_RESET "\n", txsize);
          //                                printf(ANSI_COLOR_MAGENTA "Streaming: 1. Value of Channel 2: %f"
          //                                ANSI_COLOR_RESET "\n", senddata->IRS_ch2[0]);      // for debugging
          //                                printf(ANSI_COLOR_MAGENTA "Streaming: 2. Value of Channel 2: %f"
          //                                ANSI_COLOR_RESET "\n", senddata->IRS_ch2[1]);      // for debugging
          txlen = 0;
          while( (txlen < txsize) && !streaming_shutdwn) {
            len = send(sock2_acpt, &(senddata->IRS_ch2[0]), txsize - txlen, 0);
            if(len < 0) {
              perror(ANSI_COLOR_RED "Streaming: ERROR: Cannot send Channel 2");
              printf(ANSI_COLOR_RESET "\n");
            }
            txlen = txlen + len;
          }
          
          // Status Data
          printf(ANSI_COLOR_MAGENTA "Streaming: Sending Status Data (%u Bytes)" ANSI_COLOR_RESET "\n", txsize);
          printf(ANSI_COLOR_MAGENTA "Streaming: 1. Value of StatusData: %f" ANSI_COLOR_RESET "\n", status[0]);
          txlen = 0;
          while( (txlen < txsize) && !streaming_shutdwn) {
              len = send(sock3_acpt, &(status[0]), txsize - txlen, 0);
              if(len < 0) {
                  perror(ANSI_COLOR_RED "Streaming: ERROR: Cannot send StatusData");
                  printf(ANSI_COLOR_RESET "\n");
              }
              txlen = txlen + len;
          }
          */

          // update flags
          mMtxStreaming.lock();
            tExitStream = streaming_shutdwn;
          mMtxStreaming.unlock();
          mMtxSharedData.lock();
            tExitFlag   = mState.mExitFlag;
          mMtxSharedData.unlock();
        } // streaming while loop

        if (tDebug)
        {
          printf(ANSI_COLOR_MAGENTA "Streaming: leaving streaming loop with flags streaming = %d | exit = %d" ANSI_COLOR_RESET "\n", tExitStream, tExitFlag);
        }
      
        // close all opened client socket
        //printf(ANSI_COLOR_MAGENTA "Streaming: closing client socket(s)..." ANSI_COLOR_RESET "\n");
        //close(sock1_acpt);
      } // new client connected

      // Either application exits or one or more sockets have no connection (including command connection)
      
      //close all opened sockets
      //printf(ANSI_COLOR_MAGENTA "Streaming: closing remaining sockets..." ANSI_COLOR_RESET "\n");
      //close(sock1);
    } 
    catch(ilmsens::mliquid::dairy::Error& e) 
    {
      printf(ANSI_COLOR_RED "Streaming: APP-exception: " ANSI_COLOR_RESET);
      cout << e.what() << std::dec << " Code was: " << e.getErrorCode() << std::endl;
    }
    catch(std::exception& e) 
    {
      printf(ANSI_COLOR_RED "Streaming: std. exception: " ANSI_COLOR_RESET);
      cout << e.what() << endl;
    }
    catch(...) 
    {
      printf(ANSI_COLOR_RED "Streaming: unknown exception!" ANSI_COLOR_RESET);
    }
    
    // close server/client TCP connection
    if (sock1_acpt > 0)
    {
      printf(ANSI_COLOR_MAGENTA "Streaming: closing client socket(s)..." ANSI_COLOR_RESET "\n");
      close(sock1_acpt);
    }

  } // End of while(!tExitFlag)

  // close server TCP connection
  if (sock1 > 0)
  {
    printf(ANSI_COLOR_MAGENTA "Streaming: closing remaining server sockets..." ANSI_COLOR_RESET "\n");
    close(sock1);
  }
  
  // can go now
  printf(ANSI_COLOR_MAGENTA "Streaming: shutdown complete, exiting thread" ANSI_COLOR_RESET "\n");
  return NULL;
}


/* -------------------------------------------------------------------------------------------------
 * Main program entry point
 * 
 * Checks command line options (if required), and dispatches the controlling and streaming thread.
 * 
 * When the EXIT command has been received from a client, the corresponding threads shut down
 * and main() cleans up any remaining ressources.
 * 
 * -------------------------------------------------------------------------------------------------
 */

//int main(int argn, char* argv[])
int main(void)
{
  /*
  // setup Poco logging  
  configureLogging(Poco::Message::PRIO_NOTICE);
  */

  // print greeting
  printf(ANSI_COLOR_RESET "\n");
  std::cout << std::endl << "**** Welcome to Isoptera Scanner V" << std::dec 
            << ILMSENS_MLIQUID_DAIRY_VER_MAJOR << "."
            << ILMSENS_MLIQUID_DAIRY_VER_MINOR << "."
            << ILMSENS_MLIQUID_DAIRY_VER_BUILD
            << "! ****" << std::endl << std::endl;
    

  /*
  // Set Realtime Priority
  struct sched_param param;
  sched_getparam(0, &param);
  param.sched_priority = 99;
  
  // New: Realtime Priority
  if(sched_setscheduler(0, SCHED_RR, &param) != 0)
  {
    perror("Main::ERROR: could not set scheduling priority");
    return EXIT_FAILURE;
    //exit(-1);
  }
  //sched_getparam(0, &param);
  */
  
  /*** Create always running threads ***/
  pthread_t thread3; // Streaming
  pthread_t thread4; // Controlling
  signed int rc;
  
  // Thread 4 (Controlling)
  rc = pthread_create(&thread4, NULL, &controlling, 0);
  if(rc != 0) 
  {
    printf(ANSI_COLOR_RED "Main::ERROR: cannot create controlling thread! " ANSI_COLOR_RESET "\n");
    return EXIT_FAILURE;
  }
  
  rc = pthread_create(&thread3, NULL, &streaming, 0); // Thread 3 (Streaming)
  if(rc != 0) 
  {
    printf(ANSI_COLOR_RED "Main::ERROR: cannot create streaming thread! " ANSI_COLOR_RESET " \n");
    
    // end the already existing thread
    pthread_cancel(thread4);
    // make sure thread has ended
    pthread_join(thread4, NULL);

    return EXIT_FAILURE;
  }
  
  // Wait until controlling and streaming threads are ended (upon EXIT command from client)
  pthread_join(thread4, NULL);
  pthread_join(thread3, NULL);
  
  printf("\n" ANSI_COLOR_MAGENTA "***\nAll Threads closed, exiting program. Good Bye!\n***" ANSI_COLOR_RESET "\n");
  
  return EXIT_SUCCESS;
}
