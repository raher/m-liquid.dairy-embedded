/// @file datahandle.hpp
/// @brief Basic handling and processing of measured data.
///
/// Measured data history is buffered. Optionally, exponential 
/// Background estimation and removal can be performed on teh stored data.
///
/// This class ensures a more clear data and processing flow.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef DATAHANDLE_HPP
#define DATAHANDLE_HPP

#include <algorithm>
#include <string>

#include "global.hpp"

class datahandle
{
  public:
    datahandle(unsigned int pMLBSLen = 511, unsigned int pBGRLen = 10);
    ~datahandle();

    const TRealIRF& getSingleIRF(std::string pCh, unsigned int pChNum = 0);    // possible strings: "rx1" | "rx2", or channel numbers 1 or 2
    
    void attachNewIRF(const TRealIRF& pRx1, const TRealIRF& pRx2);

    void dynamicBGR(double pAlpha);
    void resetBGR(void);
    void rotateIRFs(int pRot);
    
  private:

    //MLBS properties
    unsigned int mMLBSLen;

    //BGR properties
    unsigned int mBGRLen;                   // Number of Impulse Responses used for dynamic background subtraction

    //BGR buffers
    std::vector < TRealIRF > mRx1;          // Ringbuffer Rx1
    std::vector < TRealIRF > mRx2;          // Ringbuffer Rx2
    TRealIRF mBG1, mBG2;                    // Background for dynamic background subtraction
    TRealIRF mOut1, mOut2;                  // Buffer for result of dynamic background subtraction
};

#endif // DATAHANDLE_HPP
