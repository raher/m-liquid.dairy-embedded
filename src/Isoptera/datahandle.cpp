/* DATAHANDLE.CPP
 * Basic handling of the data matrices for more clarity in the main.cpp.
 */

#include <iostream>
#include <string.h>

#include "datahandle.hpp"

/* Constructor */
datahandle::datahandle(unsigned int pMLBSLen, unsigned int pBGRLen)
:mMLBSLen(pMLBSLen), mBGRLen(pBGRLen)
{
  /*
  mtx_sharedata.lock();
  mlbsLen = share.mlbsLen;
  mtx_sharedata.unlock();
  */

  //resize IRF vectors & buffers according to new length
  mRx1.resize(mMLBSLen);
  mRx2.resize(mMLBSLen);
  for (unsigned int tI = 0; tI < mMLBSLen; tI++) 
  {
    mRx1[tI].resize(mBGRLen);
    mRx2[tI].resize(mBGRLen);
  }

  mOut1.resize(mMLBSLen);
  mOut2.resize(mMLBSLen);

  mBG1.resize(mMLBSLen);
  mBG2.resize(mMLBSLen);
  
  //reset BGR
  resetBGR();
  /*
  for (unsigned int j = 0; j < mlbsLen; j++) 
  {
    BG1[j] = 0;
    BG2[j] = 0;
  }
  */

}

/* Destructor */
datahandle::~datahandle()
{
}


/* Exponential dynamic background estimation and removal */
void datahandle::dynamicBGR(double pAlpha)
{
  //apply for each sample
  double         tBGWeight = 1.0 - pAlpha;
  unsigned int tLastIRFIdx = mBGRLen-1;

  for (unsigned int tJ = 0; tJ < mMLBSLen; tJ++) 
  {
    //remove estimated background from last IRF of window
    mOut1[tJ] = mRx1[tJ][tLastIRFIdx] - mBG1[tJ];
    mOut2[tJ] = mRx2[tJ][tLastIRFIdx] - mBG2[tJ];
    
    //update estimate of BG using latest stored IRF
    mBG1[tJ]  = tBGWeight * mBG1[tJ] + pAlpha * mRx1[tJ][tLastIRFIdx];
    mBG2[tJ]  = tBGWeight * mBG2[tJ] + pAlpha * mRx2[tJ][tLastIRFIdx];
  }
}

/* reset background estimate */
void datahandle::resetBGR(void)
{
  memset((void *)mBG1.data(), 0, sizeof(TRealIRF::value_type) * mMLBSLen);
  memset((void *)mBG2.data(), 0, sizeof(TRealIRF::value_type) * mMLBSLen);
}

/* rotate data vector in BGR buffer */
void datahandle::rotateIRFs(int pRot)
{
  unsigned int tRot;
  
  if (pRot < 0)
  {
    tRot = (unsigned int)(pRot + (int)mBGRLen);
  }
  else
  {
    tRot = (unsigned int)pRot;
  }

  for (unsigned int tJ = 0; tJ < mMLBSLen; tJ++) 
  {
    std::rotate(mRx1[tJ].begin(), mRx1[tJ].begin() + tRot, mRx1[tJ].end());
    std::rotate(mRx2[tJ].begin(), mRx2[tJ].begin() + tRot, mRx2[tJ].end());
  }
}

/* Add new IRFs to the end of the BGR buffer */
void datahandle::attachNewIRF(const TRealIRF& pRx1, const TRealIRF& pRx2)
{
  unsigned int tLastIRFIdx = mBGRLen-1;

  for (unsigned int tJ = 0; tJ < mMLBSLen; tJ++) 
  {
    mRx1[tJ][tLastIRFIdx] = pRx1[tJ];
    mRx2[tJ][tLastIRFIdx] = pRx2[tJ];
  }

  mOut1 = pRx1;
  mOut2 = pRx2;
}

/* Return current output IRFs for given Rx number */
const TRealIRF& datahandle::getSingleIRF(std::string pCh, unsigned int pChNum)
{
  if (pChNum)
  {
    if (pChNum == 1) return(mOut1);
    if (pChNum == 2) return(mOut2);

    std::cerr << "getSingleIRF::ERROR: wrong Rx number requested: " << std::dec << pChNum << "!" << std::endl;
  }
  else
  {
    if (pCh == "rx1") return(mOut1);
    if (pCh == "rx2") return(mOut2);

    std::cerr << "getSingleIRF::ERROR: wrong Rx number requested: ''" << pCh << "''!" << std::endl;
  }

  throw ilmsens::mliquid::dairy::Error("KKF: unknown MLBS length!", ILMSENS_ERROR_INVALID_PARAM);    
}
