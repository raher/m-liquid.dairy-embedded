/* GLOBAL.CPP
 * Shared variables, memories, synchronisation mutexes.
 */

#include "global.hpp"


// Synchronisation mutexes for state/configuration
std::mutex mMtxSharedData;
std::mutex mMtxDataReader;
std::mutex mMtxProcessing;
std::mutex mMtxStreaming;
std::mutex mMtxRun;


// Synchronisation mutexes for memories
std::mutex mMtxDRCache;      // Mutexes for Cache 1: datareader & processing
std::mutex mMtxPRCache;      // Mutexes for Cache 2: processing & streaming


// Structure for sharing global variables and programming state between running threads
TSharedData mState;


// Cache memories
TFIFOCache mDRCache; // Buffer between datareader & processing
TFIFOCache mPRCache; // Buffer between processing & streaming
