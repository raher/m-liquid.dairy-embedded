/// @file main.h
/// @brief Main porgram variables, states, and function prototypes.
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>


#ifndef MAIN_H
#define MAIN_H

/** TCP constants */
#define TCP_COMMAND_PORT   8100        ///< TCP port of command connection
#define TCP_CMD_SIZE       1           ///< TCP command size (used as buffer size) [uint32 words]
#define TCP_CMD_SOCK_REUSE 1           ///< TCP socket option value of SO_REUSEADDR for command connection

#define TCP_STREAM_PORT    8101        ///< TCP port of streaming connection
#define TCP_STR_SEND_TO    2           ///< TCP send timeout [s]
#define TCP_STR_WAIT_TO    1           ///< TCP waiting timeout for select() [s]

#define TCP_STATUS_VEC_LEN 16          ///< length of status vector to be sent along with each measured frame

#define TCP_DEBUG_OUTPUT   1           ///< show debug info about streamed data 

/** Program state transition flags */
bool streaming_shutdwn   = false;
bool processing_shutdwn  = false;
bool controlling_shutdwn = false;
bool datareader_shutdwn  = false;


/** Function prototypes for threads*/
void* processing (void*);
void* datareader (void*);
void* streaming  (void*);
void* controlling(void*);

//
//int isConnected(int socket);
//std::vector<int> readTemperature();
//void process_command(int sock1_acpt, unsigned int cmd);

#endif //MAIN_H
