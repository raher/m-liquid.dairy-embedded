/// @file global.hpp
/// @brief Global variables shared by all threads.
///
/// The variables defined here can be accessed by all threads of the program. 
/// Mutex mechanism is used to synchronise access.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>


#ifndef GLOBAL_HPP
#define GLOBAL_HPP

#include <stdexcept>
#include <mutex>
#include <queue>
#include <vector>

#include "fftw3.h"

#include "ilmsens/ilmsens_error.h"

/**   
 * @defgroup isoptera_global_type Global type definitions used by all threads.
 * @brief Types for shared program states, measured data and memory caches.
 * @{
 */  

/** Configuration and state data structure for sharing between threads */
struct sSharedData 
{
  bool mBGFlag   = false;                         ///< Background Removal enabled / disabled
  bool mHilbFlag = false;                         ///< Hilbert-Transformation enabled / disabled
  bool mExitFlag = false;                         ///< Application want to exit?
  bool mRunFlag  = false;                         ///< Measurement is running?
  bool mDebFlag  = false;                         ///< Output debug messages?
  unsigned int mMLBSLen = 511 ;                   ///< Length of M-Sequence (Samples)
  unsigned int mACK     = 0x41434b20;             ///< Acknoledge Code (ASCII "ACK ")
  unsigned int mNACK    = 0x4e41434b;             ///< Not-Acknoledge Code (ASCII "NACK")
  unsigned int mSW_AVG  = 4;                      ///< Software averages
  unsigned int mHW_AVG  = 48;                     ///< Hardware averages
};

/** Configuration and state data type */
typedef struct sSharedData TSharedData; 


/** Measured data vector type */
typedef std::vector<double> TRealIRF;
typedef std::vector<unsigned int> TStatusInfo;


/** Cache FIFO entry structure */
struct sFIFOCacheEntry
{
  TRealIRF Channel1;            ///< correlated IR of Channel 1
  TRealIRF Channel2;            ///< correlated IR of Channel 2
  TRealIRF IRS_ch1;             ///< processed IR of Channel 1
  TRealIRF IRS_ch2;             ///< processed IR of Channel 2
  unsigned int index;           ///< running Index of IR's
  unsigned int ADClevel_Rx1;    ///< ADC Level Rx1
  unsigned int ADClevel_Rx2;    ///< ADC Level Rx2
  unsigned int temp_mexpl;      ///< Temperature info from m:explore device
  unsigned int temp_xu4;        ///< max. (CPU-) Temperature of embedded XU4 Board
  unsigned int cache1size;      ///< current FIFO Size of Cache 1
  unsigned int cache2size;      ///< current FIFO Size of Cache 2
};

/** Cache FIFO entry type */
typedef struct sFIFOCacheEntry TFIFOCacheEntry; 

/** Cache FIFO type */
typedef std::queue<TFIFOCacheEntry> TFIFOCache;

/** @} isoptera_global_type */

  
/**   
 * @defgroup isoptera_global_mutex Global mutex definitions for synchronisation of threads.
 * @brief Mutexes for shared program states and memory caches.
 * @{
 */  

extern std::mutex mMtxSharedData;                 ///< Mutex for access to shared data structure
extern std::mutex mMtxDataReader;                 ///< Mutex for Datareader Run-Flag
extern std::mutex mMtxProcessing;                 ///< Mutex for Processing Run-Flag
extern std::mutex mMtxStreaming;                  ///< Mutex for Streaming Run-Flag
extern std::mutex mMtxRun;                        ///< Mutex for Measurement Run-Flag

/** @} isoptera_global_mutex */

 
/**   
 * @defgroup isoptera_global_state Global program state information.
 * @brief Structures for representing program state and sensor configuration.
 * @{
 */  

extern TSharedData mState;                        ///< global state flags and sensor configuration

/** @} isoptera_global_state */

 
/**   
 * @defgroup isoptera_global_memory Global memory caches or buffers.
 * @brief Memories for transferring measured data between different threads.
 * @{
 */  

extern TFIFOCache mDRCache;                       ///< Cache for DataReader to buffer data for processing thread
extern TFIFOCache mPRCache;                       ///< Cache for Processing to buffer data for streaming thread

extern std::mutex mMtxDRCache;                    ///< Synchronisation mutex for DR cache (datareader & processing)
extern std::mutex mMtxPRCache;                    ///< Synchronisation mutex for PR cache (processing & streaming)

/** @} isoptera_global_memory */

 
/**   
 * @defgroup isoptera_global_exception Exceptions & safety tools used by this program.
 * @brief Exceptions only used by this program and its threads. Tools for exception safe programming (RAII with FFTw:)
 * @{
 */  

namespace ilmsens 
{
  namespace mliquid
  {
    namespace dairy 
    {
      /**  Application-specific error class storing an Ilmsens error code along with a message */
      class Error : public std::runtime_error 
      {
        typedef std::runtime_error Base;

        public:
          Error(const char* pMsg, int pCode = ILMSENS_ERROR_UNKNOWN)
            : Base(pMsg), mErrorCode(pCode) {}
          
          int getErrorCode() const { return mErrorCode; }

        private:
          int mErrorCode;
      };

      /** FFTw memory delete helper struct for use with smart pointers. */
      struct sFFTwDelete
      {
        void operator()(void* pDel) { fftw_free(pDel); }
      };

    } //namespace dairy
  } //namespace mliquid
} //namespace ilmsens

/** @} isoptera_global_exception */

#endif //GLOBAL_HPP
