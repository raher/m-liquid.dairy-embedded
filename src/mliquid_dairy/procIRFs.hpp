/// @file procIRFs.hpp
/// @brief Main and support funtions for a basic data processing thread.
///
/// The thread takes raw meaured data from a cache memory and pre-processes it.
/// MLBS-correlation will be done as well as other basic processing steps.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef PROCIRFS_HPP
#define PROCIRFS_HPP


/** Sensor-related processing constants */
#define PR_MLBS_ORDER         9         ///< M-Sequence order of m:liquid.dairy sensor
#define PR_MLBS_CLOCK         12.800    ///< RF system clock of m:liquid.dairy sensor [GHz]
#define PR_MLBS_NUM_RX        2         ///< number of Rx per sensor module

/** IRF validity processing constants */
//#define PR_IP_FAK             32      ///< interpolation factor for pulse parameter extraction

#define PR_COE_START_SAMP        0      ///< start index of samples to take into account for centre-of-energy calculation
#define PR_COE_STOP_SAMP       200      ///< stop index of samples to take into account for centre-of-energy calculation

#define PR_HIST_NUM_IRF       8192      ///< number of IRFs in history buffer for selection of valid IRFs

/** calibration constants */
#define PR_Cal_Idle_NUM_IRF   100       ///< number of IRFs to average for calibration of idel probe thresholds

//#define PR_Cal_Idle_Del_OFF   3.5       ///< offset from idle probe value for delay threshold used by milk measurement detection 
//#define PR_Cal_Idle_En_OFF   -4.0       ///< offset from idle probe value for energy threshold used by milk measurement detection 

/** measurement run detection constants */
#define PR_DelayMeasThres     67            ///< max delay treshold for empty probe when no cow is measured
#define PR_EnergyMeasThres    90.0          ///< min energy treshold for empty probe when no cow is measured
#define PR_MeasHighCntThres   100           ///< number of positive indicators for start of cow measurement
#define PR_MeasLowCntThres    100           ///< number of negative indicators for start of cow measurement
#define PR_ResetLowCntThres   200           ///< number of negative indicators for reset of measurement start detection

#define PR_IdleLowCntThres    1000          ///< number of negative indicators for stop of cow measurement

#if 0

/** valid milk IRFs detection constants */
#define PR_MaxSearchDist      300       ///< maximum search distance from last detected maximum delay before stopping maximum detection
#define PR_MinSearchDist      300       ///< maximum search distance from last detected minimum delay before stopping minimum detection

#define PR_PreselectEnMax     100.0     ///< maximum IRF energy for preselecting milk IRFs
#define PR_PreselectEnMin     60.0      ///< minimum IRF energy for preselecting milk IRFs
#define PR_ValidEnMax         80.0      ///< maximum IRF energy for valid milk IRFs
#define PR_ValidEnMin         60.0      ///< minimum IRF energy for valid milk IRFs

#define PR_MaxDelaySpan       0.10      ///< maximum span of valid milk delays during a measurement
#define PR_DelayDiffThres     0.02      ///< delay difference threshold for stopping maximum delay detection
#define PR_PreSearchSteps     10        ///< # of IRFs to consider during pre-search states
#define PR_NextMaxMinFraction 0.6       ///< minimum fraction of span (CurMaxDel - CurMinDel) to define moving towards next maximum

#define PR_ValidDelDevPre     0.006     ///< maximum allowed deviation of delay values for valid IRFs within a measurement interval before maxium delay
#define PR_ValidDelDevPost    0.004     ///< maximum allowed deviation of delay values for valid IRFs within a measurement interval before minium delay

#endif

/** Data forwarding constants */
#define PR_MIN_UPDATE_WAIT_MS 1000      ///< minimum time interval [ms] between forwarding data to streaming process


/** Thread configuration constants */
#define PR_DEBUG_OUTPUT       1         ///< show debug info about processing operation


namespace ilmsens 
{
  namespace mliquid
  {
    namespace dairy 
    {
      namespace processing
      {

        /** Entry point for processing thread */
        void* procIRFs(void*);

      } //namespace processing
    } //namespace dairy
  } //namespace mliquid
} //namespace ilmsens

#endif //PROCIRFS_HPP
