/* procIRFs.cpp
 * 
 * Thread function doing basic processing of raw M-sequence data retrieved from a cache buffer.
 * MLBS-correlation is performed by using cross-correlation method.
 * Processed data is then forwarded into another cache buffer.
 */


/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */


// Std. C++ library types / STL 

// IO, and Strings
#include <iostream>
#include <iomanip>
#include <vector>
#include <deque>
#include <chrono>

#include <cstring>
#include <cmath>

// Math
#include <algorithm>                    // for max_element
#include <limits>                       // for max/min float values

/*
// Poco logging includes
#include "Poco/Logger.h"
#include "Poco/LogStream.h"
#include "Poco/Message.h"
*/

// Timing and threading includes
#include <pthread.h>                    // POSIX threads
#include <sched.h>                      // for Realtime Priority


// Shared variables, memories, and synchronisation ressources
#include "shared.hpp"

// Ilmsens processing library
#include "ilmsens/processing/ilmsens_processing_error.hpp"
#include "ilmsens/processing/KKF_interp.hpp"   // Ilmsens MLBS-KKF class
#include "ilmsens/processing/vector_iteration_functions.hpp"

// Other Ilmsens stuff
#include "ilmsens/ilmsens_error.h"      // Ilmsens error codes

// mean header for module
#include "procIRFs.hpp" 

using namespace ilmsens::processing;

namespace ilmsens 
{
namespace mliquid
{
namespace dairy 
{
namespace processing
{


/* -------------------------------------------------------------------------------------------------
 * Processing types
 */

/** Derived types for data processing */
typedef TTimeDelay TPulseDelay;           // type representing delay times of IRF pulses


/** type of history FIFO buffer entry */
typedef struct  sHistEntry
                {
                  unsigned int  mIRFNum;  // index of IRF in current measurement
                  TRealIRF      mU;       // "voltage" channel, i.e. channel through milk
                  TRealIRF      mI;       // "current" channel, i.e. reference channel
                  TIRFPara      mPara;    // pulse parameters of voltage channel with oversampling
                } THistEntry;

/** type of FIFO buffer for IRF history */
typedef std::deque< THistEntry > THistFIFO; 


/** cow measurement detection state information */
typedef struct  sMeasDetState
                {
                  TPulseDelay   mDelayThres;   // max delay treshold for empty probe when no cow is measured
                  TSample       mEnergyThres;  // min energy treshold for empty probe when no cow is measured

                  unsigned int  mMeasHighCnt;  // count long pulse delays to detect start of measurement
                  unsigned int  mMeasLowCnt;   // count short pulse delays to detect stop of measurement

                  bool          mMeasDetected; // new (cow) measurement detected?

                  size_t        mMeasHistIdx;  // index in history buffer where measurement start was detected
                } TMeasDetState;


/** IRF validiy processing state information */
typedef struct  sValidityState
                {
                  unsigned int  mState;        // state of detection state machine
                  
                  unsigned int  mStopCnt;      // count events to decide stopping a search
                  unsigned int  mSearchDist;   // current search distance from last extremum
                  
                  TPulseDelay   mCurMinDel;    // current delay minimum value
                  size_t        mCurMinIdx;    // index of current minimum in history buffer 
                  TPulseDelay   mNextMinDel;   // next delay minimum value during minimum detection
                  size_t        mNextMinIdx;   // index of next minimum in history buffer 

                  TPulseDelay   mMeasMaxDel;   // overall delay maximum value during ongoing measurement
                  TPulseDelay   mCurMaxDel;    // current delay maximum value
                  size_t        mCurMaxIdx;    // index of current maximum in history buffer 
                } TValidityState;

/** type of buffer for index lists */
typedef std::vector< unsigned int > TIdxList; 

/*
typedef struct  sValidityState
                {
                  TSample       mMinEn;        // minimum energy limit for valid IRFs
                  TSample       mMaxEn;        // maximum energy limit for valid IRFs

                  TSample       mMinAmp;       // current minimum pulse amplitude referenced in historic buffer
                  size_t        mMinHistIdx;   // minimum index in historic buffer

                  TSample       mMaxAmp;       // current maximum pulse amplitude referenced in historic buffer
                  size_t        mMaxHistIdx;   // maximum index in historic buffer

                  TSample       mThres;        // minimum energy limit for valid IRFs
                  unsigned int  mSearchDist;   // current search distance between pulse delay minima
                  unsigned int  mUpSlopeCnt;   // counts delay that are on the rising slope (e.g. when more milk is on probe)

                  TSample       mStopMaxThres; // threshold for detecting end of milk measurement: last maximum delay too low
                  TSample       mStopMinThres; // threshold for detecting end of milk measurement: delay much lower than mThres
                  
                  bool          mMeasDetected; // new (cow) measurement detected?
                  size_t        mMeasHistIdx;  // index in history buffer where measurement start was detected
                  unsigned int  mHighCnt;      // count long pulse delays to detect start of measurement
                  unsigned int  mLowCnt;       // count short pulse delays to detect stop of measurement
                } TValidityState;
*/


/* -------------------------------------------------------------------------------------------------
 * Local processing defines
 */

// validity detection state machine
#define VS_OFF      0                        // validity detection not running (i.e. when no measurement is detected)
#define VS_FIND_MAX 1                        // search for next maximum delay
#define VS_PRE_MIN  2                        // prepare search for next minimum delay after last maximum
#define VS_FIND_MIN 3                        // search for next minimum delay
#define VS_PRE_MAX  4                        // prepare search for next maximum delay after last minimum

/* -------------------------------------------------------------------------------------------------
 * Processing buffers and variables
 */

static TRealIRF     sCalIdleIRF;            // IRF buffer for idle probe calibration
static unsigned int sCalIdleIRFCnt = 0;     // count averaged IRFs for idle probe calibration

static TIRFPara   sPulsePara;               // global pulse parameters

static THistEntry sIRFEntry;                // default history entry (will have correct sizes)
static THistFIFO  sHistBuf;                 // buffer for IRF history during valid IRF selection
static size_t     sHistIdx;                 // index of next history entry

static TRealIRF   sProcIRF;                 // IRF prepared for processing/validity analysis

static TMeasDetState  sMDState;             // processing states of measurement detection
static TValidityState sValIRFState;         // processing states of validity detection

static TRealIRF       sAvgMilkIRF;          // averaged milk IRF for current measurement
static unsigned int   sAvgCnt;              // number of accumulated IRFs
static TRealIRF       sAvgRefIRF;           // averaged reference IRF for current measurement

static TIdxList       sMinIdxList;          // list of detected delay minima indices
static TIdxList       sMaxIdxList;          // list of detected delay maxima indices
static TIdxList       sValIdxList;          // list of valid milk IRF indices


/* -------------------------------------------------------------------------------------------------
 * Calibration helpers
 */

void calIdleProbe(TRealIRF& pIRF)
{
  // accumulate the current IRF
  addRlIRFs(sCalIdleIRF, sCalIdleIRF, pIRF);
  sCalIdleIRFCnt --;

  // check for end of averaging and analyse the resulting IRF
  if (sCalIdleIRFCnt == 0)
  {
    // complete the averaging
    TSample tNumAvg = PR_Cal_Idle_NUM_IRF;
    scaleRlIRFs(sCalIdleIRF, sCalIdleIRF, 1/tNumAvg);

    // get the IRF energy
    TSample     tEn  = getRlEn(sCalIdleIRF);

    // find current transmission pulse by using centre-of-energy in certian IRF interval
    TRealIRF::const_iterator tBeginCoE = sCalIdleIRF.begin() + PR_COE_START_SAMP;
    TRealIRF::const_iterator tStopCoE  = sCalIdleIRF.begin() + PR_COE_STOP_SAMP;
    TRealIRF tCoEIRF(tBeginCoE, tStopCoE);  
    TPulseDelay tCoE = getRlCoE(tCoEIRF);

    // apply the energy and delay threshold used for measurement detection
    mMtxSharedData.lock();
      mState.mDelThres      = tCoE + mState.mDelOff; // PR_Cal_Idle_Del_OFF;
      mState.mEnThres       = tEn  + mState.mEnOff; // PR_Cal_Idle_En_OFF;

      sMDState.mDelayThres  = mState.mDelThres;
      sMDState.mEnergyThres = mState.mEnThres;
    mMtxSharedData.unlock();

    // output calibration results
    std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: finished idle probe threshold calibration: " << std::endl
                                   << "                  Energy threshold set to :" << std::dec << mState.mEnThres << std::endl
                                   << "                  Delay threshold set to  :" << std::dec << mState.mDelThres << std::endl
                                   << ANSI_COLOR_RESET << std::endl;
  }
}


/* -------------------------------------------------------------------------------------------------
 * Processing helpers
 */

void resetMDState(void)
{
  // measurement detection states
  sMDState.mMeasDetected  = false;

  sMDState.mMeasHighCnt   = 0;
  sMDState.mMeasLowCnt    = 0;
  sMDState.mMeasHistIdx   = 0;

  // processing constants
  mMtxSharedData.lock();
    sMDState.mDelayThres  = mState.mDelThres;
    sMDState.mEnergyThres = mState.mEnThres;
  mMtxSharedData.unlock();
}

void resetValIRFState(void)
{
  // state machine
  sValIRFState.mState      = VS_OFF;

  // event counters
  sValIRFState.mStopCnt    = 0;
  sValIRFState.mSearchDist = 0;

  // minimum delay search
  sValIRFState.mCurMinDel  = std::numeric_limits<TPulseDelay>::max();
  sValIRFState.mCurMinIdx  = 0;
  sValIRFState.mNextMinDel = std::numeric_limits<TPulseDelay>::max();
  sValIRFState.mNextMinIdx = 0;

  // maximum delay search
  sValIRFState.mMeasMaxDel = std::numeric_limits<TPulseDelay>::lowest();
  sValIRFState.mCurMaxDel  = std::numeric_limits<TPulseDelay>::lowest();
  sValIRFState.mCurMaxIdx  = 0;

  // delay index lists
  sMinIdxList.clear();
  sMaxIdxList.clear();
  sValIdxList.clear();
}

/*
void resetValidityState(void)
{
  // energy thresholds
  sValIRFState.mMinEn = PR_VALID_IRF_MIN_EN;
  sValIRFState.mMaxEn = PR_VALID_IRF_MAX_EN;

  // min/max pulse delays in history buffer
  sValIRFState.mMinAmp      = +100;
  sValIRFState.mMaxAmp      = -100;
  sValIRFState.mMinHistIdx  = 0;
  sValIRFState.mMaxHistIdx  = 0;

  // local minimum delay detection
  sValIRFState.mThres       = PR_VALID_IRF_MAX_DEL - PR_VALID_STOP_MIN_THRES;
  sValIRFState.mSearchDist  = PR_VALID_MIN_SEARCH_DIST;
  sValIRFState.mUpSlopeCnt  = PR_VALID_UP_SLOPE_CNT_MAX;  
  
  // stopping critria for end of milking detection
  sValIRFState.mStopMaxThres  = PR_VALID_STOP_MAX_THRES;
  sValIRFState.mStopMinThres  = PR_VALID_STOP_MIN_THRES;
  
  // states
  sValIRFState.mMeasDetected  = false;
  sValIRFState.mMeasHistIdx   = 0;
  sValIRFState.mHighCnt       = 0;
  sValIRFState.mLowCnt        = 0;
}
*/

// prototypes for separate parts of processing
void detectMeasurement(TPulseDelay pCurDel, TSample pCurEn, bool pDebug);
void detectValidIRFs  (void);
void identifyValidIRFs(void);
void extractMilkParameters(void);


/* -------------------------------------------------------------------------------------------------
 * Online data analysis algorithm(s)
 * 
 * Receives correlated and time-zero-aligned data to decide, if current IRF is a valid
 * milk IRF and must be included for milk analysis.
 * 
 * It requires access to the FRF of the last correlation operation on the 'milk' channel
 * to perform efficient FFT-interpolation
 * 
 * -------------------------------------------------------------------------------------------------
 */


//void procMilkData (unsigned int pIRFNum, TRealIRF& pU, TRealIRF& pI, KKF_interp& pKKF, TIRFPara& pPara, bool pDebug)
bool procMilkData (unsigned int pIRFNum, TRealIRF& pU, TRealIRF& pI, KKF& pKKF, TIRFPara& pPara, bool pDebug)
{
  /* get parameters for current IRF */

  // get interpolated milk channel IRF
  sProcIRF = pKKF.getLastIRF(); // directly get last IRF

  // find current transmission pulse by using centre-of-energy in certian IRF interval
  TRealIRF::const_iterator tBeginCoE = sProcIRF.begin() + PR_COE_START_SAMP;
  TRealIRF::const_iterator tStopCoE  = sProcIRF.begin() + PR_COE_STOP_SAMP;
  TRealIRF tCoEIRF(tBeginCoE, tStopCoE);  
  TPulseDelay tCoE = getRlCoE(tCoEIRF);

  // extract current main pulse parameters
  TMinMax    tIPMinMax = getRlMinMax(sProcIRF);
  TIdxSample tMaxVal   = tIPMinMax.mMax;
  if (fabs(tIPMinMax.mMin.mVal) > tIPMinMax.mMax.mVal)
  {
    tMaxVal = tIPMinMax.mMin;
  }

  // output result
  if (pDebug)
  {
    std::cout << ANSI_COLOR_YELLOW << "procMilkData::INFO: pulse parameters U:" << std::endl 
              << "max. val | idx: " << std::dec << tIPMinMax.mMax.mVal << " | " << tIPMinMax.mMax.mIdx << std::endl
              << "min. val | idx: " << std::dec << tIPMinMax.mMin.mVal << " | " << tIPMinMax.mMin.mIdx << std::endl
              << "CoE as idx    : " << std::dec << tCoE << std::endl
              << ANSI_COLOR_RESET << std::endl;
  }

  /* put current IRF and results into history buffer */
  sPulsePara.mMin = tIPMinMax.mMin;
  sPulsePara.mMax = tIPMinMax.mMax;
  sPulsePara.mExt = tMaxVal;
  sPulsePara.mEn  = pPara.mEn;
  sPulsePara.mSum = pPara.mSum;
  sPulsePara.mCoE = tCoE;

  sIRFEntry.mIRFNum = pIRFNum;
  sIRFEntry.mPara   = sPulsePara;
  sIRFEntry.mU      = pU;
  sIRFEntry.mI      = pI;

  if (sHistIdx >= sHistBuf.size())
  {
    // buffer is full, so delete oldest entry and append new one
    sHistBuf.pop_front();
    sHistBuf.push_back(sIRFEntry);

    // must adopt other indices related to historic buffer
    if (sMDState.mMeasDetected)
    {
      if (sValIRFState.mCurMinIdx ) sValIRFState.mCurMinIdx --;
      if (sValIRFState.mNextMinIdx) sValIRFState.mNextMinIdx --;
      if (sValIRFState.mCurMaxIdx ) sValIRFState.mCurMaxIdx --;
    }
  }
  else
  {
    // still not full, so directly set the entry and increase buffer occupation
    sHistBuf[sHistIdx] = sIRFEntry;
    sHistIdx ++;
  }

  // get current parameters
  TPulseDelay tCurDel = tCoE; // tMaxVal.mIdx;
  TSample     tCurEn  = pPara.mEn;

  /* detect, if measurement is running */
  detectMeasurement(tCurDel, tCurEn, pDebug);

  /* detect valid milk IRFs */
  //detectValidIRFs();

  // original data has not been changed, only analysed
  return(false);
}


/* detect, if a cow is being processed and milk is flowing through sonde */
void detectMeasurement(TPulseDelay pCurDel, TSample pCurEn, bool pDebug)
{
  if (sMDState.mMeasDetected)
  {
    // cow measurement running, so try to detect end of measurement
    if ((pCurDel >= sMDState.mDelayThres) && (pCurEn <= sMDState.mEnergyThres))
    {
      // reduce negative indicators when positive indicators occur
      if (sMDState.mMeasLowCnt)
      {
        sMDState.mMeasLowCnt--;
      }
    }
    else
    {
      // count negative indicators
      sMDState.mMeasLowCnt++;
    }

    // check neg indicators to decide stop of measurement
    if (sMDState.mMeasLowCnt >= PR_IdleLowCntThres)
    {
      // the current cow measurement seems to be stopped
      sMDState.mMeasDetected = false;
      sMDState.mMeasHighCnt  = 0;
      sMDState.mMeasLowCnt   = 0;

      // change global state
      mMtxSharedData.unlock();
        mState.mMeasFlag = false;
      mMtxSharedData.unlock();

      if (pDebug)
      {
        std::cout << ANSI_COLOR_YELLOW << "procMilkData::INFO: stop of measurement detected!" << ANSI_COLOR_RESET << std::endl;
      }
    }

    // all historic indices should been processed
    sMDState.mMeasHistIdx = sHistIdx - 1;
  }
  else
  {
    // no cow is being processed, try to detect start of cow measurement
    if ((pCurDel >= sMDState.mDelayThres) && (pCurEn <= sMDState.mEnergyThres))
    {
      // count this IRF as positive indicator
      sMDState.mMeasHighCnt++;
    }
    else
    {
      // count this IRF as negative indicator, if positive indicators have been there
      if (sMDState.mMeasHighCnt)
      {
        sMDState.mMeasLowCnt++;
      }
    }

    // check pos/neg indicators to decide start of measurement
    if ((sMDState.mMeasHighCnt >= PR_MeasHighCntThres) && (sMDState.mMeasLowCnt <= PR_MeasLowCntThres))
    {
      /* more positive than negative indicators have occured, a measurement might have
       * been started, mark start of measurement in historic buffer
       */
      int tMeasHistIdx = (int)sHistIdx - (int)(sMDState.mMeasHighCnt) - (int)(sMDState.mMeasLowCnt) - 30;
      if (tMeasHistIdx < 0) 
      {
        sMDState.mMeasHistIdx = 0;
      }
      else
      {
        sMDState.mMeasHistIdx = (unsigned int)tMeasHistIdx;
      }

      sMDState.mMeasDetected = true;
      sMDState.mMeasHighCnt  = 0;
      sMDState.mMeasLowCnt   = 0;

      // get info from first IRF of detected measurement and boot-strap validity detection
      sIRFEntry = sHistBuf[sMDState.mMeasHistIdx];
      TPulseDelay tMDel = sIRFEntry.mPara.mCoE; // .mExt.mIdx;

      sValIRFState.mCurMinDel    = tMDel;
      sValIRFState.mCurMinIdx    = sMDState.mMeasHistIdx;
      sValIRFState.mNextMinDel   = std::numeric_limits<TPulseDelay>::max();
      sValIRFState.mNextMinIdx   = 0;

      sValIRFState.mMeasMaxDel   = pCurDel;
      sValIRFState.mCurMaxDel    = tMDel;
      sValIRFState.mCurMaxIdx    = sMDState.mMeasHistIdx;
      sValIRFState.mSearchDist   = 0;

      sValIRFState.mStopCnt      = 0;
      sValIRFState.mState        = VS_PRE_MAX;

      // reset the result buffers
      memset((void*)sAvgMilkIRF.data(), 0, sizeof(TRealIRF::value_type) * sAvgMilkIRF.size());
      memset((void*)sAvgRefIRF.data() , 0, sizeof(TRealIRF::value_type) * sAvgRefIRF.size());
      sAvgCnt = 0;

      // change global state
      mMtxSharedData.unlock();
        mState.mMeasFlag = true;
      mMtxSharedData.unlock();

      if (pDebug)
      {
        std::cout << ANSI_COLOR_YELLOW << "procMilkData::INFO: start of measurement detected!" << ANSI_COLOR_RESET << std::endl;
      }
    }
    else
    {
      // reset positive counts, if too many negative counts have occured
      if (sMDState.mMeasLowCnt >= PR_ResetLowCntThres)
      {
        // too many negative indicators, so start over
        sMDState.mMeasHighCnt = 0;
        sMDState.mMeasLowCnt  = 0;
      }
    }
  }
}

#if 0
/* detect, if new IRFs in historic buffer are valid milk IRFs used for milk parameter extraction */
void detectValidIRFs(void)
{
  if (sMDState.mMeasDetected)
  {
    // a measurement has been detected, so process all new IRFs and check for validity
    for (unsigned int tI = sMDState.mMeasHistIdx; tI < sHistIdx; tI++)
    {
      sIRFEntry = sHistBuf[tI];

      TPulseDelay tCurDel = sIRFEntry.mPara.mCoE; // .mExt.mIdx;
      TSample     tCurEn  = sIRFEntry.mPara.mEn;

      // check for overall delay maximum during a measurement
      if (tCurDel > sValIRFState.mMeasMaxDel)
      {
        sValIRFState.mMeasMaxDel = tCurDel;
      }

      // check IRF energy to see, if current IRF must be considered for delay analysis
      if ((tCurEn >= PR_PreselectEnMin) && (tCurEn <= PR_PreselectEnMax))
      {
        // pulse delay analysis state machine
        switch (sValIRFState.mState)
        {
          // find new delay maximum
          case VS_FIND_MAX:
            // check for sanity of currentr delay value
            if (tCurDel > (sValIRFState.mMeasMaxDel - PR_MaxDelaySpan))
            {
              if (tCurDel > sValIRFState.mCurMaxDel)
              {
                // found new maximum delay
                sValIRFState.mCurMaxDel  = tCurDel;
                sValIRFState.mCurMaxIdx  = tI;
                sValIRFState.mSearchDist = 0;
              }
              else
              {
                // mind the current search distance
                sValIRFState.mSearchDist++;

                //check, if maximum detection must be ended  
                if (
                    (tCurDel < (sValIRFState.mCurMaxDel - PR_DelayDiffThres)) ||
                    (sValIRFState.mSearchDist > PR_MaxSearchDist)
                   )
                {
                  // set current value as new minimum
                  sValIRFState.mNextMinDel = tCurDel;
                  sValIRFState.mNextMinIdx = tI;
                  sValIRFState.mStopCnt    = 0;
                  sValIRFState.mSearchDist = 0;

                  sValIRFState.mState      = VS_PRE_MIN;
                }
              }
            }

            break;

          // find the next delay minimum: first just skip some IRFs after last maximum
          case VS_PRE_MIN:
            // found a lower delay?
            if (tCurDel < sValIRFState.mNextMinDel)
            {
              // found new minimum delay
              sValIRFState.mNextMinDel = tCurDel;
              sValIRFState.mNextMinIdx = tI;
            }

            // count IRFs in this state
            if (sValIRFState.mStopCnt < PR_PreSearchSteps)
            {
              sValIRFState.mStopCnt++;
            }
            else
            {
              // need to go to next state and continue minimum search
              sValIRFState.mState   = VS_FIND_MIN;
              sValIRFState.mStopCnt = 0;
            }

            break;

          // find the next delay minimum until some criteria are fullfilled
          case VS_FIND_MIN:
            // found a lower delay?
            if (tCurDel < sValIRFState.mNextMinDel)
            {
              // found new minimum delay
              sValIRFState.mNextMinDel = tCurDel;
              sValIRFState.mNextMinIdx = tI;
              sValIRFState.mStopCnt    = 0;
              sValIRFState.mSearchDist = 0;
            }
            else
            {
              // mind the current search distance
              sValIRFState.mSearchDist++;

              // try to detect up-ramping of delays
              float tStopThres = (float)(sValIRFState.mNextMinDel) + PR_NextMaxMinFraction * (float)(sValIRFState.mCurMaxDel - sValIRFState.mNextMinDel);
              if ((float)tCurDel > tStopThres)
              {
                sValIRFState.mStopCnt++;
              }
            }

            // go on to finding next maximum when on the way up or gone too far
            if ((sValIRFState.mStopCnt >= PR_PreSearchSteps) || (sValIRFState.mSearchDist > PR_MinSearchDist))
            {
              // found maximum between two minima -> add relevant indices to output
              sMinIdxList.push_back(sHistBuf[sValIRFState.mCurMinIdx].mIRFNum);
              sMaxIdxList.push_back(sHistBuf[sValIRFState.mCurMaxIdx].mIRFNum);

              // identify valid IRFs between minima
              identifyValidIRFs();

              // make next minimum the current minimum
              sValIRFState.mCurMinDel = sValIRFState.mNextMinDel;
              sValIRFState.mCurMinIdx = sValIRFState.mNextMinIdx;

              // need to go to next state and prepare new maximum sreach
              sValIRFState.mCurMaxDel = tCurDel;
              sValIRFState.mCurMaxIdx = tI;

              sValIRFState.mState      = VS_PRE_MAX;
              sValIRFState.mStopCnt    = 0;
              sValIRFState.mSearchDist = 0;
            }

            break;

          // find the next delay maximum: first just skip some IRFs after last minimum
          case VS_PRE_MAX:
            // found a higher delay?
            if (tCurDel > sValIRFState.mCurMaxDel)
            {
              // found new maximum delay
              sValIRFState.mCurMaxDel = tCurDel;
              sValIRFState.mCurMaxIdx = tI;
            }

            // count IRFs in this state
            if (sValIRFState.mStopCnt < PR_PreSearchSteps)
            {
              sValIRFState.mStopCnt++;
            }
            else
            {
              // need to go to next state and continue maximum search
              sValIRFState.mState   = VS_FIND_MAX;
              sValIRFState.mStopCnt = 0;
            }

            break;

          case VS_OFF:
            // this should not happen, the default state should be set by milking detection
            std::cout << ANSI_COLOR_RED << "Processing::ERROR: wrong state (OFF) during validity detection: " << std::dec << sValIRFState.mState << ANSI_COLOR_RESET << std::endl;
            sValIRFState.mState = VS_PRE_MAX;
            break;

          default:
            // this should not happen
            std::cout << ANSI_COLOR_RED << "Processing::ERROR: unknown state of validity detection: " << std::dec << sValIRFState.mState << ANSI_COLOR_RESET << std::endl;
            sValIRFState.mState = VS_PRE_MAX;
            break;
        }
      }
    }
  }
  else
  {
    if (sValIRFState.mState != VS_OFF)
    {
      // end valid IRF detection depending on last state
      switch (sValIRFState.mState)
      {
        case VS_PRE_MAX:
        case VS_FIND_MAX:
          // stopped on the search for next maximum delay, i.e. add last minimum to respective list
          sMinIdxList.push_back(sHistBuf[sValIRFState.mNextMinIdx].mIRFNum);

          break;

        case VS_PRE_MIN:
          // nothing to do when starting the search for new minimum
          break;

        case VS_FIND_MIN:
          // when on the search for new minimum, the last IRF of the measurememt
          // will be used as last IRF and the current IRF interval will be included
          // in valid IRF averaging
          identifyValidIRFs();

          // save current maximum and latest minima to respective lists
          sMinIdxList.push_back(sHistBuf[sValIRFState.mCurMinIdx].mIRFNum);
          sMinIdxList.push_back(sHistBuf[sValIRFState.mNextMinIdx].mIRFNum);
          sMaxIdxList.push_back(sHistBuf[sValIRFState.mCurMaxIdx].mIRFNum);

          break;

        default:
          // this should not happen
          std::cout << ANSI_COLOR_RED << "Processing::ERROR: unknown state of validity detection: " << std::dec << sValIRFState.mState << ANSI_COLOR_RESET << std::endl;
          sValIRFState.mState = VS_PRE_MAX;
          break;
      }

      // need to reset state(s) and counters
      resetValIRFState();

      // calculate averaged result IRFs
      if (sAvgCnt)
      {
        // finish averaging of milk channel
        TSample tNumAvg = (TSample) sAvgCnt;
        scaleRlIRFs (sAvgMilkIRF, sAvgMilkIRF, 1/tNumAvg);

        // finish averaging of ref channel
        scaleRlIRFs (sAvgRefIRF, sAvgRefIRF, 1/tNumAvg);

        // save index lists to mass storage!

        //XYZ

        // finally, extract milk parameters
        extractMilkParameters();
      }
    }
  }
}
#endif

#if 0
// identify valid IRFs between current and next minima
void identifyValidIRFs(void)
{
  // identify maximum delay entry between minima
  TPulseDelay  tMaxDel    = std::numeric_limits<TPulseDelay>::lowest();
  unsigned int tMaxDelIdx = 0;

  for (unsigned int tV = sValIRFState.mCurMinIdx; tV <= sValIRFState.mNextMinIdx; tV++)
  {
//    if (sHistBuf[tV].mPara.mExt.mIdx > tMaxDel)
    if (sHistBuf[tV].mPara.mCoE > tMaxDel)
    {
      tMaxDel    = sHistBuf[tV].mPara.mCoE; // .mExt.mIdx;
      tMaxDelIdx = tV;
    }
  }

  // test all entries, if they are close to the maximum delay in the interval
  TPulseDelay tDelThres = tMaxDel - PR_ValidDelDevPre;
  for (unsigned int tV = sValIRFState.mCurMinIdx; tV <= sValIRFState.mNextMinIdx; tV++)
  {
    if (tV == tMaxDelIdx)
    {
      tDelThres = tMaxDel - PR_ValidDelDevPost;
    }

    // finally check validity
    TSample tCurEn = sHistBuf[tV].mPara.mEn;
//         (sHistBuf[tV].mPara.mExt.mIdx >= tDelThres) &&
    if (
         (sHistBuf[tV].mPara.mCoE >= tDelThres) &&
         (tCurEn > PR_ValidEnMin) &&
         (tCurEn < PR_ValidEnMax)
       )
    {
      // append valid IRF index to valid list -> valid IRFs will be averaged later
      sValIdxList.push_back(sHistBuf[tV].mIRFNum);
      
      // accumulate the valid IRF to the previous ones
      addRlIRFs(sAvgMilkIRF, sAvgMilkIRF, sHistBuf[tV].mU);
      addRlIRFs(sAvgRefIRF , sAvgRefIRF , sHistBuf[tV].mI);
      sAvgCnt++;
    }
  }
}
#endif

#if 0
// when a cow measurement has ended, extract milk parameters from averaged IRFs
void extractMilkParameters(void)
{
  // XYZ: todo
}
#endif

/* -------------------------------------------------------------------------------------------------
 * Processing thread
 * 
 * Receives data during running measurement from DataReader and process it according to requested
 * processing flags from client. The thread is created and shutdown by the DataReader.
 * 
 * By default, MLBS correlation by KKF is performed. 
 * 
 * -------------------------------------------------------------------------------------------------
 */

void* procIRFs(void*)
{
  try 
  {
    std::cout << ANSI_COLOR_YELLOW << "Processing: thread created" << ANSI_COLOR_RESET << std::endl;

    /* Bind Thread to single CPU Core */
#ifndef WIN32
    cpu_set_t tCPUSet;
    CPU_ZERO(&tCPUSet);

    CPU_SET(7, &tCPUSet); // CPU #7
//    CPU_SET(8, &tCPUSet); // CPU #8
    if(pthread_setaffinity_np(pthread_self(), sizeof(tCPUSet), &tCPUSet) != 0) 
    {
      std::cerr << ANSI_COLOR_RED "Processing::ERROR: pthread_setaffinity_np did not succeed: " << ANSI_COLOR_RESET 
                << strerror(errno) << std::endl;
    }
#endif

    /* Get required configuration information for processing */
    mMtxSharedData.lock();
      unsigned int tMLBSLen = mState.mMLBSLen;
      bool tDebug           = mState.mDebFlag;
    mMtxSharedData.unlock();


    /* Acquire pre-processing ressources as needed */
//    unsigned int tInterpLen = (tMLBSLen+1) * PR_IP_FAK;
//    KKF_interp myKKF(tMLBSLen, tInterpLen);
    KKF myKKF(tMLBSLen);
    std::cout << ANSI_COLOR_YELLOW << "Processing: KKF handler created" << ANSI_COLOR_RESET << std::endl;

    // additional vectors/buffers
    TFIFOCacheEntry tRawDat;
    TFIFOCacheEntry tProcData;

    tProcData.mProcRx1.resize(tMLBSLen);
    tProcData.mProcRx2.resize(tMLBSLen);

    // buffers for local processing
    TRealIRF tIRF1(tMLBSLen, 0);
    TRealIRF tIRF2(tMLBSLen, 0);

    sProcIRF.resize(tMLBSLen); // adapt processing vector size to current IRF length


    /* prepare IRF history buffer & validity processing */
    
    // generate default entry with correct sizes
    sIRFEntry.mU    = tIRF1;
    sIRFEntry.mI    = tIRF2;
    sIRFEntry.mPara = sPulsePara;

    // pre-allocate the full history buffer
    sHistIdx = 0;
    sHistBuf.resize(PR_HIST_NUM_IRF, sIRFEntry);
    
    // reset validity processing states and buffers
    resetMDState();
    resetValIRFState();
    
    sAvgMilkIRF.resize(tMLBSLen);
    sAvgRefIRF.resize(tMLBSLen);
    sAvgCnt = 0;
    
    /* prepare processing states */
    unsigned int tZeroIdx = 0;      // index of time-zero for milk analysis
    bool tGetRefPulsePos  = true;   // get current reference index of pulse from 1st IRF
    bool tMeasDetected    = false;  // flag to store last measurement detection state

    /* main loop is executed until shutdown flag is received */

    // initialize execution-time-measurement
    auto tStart = std::chrono::system_clock::now();
    auto tEnd   = std::chrono::system_clock::now();

    // set processing ready flag and get further processing flags
    mMtxSharedData.lock();
      mState.mProcRdy = true;
      bool tCalIdle   = mState.mCalIdleFlag;

      // output current processing constants
      std::cout << ANSI_COLOR_YELLOW
                << "Processing::INFO: data processing constants ..." << std::endl
                << std::endl
                << "Measurement delay threshold    : " << sMDState.mDelayThres << std::endl
                << "Measurement energy threshold   : " << std::setprecision(6) << sMDState.mEnergyThres << std::endl
                << "Measurement delay idle offset  : " << std::setprecision(2) << mState.mDelOff << std::endl
                << "Measurement energy idle offset : " << std::setprecision(2) << mState.mEnOff << std::endl
                << ANSI_COLOR_RESET << std::endl;
    mMtxSharedData.unlock();

    // get state of processing
    std::cout << ANSI_COLOR_YELLOW << "Processing: waiting for data to process ..." << ANSI_COLOR_RESET << std::endl;
    mMtxProcessing.lock();
      bool tEndProc = mProcShutdwn;
    mMtxProcessing.unlock();
    while(!tEndProc) 
    {
      // Check if new data is available in DataReader cache
      mMtxDRCache.lock();
        bool tDRCEmpty = mDRCache.empty();
        
      if (!tDRCEmpty)
      {
        /* new data is there, so get & process it */
        
        // pop data from cache
          tRawDat = mDRCache.front(); // Get new Data
          mDRCache.pop();             // Delete new Data in Cache
        mMtxDRCache.unlock();

        if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: received new data ..." << ANSI_COLOR_RESET << std::endl;


        /* process new data for milk analysis */
        auto tStartProc = std::chrono::system_clock::now();
        
        // rotate vectors to time-zero (ATTENTION: will not be valid for first IRF!)
        if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: time-zero correction ..." << ANSI_COLOR_RESET << std::endl;
        std::rotate(tRawDat.mProcRx1.begin(), tRawDat.mProcRx1.begin() + tZeroIdx, tRawDat.mProcRx1.end());
        std::rotate(tRawDat.mProcRx2.begin(), tRawDat.mProcRx2.begin() + tZeroIdx, tRawDat.mProcRx2.end());

        auto tElRot = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::system_clock::now() - tStartProc);

        // do MLBS correlation
        if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: starting KKF ..." << ANSI_COLOR_RESET << std::endl;
        tIRF2 = myKKF.calculateKKF(tRawDat.mProcRx2);
        tIRF1 = myKKF.calculateKKF(tRawDat.mProcRx1);
        if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: KKF done ..." << ANSI_COLOR_RESET << std::endl;

        auto tElKKF = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::system_clock::now() - tStartProc);

        // copy state and additional info
        tProcData.mIRFNum     = tRawDat.mIRFNum;      // Copy index
        tProcData.mADCLevel1  = tRawDat.mADCLevel1;   // Copy ADC Level
        tProcData.mADCLevel2  = tRawDat.mADCLevel2;   // Copy ADC Level
        tProcData.mSenTempVal = tRawDat.mSenTempVal;  // Copy Temp. (additional IRF info from Rx2 of sensor)
        tProcData.mXU4MaxTemp = tRawDat.mXU4MaxTemp;  // max. Temp. of XU4
        tProcData.mXU4Temp    = tRawDat.mXU4Temp;     // Copy XU4 Temp.
        tProcData.mSenTemp    = tRawDat.mSenTemp;     // probe temperature
        tProcData.mModTemp    = tRawDat.mModTemp;     // module electronic's temperature

        // copy correlated and aligned data to cache entry
        tProcData.mProcRx1 = tIRF1;
        tProcData.mProcRx2 = tIRF2;

        /* extract pulse parameters for time-zero and energy analysis */       
        if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: starting raw pulse analysis ..." << ANSI_COLOR_RESET << std::endl;

        // analyse milk channel parameters
        TIRFPara tVoltPara = getRlIRFPara(tIRF1); 
        if (tDebug)
        {
          std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: pulse parameters Rx1:" << std::endl 
                    << "total energy  : " << std::dec << tVoltPara.mEn << std::endl
                    << "max. val | idx: " << std::dec << tVoltPara.mMax.mVal << " | " << tVoltPara.mMax.mIdx << std::endl
                    << "min. val | idx: " << std::dec << tVoltPara.mMin.mVal << " | " << tVoltPara.mMin.mIdx << std::endl
                    << "Processing::INFO: temperatures (probe/module/XU4): " << std::dec << tProcData.mSenTemp << "°C | " << tProcData.mModTemp << "°C | " << tProcData.mXU4Temp << "°C" << std::endl 
                    << ANSI_COLOR_RESET << std::endl;
        }

        // get time-zero reference ?
        if (tGetRefPulsePos)
        {
          // get current pulse position of reference channel for time-zero
          TMinMax tRefMinMax = getRlMinMax(tIRF2);
          if (fabs(tRefMinMax.mMin.mVal) > tRefMinMax.mMax.mVal)
          {
            tZeroIdx = tRefMinMax.mMin.mIdx;
          }
          else
          {
            tZeroIdx = tRefMinMax.mMax.mIdx;
          }

          tGetRefPulsePos = false;
        }
        auto tElPara = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::system_clock::now() - tStartProc);


        /* --- perform calibration steps, if requested --- */
        if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: beginning calibration section ..." << ANSI_COLOR_RESET << std::endl;

        // idle probe treshold calibration
        if (tCalIdle)
        {
          std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: starting idle probe threshold calibration ..." << ANSI_COLOR_RESET << std::endl;

          // prepare calibration IRF averaging
          sCalIdleIRF.assign(tIRF1.size(), 0.0);
          sCalIdleIRFCnt = PR_Cal_Idle_NUM_IRF;

          // Update debugging and run state
          mMtxSharedData.lock();
            mState.mCalIdleFlag = false;
          mMtxSharedData.unlock();
          tCalIdle = false;
        }

        // do idle probe calibration
        if (sCalIdleIRFCnt)
        {
          // do calibration averaging of voltage IRF and apply calibration results at the end
          calIdleProbe(tIRF1);
        }
        auto tElCal = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::system_clock::now() - tStartProc);


        /* --- analyse pulse parameters to find valid IRFs for milk analysis --- */
        if (tDebug) std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: starting validity analysis ..." << ANSI_COLOR_RESET << std::endl;

        bool tDataChanged = procMilkData (tProcData.mIRFNum, tIRF1, tIRF2, myKKF, tVoltPara, tDebug);

        // output processing state: measurement detected or not?
        if (tMeasDetected != sMDState.mMeasDetected)
        {
          if (sMDState.mMeasDetected)
          {
            std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: cow measurement state changed: DETECTED." << ANSI_COLOR_RESET << std::endl;
          }
          else
          {
            std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: cow measurement state changed: IDLE." << ANSI_COLOR_RESET << std::endl;
          }

          tMeasDetected = sMDState.mMeasDetected;
        }
        auto tElProc = std::chrono::duration_cast< std::chrono::microseconds > (std::chrono::system_clock::now() - tStartProc);

        // output timing info
        if (tDebug)
        {
          std::cout << ANSI_COLOR_YELLOW << "Processing::INFO: durations of different processing stages: " << std::endl
                    << "Rotation       : " << std::dec << tElRot.count() << " us" << std::endl
                    << "KKF            : " << std::dec << tElKKF.count() << " us" << std::endl
                    << "Pulse parameter: " << std::dec << tElPara.count() << " us" << std::endl
                    << "Cal. routines  : " << std::dec << tElCal.count() << " us" << std::endl
                    << "Milk processing: " << std::dec << tElProc.count() << " us" << std::endl
                    << ANSI_COLOR_RESET << std::endl;
        }


        /* forward new data to streaming, if suitable time interval has elapsed */

        // get time elpased since last data retrieval
        tEnd = std::chrono::system_clock::now();
        auto tElapsed = std::chrono::duration_cast< std::chrono::milliseconds > (tEnd - tStart);
        if (tElapsed.count() >= PR_MIN_UPDATE_WAIT_MS)
        {
          // enough time has passed, so forward data to streaming process
          tStart = tEnd;

          // copy processed data, if required
          if (tDataChanged)
          {
            tProcData.mProcRx1 = tIRF1;
            tProcData.mProcRx2 = tIRF2;
          }

          /* Store data in streaming cache */
          mMtxPRCache.lock();
            tProcData.mCacheSize = (TAddInfo) mPRCache.size();
            mPRCache.push(tProcData);
          mMtxPRCache.unlock();
        }
      }
      else
      {
        // unlock DR cache
        mMtxDRCache.unlock();

        // wait some time to lower CPU burden
        delay_ms(1);

        // Update processing state - only when cache is empty.
        // This should ensure, that all received data is being processed, before the thread is closed
        mMtxProcessing.lock();
          tEndProc = mProcShutdwn;
        mMtxProcessing.unlock();
      }

      // Update debugging and run state
      mMtxSharedData.lock();
        tDebug   = mState.mDebFlag;
        tCalIdle = mState.mCalIdleFlag;
      mMtxSharedData.unlock();
    }

    //give time for synchronisation
    delay_ms(1);
  } 
  catch(ilmsens::mliquid::dairy::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "Processing::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;
  }
  catch(ilmsens::processing::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "Processing::Prc-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl;
  }
  catch(std::exception& tErr) 
  {
    std::cerr << ANSI_COLOR_RED << "Processing::Std-EXCEPTION: " << ANSI_COLOR_RESET 
              << tErr.what() << std::endl;
  }
  catch(...) 
  {
    std::cerr << ANSI_COLOR_RED << "Processing::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;
  }
  
  // cleanup history buffers
  sHistBuf.clear();
  
  // can go now
  std::cout << ANSI_COLOR_YELLOW << "Processing: shutdown complete, exiting thread" << ANSI_COLOR_RESET << std::endl;
  return(NULL);
}

} //namespace processing
} //namespace dairy
} //namespace mliquid
} //namespace ilmsens
