/// @file main.hpp
/// @brief App definitions, declarations, and function prototypes.
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>


#ifndef MAIN_HPP
#define MAIN_HPP

// Logging & Debug
//#define LOG_LEVEL_DEFAULT  Poco::Message::Priority::PRIO_INFORMATION      ///< Default Poco log level of app
#define LOG_LEVEL_DEFAULT               3                                 ///< Default log level of app (similar to Poco: 0=Off, 3=error .. 7=debug .. 8=trace)

// sensor setup
#define DAIRY_SENSOR_SETUP_TIMEOUT 10000                                  ///< Timeout while waiting for sensor setup by dataReader thread

// App configuration
#define DAIRY_AUTO_START_MEAS false                                       ///< automatically start a measurement at application start?

// Measurement timing
#define DAIRY_SW_AVG_DEFAULT  4                                           ///< default software averages
#define DAIRY_WC_DEFAULT      0                                           ///< default wait cycles

// raw data storage
#define DAIRY_ACT_STORAGE     false                                       ///< activate storage of raw measued data by default?

// data processing constants
#define DAIRY_DELAY_THRES_DEFAULT     73                                  ///< max delay treshold for empty probe when no cow is measured
#define DAIRY_ENERGY_THRES_DEFAULT    90.0                                ///< max energy treshold for empty probe when no cow is measured

#define DAIRY_Cal_Idle_Del_OFF        3.5                                 ///< offset from idle probe value for delay threshold used by milk measurement detection 
#define DAIRY_Cal_Idle_En_OFF        -4.0                                 ///< offset from idle probe value for energy threshold used by milk measurement detection 

// milking timing constants
#define DAIRY_MILKING_1_START    6                 ///< starting hour of early morning milking
#define DAIRY_MILKING_1_STOP     9                 ///< stopping hour of early morning milking
#define DAIRY_MILKING_2_START    14                ///< starting hour of afternoon milking
#define DAIRY_MILKING_2_STOP     17                ///< stopping hour of afternoon milking

#endif //MAIN_HPP
