/// @file shared.hpp
/// @brief Global variables shared by all threads.
///
/// The variables defined here can be accessed by all threads of the program. 
/// Mutex mechanism is used to synchronise access.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>


#ifndef SHARED_HPP
#define SHARED_HPP

// C++ STL and std.-libs
#include <stdexcept>
#include <mutex>

// processing-related cache buffer types
#include "data_cache_FIFO.hpp"

// exceptions & error handling
#include "ilmsens/ilmsens_error.h"

// Ilmsens HAL constants and types
#include "ilmsens/hal/ilmsens_hal.h"



/**   
 * @defgroup mliquid_dairy_global_defn Common definitions used by the app.
 * @brief Defintions used by all threads, e.g. ANSI color codes, etc.
 * @{
 */  


/** Defining colored outputs in terminal */
#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

/** @} mliquid_dairy_global_defn */



/**   
 * @defgroup mliquid_dairy_global_types Type definitions used by the app.
 * @brief Types for shared program states.
 * @{
 */  

/** Sensor configuration and state data structure for sharing between threads */
struct sSharedData 
{
  bool mSenAct;                                   ///< dataReader could activate the sensor?
  bool mRunFlag;                                  ///< Measurement is running?
  bool mProcRun;                                  ///< Processing is running?

  bool mDebFlag;                                  ///< Output debug messages?
  bool mSaveFlag;                                 ///< Save raw data to storage?
  bool mMeasFlag;                                 ///< Cow measurement detected?
  bool mProcRdy;                                  ///< Processing is ready?

  unsigned int mMLBSLen;                          ///< Length of M-Sequence (Samples)
  unsigned int mACK;                              ///< Acknoledge Code (ASCII "ACK ")
  unsigned int mNACK;                             ///< Not-Acknoledge Code (ASCII "NACK")
  unsigned int mSW_AVG;                           ///< Software averages
  unsigned int mWaitCyc;                          ///< Wait cycles between averaging cycles
  unsigned int mHW_AVG;                           ///< Hardware averages

  unsigned int mNumIRF;                           ///< Number of IRFs to acquire per run, 0 = infinite run

  unsigned int mCow;                              ///< Number of cow currently measured/measurement index (to be set by client for now)

  bool         mCalIdleFlag;                      ///< take current IRF parameters (energy, pulse delay) as basis for milk measurement thresholds
  double       mDelThres;                         ///< max delay treshold for empty probe when no cow is measured
  double       mEnThres;                          ///< max energy treshold for empty probe when no cow is measured
  double       mDelOff;                           ///< delay threshold offset from empty probe when no cow is measured
  double       mEnOff;                            ///< energy threshold offset from empty probe when no cow is measured

  unsigned int mMilkStart1;                       ///< starting hour of morning milking (UTC)
  unsigned int mMilkStart2;                       ///< starting hour of afternoon milking (UTC)
  unsigned int mMilkStop1;                        ///< stopping hour of morning milking (UTC)
  unsigned int mMilkStop2;                        ///< stopping hour of afternoon milking (UTC)

  char                mID[ILMSENS_HAL_MOD_ID_BUF_SIZE];  ///< Device ID string as read from sensor
  ilmsens_hal_ModInfo mModInfo;                                ///< complete module info after configuration

  //default constructor
  sSharedData():
    mSenAct(false),
    mRunFlag(false),
    mProcRun(false),
    mDebFlag(false),
    mSaveFlag(false),
    mMeasFlag(false),
    mProcRdy(false),
    mMLBSLen(511),
    mACK(0x41434b20),
    mNACK(0x4e41434b),
    mSW_AVG(4),
    mWaitCyc(0),
    mHW_AVG(48),
    mNumIRF(0),
    mCow(0),
    mCalIdleFlag(false),
    mDelThres(0.0),
    mEnThres(90.0),
    mDelOff(3.5),
    mEnOff(-10.0),
    mMilkStart1(6),
    mMilkStart2(14),
    mMilkStop1(9),
    mMilkStop2(17)
  {}
};

/** Configuration and state data type */
typedef struct sSharedData TSharedData; 

/** @} mliquid_dairy_global_types */


/**   
 * @defgroup mliquid_dairy_global_synch Mutex definitions for synchronisation of threads.
 * @brief Mutexes for shared program states and memory caches.
 * @{
 */  

// Synchronisation mutexes for configuration and measurement state 
extern std::mutex mMtxSharedData;                 ///< Mutex for access to shared data structure

// Synchronisation mutex for threads
extern std::mutex mMtxExit;                       ///< Mutex for App's EXIT-Flag
extern std::mutex mMtxLogging;                    ///< Mutex for Logging Shutdown-Flag
extern std::mutex mMtxProcessing;                 ///< Mutex for Processing Shutdown-Flag

// Synchronisation mutexes for shared memories
extern std::mutex mMtxDRCache;                    ///< Synchronisation mutex for DR cache (datareader & processing)
extern std::mutex mMtxPRCache;                    ///< Synchronisation mutex for PR cache (processing & streaming)

/** @} mliquid_dairy_global_synch */


/**   
 * @defgroup mliquid_dairy_global_state State and sensor configuration information.
 * @brief Variables representing program or thread state and sensor configuration.
 * @{
 */  

extern TSharedData mState;                        ///< global state flags and sensor configuration

extern bool mExitShutdwn;                         ///< Application shutdown flag
extern bool mLogMShutdwn;                         ///< Logging thread shutdown flag
extern bool mProcShutdwn;                         ///< Processing thread shutdown flag

/** @} mliquid_dairy_global_state */


/**   
 * @defgroup mliquid_dairy_global_memory Global memory caches or buffers.
 * @brief Memories for transferring measured data between different threads.
 * @{
 */  

extern TFIFOCache mDRCache;                       ///< Cache for DataReader to buffer data for processing thread
extern TFIFOCache mPRCache;                       ///< Cache for Processing to buffer data for streaming thread

/** @} mliquid_dairy_global_memory */



/**   
 * @defgroup mliquid_dairy_exception Exceptions & safety tools used by this app.
 * @brief Exceptions only used by this app and its threads. Tools for exception safe programming.
 * @{
 */  

namespace ilmsens 
{
  namespace mliquid
  {
    namespace dairy 
    {

      /**  Application-specific error class storing an Ilmsens error code along with a message */
      class Error : public std::runtime_error 
      {
        typedef std::runtime_error Base;

        public:
          Error(const char* pMsg, int pCode = ILMSENS_ERROR_UNKNOWN, int pErrorNum = 0)
            : Base(pMsg), mErrorCode(pCode), mErrorNum(pErrorNum) {}
          
          int getErrorCode() const { return mErrorCode; }
          int getErrorNum () const { return mErrorNum; }

        private:
          int mErrorCode;
          int mErrorNum;
      };

    } //namespace dairy
  } //namespace mliquid
} //namespace ilmsens

/** @} mliquid_dairy_exception */



/**   
 * @defgroup mliquid_dairy_tools Common tools used by this app.
 * @brief Tools used by this app and its threads, e.g. sleep function.
 * @{
 */  

namespace ilmsens 
{
  namespace mliquid
  {
    namespace dairy 
    {

      void delay_ms(unsigned int pDel_ms);

    } //namespace dairy
  } //namespace mliquid
} //namespace ilmsens

/** @} mliquid_dairy_exception */

#endif //SHARED_HPP
