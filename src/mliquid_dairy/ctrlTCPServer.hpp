/// @file ctrlTCPServer.hpp
/// @brief Main and support funtions for a controlling TCP server thread.
///
/// The TCP server running in a separate thread will accept a single client connection
/// and subsequently receive control commands and info requests. According to the commands,
/// other working threads (Ilmsens DataReader) will be started/stopped and the app's state changed.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef CTRLTCPSERVER_HPP
#define CTRLTCPSERVER_HPP


/** TCP constants */
#define TCP_COMMAND_PORT   8100             ///< TCP port of command connection
#define TCP_MSG_SIZE       1                ///< number of unit32 words per message on command connection
#define TCP_CMD_SIZE       (TCP_MSG_SIZE)   ///< TCP command size (used as buffer size) [uint32 words]
#define TCP_CMD_WAIT_TO_MS 1000             ///< timeout while waiting for new client [ms]
#define TCP_CMD_RECV_TO_MS 1000             ///< timeout while receiving command data from client [ms]
#define TCP_CMD_SEND_TO_MS 1000             ///< timeout while sending data on command connection to client [ms]

#define TCP_CMD_DEBUG_OUTPUT   1            ///< show debug info about streamed data 


namespace ilmsens 
{
  namespace mliquid
  {
    namespace dairy 
    {
      namespace controlling
      {

        /** Entry point for controlling TCP server thread */
        void* ctrlTCPServer(void*);

      } //namespace controlling
    } //namespace dairy
  } //namespace mliquid
} //namespace ilmsens

#endif //CTRLTCPSERVER_HPP
