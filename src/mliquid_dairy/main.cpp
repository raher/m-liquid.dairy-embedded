//============================================================================
// Name        : m:liquid.dairy measurement and processing application
// Author      : Ralf Herrmann
// Version     : 0.2.0
// Copyright   : (C) Ilmsens GmbH 2017
// Description : TCP control+streaming server & measurement application written  
//               embedded ARM-Systems using an Ilmsens m:liquid.dairy UWB-Sensor.
// Changelog:
//
// Rev. 0.4.0 (11/2017, HER)
//
//  - removed 32x interpolation for pulse parameter extraction 
//  + added centre of mass calculation to extract fine pulse delay
//  + added command line option to set delay and energy threshold for milk measurement detection
//    to current values (when no cow is being milked)
//  + added parameters to set starting and stopping hours of morning and afternoon milking
//  + added parameters to set idle cal. delay and energy offsets
//  = changed default sensor configuration to SoC parameters
//
// Rev. 0.3.0 (08/2017, HER)
//
//  + added command line option to set delay threshold and energy threshold for milk measurement detection
//  = fixed processing constants for milk measurement detection
//  = bugfix to amplitude scaling of FFT calculation (with and without interpolation)
//  - cut history buffer half to spare ressources
//  - removed duplicate IRFs (mCorrRx*) from cache buffer entries
//  + added raw data to cache entries
//  = changed interpretation of add info from Rx2 (int. and ext. temperature)
//
// Rev. 0.2.0 (05/2017, HER)
//
//  + added raw data storage to dataReader Thread
//  + added data types and minimal processing analysis functions to processing lib
//  + added robust class for KKF calculation via FFTw3; including FFT interpolation capability
//  + added milking start/stop detection during measurements in processing thread
//  + added basic milk IRF validity analysis working online during a measurement in processing thread
//  = changed processing thread to forward data to streaming server only every 100 ms
//  + added TCP server class for single client service, accepting with timeout and more robust 
//    detection of closed connections
//
// Rev. 0.1.0 (03/2017, HER)
//
//  = rework of support classes (enforcing minimal coding guide to enhance
//    clarity)
//  = BugFix: removed memory leaks in support classes for FFTw buffers
//  + added Poco logging facilities in main program 
//
// Rev. 0.12 (02/2017, ZEN)
//
//  = Changed state-machine: processing-thread will be created by 
//    Datareader-Thread (instead of Controlling-Thread)
//
//============================================================================


/* -------------------------------------------------------------------------------------------------
 * Includes
 * -------------------------------------------------------------------------------------------------
 */

// Std. C++ library types / STL 
#include <csignal>     // signal handling
#include <cerrno>      // IO/system call error handling

#if 0
  // Poco logging includes
  #include "Poco/AutoPtr.h"
  #include "Poco/Logger.h"
  #include "Poco/ConsoleChannel.h"
  #include "Poco/PatternFormatter.h"
  #include "Poco/FormattingChannel.h"
  #include "Poco/LogStream.h"
  #include "Poco/Message.h"
#endif

// Timing and threading includes
#include <chrono>                       // For execution time measurements
#include <pthread.h>                    // POSIX threads
#include <sched.h>                      // for Realtime Priority

// synchronisation
#include <mutex>

// File, IO, and Strings
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstring>
#include <string>

// Ilmsens includes
#include "ilmsens/ilmsens_error.h"      // Ilmsens error codes

// Ilmsens m:liquid.dairy includes
#include "ilmsens/mliquid_dairy/ilmsens_mliquid_dairy_version.h" //project version & revision

// Shared variables, memories, and synchronisation ressources
#include "shared.hpp"

// TCP server threads
#include "logTCPServer.hpp"
#include "ctrlTCPServer.hpp"
#include "strmTCPServer.hpp"

// sensor & working threads
#include "dataReader.hpp"

// XU4 board tools
#include "XU4_tools.hpp"

// Header for main program
#include "main.hpp" 

/* -------------------------------------------------------------------------------------------------
 * Using name spaces
 * -------------------------------------------------------------------------------------------------
 */

using namespace std;


/* -------------------------------------------------------------------------------------------------
 * Local definitions
 * -------------------------------------------------------------------------------------------------
 */


/* -------------------------------------------------------------------------------------------------
 * Local types and classes
 * -------------------------------------------------------------------------------------------------
 */


/* -------------------------------------------------------------------------------------------------
 * Local variables 
 * -------------------------------------------------------------------------------------------------
 */

// logging
//static Poco::Message::Priority sLogLevel = LOG_LEVEL_DEFAULT;
static unsigned int sLogLevel = LOG_LEVEL_DEFAULT;

// console redirects
static bool sDoRedir = false;

// measurement timing
static unsigned int sSWAvg = DAIRY_SW_AVG_DEFAULT;
static unsigned int sWC    = DAIRY_WC_DEFAULT;

// measurement start
static bool sMeasAutoStart = DAIRY_AUTO_START_MEAS;

// raw data storage
static bool sActStorage    = DAIRY_ACT_STORAGE;

// data processing parameters
static double sMeasDelayThres  = DAIRY_DELAY_THRES_DEFAULT;
static double sMeasEnergyThres = DAIRY_ENERGY_THRES_DEFAULT;

static double sMeasDelayOff  = DAIRY_Cal_Idle_Del_OFF;
static double sMeasEnergyOff = DAIRY_Cal_Idle_En_OFF;

// milking timing
static unsigned int sMilkStart1  = DAIRY_MILKING_1_START;
static unsigned int sMilkStart2  = DAIRY_MILKING_2_START;
static unsigned int sMilkStop1   = DAIRY_MILKING_1_STOP;
static unsigned int sMilkStop2   = DAIRY_MILKING_2_STOP;

// thread handles
static pthread_t sLogThread;          // Logging redirect TCP server
static pthread_t sCtrlThread;         // Controlling TCP server
static pthread_t sStrmThread;         // Streaming TP server
static pthread_t sDRThread;           // DataReader thread controlling the sensor

#if 0
/* -------------------------------------------------------------------------------------------------
 * Logging & logging helpers
 * 
 * Powered by Poco:-)
 * -------------------------------------------------------------------------------------------------
 */

// Configure Poco logging: create a formatted console output
void configureLogging(int pRootLevel)
{
  using Poco::AutoPtr;
  AutoPtr< Poco::ConsoleChannel >   tConsole  (new Poco::ConsoleChannel);
  AutoPtr< Poco::PatternFormatter > tFormatter(new Poco::PatternFormatter);
  
  tFormatter->setProperty("pattern", "%E.%i %N:%I %s:%q: %t");
  
  AutoPtr< Poco::FormattingChannel > tChannel(new Poco::FormattingChannel(tFormatter, tConsole));

  // set newly created formatted console channel as default channel at Poco root
  Poco::Logger::root().setChannel(tChannel);
  Poco::Logger::root().setLevel(pRootLevel);
}

// Logger for main app
Poco::Logger& log()
{
  static Poco::Logger& sLog = Poco::Logger::get("ilmsens.mliquid.dairy");
  return(sLog);
}

// Logstream for main app
Poco::LogStream& logStream()
{
  // REVISIT: needs protection?
  static Poco::LogStream sStream(log());
  return(sStream);
}
#endif


/* -------------------------------------------------------------------------------------------------
 * Command line helpers
 * 
 * -------------------------------------------------------------------------------------------------
 */

// apply defaults to all config variables that can be changed from command line
void resetDefaults(void)
{
  //logging
  sLogLevel       = LOG_LEVEL_DEFAULT;

  // console redirects
  sDoRedir        = false;

  // measurement timing
  sSWAvg          = DAIRY_SW_AVG_DEFAULT;
  sWC             = DAIRY_WC_DEFAULT;

  // measurement start
  sMeasAutoStart  = DAIRY_AUTO_START_MEAS;

  // raw data storage
  sActStorage     = DAIRY_ACT_STORAGE;

  // data processing
  sMeasDelayThres  = DAIRY_DELAY_THRES_DEFAULT;
  sMeasEnergyThres = DAIRY_ENERGY_THRES_DEFAULT;
  sMeasDelayOff    = DAIRY_Cal_Idle_Del_OFF;
  sMeasEnergyOff   = DAIRY_Cal_Idle_En_OFF;

  //milking timing
  sMilkStart1  = DAIRY_MILKING_1_START;
  sMilkStart2  = DAIRY_MILKING_2_START;
  sMilkStop1   = DAIRY_MILKING_1_STOP;
  sMilkStop2   = DAIRY_MILKING_2_STOP;
}

// show help message
void printCommandLineHelp(void)
{
  //output help text and exit
  std::cout << ANSI_COLOR_GREEN << "m:liquid.dairy::HELP: The following command line options are supported:" << std::endl
            << "--logLevel <Level> : set verbosity of logging output (0 = off .. 8 = trace)" << std::endl
            << "--SWAvg    <Avg>   : set default software averages of measurement (1 .. 4095)" << std::endl
            << "--WaitCyc  <WC>    : set default wait cycles of measurement (0 .. 1023)" << std::endl
            << "--noAutoMeas       : do not automatically start a measurement" << std::endl
            << "--startMeas        : automatically start a measurement after sensor setup" << std::endl
            << "--storeRaw         : activate saving of raw measured data to mass storage" << std::endl
            << std::endl
            << "--DelThres <Del>   : set delay threshold for detecting an ongoing liquid measurement (0.0 .. 4094.9)" << std::endl
            << "--EnThres <EN>     : set energy threshold for detecting an ongoing liquid measurement (10.0 .. 200.0)" << std::endl
            << "--DelOff  <Off>    : set delay offset from idle for detecting an ongoing liquid measurement (0.0 .. 100.0)" << std::endl
            << "--EnOff   <Off>    : set energy offset from idle for detecting an ongoing liquid measurement (-100.0 .. 0.0)" << std::endl
            << std::endl
            << "--MilkStart1 <hour>: set starting hour of morning milking (0 .. 23)" << std::endl
            << "--MilkStop1 <hour> : set stopping hour of morning milking (0 .. 23)" << std::endl
            << "--MilkStart2 <hour>: set starting hour of afternoon milking (0 .. 23)" << std::endl
            << "--MilkStop2 <hour> : set stopping hour of afternoon milking (0 .. 23)" << std::endl
            << std::endl
            << "--redirCon         : redirect/copy console output (cout & cerr) to TCP server at port 8200 for remote supervision" << std::endl
            << "--help / -?        : show this help screen" << std::endl
            << ANSI_COLOR_RESET << std::endl;
}

// parse command line to set various parameters
void parseCommandLine(int argc, char* argv[])
{
  //apply defaults first
  resetDefaults();

  for (int tI = 1; tI < argc; ++tI) 
  {
    if (0 == std::string("--logLevel").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
//        sLogLevel = (Poco::Message::Priority)std::stoi(argv[tI + 1]);
        sLogLevel = (unsigned int)std::stoi(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mliquid::dairy::Error("Missing log level argument.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--SWAvg").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sSWAvg = (unsigned int)std::stoi(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mliquid::dairy::Error("Missing software averages value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--WaitCyc").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sWC = (unsigned int)std::stoi(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mliquid::dairy::Error("Missing wait cycles value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--MilkStart1").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sMilkStart1 = (unsigned int)std::stoi(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mliquid::dairy::Error("Missing hour value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--MilkStop1").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sMilkStop1 = (unsigned int)std::stoi(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mliquid::dairy::Error("Missing hour value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--MilkStart2").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sMilkStart2 = (unsigned int)std::stoi(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mliquid::dairy::Error("Missing hour value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--MilkStop2").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sMilkStop2 = (unsigned int)std::stoi(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mliquid::dairy::Error("Missing hour value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--noAutoMeas").compare(argv[tI])) 
    {
      sMeasAutoStart = false;
    } else if (0 == std::string("--startMeas").compare(argv[tI])) 
    {
      sMeasAutoStart = true;
    } else if (0 == std::string("--storeRaw").compare(argv[tI])) 
    {
      sActStorage = true;
    } else if (0 == std::string("--DelThres").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sMeasDelayThres = std::stod(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mliquid::dairy::Error("Missing delay threshold value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--DelOff").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sMeasDelayOff = std::stod(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mliquid::dairy::Error("Missing delay offset value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--EnThres").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sMeasEnergyThres = std::stod(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mliquid::dairy::Error("Missing energy threshold value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--EnOff").compare(argv[tI])) 
    {
      if (tI + 1 < argc) 
      {
        sMeasEnergyOff = std::stod(argv[tI + 1]);
        tI ++;
      } 
      else
      {
        throw ilmsens::mliquid::dairy::Error("Missing energy offset value.", ILMSENS_ERROR_INVALID_PARAM);
      }
    } else if (0 == std::string("--redirCon").compare(argv[tI])) 
    {
      sDoRedir = true;
    } else if ( (0 == std::string("--help").compare(argv[tI])) || (0 == std::string("-?").compare(argv[tI])) )
    {
      printCommandLineHelp();
      throw ilmsens::mliquid::dairy::Error("Command line help requested.", ILMSENS_SUCCESS);
    }
    else
    {
      //output warning
      std::cerr << ANSI_COLOR_YELLOW << "m:liquid.dairy::WARNING: unknown command line option '" << argv[tI] << "'. Use '--help' to get a list of valid options." << ANSI_COLOR_RESET << std::endl;
    }
  }
}


/* -------------------------------------------------------------------------------------------------
 * Signal handling
 * -------------------------------------------------------------------------------------------------
 */

// initiate a proper shutdown
void doShutDown(void)
{
  // make sure all other threads are signalled to end
  mMtxExit.lock();
    mExitShutdwn = true;
  mMtxExit.unlock();

  // wait some time for synchronisation (may be too short, if DR thread is waiting for a slow measurement)
  ilmsens::mliquid::dairy::delay_ms(200);

  /* Wait until all threads are ended */
  pthread_join(sCtrlThread, NULL);
  pthread_join(sStrmThread, NULL);
  pthread_join(sDRThread, NULL);

  /* Finally, wait until logging threads are ended */
  if (sDoRedir)
  {
    mMtxLogging.lock();
      mLogMShutdwn = true;
    mMtxLogging.unlock();
    pthread_join(sLogThread, NULL);
  }

  ilmsens::mliquid::dairy::delay_ms(10);
}

// system signal handler
void handleTermSigs(int pSigNum) 
{
  /* output signal just received */
  std::cerr << "m:liquid.dairy::SIGNAL: app received signal " << std::dec << pSigNum << "." << std::endl;

  switch (pSigNum)
  {
    // caused by abort()
    case SIGABRT:
      std::cerr << "m:liquid.dairy::SIGNAL: signal was SIGABRT." << std::endl;
      exit(EXIT_FAILURE);

    // floating point error (e.g. divide by zero)
    case SIGFPE:
      std::cerr << "m:liquid.dairy::SIGNAL: signal was SIGFPE." << std::endl;
      break;

    // illegal instruction
    case SIGILL:
      std::cerr << "m:liquid.dairy::SIGNAL: signal was SIGILL." << std::endl;
      break;

    // interrupt, e.g. CTRL+C
    case SIGINT:
      std::cerr << "m:liquid.dairy::SIGNAL: signal was SIGINT." << std::endl;
      doShutDown();
      exit(EXIT_SUCCESS);

    // segmentation violation fault
    case SIGSEGV:
      std::cerr << "m:liquid.dairy::SIGNAL: signal was SIGSEGV." << std::endl;
      exit(EXIT_FAILURE);

    // termination request
    case SIGTERM:
      std::cerr << "m:liquid.dairy::SIGNAL: signal was SIGTERM." << std::endl;
      doShutDown();
      exit(EXIT_FAILURE);

    default:
      std::cerr << "m:liquid.dairy::SIGNAL: this unknown signal is unhandled!" << std::endl;
  }
}


/* -------------------------------------------------------------------------------------------------
 * Main program entry point
 * 
 * Checks command line options (if required), and dispatches the controlling and streaming thread.
 * 
 * When the EXIT command has been received from a client, the corresponding threads shut down
 * and main() cleans up any remaining ressources.
 * -------------------------------------------------------------------------------------------------
 */

int main(int argc, char* argv[])
{
  /* setup signal handlers */
  
#ifdef _MSC_VER
  #define _CRT_SECURE_NO_WARNINGS 

  #pragma warning(push)
  #pragma warning(disable:4996)
#endif

  if (signal(SIGABRT, handleTermSigs) == SIG_ERR)
  {
    
    std::cerr << "m:liquid.dairy::SIGNAL: could not set signal handler for SIGABRT. Error was: " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  } 
  else if (signal(SIGFPE, handleTermSigs) == SIG_ERR)
  {
    std::cerr << "m:liquid.dairy::SIGNAL: could not set signal handler for SIGFPE. Error was: " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  } 
  else if (signal(SIGILL, handleTermSigs) == SIG_ERR)
  {
    std::cerr << "m:liquid.dairy::SIGNAL: could not set signal handler for SIGILL. Error was: " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  } 
  else if (signal(SIGINT, handleTermSigs) == SIG_ERR)
  {
    std::cerr << "m:liquid.dairy::SIGNAL: could not set signal handler for SIGINT. Error was: " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  } 
  else if (signal(SIGSEGV, handleTermSigs) == SIG_ERR)
  {
    std::cerr << "m:liquid.dairy::SIGNAL: could not set signal handler for SIGSEGV. Error was: " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  } 
  else if (signal(SIGTERM, handleTermSigs) == SIG_ERR)
  {
    std::cerr << "m:liquid.dairy::SIGNAL: could not set signal handler for SIGTERM. Error was: " << strerror(errno) << std::endl;
    exit(EXIT_FAILURE);
  }

#ifdef _MSC_VER
  #undef _CRT_SECURE_NO_WARNINGS 
  #pragma warning(pop)
#endif

#if 0
  /*
  // Set Realtime Priority
  struct sched_param param;
  sched_getparam(0, &param);
  param.sched_priority = 99;
  
  // New: Realtime Priority
  if(sched_setscheduler(0, SCHED_RR, &param) != 0)
  {
    perror("Main::ERROR: could not set scheduling priority");
    return EXIT_FAILURE;
    //exit(-1);
  }
  //sched_getparam(0, &param);
  */
#endif 

  int tExitCode = EXIT_SUCCESS;

  /* Main part - create threads and wait for their termination */
  try 
  {
    /* print greeting */
    std::cout << std::endl << ANSI_COLOR_GREEN << "**** Welcome to m:liquid.dairy V" << std::dec 
              << ILMSENS_MLIQUID_DAIRY_VER_MAJOR << "."
              << ILMSENS_MLIQUID_DAIRY_VER_MINOR << "."
              << ILMSENS_MLIQUID_DAIRY_VER_BUILD
              << "! ****" << ANSI_COLOR_RESET << std::endl << std::endl;

    /* parse command line */
    parseCommandLine(argc, argv);

    /* setup Poco logging */
//    configureLogging(sLogLevel);

    /* check, if cout and cerr should be redirected to TCP server */
    signed int tRes;
    if (sDoRedir)
    {
      /*** Create TCP server thread for log messages ***/
      tRes = pthread_create(&sLogThread, NULL, &ilmsens::mliquid::dairy::logging::logTCPServer, NULL);
      if(tRes != 0) 
      {
        std::cerr << ANSI_COLOR_RED << "m:liquid.dairy::ERROR: cannot create logging TCP server thread! Logging will be local only." << ANSI_COLOR_RESET << std::endl;
        sDoRedir = false;
      }
      else
      {
        std::cout << ANSI_COLOR_GREEN << "m:liquid.dairy::INFO: created thread for logging TCP server." << ANSI_COLOR_RESET << std::endl;
      }
    }

    // get & print current XU4 temperatures
    const TTemperatures& tXU4Temps = readXU4Temperature();
    std::cout << ANSI_COLOR_CYAN << "Received " << std::dec << tXU4Temps.size() << " temperature values from XU4 board:" << std::endl;

    for (unsigned int tI = 0; tI < tXU4Temps.size(); tI++)
    {
      std::cout << "Temperature # " << std::dec << tI << ": " << tXU4Temps[tI] << "°C" << std::endl;
    }

    std::cout << ANSI_COLOR_RESET << std::endl;

#if 0
    // redirect std::cout to file
    std::ofstream tCOUTLog("cout.log");
    ScopedOSRedir tRedirCOUT(std::cout, tCOUTLog);

    // redirect std::cerr to file
    std::ofstream tCERRLog("cerr.log");
    ScopedOSRedir tRedirCERR(std::cerr, tCERRLog);

    if (sDoRedir)
    {
      tRedirCOUT.doRedir();
      tRedirCERR.doRedir();
    }

    // Intercept std::cout and save everything to a file
    std::ofstream tCOUTLog("cout.log");
    ScopedOSIntercept tRedirCOUT(std::cout, tCOUTLog);

    // Intercept std::cerr and save everything to a file
    std::ofstream tCERRLog("cerr.log");
    ScopedOSIntercept tRedirCERR(std::cerr, tCERRLog);
#endif

    /* pre-configure states, sensor, and measurement run */
    mMtxSharedData.lock();
      mState.mSenAct   = false;
      mState.mRunFlag  = false;

      mState.mSW_AVG   = sSWAvg;
      mState.mWaitCyc  = sWC;
      mState.mSaveFlag = sActStorage;

      mState.mCalIdleFlag = false;

      mState.mDelThres    = sMeasDelayThres;
      mState.mEnThres     = sMeasEnergyThres;
      mState.mDelOff      = sMeasDelayOff;
      mState.mEnOff       = sMeasEnergyOff;

      mState.mMilkStart1  = sMilkStart1;
      mState.mMilkStart2  = sMilkStart2;
      mState.mMilkStop1   = sMilkStop1;
      mState.mMilkStop2   = sMilkStop2;
    mMtxSharedData.unlock();


    /*** Create always running DataReader thread ***/
    tRes = pthread_create(&sDRThread, NULL, &ilmsens::mliquid::dairy::datareader::dataReader, 0);
    if(tRes != 0) 
    {
      //make sure all other running threads are ended
      if (sDoRedir)
      {
        mMtxLogging.lock();
          mLogMShutdwn = true;
        mMtxLogging.unlock();

        pthread_join(sLogThread, NULL);
      }

      throw ilmsens::mliquid::dairy::Error("m:liquid.dairy::ERROR: cannot create DataReader thread!", ILMSENS_ERROR_NO_MEMORY, tRes);
    }
    else
    {
      std::cout << ANSI_COLOR_GREEN << "m:liquid.dairy::INFO: created DataReader thread." << ANSI_COLOR_RESET << std::endl;
    }

    // wait, until dataReader has activated the sensor
    mMtxSharedData.lock();
      bool tSenAct = mState.mSenAct;
    mMtxSharedData.unlock();
    auto tStartWait = std::chrono::system_clock::now();
    while (!tSenAct)
    {
      // wait some time to lower CPU burden
      ilmsens::mliquid::dairy::delay_ms(100);

      // mind the timeout
      auto tElWait = std::chrono::duration_cast< std::chrono::milliseconds > (std::chrono::system_clock::now() - tStartWait);
      if (tElWait.count() > DAIRY_SENSOR_SETUP_TIMEOUT)
      {
        // need to stop waiting, could not activate sensor in time
        std::cout << std::endl << ANSI_COLOR_GREEN << "m:liquid.dairy::ERROR: sensor setup could not be finished within timeout, exiting m:liquid.dairy." << ANSI_COLOR_RESET << std::endl;
        // app not successful
        tExitCode = EXIT_FAILURE;
        break;
      }

      // update sensor status
      mMtxSharedData.lock();
        tSenAct = mState.mSenAct;
      mMtxSharedData.unlock();
    }
    
    if (tSenAct)
    {
      /*** Create always running TCP server threads ***/

      /* Thread for controlling TCP server */
      tRes = pthread_create(&sCtrlThread, NULL, &ilmsens::mliquid::dairy::controlling::ctrlTCPServer, NULL);
      if(tRes != 0) 
      {
        //make sure all other running threads are ended
        mMtxExit.lock();
          mExitShutdwn = true;
        mMtxExit.unlock();
        pthread_join(sDRThread, NULL);

        if (sDoRedir)
        {
          mMtxLogging.lock();
            mLogMShutdwn = true;
          mMtxLogging.unlock();

          pthread_join(sLogThread, NULL);
        }

        throw ilmsens::mliquid::dairy::Error("m:liquid.dairy::ERROR: cannot create controlling TCP server thread!", ILMSENS_ERROR_NO_MEMORY, tRes);
      }
      else
      {
        std::cout << ANSI_COLOR_GREEN << "m:liquid.dairy::INFO: created thread for controlling TCP server." << ANSI_COLOR_RESET << std::endl;
      }

      /* Thread for streaming TCP server */
      tRes = pthread_create(&sStrmThread, NULL, &ilmsens::mliquid::dairy::streaming::strmTCPServer, NULL);
      if(tRes != 0) 
      {
        //make sure all other running threads are ended
        mMtxExit.lock();
          mExitShutdwn = true;
        mMtxExit.unlock();
        pthread_join(sCtrlThread, NULL);
        pthread_join(sDRThread, NULL);

        if (sDoRedir)
        {
          mMtxLogging.lock();
            mLogMShutdwn = true;
          mMtxLogging.unlock();

          pthread_join(sLogThread, NULL);
        }

        throw ilmsens::mliquid::dairy::Error("m:liquid.dairy::ERROR: cannot create streaming TCP server thread!", ILMSENS_ERROR_NO_MEMORY, tRes);
      }
      else
      {
        std::cout << ANSI_COLOR_GREEN << "m:liquid.dairy::INFO: created thread for streaming TCP server." << ANSI_COLOR_RESET << std::endl;
      }

      /** all threads started */

      /* start a continuous measurement run (?) */
      mMtxSharedData.lock();
        mState.mRunFlag = sMeasAutoStart;
      mMtxSharedData.unlock();

      /*** Wait until controlling and streaming threads are ended (upon EXIT command from client or signal from system) ***/
      pthread_join(sCtrlThread, NULL);
      pthread_join(sStrmThread, NULL);
      pthread_join(sDRThread, NULL);
    }
    else
    {
      //could not finish sensor setup in time
      mMtxExit.lock();
        mExitShutdwn = true;
      mMtxExit.unlock();
      pthread_join(sDRThread, NULL);
    }


    /* Finally, trigger and wait until logging threads are ended */
    if (sDoRedir)
    {
      mMtxLogging.lock();
        mLogMShutdwn = true;
      mMtxLogging.unlock();

      pthread_join(sLogThread, NULL);
    }

    std::cout << std::endl << ANSI_COLOR_GREEN << "m:liquid.dairy::INFO: All Threads closed, exiting m:liquid.dairy app." << ANSI_COLOR_RESET << std::endl;
  }
  catch(ilmsens::mliquid::dairy::Error& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "m:liquid.dairy::App-EXCEPTION: " << ANSI_COLOR_RESET << std::endl
              << " Description     : " << tErr.what() << std::endl
              << " Code was        : " << std::dec << tErr.getErrorCode() << std::endl
              << " Error number was: " << std::dec << tErr.getErrorNum() << std::endl;

    // not successful (?)
    if (tErr.getErrorCode() != ILMSENS_SUCCESS) tExitCode = EXIT_FAILURE;
  }
  catch(std::exception& tErr) 
  {
    std::cerr << ANSI_COLOR_RED "m:liquid.dairy::Std-EXCEPTION: " << ANSI_COLOR_RESET 
              << tErr.what() << std::endl;

    // not successful
    tExitCode = EXIT_FAILURE;
  }
  catch(...) 
  {
    std::cerr << ANSI_COLOR_RED "m:liquid.dairy::EXCEPTION: caught unknown exception!" << ANSI_COLOR_RESET << std::endl;

    // not successful
    tExitCode = EXIT_FAILURE;
  }


  /* all was finished, we can go now */
  std::cout << std::endl << ANSI_COLOR_GREEN << "*** Good Bye! ***" << ANSI_COLOR_RESET << std::endl;
  return (tExitCode);
}
