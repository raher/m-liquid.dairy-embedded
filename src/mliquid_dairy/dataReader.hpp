/// @file dataReader.hpp
/// @brief Main and support funtions for a sensor handling thread.
///
/// The thread controls and handles ILmsens m:liquid.diary sensors using 
/// Ilmsens HAL library. Measured data is stored into a FIFO cache for
/// further processing by another thread.
///
/// @author Ralf Herrmann <ralf.herrmann@ilmsens.com>

#ifndef DATAREADER_HPP
#define DATAREADER_HPP

// Ilmsens HAL wrapper
#include "hal_wrap.hpp"                 // wraps Ilmsens HAL library in C++ classes


/** Sensor constants */
#define DR_MLBS_ORDER         9         ///< M-Sequence order of m:liquid.dairy sensor
#define DR_MLBS_CLOCK         12.800    ///< RF system clock of m:liquid.dairy sensor [GHz]
#define DR_MLBS_NUM_RX        2         ///< number of Rx per sensor module


/** HAL library configuration and measurement parameters */
#define HAL_LOG_LEVEL_INIT    ILMSENS_DEB_MOST  ///< default HAL log level during initialisation of sensors
#define HAL_LOG_LEVEL_NORM    ILMSENS_DEB_INFO  ///< default HAL log level during normal operation (i.e. measurements)
#define HAL_LOG_LEVEL_DEIN    ILMSENS_DEB_MOST  ///< default HAL log level during library unloading/deinit
#define HAL_MEAS_MODE         false             ///< Measurement mode: false = raw | true = buffered by HAL thread

/*** Thread configuration constants */
#define DR_DEBUG_OUTPUT       1                 ///< show debug info about DataReader operation

/*** Additional information constants */
#define DR_DEF_TEMP_INVALID   -300.0            ///< default temperature value (invalid)
#define DR_TEMP_UPDATE_MS     1000              ///< time interval between updates of module temperature
#define DR_TEMP_VAL_MASK      0x0000FFF8        ///< mask for extracting the actual temperature value of module
#define DR_TEMP_VAL_SIGN      0x00008000        ///< sign bit of temperature value of module
#define DR_TEMP_VAL_OFF       65536.0           ///< offset of neg. integer temperature value
#define DR_TEMP_VAL_SCALE     128.0             ///< scaling of integer temperature value

#define DR_PTEMP_VAL_MASK     0x0000FFFF        ///< mask for extracting actual probe temperature value
#define DR_PTEMP_VAL_SHIFT    1                 ///< bit shift for extracting actual probe temperature value
#define DR_PTEMP_VAL_OFF      256.0             ///< offset of probe temperature value
#define DR_PTEMP_VAL_SCALE    32.0              ///< scaling of integer probe temperature value

/*** Reference measurement storage constants */
#define DR_REF_MEAS_NUM_IRF   1000              ///< Number of IRFs to acquire per reference measurement

/*** Raw data storage constants */
#define DR_ACT_RAW_STORAGE    true                      ///< set to true, if raw data storage to disk shall be activated
#define DR_STOARGE_PATH       "/mnt/Data/Measurements/" ///< absolute path for raw data storage location
#define DR_STO_NUM_BUF         4                        ///< number of IRF buffers
#define DR_STO_NUM_IRF        64                        ///< number of IRFs per buffer before committing to disc. Should be worth 2 MByte.


namespace ilmsens 
{
  namespace mliquid
  {
    namespace dairy 
    {
      namespace datareader
      {

        /** Entry point for DataReader thread */
        void* dataReader(void*);

      } //namespace datareader
    } //namespace dairy
  } //namespace mliquid
} //namespace ilmsens

#endif //DATAREADER_HPP
