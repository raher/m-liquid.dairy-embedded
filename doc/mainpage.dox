
/** \mainpage Function Reference Manual - HAL API


HAL-API for software development with **m:explore** ultra-wideband sensors


\section sec_introduction Introduction

As a service to our customers who want to integrate Ilmsens UWB sensors into their software environment, 
Ilmsens offers a hardware abstraction layer (**HAL**) application programming interface (**API**) for the **m:explore** 
ultra-wideband (UWB) sensors ("the software"). The HAL API comes in form of a dynamic library working on top 
of the device drivers with corresponding C header files. This is the function reference for the API. 

The HAL API is available for different operating systems and allows device management, sensor configuration, 
acquisition configuration, and performing measurements. It abstracts from device- or digital interface-specific 
details as much as possible to enable portability of the application software. Future generations of the HAL API 
and Ilmsens UWB sensors will be developed with backward compatibility in mind. 
Product-specific extensions will extend the API rather than changing existing functions.

By default, the HAL API is provided as software in binary (compiled) form for many popular operating systems. 
Should the provided functionality be insufficient for the customer's purposes, please contact Ilmsens for 
other options (see below). We provide limited software support for the HAL API. To report problems, ask for help, 
or suggest improvements, contact Ilmsens (see below).

\section sec_disclaimer Copyright and Disclaimer

\copyright Copyright &copy; 2017 Ilmsens GmbH. All rights reserved.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR ANYONE 
DISTRIBUTING THE SOFTWARE BE LIABLE FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 

All product and company names in this document may be the trademarks and tradenames of their respective owners and are hereby acknowledged.

\section sec_further_doc Further documentation

This is a function reference only. Ilmsens provides additional documentation in various guides.

- **HAL API Programming Guide**

  A software developer can find design and basic background information in the programming guide to ease application development.
  Data format, buffer layout, and typical data processing steps are explained as well.

- **HAL API Setup Guide**

  The setup guide explains how to install the HAL library, the dependencies, and how to test successful installation. 
 
\section sec_3rd_party_software Third party software

The HAL library makes use of great software developed by open source communities.
These modules and libraries allow us to make the HAL library available on many platforms
and operating systems. We appriciate this work very much and would like to thank the contributors
of the following projects:

\subsection sec_libUSB The libusb project

\copyright Copyright &copy; 2012-2015 libusb

We are using libusb-1.0 for communication with our sensors. The library is available
on many Linux flavours and most modern Windows platfoms (where it makes use of the built-in
WinUSB drivers thus avoiding the need for proprietary driver development).

Check out these ressources:

- Project website: <a href="http://libusb.info" target="_blank">http://libusb.info</a>
- License: check the latest link on the project website. Currently the license is the 
  <a href="http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html" target="_blank">GNU Lesser General Public License, version 2.1</a>

\subsection sec_libPOCO The POCO project

\copyright Copyright &copy; 2006-2017 by Applied Informatics Software Engineering GmbH

POCO provides a very powerful multi-threaded framework for logging messages, errors, etc. 
It can be setup in a hierarchical manner and allows fine grain control of what gets logged to where.
We are using libpocofoundation-1.7 for logs from the HAL library.

Check out these ressources:

- Project website: <a href="https://pocoproject.org" target="_blank">pocoproject.org</a>
- License: <a href="https://pocoproject.org/license.html" target="_blank">The Boost Software License 1.0</a>
 
\section sec_3rd_party_tools Third party tools

The development of the HAL library is done with the following great and free tools. They help a lot in
keeping the library consitent, easy to build, and the documentation up to date. We appriciate these
tools very much and would like to thank their contributors.

Check out these tools:

- Version control system: <a href="https://git-scm.com/" target="_blank">Git</a>
- Building and Packaging: <a href="https://cmake.org/" target="_blank">CMake</a>
- Code documentation: <a href="http://www.doxygen.org/" target="_blank">Doxygen</a>

 
\section sec_contact Contact Ilmsens

Ilmsens GmbH\n
Ehrenbergstr. 11\n
98693 Ilmenau\n
Germany\n


Tel.:	+49 3677 76130-30\n
Fax:	+49 3677 76130-39\n
Email:	hal-api@ilmsens.com
*/

/** \page ilmsens_hw_data_format Measurement Data Format

This section briefly explains the format and buffer layout for measured data returned by the HAL API.
More detailed information can be found in the **HAL API Programming Guide**.

\section sec_raw_data_format Data format of raw measured data

The data retrieved from the sensors is returned as 32 bit integers (**int**) to avoid
any loss of precision while keeping the amount of data as small as possible.
Besides the actual signal samples, additional status information is contained
in the output buffer. How to convert raw integers into physical units is explained in detail
in the **HAL API Programming Guide**.

\section sec_output_buffer_size Calculating the output-buffer size

This section describes how to calculate the minimum size of the output buffer for retrieving measured data
for a given sensor configuration.

- In case of sensors with different configurations included in the measurement run

  \f{eqnarray*}
    BufferSize_{min} &=& \sum_{i} ChannelSize(i) \times NumberOfRx(i) \\
                     &=& \sum_{i} 2^{MLBS-order(i)} \times NumberOfOversampling(i) \times NumberOfRx(i)
  \f}  

- In case of common configuration for all sensors included in the measurement run

  \f{eqnarray*}
  BufferSize_{min} &=& NumberOfSensors \times ChannelSize \times NumberOfRx \\
                   &=& NumberOfSensors \times 2^{MLBS-order} \times NumberOfOversampling \times NumberOfRx
  \f}  

\section sec_output_buffer_layout Layout example of output buffer

The following layout assumes a measurement with two indentically configures 9th order **m:explore** sensors.

*/


