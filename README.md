### What is this repository for? ###

* Embedded software for m:liquid sensors measuring milk
* Current Version: V0.4 (MLIQD5-Release-V0.4)

### How do I get set up? ###

* Summary of set up: 
	install dependencies
* Configuration: 
	release 
	debug
* Dependencies: 
	PocoFoundation(+Dev)
	Ilmsens HAL
	FFTW3 (+Dev)
* Deployment instructions: copy executable into user home

### Who do I talk to? ###

* ralf.herrmann@ilmsens.com
