#!/bin/bash 

# adjust automatic fan control settings: adjust temperature thresholds and PWM values (%)

# test, if a supported fan control device exists
if [ -f /sys/devices/odroid_fan.13/fan_mode ]; then
   FAN=13
elif [ -f /sys/devices/odroid_fan.14/fan_mode ]; then
   FAN=14
else
   echo "set_XU4_fan::ERROR: This machine is not supported."
   exit 1
fi 
echo "set_XU4_fan::INFO: using fan control device 'odroid_fan.$FAN'."

# set the file names for read temps and setting values
TEMP_READ_FILE="/sys/devices/10060000.tmu/temp"
TEMP_THRES_FILE="/sys/devices/odroid_fan.$FAN/temp_levels"

FAN_MODE_FILE="/sys/devices/odroid_fan.$FAN/fan_mode"
FAN_SPEED_FILE="/sys/devices/odroid_fan.$FAN/fan_speeds"
#FAN_SPEED_FILE="/sys/devices/odroid_fan.$FAN/pwm_duty"

# make sure. automatic control mode is enabled
echo 1 > ${FAN_MODE_FILE} 

# set the PWM values [%] for the 4 automatic temperature regions
if [ -z "$1" ]; then 
  PWM_VALUES="31 61 81 97"
  echo "set_XU4_fan::INFO: using default PWM values."
else 
  PWM_VALUES="$1"
  echo "set_XU4_fan::INFO: using command line PWM values: '$PWM_VALUES'"
fi

echo "$PWM_VALUES" > ${FAN_SPEED_FILE}

# set the temperature thresholds (optionally)
if [ -z "$2" ]; then 
  echo "set_XU4_fan::INFO: keeping current temperature thresholds."
else 
  TEMP_VALUES="$2"
  echo "set_XU4_fan::INFO: using command line temperature thresholds: '$TEMP_VALUES'"
  echo "$TEMP_VALUES" > ${TEMP_THRES_FILE}
fi
