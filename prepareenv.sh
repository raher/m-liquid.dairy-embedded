#!/bin/sh

#prepare
apt-get update

#get basic build environment + poco headers and dev. libs
apt-get install build-essential libpoco-dev 

#get documentation environment
apt-get install doxygen doxygen-latex 
#apt-get install texlive

#TODO: check installation of Imsens HAL package
